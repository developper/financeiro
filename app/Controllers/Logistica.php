<?php

namespace app\Controllers;

use \App\Models\Administracao\Filial;
use \App\Models\Administracao\Paciente;
use \App\Models\DB\ConnMysqli;
use \App\Models\Financeiro\Fornecedor;
use \App\Models\Logistica\Entrada;
use \App\Models\Logistica\Estoque;
use \App\Models\Logistica\SaidaInterna;
use \App\Models\Logistica\SaidaRemessa;
use \App\Models\Logistica\SaidasCondensadas;
use \App\Models\Logistica\XML;
use \App\Helpers\StringHelper;
use \App\Models\Sistema\Equipamentos;
use \App\Models\Administracao\Usuario;


class Logistica
{
    public static function menu()
    {
        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/notas/templates/menu.phtml');
    }

    public static function formEntradaProdutos()
    {
        $fornecedores   = Fornecedor::getAll();
        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/notas/templates/entrada-notas-form.phtml');
    }

    public static function salvarEntradaProdutos()
    {
        if(isset($_POST) && !empty($_POST)){
            $msg = base64_encode("É necessário ao mínimo 1 produto para salvar essa entrada!");
            $type = 'f';
            if(count($_POST['tipo']) > 0) {
                $estoque = new Estoque(ConnMysqli::getConnection());
                try{
                    list($response, $notaId) = $estoque->saveEntradaProdutos ($_POST);
                    $msg = base64_encode("Produtos inseridos com sucesso! TR {$notaId} criada!");
                    $type = 's';
                } catch (\Exception $e){
                    die($e->getMessage());
                }
                if(!$response){
                    $msg = base64_encode('Houve um problema ao inserir os produtos! Entre em contato com a equipe de TI.');
                    $type = 'f';
                }
            }
            header("Location: /logistica/notas/?action=entrada-produtos&msg={$msg}&type={$type}");
        }
    }

    public static function importarXMLEntradaProdutos()
    {
        $xml            = new XML();
        $response       = $xml->moveXMLToTemporaryPath($_FILES);
        $fornecedores   = Fornecedor::getAll();
        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/notas/templates/entrada-notas-form.phtml');
    }

    public static function formRelatorioItensVencidos()
    {
        $urs = Filial::getUnidadesAtivas();
        require($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/itens-vencidos/relatorio-itens-vencidos-form.phtml');
    }

    public static function relatorioItensVencidos()
    {
        if(isset($_POST) && !empty($_POST)){
            $periodoInicio  = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim     = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $urBusca        = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $tipo           = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING);
            $buscaItem      = filter_input(INPUT_POST, 'busca_item', FILTER_SANITIZE_STRING);
            $item           = filter_input(INPUT_POST, 'item', FILTER_SANITIZE_NUMBER_INT);
            $laboratorio    = filter_input(INPUT_POST, 'laboratorio', FILTER_SANITIZE_STRING);
            $mostrar        = $_POST['mostrar'];

            $response       = [];
            $entradasUr     = [];
            $devolucoes     = [];
            $saidas         = [];
            $saidasInternas = [];
            $red    = [];
            $orange = [];
            $gold   = [];
            $black  = [];
            $gray   = [];
            $nV     = [];
            $count  = [];
            $count['red']   = 0;
            $count['orange']= 0;
            $count['gold']  = 0;
            $count['black'] = 0;
            $count['gray']  = 0;
            $count['nV']    = 0;
            $tipos  = [
                0 => 'medicamento(s)',
                1 => 'material(is)',
                3 => 'dieta(s)',
            ];

            $tipoItem = $tipo == '' ? 'item(ns)' : $tipos[$tipo];

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'ur'            => $urBusca,
                'tipo'          => $tipo,
                'laboratorio'   => $laboratorio,
                'item'          => $item,
                'mostrar'       => $mostrar
            ];

            $entradas 	        = Estoque::getEntradasRelatorioItensVencidos($filtros);
            $respDevolucoes     = Estoque::getDevolucoesRelatorioItensVencidos($filtros);
            $respSaidas 	    = Estoque::getSaidasRelatorioItensVencidos($filtros);
            $respSaidasInternas = Estoque::getSaidasInternasRelatorioItensVencidos($filtros);

            if($urBusca != '1' || $urBusca != '11') {
                $respEntradasUr     = Estoque::getEntradasUrRelatorioItensVencidos($filtros);

                foreach ($respEntradasUr as $entradaUr){
                    $entradasUr[$entradaUr['idItem']][$entradaUr['LOTE']] = $entradaUr['qtd'];
                }
            }

            foreach ($respDevolucoes as $devolucao){
                $devolucoes[$devolucao['idItem']][$devolucao['LOTE']] = $devolucao['qtd'];
            }

            foreach ($respSaidas as $saida){
                $saidas[$saida['idItem']][$saida['LOTE']] = $saida['qtd'];
            }

            foreach ($respSaidasInternas as $saidaInterna){
                $saidasInternas[$saidaInterna['idItem']][$saidaInterna['LOTE']] = $saidaInterna['qtd'];
            }

            foreach ($entradas as $key => $entrada){
                $entrada['qtdEntradaUr'] = 0;
                $entrada['qtdDevolucao'] = 0;
                $entrada['qtdSaida'] = 0;
                $entrada['qtdSaidaInterna'] = 0;

                if($urBusca != '1' || $urBusca != '11') {
                    if(isset($entradasUr[$entrada['idItem']][$entrada['LOTE']])){
                        $entrada['qtdEntradaUr'] = $entradasUr[$entrada['idItem']][$entrada['LOTE']];
                    }
                }
                if(isset($devolucoes[$entrada['idItem']][$entrada['LOTE']])){
                    $entrada['qtdDevolucao'] = $devolucoes[$entrada['idItem']][$entrada['LOTE']];
                }
                if(isset($saidas[$entrada['idItem']][$entrada['LOTE']])){
                    $entrada['qtdSaida'] = $saidas[$entrada['idItem']][$entrada['LOTE']];
                }
                if(isset($saidasInternas[$entrada['idItem']][$entrada['LOTE']])){
                    $entrada['qtdSaidaInterna'] = $saidasInternas[$entrada['idItem']][$entrada['LOTE']];
                }

                $response[] = $entrada;
            }

            $now = new \DateTime();

            foreach ($response as $data){
                $validade = \DateTime::createFromFormat('Y-m-d', $data['VALIDADE']);
                $estoque = ($data['qtd'] + $data['qtdEntradaUr'] + $data['qtdDevolucao']) - ($data['qtdSaida'] + $data['qtdSaidaInterna']);
                $data['estoque'] = sprintf(
                    '<b>%s</b>',
                    $estoque
                );
                if($data['VALIDADE'] == '0000-00-00') {
                    $nV[] = $data;
                    $count['nV']++;
                } else {
                    $diffValidade = $now->diff($validade);
                    if ($estoque != 0) {
                        // VENCIDOS
                        if (in_array('red', $mostrar)) {
                            if ($validade < $now) {
                                $red[] = $data;
                                $count['red']++;
                            }
                        }

                        // MENOS DE 1 MES
                        if (in_array('orange', $mostrar)) {
                            if ($validade >= $now && $diffValidade->days <= 31) {
                                $orange[] = $data;
                                $count['orange']++;
                            }
                        }

                        // ENTRE 1 E 6 MESES
                        if (in_array('gold', $mostrar)) {
                            $meses = $diffValidade->days / 30;
                            if ($meses >= 1 && $meses <= 6 && $validade >= $now) {
                                $gold[] = $data;
                                $count['gold']++;
                            }
                        }

                        // ENTRE 6 MESES E 1 ANO
                        if (in_array('black', $mostrar)) {
                            $meses = $diffValidade->days / 30;
                            if ($meses > 6 && $diffValidade->y == 0 && $validade >= $now) {
                                $black[] = $data;
                                $count['black']++;
                            }
                        }

                        // MAIS DE 1 ANO
                        if (in_array('gray', $mostrar)) {
                            if ($diffValidade->y >= 1 && $validade >= $now) {
                                $gray[] = $data;
                                $count['gray']++;
                            }
                        }

                    }
                }
            }


            $urs = Filial::getUnidadesAtivas();



            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/itens-vencidos/relatorio-itens-vencidos-form.phtml');
        }
    }

    public static function excelRelatorioItensVencidos()
    {
        if(isset($_GET) && !empty($_GET)){
            $periodoInicio  = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $periodoFim     = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $urBusca        = filter_input(INPUT_GET, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $tipo           = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING);
            $laboratorio    = filter_input(INPUT_GET, 'laboratorio', FILTER_SANITIZE_STRING);
            $item           = filter_input(INPUT_GET, 'item', FILTER_SANITIZE_NUMBER_INT);
            $mostrar        = $_GET['mostrar'];
            $mostrar        = explode('-', $mostrar);
            $red    = [];
            $orange = [];
            $gold   = [];
            $black  = [];
            $gray   = [];
            $nV     = [];
            $count  = [];
            $count['red']   = 0;
            $count['orange']= 0;
            $count['gold']  = 0;
            $count['black'] = 0;
            $count['gray']  = 0;
            $count['nV']    = 0;

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'ur'            => $urBusca,
                'tipo'          => $tipo,
                'laboratorio'   => $laboratorio,
                'item'          => $item,
                'mostrar'       => $mostrar
            ];

            $entradas 	        = Estoque::getEntradasRelatorioItensVencidos($filtros);
            $respDevolucoes     = Estoque::getDevolucoesRelatorioItensVencidos($filtros);
            $respSaidas 	    = Estoque::getSaidasRelatorioItensVencidos($filtros);
            $respSaidasInternas = Estoque::getSaidasInternasRelatorioItensVencidos($filtros);

            if($urBusca != '1' || $urBusca != '11') {
                $respEntradasUr     = Estoque::getEntradasUrRelatorioItensVencidos($filtros);

                foreach ($respEntradasUr as $entradaUr){
                    $entradasUr[$entradaUr['idItem']][$entradaUr['LOTE']] = $entradaUr['qtd'];
                }
            }

            foreach ($respDevolucoes as $devolucao){
                $devolucoes[$devolucao['idItem']][$devolucao['LOTE']] = $devolucao['qtd'];
            }

            foreach ($respSaidas as $saida){
                $saidas[$saida['idItem']][$saida['LOTE']] = $saida['qtd'];
            }

            foreach ($respSaidasInternas as $saidaInterna){
                $saidasInternas[$saidaInterna['idItem']][$saidaInterna['LOTE']] = $saidaInterna['qtd'];
            }

            foreach ($entradas as $key => $entrada){
                $entrada['qtdEntradaUr'] = 0;
                $entrada['qtdDevolucao'] = 0;
                $entrada['qtdSaida'] = 0;
                $entrada['qtdSaidaInterna'] = 0;

                if($urBusca != '1' || $urBusca != '11') {
                    if(isset($entradasUr[$entrada['idItem']][$entrada['LOTE']])){
                        $entrada['qtdEntradaUr'] = $entradasUr[$entrada['idItem']][$entrada['LOTE']];
                    }
                }
                if(isset($devolucoes[$entrada['idItem']][$entrada['LOTE']])){
                    $entrada['qtdDevolucao'] = $devolucoes[$entrada['idItem']][$entrada['LOTE']];
                }
                if(isset($saidas[$entrada['idItem']][$entrada['LOTE']])){
                    $entrada['qtdSaida'] = $saidas[$entrada['idItem']][$entrada['LOTE']];
                }
                if(isset($saidasInternas[$entrada['idItem']][$entrada['LOTE']])){
                    $entrada['qtdSaidaInterna'] = $saidasInternas[$entrada['idItem']][$entrada['LOTE']];
                }

                $response[] = $entrada;
            }

            $now = new \DateTime();

            foreach ($response as $data){
                $validade = \DateTime::createFromFormat('Y-m-d', $data['VALIDADE']);
                $estoque = ($data['qtd'] + $data['qtdEntradaUr'] + $data['qtdDevolucao']) - ($data['qtdSaida'] + $data['qtdSaidaInterna']);
                $data['estoque'] = sprintf(
                    '<b>%s</b>',
                    $estoque
                );
                if($data['VALIDADE'] == '0000-00-00') {
                    $nV[] = $data;
                    $count['nV']++;
                } else {
                    $diffValidade = $now->diff($validade);
                    if ($estoque != 0) {
                        // VENCIDOS
                        if (in_array('red', $mostrar)) {
                            if ($validade < $now) {
                                $red[] = $data;
                                $count['red']++;
                            }
                        }

                        // MENOS DE 1 MES
                        if (in_array('orange', $mostrar)) {
                            if ($validade >= $now && $diffValidade->days <= 31) {
                                $orange[] = $data;
                                $count['orange']++;
                            }
                        }

                        // ENTRE 1 E 6 MESES
                        if (in_array('gold', $mostrar)) {
                            $meses = $diffValidade->days / 30;
                            if ($meses >= 1 && $meses <= 6 && $validade >= $now) {
                                $gold[] = $data;
                                $count['gold']++;
                            }
                        }

                        // ENTRE 6 MESES E 1 ANO
                        if (in_array('black', $mostrar)) {
                            $meses = $diffValidade->days / 30;
                            if ($meses > 6 && $diffValidade->y == 0 && $validade >= $now) {
                                $black[] = $data;
                                $count['black']++;
                            }
                        }

                        // MAIS DE 1 ANO
                        if (in_array('gray', $mostrar)) {
                            if ($diffValidade->y >= 1 && $validade >= $now) {
                                $gray[] = $data;
                                $count['gray']++;
                            }
                        }
                    }
                }
            }

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $periodoInicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $periodoFim);
            $filename = sprintf(
                "relatorio_itens_vencidos_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/itens-vencidos/relatorio-itens-vencidos-excel.phtml');
        }
    }

    public static function formEditarEntradaProdutos()
    {
        $fornecedores   = Fornecedor::getAll();
        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/notas/templates/editar-entrada-produtos-form.phtml');
    }

    public static function buscarEntradaProdutos()
    {
        if(isset($_POST) && !empty($_POST)){
            $periodoInicio      = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim         = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $fornecedorBusca    = filter_input(INPUT_POST, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
            $buscaItem          = filter_input(INPUT_POST, 'busca_item', FILTER_SANITIZE_STRING);
            $item               = filter_input(INPUT_POST, 'item', FILTER_SANITIZE_NUMBER_INT);

            if($buscaItem == ''){
                $item = '';
            }

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'fornecedor'    => $fornecedorBusca,
                'item'          => $item,
            ];

            $response 	    = Estoque::getEntradasByFilters($filtros);
            $fornecedores   = Fornecedor::getAll();
            $count          = count($response);

            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/notas/templates/editar-entrada-produtos-form.phtml');
        }
    }

    public static function atualizarEntradaProdutos()
    {
        if(isset($_POST) && !empty($_POST)){
            $data               = $_POST;
            $itens              = [];
            $itensAnteriores    = [];

            foreach ($data['item'] as $idEntrada => $item) {
                $mudancaItem     = $data['item_anterior'][$idEntrada] != $item;
                $mudancaLote     = $data['lote_anterior'][$idEntrada] != $data['lote'][$idEntrada];
                $mudancaQtd      = $data['qtd_anterior'][$idEntrada] != $data['qtd'][$idEntrada];
                $mudancaValidade = $data['validade_anterior'][$idEntrada] != $data['validade'][$idEntrada];

                $data['valor'][$idEntrada] = StringHelper::moneyStringToFloat($data['valor'][$idEntrada]);
                $mudancaValor    = $data['valor_anterior'][$idEntrada]!= $data['valor'][$idEntrada];



                if($mudancaItem || $mudancaLote || $mudancaQtd || $mudancaValidade || $mudancaValor ){
                    $itens[$idEntrada]['item']      = $item;
                    $itens[$idEntrada]['lote']      = $data['lote'][$idEntrada];
                    $itens[$idEntrada]['qtd']       = $data['qtd'][$idEntrada];
                    $itens[$idEntrada]['validade']  = $data['validade'][$idEntrada];
                    $itens[$idEntrada]['valor']  = $data['valor'][$idEntrada];

                    $itensAnteriores[$idEntrada]['itemNovo']            = $item;
                    $itensAnteriores[$idEntrada]['itemAnterior']        = $data['item_anterior'][$idEntrada];
                    $itensAnteriores[$idEntrada]['qtdNovo']             = $data['qtd'][$idEntrada];
                    $itensAnteriores[$idEntrada]['qtdAnterior']         = $data['qtd_anterior'][$idEntrada];
                    $itensAnteriores[$idEntrada]['loteNovo']            = $data['lote'][$idEntrada];
                    $itensAnteriores[$idEntrada]['loteAnterior']        = $data['lote_anterior'][$idEntrada];
                    $itensAnteriores[$idEntrada]['validadeNovo']        = $data['validade'][$idEntrada];
                    $itensAnteriores[$idEntrada]['validadeAnterior']    = $data['validade_anterior'][$idEntrada];
                    $itensAnteriores[$idEntrada]['valorNovo']           = $data['valor'][$idEntrada];
                    $itensAnteriores[$idEntrada]['valorAnterior']       = $data['valor_anterior'][$idEntrada];
                }
            }

            $response = Estoque::updateEntradaProdutos($itens, $itensAnteriores);

            if($response){
                $msg = base64_encode("Entrada(s) atualizada(s) com sucesso!");
                header("Location: /logistica/notas/?action=editar-entrada-produtos&msg={$msg}&type=s");
            } else {
                $msg = base64_encode("Houve um problema ao atualizar as entradas.");
                header("Location: /logistica/notas/?action=editar-entrada-produtos&msg={$msg}&type=f");
            }
        }
    }

    public static function formRelatorioNotasEntrada()
    {
        $fornecedores   = Fornecedor::getAll();
        $urs            = Filial::getUnidadesAtivas();
        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/relatorio-notas-entrada/relatorio-notas-entrada.phtml');
    }

    public static function relatorioNotasEntrada()
    {
        if(isset($_POST) && !empty($_POST)){
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $fornecedorBusca = filter_input(INPUT_POST, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
            $urBusca = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $tipoRelatorio = filter_input(INPUT_POST, 'tipo_relatorio', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'fornecedor'    => $fornecedorBusca,
                'ur'            => $urBusca,
            ];
            if($tipoRelatorio == 'S') {
                $response = Entrada::getCustoItensEntrada($filtros);
            } else {
                $data = Entrada::getNotasEntrada($filtros);
                $response = [];
                foreach ($data as $row) {
                    $response[$row['TR']]['fornecedor'] = $row['razaoSocial'];
                    $response[$row['TR']]['numeroNota'] = $row['numeroNota'];
                    $response[$row['TR']]['valorNota'] = StringHelper::currencyFormat($row['valorNota']);
                    $response[$row['TR']]['itens'][] = [
                        'item' => $row['item'],
                        'qtd' => $row['qtd'],
                        'dataEntrada' => $row['dataEntrada'],
                        'custo' => StringHelper::currencyFormat($row['custo']),
                        'lote' => $row['LOTE'],
                    ];
                }
            }

            $fornecedores   = Fornecedor::getAll();
            $urs            = Filial::getUnidadesAtivas();
            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/relatorio-notas-entrada/relatorio-notas-entrada.phtml');
        }
    }

    public static function excelRelatorioNotasEntrada()
    {
        if(isset($_GET) && !empty($_GET)){
            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $fornecedorBusca = filter_input(INPUT_GET, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
            $urBusca = filter_input(INPUT_GET, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $tipoRelatorio = filter_input(INPUT_GET, 'tiporelatorio', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'fornecedor'    => $fornecedorBusca,
                'ur'            => $urBusca,
            ];
            if($tipoRelatorio == 'S') {
                $response = Entrada::getCustoItensEntrada($filtros);
            } else {
                $data = Entrada::getNotasEntrada($filtros);
                $response = [];
                foreach ($data as $row) {
                    $response[$row['TR']]['fornecedor'] = $row['razaoSocial'];
                    $response[$row['TR']]['numeroNota'] = $row['numeroNota'];
                    $response[$row['TR']]['valorNota'] = StringHelper::currencyFormat($row['valorNota']);
                    $response[$row['TR']]['itens'][] = [
                        'item' => $row['item'],
                        'qtd' => $row['qtd'],
                        'dataEntrada' => $row['dataEntrada'],
                        'custo' => StringHelper::currencyFormat($row['custo']),
                        'lote' => $row['LOTE'],
                    ];
                }
            }

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $inicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $fim);
            $tipoPlanilha = $tipoRelatorio == 'S' ? 'sintetico' : 'analitico';
            $filename = sprintf(
                "relatorio_notas_entrada_%s_%s_%s.%s",
                $tipoPlanilha,
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/relatorio-notas-entrada/relatorio-notas-entrada-excel.phtml');
        }
    }

    public static function formRelatorioSaidaInterna()
    {
        $urs            = Filial::getUnidadesAtivas();
        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/saida-interna/relatorio-saida-interna.phtml');
    }

    public static function relatorioSaidaInterna()
    {
        if(isset($_POST) && !empty($_POST)){
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $urBusca = $_POST['ur'];
            $urBuscaJoin = '';
            if(!empty($urBusca)){
                $urBuscaJoin = join(',',$urBusca);
            }
            $filtrarPor = filter_input(INPUT_POST, 'filtrarPor', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'ur'            => $urBuscaJoin,
                'filtrarPor'    => $filtrarPor,
            ];


            $response = SaidaInterna::getSaidasInternas($filtros);

            $urs = Filial::getUnidadesAtivas();
            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/saida-interna/relatorio-saida-interna.phtml');
        }
    }

    public static function excelRelatorioSaidaInterna()
    {
        if(isset($_GET) && !empty($_GET)){
            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $urBusca = filter_input(INPUT_GET, 'ur', FILTER_SANITIZE_STRING);
            $urBuscaJoin = '';

            if(!empty($urBusca)){
                $urBuscaJoin = str_replace(' ', ',', $urBusca);

                $urBusca = explode('+', $urBusca);
            }


            $filtrarPor = filter_input(INPUT_GET, 'filtrarpor', FILTER_SANITIZE_STRING);



            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'ur'            => $urBuscaJoin,
                'filtrarPor'    => $filtrarPor,
            ];



            $response = SaidaInterna::getSaidasInternas($filtros);

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $inicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $fim);
            $tipoPlanilha = $filtrarPor == 'VD' ? 'vencidos' : 'colaboradores';
            $filename = sprintf(
                "relatorio_saidas_internas_%s_%s_%s.%s",
                $tipoPlanilha,
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/saida-interna/relatorio-saida-interna-excel.phtml');
        }
    }

    public static function formRelatorioSaidaCondensada()
    {
        // $urs            = Filial::getUnidadesAtivas();
        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/saida/relatorio-saida-condensada.phtml');
    }

    public static function relatorioSaidaCondensada()
    {
        if(isset($_POST) && !empty($_POST)){
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            //$urBusca = $_POST['ur'];
            $filters = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                // 'ur'            => $urBusca,
            ];
            $responseMedicamento = SaidasCondensadas::getMedicamento($filters);
            $responseMaterial = SaidasCondensadas::getMaterial($filters);
            $responseDieta = SaidasCondensadas::getDieta($filters);
            $responseEquipamento = SaidasCondensadas::getEquipamento($filters);
            $response = array_merge($responseMedicamento,$responseMaterial,$responseDieta,$responseEquipamento);
            // $urs = Filial::getUnidadesAtivas();
            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/saida/relatorio-saida-condensada.phtml');
        }
    }

    public static function excelRelatorioSaidaCondensada()
    {
        if(isset($_GET) && !empty($_GET)){

            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            //$urBusca = $_POST['ur'];
            $filters = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                // 'ur'            => $urBusca,
            ];
            $responseMedicamento = SaidasCondensadas::getMedicamento($filters);
            $responseMaterial = SaidasCondensadas::getMaterial($filters);
            $responseDieta = SaidasCondensadas::getDieta($filters);
            $responseEquipamento = SaidasCondensadas::getEquipamento($filters);
            $response = array_merge($responseMedicamento,$responseMaterial,$responseDieta,$responseEquipamento);


            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $inicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $fim);

            $filename = sprintf(
                "relatorio_saidas_condensadas_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );


            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/saida/relatorio-saida-condensada-excel.phtml');
        }
    }

    // -----------------------------
    public static function formRelatorioAluguelEquipamentos()
    {
        $urs = Filial::getUnidadesAtivas();
        $pacientes = Paciente::getAll();
        $fornecedores = Fornecedor::getAll();
        $equipamentos = Equipamentos::getAll();

        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/aluguel-equipamentos/relatorio-aluguel-equipamentos.phtml');
    }

    public static function relatorioAluguelEquipamentos()
    {
        if(isset($_POST) && !empty($_POST)) {
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);

            $inicioBD = \DateTime::createFromFormat('d/m/Y', $inicio)->format('Y-m-d');
            $fimBD = \DateTime::createFromFormat('d/m/Y', $fim)->format('Y-m-d');

            $pacienteId = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $equipamentoId = filter_input(INPUT_POST, 'equipamento', FILTER_SANITIZE_NUMBER_INT);
            $fornecedorId = filter_input(INPUT_POST, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
            $urId = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);

            $filtros = [
                'inicio'        => $inicioBD,
                'fim'           => $fimBD,
                'paciente'      => $pacienteId,
                'equipamento'   => $equipamentoId,
                'fornecedor'    => $fornecedorId,
                'ur'            => $urId
            ];

            $response = Equipamentos::getEquipamentosAlugados($filtros);

            $inicioUrl = \DateTime::createFromFormat('d/m/Y', $inicio)->format('Y-m-d');
            $fimUrl = \DateTime::createFromFormat('d/m/Y', $fim)->format('Y-m-d');
            $urs = Filial::getUnidadesAtivas();
            $pacientes = Paciente::getAll();
            $fornecedores = Fornecedor::getAll();
            $equipamentos = Equipamentos::getAll();

            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/aluguel-equipamentos/relatorio-aluguel-equipamentos.phtml');
        }
    }

    public static function excelRelatorioAluguelEquipamentos()
    {
        if(isset($_GET) && !empty($_GET)){
            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);

            $pacienteId = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $equipamentoId = filter_input(INPUT_GET, 'equipamento', FILTER_SANITIZE_NUMBER_INT);
            $fornecedorId = filter_input(INPUT_GET, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
            $urId = filter_input(INPUT_GET, 'ur', FILTER_SANITIZE_NUMBER_INT);

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'paciente'      => $pacienteId,
                'equipamento'   => $equipamentoId,
                'fornecedor'    => $fornecedorId,
                'ur'            => $urId
            ];

            $response = Equipamentos::getEquipamentosAlugados($filtros);

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $inicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $fim);
            $filename = sprintf(
                "relatorio_aluguel_equipamentos_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/aluguel-equipamentos/relatorio-aluguel-equipamentos-excel.phtml');
        }
    }

    public static function formRelatorioSaidaRemessa()
    {

        // $urs            = Filial::getUnidadesAtivas();
        $pacientes = Paciente::getPacientesByUr();
        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/saida-remessa/relatorio-saida-remessa.phtml');
    }

    public static function relatorioSaidaRemessa()
    {
        if(isset($_POST) && !empty($_POST)){

            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $paciente = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_STRING);
            $remessa = filter_input(INPUT_POST, 'remessa', FILTER_SANITIZE_STRING);
            $opcao = filter_input(INPUT_POST, 'opcao', FILTER_SANITIZE_STRING);
            $filters = [
                'inicio'    => $inicio,
                'fim'       => $fim,
                'paciente'  => $paciente,
                'remessa'   => $remessa,

            ];

            if($opcao == 'C'){
                $response = SaidaRemessa::getRemessaCondensada($filters);
            }else{
                $response = SaidaRemessa::getRemessaAnalitico($filters);
            }
            $pacientes = Paciente::getPacientesByUr();

            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/saida-remessa/relatorio-saida-remessa.phtml');
        }
    }

    public static function excelRelatorioSaidaRemessa()
    {
        if(isset($_GET) && !empty($_GET)){
            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $paciente = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_STRING);
            $remessa = filter_input(INPUT_GET, 'remessa', FILTER_SANITIZE_STRING);
            $opcao = filter_input(INPUT_GET, 'opcao', FILTER_SANITIZE_STRING);

            $filters = [
                'inicio'    => $inicio,
                'fim'       => $fim,
                'paciente'  => $paciente,
                'remessa'   => $remessa,

            ];

            $nomePaciente = '';
            if(!empty($paciente)){
                $responsePaciente = Paciente::getById($paciente);
                $nomePaciente = $responsePaciente[0]['nome'];
            }

            $nomeUsuario = $_SESSION['nome_user'];

            if($opcao == 'C'){
                $response = SaidaRemessa::getRemessaCondensada($filters);
                $nomeOpcao = 'Condensado';
                $colspanTitulo = 4;
                $colspan1 = 3;
                $colspan2 = '';
                $colspan3 = 2;
            }else{
                $response = SaidaRemessa::getRemessaAnalitico($filters);
                $nomeOpcao = 'Analítico';
                $colspanTitulo = 7;
                $colspan1 = 5;
                $colspan2 = 2;
                $colspan3 = 3;
            }

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $agora = date('d/m/Y H:i');

            $inicioBr = empty($inicio) ? '' :  \DateTime::createFromFormat('Y-m-d', $inicio)->format('d-m-Y');
            $fimBr = empty($fim) ? '' :  \DateTime::createFromFormat('Y-m-d', $fim)->format('d-m-Y');

            $filename = sprintf(
                "relatorio_saidas_por_remessa_%s_%s.%s",
                $inicioBr,
                $fimBr,
                $extension
            );

            $inicioBr2 = empty($inicio) ? '' :  \DateTime::createFromFormat('Y-m-d', $inicio)->format('d/m/Y');
            $fimBr2 = empty($fim) ? '' :  \DateTime::createFromFormat('Y-m-d', $fim)->format('d/m/Y');




            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/saida-remessa/relatorio-saida-remessa-excel.phtml');
        }
    }

    public static function excelPesquisarMovimentacoes()
    {
        if (isset($_GET) && !empty($_GET)) {
            $empresa_principal = $_SESSION['empresa_principal'];
            $origem = $_GET['origem'];
            $tipo = $_GET['tipo-busca-saida'];
            $codigo = $_GET['codigo'];
            $resultado = $_GET['resultado'];
            $inicio = $_GET['inicio'];
            $fim = $_GET['fim'];

            $i = implode("-",array_reverse(explode("/",$inicio)));
            $f = implode("-",array_reverse(explode("/",$fim)));
            $fimP = $f;

            if($resultado == 'a'){
                $aSelect = ", s.data";
                $aGroupBy = "GROUP BY s.data,  nome, paciente";
            } else {
                $aGroupBy = "GROUP BY s.CATALOGO_ID";
            }

            $cor = false;
            $total = 0;

            if($tipo == 'eqp'){
                $condCod = $codigo == 'T' ? '' : "s.CATALOGO_ID ={$codigo} and";

                $sql= "SELECT 
              		c.nome as paciente, 
              		cb.id as cod, 
              		cb.item as nome,
              		s.LOTE as LOTE,
              		SUM(s.quantidade) as qtd ,
            		  s.data,
                  DATE_FORMAT(s.data,'%Y-%m-%d') as dt,
                  COALESCE(cs.CUSTO, 0.00) as valor		
  		        FROM 
                  cobrancaplanos as cb inner join 
                  saida as s on (cb.id = s.CATALOGO_ID) inner join  
                  clientes as c on (c.idClientes = s.idCliente)
                  LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$empresa_principal} and cs.STATUS = 'A'
		          WHERE 
                  s.tipo = 5 and 
                  {$condCod}
                  s.data BETWEEN '{$i}' AND '{$f}' and 
                  s.idCliente != 0 AND 
                  c.empresa = ".$empresa_principal." 
		          {$aGroupBy}
		  UNION ALL
      		SELECT 
      		        c.nome as paciente,
              		cb.id as cod, 
              		cb.item as nome,
              		s.LOTE as LOTE,
              		SUM(s.quantidade) as qtd , 
              		s.data,
              		DATE_FORMAT(s.data,'%Y-%m-%d') as dt,
              		COALESCE(cs.CUSTO, 0.00) as valor
      		FROM 
                  cobrancaplanos as cb inner join 
                  saida  as s on (cb.id = s.CATALOGO_ID) inner join  
                  empresas as c on (c.id = s.UR) 
                  LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$empresa_principal} and cs.STATUS = 'A'
      		WHERE 
                  s.tipo = 5 and 
                  {$condCod}
                  s.data BETWEEN '{$i}' AND '{$f}' and 
                  s.UR != 0
      		{$aGroupBy}
      		  ORDER BY data ASC ";
            }else if($tipo == 'ur'){

                if($resultado == 'a'){

                    $aGroupBy = "GROUP BY data, catalogo_id";
                }else{
                    $aGroupBy = "GROUP BY  nome";
                }

                if( $codigo== 1)
                {
                    $sql =
                        "SELECT 
                p.empresa,   
    		        IF(s.UR = 0 ,p.nome,e.nome) as paciente, 
                s.NUMERO_TISS,
                s.CATALOGO_ID,
                concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                s.LOTE, 
                s.quantidade as qtd,
                cb.valorMedio as valor,
                s.data
          FROM 
                saida as s Left join 
                clientes as p on ( p.idClientes = s.idCliente) inner join 
                catalogo as cb on(cb.ID = s.CATALOGO_ID) Left join
    			      empresas as e on (s.UR = e.id)
    			WHERE  
                s.tipo in (0,1,3) and
                s.data BETWEEN '$i' AND '$f' AND 
    			      (                       
                  idCliente in(
                               select 
                                  idClientes 
                               from 
                                  clientes 
                               where 
                                  empresa = '1'
                              )
    		           OR s.UR <> 0
    			      )
    		
    		UNION ALL
    		SELECT 
                 p.empresa,   
    					   IF(s.UR = 0 ,p.nome,e.nome) as paciente, 
                 s.NUMERO_TISS,
                 s.CATALOGO_ID,
                 concat(cb.item,' -  ',if(s.segundoNome is null,'',s.segundonome)) as nome,
                 s.LOTE, 
                 s.quantidade as qtd,
                 cs.CUSTO as valor,
                 s.data
        FROM 
                 saida as s Left join 
                 clientes as p on ( p.idClientes = s.idCliente) inner join 
                 cobrancaplanos as cb on(cb.ID = s.CATALOGO_ID) Left join
    		         empresas as e on (s.UR = e.id)
    		         LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$empresa_principal} and cs.STATUS = 'A'
    		 WHERE  
                 s.tipo =5 and
                 s.data BETWEEN '$i' AND '$f' AND 
    						(                       
                 idCliente in(
                             select 
                                idClientes 
                             from 
                                clientes 
                             where 
                                empresa = '1'
                            )
    					    OR s.UR <> 0
    						)
    		 ORDER BY 
              data ";
                }else{
                    $condCod = $codigo == 'T' ? '' : "AND s.UR ={$codigo} ";

                    $sql ="SELECT 
                  e.nome as paciente,
                  cb.id as cod,
                  cb.id as catalogo_id,
                  cb.item as nome,
                  s.LOTE as LOTE,
                  sum(s.quantidade) as qtd ,
                  cs.CUSTO as valor,
                  s.data
              FROM 
                  cobrancaplanos as cb inner join 
                  saida as s on (cb.id = s.CATALOGO_ID) inner join 
                  empresas as e on (e.id = s.UR)
                  LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$empresa_principal} and cs.STATUS = 'A' 
              WHERE 
                  s.tipo = 5 and
                  s.data BETWEEN '{$i}' AND '{$f}' and
                  s.idCliente = 0 
                  {$condCod}
                  {$aGroupBy}
              UNION ALL
              SELECT 
                  e.nome as paciente, 
                  cb.NUMERO_TISS as cod, 
                  cb.ID as catalogo_id,
                  concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                  s.LOTE as LOTE,
                  sum(s.quantidade )as qtd ,
                  cb.valorMedio as valor,
                  s.data 
              FROM 
                  catalogo as cb  inner join 
                  saida as s on (cb.ID = s.CATALOGO_ID) inner join 
                  empresas as e on (e.id = s.UR) 
              WHERE 
                  s.tipo in(0,1,3)  and 
                  s.data BETWEEN '{$i}'  AND '{$f}'
                  {$condCod}
                  {$aGroupBy} 
              ORDER BY 
                  data ASC ";
                }


            }else if($tipo == 'paciente'){
                if($resultado == 'a'){

                    $aGroupBy = "group by data,s.CATALOGO_ID";
                }else{
                    $aGroupBy = "group by  s.CATALOGO_ID";
                }
                $condPaciente = $codigo == 'T' ? '' : "s.idCliente ={$codigo} and";
                $condPacienteEmpresa = $_SESSION['empresa_principal'] == 1 ? '' : "and p.empresa = {$_SESSION['empresa_principal']}" ;

                $sql =" SELECT 
		p.nome as paciente, 
		s.NUMERO_TISS,
         s.CATALOGO_ID,
		concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
		s.LOTE, 
		s.quantidade as qtd,
		s.data,
		cb.valorMedio AS valor
		FROM saida as s inner join clientes as p on ( p.idClientes = s.idCliente) 
		inner join catalogo as cb on(cb.ID= s.CATALOGO_ID)
		WHERE $condPaciente s.tipo in (0,1,3) $condPacienteEmpresa AND s.data BETWEEN '$i' AND '$f'
		
		union ALL
		SELECT
                p.nome as paciente, 
                s.numero_tiss,
                s.CATALOGO_ID,
		cb.item as nome, 
		s.LOTE, 
		s.quantidade as qtd,
		s.data ,
		cs.CUSTO AS valor
		FROM 
		saida as s inner join clientes as p on ( p.idClientes = s.idCliente) 
		inner join cobrancaplanos as cb on (cb.id = s.CATALOGO_ID)
		LEFT JOIN custo_servicos_plano_ur AS cs ON cb.id = cs.COBRANCAPLANO_ID AND cs.UR={$empresa_principal} and cs.STATUS = 'A' 
		WHERE $condPaciente s.tipo = 5 $condPacienteEmpresa
		AND s.data BETWEEN '$i' AND '$f'  
		ORDER BY `nome` ASC";
            }
            else{

                $condCod = $codigo == 'T' ? '' : "AND s.CATALOGO_ID= {$codigo} ";

                if($tipo == 'med'){
                    $tip = 0;
                }else if($tipo == 'mat'){
                    $tip = 1;
                }else{
                    $tip = 3;
                }

                if( $_SESSION['empresa_principal']== 1){

                    $sql="SELECT 
                        p.nome as paciente, 
                        s.NUMERO_TISS,
                        s.CATALOGO_ID,
                        concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                        s.LOTE, 
                        s.quantidade as qtd,
                        s.data,
                        cb.valorMedio AS valor
                    FROM 
                        saida as s inner join 
                        clientes as p on ( p.idClientes = s.idCliente) inner join 
                        catalogo as cb on(cb.ID = s.CATALOGO_ID)
                    WHERE  
                        s.tipo ={$tip} AND 
                        s.data BETWEEN '$i' AND '$f' 
                        {$condCod}
                        AND idCliente in(
                                     select 
                                        idClientes 
                                     from 
                                        clientes 
                                     where 
                                        empresa = '{$_SESSION['empresa_principal']}'
                                    )
		
		union all
		SELECT 
                    p.nome as paciente, 
                    s.NUMERO_TISS,
                    s.CATALOGO_ID,
                    concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                    s.LOTE, 
                    s.quantidade as qtd,
                    s.data,
                    cb.valorMedio AS valor
		FROM 
                    saida as s inner join 
                    empresas as p on ( p.id = s.UR) inner join 
                    catalogo as cb on (cb.ID = s.CATALOGO_ID)
		WHERE 
                    s.tipo ={$tip} AND 
                    s.data BETWEEN '$i' AND '$f' AND 
                    s.UR <> 0 AND 
                    s.idCliente = 0 
                    {$condCod}
		
		 order by data";



                }else{
                    $sql="SELECT 
                                p.nome as paciente, 
                                s.NUMERO_TISS,
                                s.CATALOGO_ID,
                                concat(cb.principio,' - ',cb.apresentacao, ' LAB: ', cb.lab_desc) as nome,
                                s.LOTE, 
                                sum(s.quantidade )as qtd,
                                s.data,
                                cb.valorMedio AS valor
                            FROM 
                                saida as s inner join 
                                clientes as p on ( p.idClientes = s.idCliente) inner join 
                                catalogo as cb on(cb.ID = s.CATALOGO_ID)
                            WHERE  
                                s.tipo ={$tip} AND 
                                s.data BETWEEN '$i' AND '$f'
                                {$condCod}
                                AND idCliente in(
                                                    select 
                                                        idClientes 
                                                     from 
                                                        clientes 
                                                     where 
                                                        empresa ='{$_SESSION['empresa_principal']}'
                                                 )
                             $aGroupBy  
                             order by 
                                data";

                }

            }
            $result = mysql_query($sql) or die(mysql_error());
            $f = true;
            $totalItens = 0;
            $totalCusto = 0;
            $colspan = $resultado == 'a' ? 3 : 2;

            $extension = 'xls';
            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicioP = \DateTime::createFromFormat('Y-m-d', $i);
            $fimP =  \DateTime::createFromFormat('Y-m-d', $fimP);
            $filename = sprintf(
                "relatorio_saidas_%s_%s.%s",
                $inicioP->format('d-m-Y'),
                $fimP->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/saida/pesquisar-movimentacoes-excel.phtml');
        }
    }
}
