<?php

namespace App\Controllers\ConfiguracoesSistema;

use App\Helpers\Paginator;
use App\Models\Sistema\CategoriaMaterial AS CategoriaModel;
use App\Models\DB\ConnMysqli;

class CategoriaMaterial
{
    public static function formCategoriasMateriais()
    {
        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/configuracoes-sistema/templates/form.phtml');
    }

    public static function salvarCategoriasMateriais()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];

            $data['nome'] = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);

            $response = CategoriaModel::save($data);

            $msg = 'Houve um erro ao registrar a categoria! '.$response;
            $type = 'f';

            if($response === true) {
                $msg = 'Categoria registrada com sucesso!';
                $type = 's';
            }

            header("Location: /logistica/configuracoes-sistema/?action=categorias-materiais&msg=" . $msg . "&type=" . $type);
        }
    }

    public static function formEditarCategoriasMateriais()
    {
        if(isset($_GET) && !empty($_GET)) {
            $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
            $data = CategoriaModel::getById($id);

            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/configuracoes-sistema/templates/editar-form.phtml');
        }
    }

    public static function atualizarCategoriasMateriais()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['id'] = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $data['nome'] = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);

            $response = CategoriaModel::update($data);

            $msg = 'Houve um erro ao registrar a categoria!';
            $type = 'f';

            if($response === true) {
                $msg = 'Categoria atualizada com sucesso!';
                $type = 's';
            }

            header("Location: /logistica/configuracoes-sistema/?action=editar-categorias-materiais&id=" . $data['id'] . "&msg=" . $msg . "&type=" . $type);
        }
    }

    public static function listarCategoriasMateriais()
    {
        $data = [];
        $data['nome'] = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);

        $conn = ConnMysqli::getConnection();
        $query = CategoriaModel::getUsersListSQL($data);

        $limit = isset($_GET['limit']) ? $_GET['limit'] : 50;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $links = 5;

        $paginator = new Paginator($conn, $query);
        $orderBy = "ORDER BY ITEM";
        $response = $paginator->getData($limit, $page, $orderBy);

        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/configuracoes-sistema/templates/listar-form.phtml');
    }

    public static function filtrarCategoriasMateriais()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['nome'] = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);

            $conn = ConnMysqli::getConnection();
            $query = CategoriaModel::getUsersListSQL($data);

            $limit = isset($_GET['limit']) ? $_GET['limit'] : 50;
            $page = isset($_GET['page']) ? $_GET['page'] : 1;
            $links = 5;

            $paginator = new Paginator($conn, $query);
            $orderBy = "ORDER BY ITEM";
            $response = $paginator->getData($limit, $page, $orderBy);

            require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/configuracoes-sistema/templates/listar-form.phtml');
        }
    }

    public static function excluirCategoriasMateriais()
    {
        if(isset($_GET) && !empty($_GET)) {
            $data['id'] = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

            $check = CategoriaModel::checkIfExistsMaterial($data['id']);

            if(!$check) {
                $response = CategoriaModel::delete($data['id']);
                echo 1;
            } else {
                echo 2;
            }
        }
    }
}
