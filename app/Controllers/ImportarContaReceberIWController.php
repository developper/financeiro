<?php
namespace app\Controllers;

use \App\Models\Administracao\Filial;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\IPCF;
use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\Notas;
use App\Helpers\StringHelper;
use \App\Models\Financeiro\ImportarContaReceberIW as ModelContaReceberIW;
use Zend\Stdlib\DateTime;
use App\Models\Financeiro\Assinatura;
use App\Helpers\DateHelper;

include_once('../../utils/codigos.php');

//ini_set('display_errors',1);

class ImportarContaReceberIWController
{
    //ID 18 tabela plano_contas 1 Receitas 1.2 RECEITA PREVISTA 1.2.1 RECEITA A FATURAR 1.2.1.01 PREVISAO SERV. AT. DOMICILIAR
    // codigo no arquivo 1201
    private static $planoContasNaturezaIW = 1201;

    // ID 28 tabela assinaturas centro de custo que veem no aquivo 990 que é o 
    private static $centroCusto = 990;

    // ID 14 tipo_documento_financieor, padrão que veem no arquivo FT
    private static $tipoDocumentoFinanceiro = 'FT';

    // registro tipo 1 = 16 coluns registro tipo 2 igual a 4 colunas
    private static $numeroColunas = [1 => 16,
        2=> 4,
        3=>17
    ];
    // primeira coluna recebe o tipo de registro que é 1 ou 2
    private static $registroPrimeiraColuna = [0 => 1,
        1=> 2
    ];


    public static function index()
    {
        $filiais = Filial::getUnidadesAtivas();
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/importar-conta-receber-iw/templates/index.phtml');
    }

    public static function importarArquivo()
    {
        $filiais = Filial::getUnidadesAtivas();

        if(isset($_FILES) && !empty($_FILES['conta_receber_iw']['size']) && !empty($_POST['empresa'])){
            // salvando arquivo ofx na pasta ofx do financeiro
            $responseMoveTxt = self::moveTxtToTemporaryPath($_FILES);
            $empresa = $_POST['empresa'];

            if(!empty($responseMoveTxt['acerto'])){
                // lendo arquivo ofx
                $nomeArquivo = $_FILES['conta_receber_iw']['name'];
                $caminhoArquivo = $responseMoveTxt['acerto'];
                $responseGetTxt = self::getTXT($caminhoArquivo);
                if(!empty($responseGetTxt['itensTxt'])){
                    $responseInsert = ModelContaReceberIW::inserirNotas($responseGetTxt['itensTxt'], $empresa);

                }
                unlink($caminhoArquivo);
            }else{
                $response['error'][] = $responseMoveTxt['erro'];
            }

            if(!empty($responseGetTxt['error'])){
                foreach($responseGetTxt['error'] as $erro){
                    $response['error'][] = $erro;
                }
            }
            if(!empty($responseGetTxt['jaCadastrada'])){
                foreach($responseGetTxt['jaCadastrada'] as $jaCadastrada){
                    $response['jaCadastrada'][] = $jaCadastrada;
                }
            }

            if(!empty($responseInsert['error'])){
                foreach($responseInsert['error'] as $erro){
                    $response['error'][] = $erro;
                }
            }

            if(!empty($responseInsert['insert'])){
                foreach($responseInsert['insert'] as $insert){
                    $response['insert'][] = $insert;
                }
            }

            if(!empty($responseMoveTxt['erro'])){
                $response['error'][] = $responseMoveTxt['erro'];
            }
        }else{
            if(empty($_POST['empresa'])){
                $response['error'][] = "Selecione a Filial antes de fazer Upload";
            }else{
                $response['error'][] = "Selecione um arquivo antes de fazer Upload";
            }

        }

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/importar-conta-receber-iw/templates/index.phtml');
    }

    public static function moveTxtToTemporaryPath($file)
    {
        $errors = [];

        $path = $_SERVER['DOCUMENT_ROOT'] . '/financeiros/arquivos-iw/contas-receber/';
        $size = 1024 * 1024 *1024 * 1024 *1024 * 1024 * 2;
        $array['erro'] = '';
        $array['acerto'] = '';

        $errors[1] = 'O arquivo no upload é maior do que o permitido';
        $errors[2] = 'O arquivo ultrapassa o limite de tamanho especifiado';
        $errors[3] = 'O upload do arquivo foi feito parcialmente';
        $errors[4] = 'Não foi feito o upload do arquivo!';

        if ($file['conta_receber_iw']['error'] != 0) {
            $array['erro'] = "Não foi possível fazer o upload, erro:<br />" . $errors[$file['conta_receber_iw']['error']];
            return $array;
        }

        $ext = explode('.', $file['conta_receber_iw']['name']);

        $extensao = end($ext);

        $type = $file['conta_receber_iw']['type'];

        if ($extensao != 'txt' && $extensao != 'TXT' && $type != 'text/plain') {
            $array['erro'] = "Por favor, envie apenas arquivos com no formato .csv!";
            return $array;
        }

        if ($size < $file['conta_receber_iw']['size']) {
            $array['erro'] = "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
            return $array;
        }

        $agora= date('Y_m_d_H_i_s_');

        $name = $file['conta_receber_iw']['name'];
        $fullPath = $path .$agora.$name;
        $tmp = $file['conta_receber_iw']['tmp_name'];

        if (move_uploaded_file($tmp, $fullPath)) {
            $array['acerto'] = $fullPath;
            return $array;
        }

        $array['erro'] = "Não foi possível enviar o arquivo, tente novamente! Caso o erro persista, entre em contato com o setor de TI.";
        return $array['erro'];
    }

    public static function formatDate($data){
        $date = DateTime::createFromFormat('d/m/Y', $data);
        return  $date->format('Y-m-d');
    }


    public static function getTXT($caminhoArquivo)
    {
        $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();
        $data_fechamento_obj = new \DateTime($fechamento_caixa['data_fechamento']);
        $file = fopen($caminhoArquivo, "r");

        $header = true;
        $txtToArray = [];
        $arrayErro = [];
        $contLinhas = 1;
        $auxFile = $file;
        $primeiroRegistro = 1;
        $colunaEsperadas = implode(', ', self::$numeroColunas );
        $contLinhas = 0;

        // verificando se arquivo é vazio.
        // if(!empty($primeiroRegistro)){
        while (($txtLine = fgets($file)) !== FALSE) {
            
            $contLinhas++;
            if($txtLine){
                // separados por |
                $txtLine = explode('|', $txtLine);
               
                if(!in_array(count($txtLine), self::$numeroColunas)) {
                    $arrayErro[] =  " Linha {$contLinhas}: Foram encontradas ".count($txtLine)." colunas em vez de ".$colunaEsperadas.".";
                    if ($header) {
                        $header = false;
                    }
                }else{
                    if(!in_array($txtLine[0], self::$registroPrimeiraColuna)){
                        $arrayErro[] =  " Linha {$contLinhas}: Tipo de registro fora do padrão.";
                        if ($header) {
                            $header = false;
                        }
                    }else{

                        if($txtLine[0] == 1){
                            $arrayParcelas = [];
                            $arrayRateio = [];

                            // vericaando se fornecedor já existe no sistema
                            $cnpj = $txtLine[1];
                            $fornecedorCadastrado = Fornecedor::checkIfExistsFornecedorByCNPJ($cnpj,'C');
                            if(empty($fornecedorCadastrado)){
                                $arrayErro[] = "Linha {$contLinhas}; LOTE:{$txtLine[8]}; CNPJ = {$cnpj}. Erro: 
                                                            Cliente não possui cadastro no sistema.";

                                $errorLinha = true;
                            }else{
                                $fornecedorCadastrado = Fornecedor::getById($fornecedorCadastrado);
                            }

                            // verificando se data de emissão é válida
                            if(!\DateTime::createFromFormat('d/m/Y', $txtLine[3])){
                                $arrayErro[] = "Linha {$contLinhas}; LOTE:{$txtLine[8]}; CNPJ = {$cnpj}. Erro: 
                                                            Data de emissão não é válida.";
                                $errorLinha = true;

                            } else {
                                $data_emissao_obj = \DateTime::createFromFormat('d/m/Y', $txtLine[3]);
                                if($data_emissao_obj->format('Y-m-d') <= $data_fechamento_obj->format('Y-m-d')) {
                                    $arrayErro[] = "Linha {$contLinhas}; LOTE:{$txtLine[8]}; CNPJ = {$cnpj}. Erro: 
                                                    Data de emissão (" . $data_emissao_obj->format('d/m/Y') . ") menor que a data de fechamento do período: " . $data_fechamento_obj->format('d/m/Y');
                                    $errorLinha = true;
                                }
                            }

                            // verificando se data de validade é válida
                            if(!\DateTime::createFromFormat('d/m/Y', $txtLine[4])){
                                $arrayErro[] = "Linha {$contLinhas}; LOTE:{$txtLine[8]}; CNPJ = {$cnpj}. Erro: 
                                            Data de vencimento não é válida.";
                                $errorLinha = true;

                            }
                            // Validando Centro de custo
                            if($txtLine[11] != self::$centroCusto){
                                $arrayErro[] = "Linha {$contLinhas}; LOTE:{$txtLine[8]}; CNPJ = {$cnpj}. Erro: Centro de custo {$txtLine[11]} diferente do esperado.";
                                $errorLinha = true;
                            }
                            // Validando Natureza
                            if($txtLine[12] != self::$planoContasNaturezaIW){
                                $arrayErro[] = "Linha {$contLinhas}; LOTE:{$txtLine[8]}; CNPJ = {$cnpj}. Erro: 
                                                                Natureza $txtLine[12] diferente da esperada.";
                                $errorLinha = true;
                            }
                            // Validando Tipo documento financeiro
                            if($txtLine[13] != self::$tipoDocumentoFinanceiro){
                                $arrayErro[] = "Linha {$contLinhas}; LOTE:{$txtLine[8]}; CNPJ = {$cnpj}. Erro: 
                                                                Tipo documento {$txtLine[13]} diferente do esperado.";
                                $errorLinha = true;
                            }

                            if(!$errorLinha){
                                $arrayParcelas[] = [
                                    'vencimento' => self::formatDate($txtLine[4]),
                                    'valor' => StringHelper::moneyStringToFloat($txtLine[5]),
                                    'vencimento_real' =>self::formatDate(DateHelper::diaUtilVencimentoReal($txtLine[4]))
                                ];

                                $arrayRateio[] = [
                                    'natureza' => 18,
                                    'valor' => StringHelper::moneyStringToFloat($txtLine[5]),
                                    'assinatura' => 28,
                                    'percentual' =>100
                                ];

                                $protocolo = isset($txtLine[16]) ? $txtLine[16] : '';

                                $txtToArray[] = [
                                    'linha' => $contLinhas,
                                    'cnpj' => $txtLine[1],
                                    'fornecedor_id' => $fornecedorCadastrado['idFornecedores'],
                                    'data_emissao' => self::formatDate($txtLine[3]),
                                    'data_competencia' => $data_emissao_obj->format("Y-m"),
                                    'valor_total' => StringHelper::moneyStringToFloat($txtLine[5]),
                                    'valor_servico' => StringHelper::moneyStringToFloat($txtLine[5]),
                                    'valor_frete' => 0,
                                    'tipo_documento_financeiro' => 14,
                                    'parcelas' => $arrayParcelas,
                                    'rateio' => $arrayRateio,
                                    'descricao' => $txtLine[14]." CONTAS DO ARQUIVO: ".$txtLine[15]." PROTOCOLO: ".$protocolo,
                                    'natureza_principal' => 18,
                                    'lote' => $txtLine[8],
                                    'forma_pagamento' => 5
                                ];

                            }

                        }
                    }

                }

            }

        }

        $response = [
            'error' => $arrayErro,
            'itensTxt' => $txtToArray
        ];

        return $response;



    }
}
