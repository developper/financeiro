<?php
namespace app\Controllers;

use App\Helpers\DateHelper;
use App\Helpers\StringHelper;
use \App\Models\Administracao\Filial;
use App\Models\Financeiro\AplicacaoFundos;
use App\Models\Financeiro\Assinatura;
use App\Models\Financeiro\BaixarNotaAntecipada;
use App\Models\Financeiro\ContaBancaria;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\IPCF;
use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\Notas;
use App\Models\Financeiro\Compensacao;
use App\Models\Financeiro\MultiplasNotas;

include_once('../../utils/codigos.php');
// ini_set('display_errors', 1);
class MultiplasContas
{
    
    public static function criarConta()
    {
        $tipo = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING);
        $tipo_fornecedor = $tipo == 'S' ? 'F' : 'C';
        $tipo_titulo = $tipo == 'S' ? 'Saída' : 'Entrada';
        $acaoTitulo ='Criar';
        if(!isset($_GET['tipo']) || empty($tipo)) {
            header ("Location: /financeiros/?op=notas");
            exit();
        }
        
        $filtros = [
            'notIn'=>'23, 26',
        ];

        $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();
        $interval = new \DateInterval('P1D');
        $data_fechamento_obj = \DateTime::createFromFormat('Y-m-d', $fechamento_caixa['data_fechamento'])->add($interval);
        $data_fechamento = $data_fechamento_obj->format('Y-m-d');
        $fornecedores = Fornecedor::getAllByTipo($tipo_fornecedor);        
        $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getByFiltros($filtros);
        $unidades = Filial::getUnidadesAtivas($_SESSION['empresa_user']);
        $naturezas = IPCF::getAllNatureza($tipo);
        //$hoje = (new \DateTime())->format('Y-m-d');
        $ur = $_SESSION['empresa_principal'];        
        $aplicacoes = AplicacaoFundos::getAll();
        $assinaturas = Assinatura::getAll();




        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/multiplas-contas/templates/criar-conta.phtml');
    }   
   


    public static function salvar()
    { 
        if(isset($_POST) && !empty($_POST)){
            
            //$dadosNotas['competencia'] = \DateTime::createFromFormat('m/Y', $competencia)->format('Y-m');
            $dadosNotas['data_emissao']  = filter_input(INPUT_POST, 'data_emissao', FILTER_SANITIZE_STRING);
            $dadosNotas['descricao']  = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);
            $dadosNotas['descricao'] = mb_strtoupper($dadosNotas['descricao'], 'UTF-8');
            $dadosNotas['forma_pagamento']  = filter_input(INPUT_POST, 'forma_pagamento', FILTER_SANITIZE_STRING);
            $dadosNotas['fornecedor_nota']  = filter_input(INPUT_POST, 'fornecedor_nota', FILTER_SANITIZE_NUMBER_INT);
            $dadosNotas['natureza_nota']  = filter_input(INPUT_POST, 'natureza_nota', FILTER_SANITIZE_NUMBER_INT);
            $dadosNotas['numero_nota']  = filter_input(INPUT_POST, 'numero_nota', FILTER_SANITIZE_STRING);
            $dadosNotas['origem']  = filter_input(INPUT_POST, 'origem', FILTER_SANITIZE_STRING);
            $dadosNotas['tipo_documento_nota']  = filter_input(INPUT_POST, 'tipo_documento_nota', FILTER_SANITIZE_NUMBER_INT);
            $dadosNotas['tipo_nota']  = filter_input(INPUT_POST, 'tipo_nota', FILTER_SANITIZE_STRING);         
            $dadosNotas['ur_nota']  = filter_input(INPUT_POST, 'ur_nota', FILTER_SANITIZE_NUMBER_INT);
            $dadosNotas['valor']  = filter_input(INPUT_POST, 'valor', FILTER_SANITIZE_STRING);
            $dadosNotas['valor'] = str_replace('.','',$dadosNotas['valor']);
            $dadosNotas['valor'] = str_replace(',','.',$dadosNotas['valor']);
            $numeroRepeticao = filter_input(INPUT_POST, 'numero_repeticao', FILTER_SANITIZE_NUMBER_INT);;
                      
            $competencia  = filter_input(INPUT_POST, 'competencia', FILTER_SANITIZE_STRING);            
            $competencia = \DateTime::createFromFormat('m/Y', $competencia)->format('Y-m');

            $dataEmissao  = filter_input(INPUT_POST, 'data_emissao', FILTER_SANITIZE_STRING);
            $mesAnoEmissao = \DateTime::createFromFormat('Y-m-d', $dataEmissao)->format('Y-m');
            
            $vencimento  = filter_input(INPUT_POST, 'vencimento', FILTER_SANITIZE_STRING);
            $mesAnoVencimento = \DateTime::createFromFormat('Y-m-d', $vencimento)->format('Y-m');
            
            // if($mesAnoVencimento != $mesAnoEmissao || $mesAnoEmissao != $competencia){
            //     $response = ['tipo' => 'erro',
            //     'msg'=> "Data de Emissão, Vencimento e Competência devem estar no mesmo mês/Ano"

            //     ];
            //     echo json_encode($response);
            //     return;
            // }
            $fechamentoCaixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);

            $dataEmissaoObj = new \DateTime($dataEmissao);
            $dataVencimentoOj = new \DateTime($vencimento);
            $diaEmissao = $dataEmissaoObj->format('d');
            $diaVencimento =$dataVencimentoOj->format('d');
            
            // venciento não pode ser menor que emissão 
            if($dataVencimentoOj < $dataEmissaoObj) {
                $response = ['tipo' => 'erro',
                'msg'=>"dadosNotas de vencimento ".$dataEmissaoObj->format('d/m/Y')." menor qua a dadosNotas de emissão do período ".$dataEmissaoObj->format('d/m/Y')

            ];
             echo json_encode($response);
                return;
            }  
            // emissão não pode ser menor que dadosNotas fechamento
            if($dataEmissaoObj <= $fechamentoCaixa) {
                $response = ['tipo' => 'erro',
                'msg'=>"dadosNotas de entrada ".$dataEmissaoObj->format('d/m/Y')." menor qua a dadosNotas de fechamento do período ".$fechamentoCaixa->format('d/m/Y')

            ];
             echo json_encode($response);
                return;
            }  
            // vencimento não pode ser menor que dadosNotas fechamento
            if($dataVencimentoOj <= $fechamentoCaixa) {
                $response = ['tipo' => 'erro',
                'msg'=>"dadosNotas de dagamento ".$dataVencimentoOj->format('d/m/Y')." menor qua a dadosNotas de fechamento do período ".$fechamentoCaixa->format('d/m/Y')

            ];
             echo json_encode($response);
                return;
            } 


            $notaJaCadastrada = Notas::verificarNFJaExiste($dadosNotas['fornecedor_nota'], $dadosNotas['numero_nota']);
            if(!empty($notaJaCadastrada)){
                $response = ['tipo' => 'erro',
                'msg'=>"Conta já possui cadastro no sistema TR {$notaJaCadastrada['idNotas']}."

                ];                
                echo json_encode($response);
                return;
            }

            

            $responseDatas = self::gerarDatas($competencia, $dataEmissao, $vencimento, $numeroRepeticao);
            
            
            $aux = 0;
            $unidadeRateio = $_POST['unidade_rateio'];
            $assinaturaRateio = $_POST['assinatura'];
            $percentualRealRateio = $_POST['percentual_real_rateio'];
            $valorRateio = $_POST['valor_rateio'];
            $naturezaRateio = $_POST['natureza_rateio'];
            $rateio = [];
            $totalPercentual = 0;
            $totalValorRateio = 0;
            $indexRateio = -1;
            foreach($naturezaRateio as $key => $natureza){
                $responseNatureza = IPCF::getById($natureza);
                
                $percentual = str_replace(",",".",str_replace(".",",",$percentualRealRateio[$key]));    
                     
                (float) $totalPercentual += (float) $percentual;
                $valor = str_replace(",",".",str_replace(".","",$valorRateio[$key]));               
                (float) $totalValorRateio += (float) $valor;
               
                $rateio[] = [
                    'percentual'=> $percentual,
                    'valor' => $valor,
                    'assinatura' => $assinaturaRateio[$key],
                    'centro_resultado' => $unidadeRateio[$key],
                    'ipcg4' => $naturezaRateio[$key],
                    'ipcf2' => $responseNatureza['N2'],
                    'ipcg3' => $responseNatureza['N3'],
                ];  
                $indexRateio++;           
            }  
           
            if($totalPercentual != 100){
                $difPercentual = 100 - $totalPercentual;
                $difValor = $dadosNotas['valor'] -  $totalValorRateio;
                $rateio[$indexRateio]['percentual'] = $rateio[$indexRateio]['percentual'] + $difPercentual;
                $rateio[$indexRateio]['valor'] = $rateio[$indexRateio]['valor'] + $difValor;
            }
            
            $response = MultiplasNotas::criar($dadosNotas, $responseDatas, $rateio);              
                     
            
            if($response['tipo'] == 'acerto') {
                $response = ['tipo' => 'acerto',
                'msg'=>$response['msg']

                ];
                echo json_encode($response);
                return;
            } else {
                $response = ['tipo' => 'erro',
                'msg'=> $response['msg']
                ];
                echo json_encode($response);
                return;
               
            }
        }
    } 
    
    public static function gerarDatas($competencia, $dataEmissao, $vencimento, $numeroRepeticao)
    {
        $aux = 0;
        $datas = [];
            $dataEmissaoObj = new \DateTime($dataEmissao);
            $dataVencimentoOj = new \DateTime($vencimento);
            $dataCompetenciaOj = new \DateTime($competencia.'-10');
            $diaEmissao = $dataEmissaoObj->format('d');
            $diaVencimento = $dataVencimentoOj->format('d');
            $vencimentoReal = $vencimento;

        for($i = 0; $i < $numeroRepeticao; $i++){
               
                    $datas[$competencia] = [
                        'competencia' => $competencia,
                        'vencimento' => $vencimento,
                        'dataEmissao' => $dataEmissao,
                        'vencimento_real' => $vencimentoReal
                    ];

            $dataEmissaoObj = new \DateTime($dataEmissao);
            $dataVencimentoOj = new \DateTime($vencimento); 
            $dataCompetenciaOj = new \DateTime($competencia.'-10'); 

            $nextMesVencimento = $dataVencimentoOj->modify('first day of next month');
            $nextMesEmissao = $dataEmissaoObj->modify('first day of next month');
            $nextMesCompetencia = $dataCompetenciaOj->modify('first day of next month');

            $ultimoDiaMesVencimento = $nextMesVencimento->format('t');
            $ultimoDiaMesEmissao = $nextMesEmissao->format('t');

            $dataEmissao = $nextMesEmissao->format('Y-m').'-'.$diaEmissao;
            if($diaEmissao > $ultimoDiaMesEmissao){
                $dataEmissao = $nextMesEmissao->format('Y-m-t');
            }
            $vencimento = $nextMesVencimento->format('Y-m').'-'.$diaVencimento;
            $vencimentoReal = $diaVencimento.'/'.$nextMesVencimento->format('m/Y');
            if($diaVencimento > $ultimoDiaMesVencimento){
                $vencimento = $nextMesVencimento->format('Y-m-t');
                $vencimentoReal =$nextMesVencimento->format('d/m/Y');
            }

            $vencimentoReal = DateHelper::diaUtilVencimentoReal($vencimentoReal);
            $vencimentoReal = \DateTime::createFromFormat('d/m/Y', $vencimentoReal)->format('Y-m-d');  
            $competencia = $nextMesCompetencia->format('Y-m');
         }
         return $datas;

    }
}