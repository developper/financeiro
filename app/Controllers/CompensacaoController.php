<?php
namespace app\Controllers;

use App\Helpers\StringHelper;
use \App\Models\Administracao\Filial;
use App\Models\Financeiro\Compensacao;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\IPCF;
use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\Notas;

include_once('../../utils/codigos.php');


class CompensacaoController
{
    public static function indexFaturaImposto()
    {
        $fornecedores = Fornecedor::getAll();
        $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/fatura-imposto/templates/index-fatura-imposto.phtml');
    }

    public static function criarCompensacao()
    {
        
        if(isset($_GET) && !empty($_GET['antecipada']) && !empty($_GET['id'])){
            $tipo = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING);
            $antecipada = filter_input(INPUT_GET, 'antecipada', FILTER_SANITIZE_STRING);
            $idParcela = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
            $parcela = Compensacao::getNotaByParcelaId($idParcela);
            $response = $parcela[0];
            $response['valor_compensadaBR'] = StringHelper::currencyFormat($response['valor_compensada']);
            $response['valorBR'] = StringHelper::currencyFormat($response['valor']); 
            $response['valor_a_compensar'] = $response['valor'] - $response['valor_compensada'];
            $response['valor_a_compensarBR'] = StringHelper::currencyFormat($response['valor_a_compensar']);
            $tipoNota = $response['tipo'];
            $filtros = [
                'tipo_lancamento' => $tipoNota,
                'adiantamento' => $response['nota_adiantamento'] == 'S'? 'N':'S'
            ];

            if($response['nota_adiantamento'] == 'S'){
                $filtros = [
                    'adiantamento' => $response['nota_adiantamento'] == 'S'? 'N':'S'
                ];
            }
        
            $tipo_fornecedor = $response['tipo'] == '1' ? 'F' : 'C';
            $tipo_titulo = $response['tipo'] == '1' ? 'Saída' : 'Entrada';
            $tipo_antecipada = $response['nota_adiantamento'] == 'S' ? 'Antecipada' : '';                    
            $fornecedores = Fornecedor::getAllByTipo($tipo_fornecedor);

            $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getByFiltros($filtros);
            $unidades = Filial::getUnidadesAtivas($_SESSION['empresa_user']);
            $naturezas = IPCF::getAllNatureza($tipo);
            $hoje = (new \DateTime())->format('Y-m-d');
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/compensacao/templates/criar-compensacao.phtml');
        }else{
            header ("Location: /financeiros/?op=notas&act=buscar");
            exit();
        }
    }
    
    public static function pesquisarParcelaParaCompensar()
    {
        if(isset($_GET) && !empty($_GET)){
            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $tr = filter_input(INPUT_GET, 'tr', FILTER_SANITIZE_NUMBER_INT);
            $tipo_nota = filter_input(INPUT_GET, 'tipo_nota', FILTER_SANITIZE_STRING);
            $nota_adiantamento = filter_input(INPUT_GET, 'nota_antecipada', FILTER_SANITIZE_STRING);
            $fornecedor = filter_input(INPUT_GET, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
            $tipo_documento_financeiro = filter_input(INPUT_GET, 'tipo_documento_financeiro', FILTER_SANITIZE_NUMBER_INT);
            $filtros = [
                'inicio' => $inicio,
                'fim' => $fim,
                'tr' => $tr,
                'tipo_nota' => $tipo_nota,
                'fornecedor' => $fornecedor,
                'tipo_documento_financeiro' => $tipo_documento_financeiro,
                'empresa_user' => $_SESSION['empresa_user'],
                'nota_adiantamento' => $nota_adiantamento
            ];
            $response = Compensacao::pesquisarParcelaByFiltros($filtros);
        }
        echo json_encode($response);
    }   
    public static function salvarCompensacao()
    {
        if(isset($_POST) && !empty($_POST) && !empty($_POST['parcelaCompensar']) && !empty($_POST['parcelas'])){            
            $response = Compensacao::salvarCompensacao($_POST);
            echo json_encode($response);
        }
    }  
    
    public static function cancelarCompensacao()
    {
        if(isset($_POST) && !empty($_POST) ){
            
            $response = Compensacao::cancelarCompensacao($_POST['id']);
           
            echo json_encode($response);
            
           
           
        }
    }
}
