<?php

namespace app\Controllers;

use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\RelatorioNotasEmitidas;

class RelatorioJurosDesconto
{
    public static function index() {

        require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/juros-desconto/index.phtml');

}
    public static  function pesquisar()
    {
        if(isset($_POST['inicio']) && !empty($_POST['inicio'])  && isset($_POST['fim']) && !empty($_POST['fim']) ){
            $data['inicio']  = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim']  = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $data['tipo_nota']  = filter_input(INPUT_POST, 'tipo_nota', FILTER_SANITIZE_STRING); 
            $parcelas = [];
            $response =  Parcela::getParcelasProcessadaComJurosOuDeconto($data);
           
            if(count($response) > 0){
                foreach($response as $r){
                    $parcelas[$r['parcela_id']]['parcela_id'] = $r['parcela_id'];
                    $parcelas[$r['parcela_id']]['TR'] = $r['TR'];
                    $parcelas[$r['parcela_id']]['tipo_nota'] = $r['tipo_nota']; 
                    $parcelas[$r['parcela_id']]['fornecedor'] = $r['fornecedor'];
                    $parcelas[$r['parcela_id']]['juros'] = $r['juros'];
                    $parcelas[$r['parcela_id']]['desconto'] = $r['desconto'];
                    $parcelas[$r['parcela_id']]['encargos'] = $r['encargos'];
                    $parcelas[$r['parcela_id']]['tarifas'] = $r['tarifas'];
                    $parcelas[$r['parcela_id']]['vencimento_real'] =$r['vencimento_real']; 
                    $parcelas[$r['parcela_id']]['data_processada'] = $r['data_processada'];
                    $parcelas[$r['parcela_id']]['natureza_principal'] =  $r['natureza_principal'];
                    $parcelas[$r['parcela_id']]['empresa_nota'] =  $r['empresa_nota']; 
                    $parcelas[$r['parcela_id']]['setor_empresa'][] = $r['setor_empresa'];  
                    $parcelas[$r['parcela_id']]['label'] =  $r['tipo_nota'] == "Recebimento" ? 'label-success' : 'label-danger';              
                   
                }

            }
            
        }
        require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/juros-desconto/index.phtml');


    }

    

    public static function excel()
    {
       
        if(isset($_GET['inicio']) && !empty($_GET['inicio'])  && isset($_GET['fim']) && !empty($_GET['fim']) ){
            $data['inicio']  = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim']  = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $data['tipo_nota']  = filter_input(INPUT_GET, 'tipo_nota', FILTER_SANITIZE_STRING); 
            $parcelas = [];
            $response =  Parcela::getParcelasProcessadaComJurosOuDeconto($data);
           
            if(count($response) > 0){
                foreach($response as $r){
                    $parcelas[$r['parcela_id']]['parcela_id'] = $r['parcela_id'];
                    $parcelas[$r['parcela_id']]['TR'] = $r['TR'];
                    $parcelas[$r['parcela_id']]['tipo_nota'] = $r['tipo_nota']; 
                    $parcelas[$r['parcela_id']]['fornecedor'] = $r['fornecedor'];
                    $parcelas[$r['parcela_id']]['juros'] = $r['juros'];
                    $parcelas[$r['parcela_id']]['desconto'] = $r['desconto'];
                    $parcelas[$r['parcela_id']]['encargos'] = $r['encargos'];
                    $parcelas[$r['parcela_id']]['tarifas'] = $r['tarifas'];
                    $parcelas[$r['parcela_id']]['vencimento_real'] =$r['vencimento_real']; 
                    $parcelas[$r['parcela_id']]['data_processada'] = $r['data_processada'];
                    $parcelas[$r['parcela_id']]['natureza_principal'] =  $r['natureza_principal'];
                    $parcelas[$r['parcela_id']]['empresa_nota'] =  $r['empresa_nota']; 
                    $parcelas[$r['parcela_id']]['setor_empresa'][] = $r['setor_empresa'];  
                    $parcelas[$r['parcela_id']]['label'] =  $r['tipo_nota'] == "Recebimento" ? 'label-success' : 'label-danger';              
                   
                }

            }

            $extension = '.xls';
            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = '.xlsx';
            }

            $filename = sprintf(
                "relatorio_juros_descontos_%s_%s",
                $data['inicio'],
                $data['fim']
            );

            $inicio = \DateTime::createFromFormat('Y-m-d', $data['inicio'])->format('d/m/Y');
            $fim = \DateTime::createFromFormat('Y-m-d', $data['fim'])->format('d/m/Y');
            date_default_timezone_set('America/Sao_Paulo');
            $emitidoEm = date('d/m/Y H:i');
            $emitidoPor = $_SESSION['nome_user'];
            $tipoNota = empty($data['tipo_nota']) ? 'Desembolso/Recebimento' :  ($data['tipo_nota'] == 1 ? 'Desembolso' : 'Recebimento'); 


            header("Content-type: application/x-msexce");
            header("Content-type: application/force-download;");
            header("Content-Disposition: attachment; filename={$filename}".$extension);
            header("Pragma: no-cache");
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/juros-desconto/excel.phtml');
            
        }
       


    }

    public static  function formNotasComProcessamento()
    {
        if($_GET['tipo']){
            $tipoNota = $_GET['tipo'];
            $titulo = ucfirst($_GET['tipo']);
            $filtros = [
                'notIn'=>'23, 25, 26',
            ];
            $data['reteve_imposto'] = 'N';
            $tipoDocumentoFinanceiro = TipoDocumentoFinanceiro::getByFiltros($filtros);
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/notas/form-com-processamento.phtml');
        }

    }

    public static function pesquisarNotasComProcessamento()
    {
        if(isset($_GET) && !empty($_GET['tipo_nota'])){
            $data['inicio']  = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim']  = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $data['competencia_inicio']  = filter_input(INPUT_GET, 'competencia_inicio', FILTER_SANITIZE_STRING);
            $data['competencia_fim']  = filter_input(INPUT_GET, 'competencia_fim', FILTER_SANITIZE_STRING);
            if(!empty($data['competencia_inicio'])){
                $data['competencia_inicio_db'] = \DateTime::createFromFormat('m/Y', $data['competencia_inicio'])->format('Y-m');
            }
            if(!empty($data['competencia_fim'])){
                $data['competencia_fim_db'] = \DateTime::createFromFormat('m/Y', $data['competencia_fim'])->format('Y-m');
            }

            $data['tipo_nota']  = filter_input(INPUT_GET, 'tipo_nota', FILTER_SANITIZE_STRING);
            $data['reteve_imposto']  = filter_input(INPUT_GET, 'reteve_imposto', FILTER_SANITIZE_STRING);
            $data['tipo_documento_financeiro']  = isset($_GET['tipo_documento_financeiro'])  ? $_GET['tipo_documento_financeiro'] : [];

            $data['tipo_documento_financeiro_string'] = !empty($data['tipo_documento_financeiro']) ? implode(', ', $data['tipo_documento_financeiro']) : null;

            $data['tipo_nota'] = $data['tipo_nota'] == 'pagar' ? 1 : 0;
            $tipoNota = $_GET['tipo_nota'];
            $titulo = ucfirst($_GET['tipo_nota']);
            $filtros = [
                'notIn'=>'23, 25, 26',
            ];
            $tipoDocumentoFinanceiro = TipoDocumentoFinanceiro::getByFiltros($filtros);


            //nota de desembolso
            if($data['tipo_nota'] == 1 ){
                $response = RelatorioNotasEmitidas::getNotasPagarComProcessamento($data);
            }else{
                $response = RelatorioNotasEmitidas::getNotasReceberComProcessamento($data);
            }
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/notas/form-com-processamento.phtml');
        }
    }

    public static function excelNotasProcessada()
    {

        if(isset($_GET) && !empty($_GET['tipo_nota'])){

            $data['inicio']  = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim']  = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $data['competencia_inicio']  = filter_input(INPUT_GET, 'competencia_inicio', FILTER_SANITIZE_STRING);
            $data['competencia_fim']  = filter_input(INPUT_GET, 'competencia_fim', FILTER_SANITIZE_STRING);
            $data['tipo_nota']  = filter_input(INPUT_GET, 'tipo_nota', FILTER_SANITIZE_STRING);
            $data['reteve_imposto']  = filter_input(INPUT_GET, 'reteve_imposto', FILTER_SANITIZE_STRING);

            $data['tipo_documento_financeiro_string']  = isset($_GET['tipo_documento_financeiro_string']) && !empty($_GET['tipo_documento_financeiro_string']) ? $_GET['tipo_documento_financeiro_string'] : '';

            $data['tipo_documento_financeiro'] = [];
            if(!empty($data['tipo_documento_financeiro_string'])){
                $data['tipo_documento_financeiro'] = isset($_GET['tipo_documento_financeiro_string']) ?
                    explode(',', $_GET['tipo_documento_financeiro_string']) : [];
                $data['tipo_documento_financeiro_string']= implode(', ', $data['tipo_documento_financeiro']);

            }

            $data['tipo_nota'] = $data['tipo_nota'] == 'pagar' ? 1 : 0;
            $tipoNota = $_GET['tipo_nota'];
            $titulo = ucfirst($_GET['tipo_nota']);
            $filtros = [
                'notIn'=>'23, 25, 26',
            ];

            $tipoDocFin= 'Todos';

            if(!empty($data['tipo_documento_financeiro_string'])){
                $filtro[ 'idIn'] = $data['tipo_documento_financeiro_string'];
                $responseTipoDocFin = TipoDocumentoFinanceiro::getByFiltros($filtro);
                $arrayResponseTipoDoc = [];
                foreach($responseTipoDocFin as $tipo){
                    $arrayResponseTipoDoc[] = $tipo['sigla'];
                }
                $tipoDocFin = implode(',', $arrayResponseTipoDoc );
            }


            $extension = '.xls';
            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = '.xlsx';
            }

            $filename = sprintf(
                "relatorio_notas_processadas_%s_%s",
                $data['inicio'],
                $data['fim']
            );
            $inicio = \DateTime::createFromFormat('Y-m-d', $data['inicio'])->format('d/m/Y');
            $fim = \DateTime::createFromFormat('Y-m-d', $data['fim'])->format('d/m/Y');
            $competencia_inicio = '';
            $competencia_fim = '';
            if(!empty($data['competencia_inicio'])){
                $competencia_inicio = \DateTime::createFromFormat('Y-m', $data['competencia_inicio'])->format('m/Y');
            }
            if(!empty($data['competencia_fim'])){
                $competencia_fim = \DateTime::createFromFormat('Y-m', $data['competencia_fim'])->format('m/Y');

            }
           

            $reteveImposto = $data['reteve_imposto'] == 'N' ? 'Não' : 'Sim';
            date_default_timezone_set('America/Sao_Paulo');
            $emitidoEm = date('d/m/Y H:i');
            $emitidoPor = $_SESSION['nome_user'];


            header("Content-type: application/x-msexce");
            header("Content-type: application/force-download;");
            header("Content-Disposition: attachment; filename={$filename}".$extension);
            header("Pragma: no-cache");

            //nota de desembolso
            if($data['tipo_nota'] == 1 ){
                $response = RelatorioNotasEmitidas::getNotasPagarComProcessamento($data);
            }else{
                $response = RelatorioNotasEmitidas::getNotasReceberComProcessamento($data);
            }
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/notas/excel-processada.phtml');
        }


    }


}