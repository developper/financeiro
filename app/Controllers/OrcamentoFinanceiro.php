<?php

namespace app\Controllers;

use App\Helpers\DateHelper;
use App\Helpers\StringHelper;
use \App\Models\Administracao\Filial;
use App\Models\Financeiro\IPCF;
use App\Models\Financeiro\OrcamentoFinanceiro as ModelOrcamentoFinanceiro;
use App\Controllers\OrcamentoFinanceiroRelatorioGerencial;
use DateInterval;
use DateTime;



include_once('../../utils/codigos.php');

class OrcamentoFinanceiro
{


    public static function index()
    {

        if (isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']) || $_SESSION['adm_user'] == 1) {
            $responseArray = ModelOrcamentoFinanceiro::getAll();

            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/orcamento-financeiro/templates/index.phtml');
        } else {

            require($_SERVER['DOCUMENT_ROOT'] . '/401.html');
        }
    }



    public static function novo()
    {

        if (isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']['financeiro_orcamento_financeiro_criar']) || $_SESSION['adm_user'] == 1) {

            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/orcamento-financeiro/templates/form.phtml');
        } else {

            require($_SERVER['DOCUMENT_ROOT'] . '/401.html');
        }
    }

    public static function cadastrarValoresGerenciaisByModeloOrcamentoById()
    {

        if (isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']['financeiro_orcamento_financeiro_criar']) || $_SESSION['adm_user'] == 1) {

            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/orcamento-financeiro/templates/form-cadastro-valores-gerenciais.phtml');
        } else {

            require($_SERVER['DOCUMENT_ROOT'] . '/401.html');
        }
    }


    public static function relatorioOrcamentoFinanceiro()
    {

        if (isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']['financeiro_orcamento_financeiro_relatorio']) || $_SESSION['adm_user'] == 1) {

            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/orcamento-financeiro/templates/relatorio-orcamento-financeiro.phtml');
        } else {

            require($_SERVER['DOCUMENT_ROOT'] . '/401.html');
        }
    }

    public static function relatorioByModeloAndOrcamentoById()
    {
        if ((isset($_GET['id']) && !empty($_GET['id'])) && (isset($_GET['type']) && !empty($_GET['type']))) {
            $tipo = filter_input(INPUT_GET, 'type', FILTER_SANITIZE_STRING);


            if ($_SESSION['adm_user'] <> 1) {

                if (($tipo == 'relatorio' || $tipo == 'relatorio-gerencial')  && !isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']['financeiro_orcamento_financeiro_relatorio'])) {
                    header('Status: 400');
                    echo json_encode('Permissao negadas');
                    return;
                }
            }
            $unidadeId = filter_input(INPUT_GET,'unidade', FILTER_SANITIZE_STRING);
            $idOrcamento = filter_input(INPUT_GET,'id', FILTER_SANITIZE_STRING);
            if($tipo == 'relatorio'){
               return self::relatorioOrcamentoById($idOrcamento, $unidadeId);
            }elseif($tipo == 'relatorio-gerencial'){
                return self::relatorioModeloGerencialOrcamentoById($idOrcamento);
            }else{
               
                return OrcamentoFinanceiroRelatorioGerencial::relatorioModeloGerencialAutomatico($idOrcamento);
            }
        }
    }



    public static function gerar()
    {

        if (isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']['financeiro_orcamento_financeiro_criar']) || $_SESSION['adm_user'] == 1) {

            if (
                isset($_POST['fim']) && !empty($_POST['fim']) &&
                isset($_POST['inicio']) && !empty($_POST['inicio'])
            ) {
                // passa data no formato Y-m função retorna todos os meses entre o periodo
                $inicioOrcamento = $_POST['inicio'];
                $fimOrcamento = $_POST['fim'];
                $responseDatas = DateHelper::getListaMesAno($_POST['inicio'], $_POST['fim']);
                $responeMaxVersaoEstrutura = ModelOrcamentoFinanceiro::getMaxVersionEstrutura();
                $versoaoEstrutura = $responeMaxVersaoEstrutura['versaoEstrutura'];

                $responseTopicosPais = ModelOrcamentoFinanceiro::getTopicosByIdPaiEVersao(0, $versoaoEstrutura);
                $responseEstruturaTopicos = ModelOrcamentoFinanceiro::getEstruturaByVersao($versoaoEstrutura);
                $responseTopicos = ModelOrcamentoFinanceiro::getTopicos();
                $responseVersaoOrcamento = ModelOrcamentoFinanceiro::gerarOrcamento($responeMaxVersaoEstrutura['versaoEstrutura'], $_POST['inicio'], $_POST['fim'], $responseEstruturaTopicos, $responseDatas);
                if ($versoaoEstrutura != 2) {
                    $planoContasAdicionados = [];
                } else {
                    //modelo DRE
                    // 16-06-2021
                    // $planoContasAdicionados  = [
                    //     '1',
                    //     '3',
                    //     '2',
                    //     '4',
                    //     '277',
                    //     '279',
                    //     '283',
                    //     '284',
                    //     '150',
                    //     '151',
                    //     '149',
                    //     '167',
                    //     '19',
                    //     '5',
                    //     '6',
                    //     '7',
                    //     '8',
                    //     '9',
                    //     '132',
                    //     '133',
                    //     '204',
                    //     '134',
                    //     '205',
                    //     '274',
                    //     '135',
                    //     '10',
                    //     '11',
                    //     '12',
                    //     '152',
                    //     '153',
                    //     '16',
                    //     '163',
                    //     '169',
                    //     '170',
                    //     '171',
                    //     '172',
                    //     '173',
                    //     '175',
                    //     '174',
                    //     '176',
                    //     '177',
                    //     '178',
                    //     '179',
                    //     '180',
                    //     '181',
                    //     '182',
                    //     '183',
                    //     '184',
                    //     '185',
                    //     '186',
                    //     '187',
                    //     '188',
                    //     '189',
                    //     '190',
                    //     '191',
                    //     '224'
                    // ];
                    // 16-06-2021 fim
                }

                if ($responseVersaoOrcamento['tipo'] == 'success') {
                    $idOrcamento = $responseVersaoOrcamento['id'];
                    $responseValoresTopicos = ModelOrcamentoFinanceiro::getValoresTopicosByVersao($idOrcamento);
                    $valoresTopicos = [];
                    $porcentagemTopico = [];

                    foreach ($responseTopicos as $t) {
                        $topicos[$t['id']] = $t['descricao'];
                    }

                    $auxValores = 0;
                    foreach ($responseValoresTopicos as $vt) {
                        $mesAno =  \DateTime::createFromFormat('Y-m', $vt['data']);
                        $topicoId = $vt['orcamento_financeiro_topicos'];
                        $porcentagemTopico[$topicoId] = $vt['aliquota'];
                        $valoresTopicos[$topicoId][$auxValores]['value'] = $vt['valor_mensal'];
                        $valoresTopicos[$topicoId][$auxValores]['showCell'] = false;
                        $valoresTopicos[$topicoId][$auxValores]['id'] = $vt['id'];
                        $valoresTopicos[$topicoId][$auxValores]['topico_id'] = $topicoId;
                        $valoresTopicos[$topicoId][$auxValores]['table'] = 'orcamento_financeiro_topicos_valores';
                        $valoresTopicos[$topicoId][$auxValores]['month'] = $mesAno->format('m');
                        $valoresTopicos[$topicoId][$auxValores]['year'] = $mesAno->format('Y');
                        $valoresTopicos[$topicoId][$auxValores]['data'] = $mesAno->format('Y-m');
                        $auxValores++;
                    }

                    $arrayTopico = [];
                    $aux01 = 0;


                    foreach ($responseTopicosPais as $topico) {
                        $topicoId = $topico['orcamento_finananceiro_topicos_id'];
                        $data[$aux01]['id_topico'] = $topicoId;
                        $data[$aux01]['name'] = $topicos[$topicoId];
                        $data[$aux01]['percentage'] = (float) $porcentagemTopico[$topicoId];
                        $data[$aux01]['id_budget'] = $idOrcamento;
                        $data[$aux01]['type_calc'] = "VALUE";
                        $data[$aux01]['months'] = array_values($valoresTopicos[$topicoId]);
                        $responseFilhos = ModelOrcamentoFinanceiro::getTopicosByIdPaiEVersao($topicoId, $versoaoEstrutura);

                        if (!empty($responseFilhos)) {
                            $aux02 = 0;

                            foreach ($responseFilhos as $resp) {
                                $idFilho = $resp['orcamento_finananceiro_topicos_id'];
                                $data[$aux01]['sub'][$aux02]['id_topico'] = $idFilho;
                                $data[$aux01]['sub'][$aux02]['name'] = $topicos[$idFilho];
                                $data[$aux01]['sub'][$aux02]['percentage'] = (float) 0;
                                $data[$aux01]['sub'][$aux02]['type_calc'] = "VALUE";
                                $data[$aux01]['sub'][$aux02]['id_budget'] = $idOrcamento;
                                $data[$aux01]['sub'][$aux02]['months'] = array_values($valoresTopicos[$idFilho]);
                                $responseNetos = ModelOrcamentoFinanceiro::getTopicosByIdPaiEVersao($idFilho, $versoaoEstrutura);
                                if (!empty($responseNetos)) {
                                    $aux03 = 0;
                                    foreach ($responseNetos as $respNeto) {
                                        $idNeto = $respNeto['orcamento_finananceiro_topicos_id'];
                                        $data[$aux01]['sub'][$aux02]['sub'][$aux03]['id_topico'] = $idNeto;
                                        $data[$aux01]['sub'][$aux02]['sub'][$aux03]['name'] = $topicos[$idNeto];
                                        $data[$aux01]['sub'][$aux02]['sub'][$aux03]['percentage'] = (float) 0;
                                        $data[$aux01]['sub'][$aux02]['sub'][$aux03]['type_calc'] = "VALUE";
                                        $data[$aux01]['sub'][$aux02]['sub'][$aux03]['id_budget'] = $idOrcamento;
                                        $data[$aux01]['sub'][$aux02]['sub'][$aux03]['months'] = array_values($valoresTopicos[$idNeto]);
                                        $data[$aux01]['sub'][$aux02]['sub'][$aux03]['sub'] = [];
                                        $aux03++;
                                    }
                                } else {
                                    $data[$aux01]['sub'][$aux02]['sub'] = [];
                                }
                                $aux02++;
                            }
                        } else {
                            $data[$aux01]['sub'] = [];
                        }

                        $aux01++;
                    }

                    $arrayMonths = [];

                    foreach ($responseDatas as $resp) {
                        $mesAno =  \DateTime::createFromFormat('Y-m', $resp);
                        $arrayMonths[] = [
                            'month' => $mesAno->format('m'),
                            'year' => $mesAno->format('Y'),
                            'date' => $mesAno->format('Y-m')
                        ];
                    }

                    $response = [
                        'items' => array_values($data),
                        'months' => array_values($arrayMonths),
                        'id_budget' => $idOrcamento,
                        'begin_date' => $inicioOrcamento,
                        'end_date' => $fimOrcamento,
                        'chart_of_accounts_add' => array_values($planoContasAdicionados),
                    ];

                    echo json_encode($response);
                    return;
                }
            }
            http_response_code(500);
            echo json_encode('Erro dados incorretos');
        }
        header('Status: 400');
        echo json_encode('Permissão Negada');
    }

    public static function listarNaturezas()
    {
        $responseNaturezas =  IPCF::getAll();
        $naturezas = [];

        foreach ($responseNaturezas as $resp) {
            $naturezas[] = ['id' => $resp['ID'], 'name' => $resp['COD_N4'] . ' ' . $resp['N4']];
        }


        print_r(json_encode(array_values($naturezas)));
    }

    public static function orcamentoById()
    {



        if ((isset($_GET['id']) && !empty($_GET['id'])) && (isset($_GET['type']) && !empty($_GET['type']))) {
            $tipo = filter_input(INPUT_GET, 'type', FILTER_SANITIZE_STRING);

            if ($_SESSION['adm_user'] <> 1) {

                if ($tipo == 'editar' && !isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']['financeiro_orcamento_financeiro_editar'])) {
                    header('Status: 400');
                    echo json_encode('Permissao negadas');
                    return;
                }

                if ($tipo == 'visualizar' && !isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']['financeiro_orcamento_financeiro_visualizar'])) {
                    header('Status: 400');
                    echo json_encode('Permissão negadas');
                    return;
                }
            }


            $idOrcamento = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
            $responseVersaoOrcamento  =  current(ModelOrcamentoFinanceiro::getVersaoOrcamentoById($idOrcamento));
            $versoaoEstrutura = $responseVersaoOrcamento['orcamento_financeiro_versao_estrutura'];
            $inicioOrcamento = $responseVersaoOrcamento['inicio'];
            $fimOrcamento = $responseVersaoOrcamento['fim'];


            // passa data no formato Y-m função retorna todos os meses entre o periodo
            $responseDatas = DateHelper::getListaMesAno($inicioOrcamento, $fimOrcamento);
            $responseTopicosPais = ModelOrcamentoFinanceiro::getTopicosByIdPaiEVersao(0, $versoaoEstrutura);
            $responseEstruturaTopicos = ModelOrcamentoFinanceiro::getEstruturaByVersao($versoaoEstrutura);
            $responseTopicos = ModelOrcamentoFinanceiro::getTopicos();
            $responseValoresTopicos = ModelOrcamentoFinanceiro::getValoresTopicosByVersao($idOrcamento);
            $responseValoresPlanoContas = ModelOrcamentoFinanceiro::getValoresPlanoContasByVersao($idOrcamento);
            $valoresTopicos = [];
            $valoresPlanoContas = [];
            $auxPlanoContas = 0;
            if ($versoaoEstrutura != 2) {
                $planoContasAdicionados = [];
            } else {

                //modelo DRE
              /*  16-06-2021
                $planoContasAdicionados  = [
                    '1',
                    '3',
                    '2',
                    '4',
                    '277',
                    '279',
                    '283',
                    '284',
                    '150',
                    '151',
                    '149',
                    '167',
                    '19',
                    '5',
                    '6',
                    '7',
                    '8',
                    '9',
                    '132',
                    '133',
                    '204',
                    '134',
                    '205',
                    '274',
                    '135',
                    '10',
                    '11',
                    '12',
                    '152',
                    '153',
                    '16',
                    '163',
                    '169',
                    '170',
                    '171',
                    '172',
                    '173',
                    '175',
                    '174',
                    '176',
                    '177',
                    '178',
                    '179',
                    '180',
                    '181',
                    '182',
                    '183',
                    '184',
                    '185',
                    '186',
                    '187',
                    '188',
                    '189',
                    '190',
                    '191',
                    '224'
                ];
                */
            }
            $porcentagemTopico = [];
            $porcentagemPlanoContas = [];


            if (!empty($responseValoresPlanoContas)) {
                $responseNaturezas =  IPCF::getAll();
                $naturezas = [];

                foreach ($responseNaturezas as $resp) {
                    $naturezas[$resp['ID']] =  $resp['COD_N4'] . ' ' . $resp['N4'];
                }
                $auxPlanoContas = 10000;
                foreach ($responseValoresPlanoContas as $resp) {

                    $topicoId = $resp['orcamento_financeiro_topicos_id'];
                    $planoContasId = $resp['plano_contas_id'];
                    $planoContasAdicionados[$auxPlanoContas] = $planoContasId;
                    $auxPlanoContas++;
                    $mesAno =  \DateTime::createFromFormat('Y-m', $resp['data']);

                    $paiPlanoConta[$topicoId][$planoContasId]['id_topico'] = $topicoId;
                    $paiPlanoConta[$topicoId][$planoContasId]['id_plano_contas'] = $planoContasId;
                    $paiPlanoConta[$topicoId][$planoContasId]['name'] = $naturezas[$planoContasId];
                    $paiPlanoConta[$topicoId][$planoContasId]['percentage'] = (float) $resp['aliquota'];
                    $paiPlanoConta[$topicoId][$planoContasId]['type_calc'] = (float) $resp['aliquota'] == 0 ? "VALUE" : "PERCENTAGE";
                    $paiPlanoConta[$topicoId][$planoContasId]['isAddAccountPlan'] = true;
                    $paiPlanoConta[$topicoId][$planoContasId]['sub'] = [];



                    $paiPlanoConta[$topicoId][$planoContasId]['months'][] = [
                        'value' => $resp['valor_mensal'],
                        'showCell' => false,
                        'id' => $resp['id'],
                        'topico_id' => $topicoId,
                        'table' => 'orcamento_financeiro_plano_contas_valores',
                        'month' => $mesAno->format('m'),
                        'year' => $mesAno->format('Y'),
                        'data' => $mesAno->format('Y-m')
                    ];
                }
            }


            foreach ($responseTopicos as $t) {
                $topicos[$t['id']] = $t['descricao'];
            }

            $auxValores = 0;
            foreach ($responseValoresTopicos as $vt) {
                $mesAno =  \DateTime::createFromFormat('Y-m', $vt['data']);
                $topicoId = $vt['orcamento_financeiro_topicos'];
                $porcentagemTopico[$topicoId] = $vt['aliquota'];
                $valoresTopicos[$topicoId][$auxValores]['value'] = $vt['valor_mensal'];
                $valoresTopicos[$topicoId][$auxValores]['showCell'] = false;
                $valoresTopicos[$topicoId][$auxValores]['id'] = $vt['id'];
                $valoresTopicos[$topicoId][$auxValores]['topico_id'] = $topicoId;
                $valoresTopicos[$topicoId][$auxValores]['table'] = 'orcamento_financeiro_topicos_valores';
                $valoresTopicos[$topicoId][$auxValores]['month'] = $mesAno->format('m');
                $valoresTopicos[$topicoId][$auxValores]['year'] = $mesAno->format('Y');
                $valoresTopicos[$topicoId][$auxValores]['data'] = $mesAno->format('Y-m');
                $auxValores++;
            }

            $arrayTopico = [];
            $aux01 = 0;


            foreach ($responseTopicosPais as $topico) {
                $topicoId = $topico['orcamento_finananceiro_topicos_id'];
                $data[$aux01]['id_topico'] = $topicoId;
                $data[$aux01]['name'] = $topicos[$topicoId];
                $data[$aux01]['percentage'] = (float) $porcentagemTopico[$topicoId];
                $data[$aux01]['type_calc'] = (float) $porcentagemTopico[$topicoId] == 0 ? "VALUE" : "PERCENTAGE";
                $data[$aux01]['months'] = array_values($valoresTopicos[$topicoId]);
                $colorBackGround = 'green';
                $yellowBackground = [42, 49, 53, 70, 71, 75, 76];
                if ($versoaoEstrutura != 2) {
                    if ($topicoId == 1) {
                        $colorBackGround = 'blue';
                    }
                } else {
                    if ($topicoId == 38) {
                        $colorBackGround = 'blue';
                    } elseif (in_array($topicoId, $yellowBackground)) {

                        $colorBackGround = 'yellow';
                    }
                }

                $data[$aux01]['color_background'] = $colorBackGround;
                $responseFilhos = ModelOrcamentoFinanceiro::getTopicosByIdPaiEVersao($topicoId, $versoaoEstrutura);

                if (!empty($responseFilhos)) {
                    if (isset($paiPlanoConta[$topicoId]) && !empty($paiPlanoConta[$topicoId])) {
                        $data[$aux01]['sub'] = array_values($paiPlanoConta[$topicoId]);
                        $aux02 = count($paiPlanoConta[$topicoId]);
                    } else {
                        $aux02 = 0;
                    }


                    foreach ($responseFilhos as $resp) {
                        $idFilho = $resp['orcamento_finananceiro_topicos_id'];
                        $data[$aux01]['sub'][$aux02]['id_topico'] = $idFilho;
                        $data[$aux01]['sub'][$aux02]['name'] = $topicos[$idFilho];
                        $data[$aux01]['sub'][$aux02]['percentage'] = (float) $porcentagemTopico[$idFilho];
                        $data[$aux01]['sub'][$aux02]['type_calc'] = (float) $porcentagemTopico[$idFilho] == 0 ? "VALUE" : "PERCENTAGE";
                        $data[$aux01]['sub'][$aux02]['months'] = array_values($valoresTopicos[$idFilho]);

                        $responseNetos = ModelOrcamentoFinanceiro::getTopicosByIdPaiEVersao($idFilho, $versoaoEstrutura);
                        if (!empty($responseNetos)) {
                            if (isset($paiPlanoConta[$idFilho]) && !empty($paiPlanoConta[$idFilho])) {
                                $data[$aux01]['sub'][$aux02]['sub'] = array_values($paiPlanoConta[$idFilho]);
                                $aux03 = count($paiPlanoConta[$idFilho]);
                            } else {
                                $aux03 = 0;
                            }
                            foreach ($responseNetos as $respNeto) {
                                $idNeto = $respNeto['orcamento_finananceiro_topicos_id'];
                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['id_topico'] = $idNeto;
                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['name'] = $topicos[$idNeto];
                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['percentage'] = (float) $porcentagemTopico[$idNeto];
                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['type_calc'] = (float) $porcentagemTopico[$idNeto] == 0 ? "VALUE" : "PERCENTAGE";

                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['months'] = array_values($valoresTopicos[$idNeto]);
                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['sub'] = [];
                                if (array_key_exists($idNeto, $paiPlanoConta)) {
                                    $data[$aux01]['sub'][$aux02]['sub'][$aux03]['sub'] = array_values($paiPlanoConta[$idNeto]);
                                }
                                $aux03++;
                            }
                        } else {
                            if (isset($paiPlanoConta[$idFilho]) && !empty($paiPlanoConta[$idFilho])) {
                                $data[$aux01]['sub'][$aux02]['sub'] = array_values($paiPlanoConta[$idFilho]);
                            } else {
                                $data[$aux01]['sub'][$aux02]['sub'] =  [];
                            }
                        }

                        if (array_key_exists($idFilho, $valoresPlanoContas)) {
                            $data[$aux01]['sub'][$aux02]['plano_contas'] = array_values($valoresPlanoContas[$idFilho]);
                        }
                        $aux02++;
                    }
                } else {
                    $data[$aux01]['sub'] = [];
                }
                if (array_key_exists($topicoId, $valoresPlanoContas)) {
                    $data[$aux01]['plano_contas'] = array_values($valoresPlanoContas[$topicoId]);
                }
                $aux01++;
            }

            $arrayMonths = [];

            foreach ($responseDatas as $resp) {
                $mesAno =  \DateTime::createFromFormat('Y-m', $resp);
                $arrayMonths[] = [
                    'month' => $mesAno->format('m'),
                    'year' => $mesAno->format('Y'),
                    'date' => $mesAno->format('Y-m')
                ];
            }


            $response = [
                'items' => array_values($data),
                'months' => array_values($arrayMonths),
                'id_budget' => $idOrcamento,
                'type' =>  $tipo == 'editar' ? 'edit' : 'view',
                'begin_date' => $inicioOrcamento,
                'end_date' => $fimOrcamento,
                'chart_of_accounts_add' => array_values($planoContasAdicionados),
            ];

            echo json_encode($response);
            return;
        }

        http_response_code(500);
        echo json_encode('Erro dados incorretos');
    }

    public static function updateItem()
    {

        if (
            isset($_POST['id']) && !empty($_POST['id']) &&
            isset($_POST['tabela']) && !empty($_POST['tabela'])
        ) {

            $data['valor'] = filter_input(INPUT_POST, 'valor', FILTER_SANITIZE_STRING);
            $data['porcentagem'] = filter_input(INPUT_POST, 'porcentagem', FILTER_SANITIZE_STRING);
            $data['id'] = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $data['acao'] = 'update';
            $tabela = filter_input(INPUT_POST, 'tabela', FILTER_SANITIZE_STRING);

            if ($tabela == 'orcamento_financeiro_topicos_valores') {
                $response =  ModelOrcamentoFinanceiro::updateValoresTopico($data);
            } else {
                $response = ModelOrcamentoFinanceiro::updateValoresPlanoContas($data);
            }

            if ($response['tipo'] == 'erro') {
                http_response_code(500);
                echo json_encode($response['msg']);
            } else {
                echo json_encode($response['msg']);
            }
            return;
        }
        http_response_code(500);
        echo json_encode('Erro dados incorretos');
    }

    public static function updateItens()
    {

        $itens = json_decode(file_get_contents("php://input"));
        mysql_query('begin');
        foreach ($itens as $i) {

            $data[$i->id]['valor'] = $i->valor;
            $data[$i->id]['porcentagem'] = $i->porcentagem;
            $data[$i->id]['id'] = $i->id;
            $data[$i->id]['acao'] = 'update';
            $tabela = $i->tabela;
            if ($tabela == 'orcamento_financeiro_topicos_valores') {
                $response =  ModelOrcamentoFinanceiro::updateValoresTopico($data[$i->id]);
            } else {
                $response = ModelOrcamentoFinanceiro::updateValoresPlanoContas($data[$i->id]);
            }

            if ($response['tipo'] == 'erro') {

                mysql_query('rollback');
                http_response_code(500);
                echo json_encode($response['msg']);
                return;
            }
        }
        mysql_query('commit');
        echo json_encode($response['msg']);


        //  echo json_decode($_POST);
    }


    public static function gerarItensPlanoContas()
    {

        if (
            isset($_POST['orcamento_id']) && !empty($_POST['orcamento_id']) &&
            isset($_POST['plano_conta_id']) && !empty($_POST['plano_conta_id']) &&
            isset($_POST['id_topico']) && !empty($_POST['id_topico']) &&
            isset($_POST['data_inicio']) && !empty($_POST['data_inicio']) &&
            isset($_POST['data_fim']) && !empty($_POST['data_fim'])
        ) {

            $dados['orcamento_id'] = filter_input(INPUT_POST, 'orcamento_id', FILTER_SANITIZE_NUMBER_INT);
            $dados['plano_conta_id'] = filter_input(INPUT_POST, 'plano_conta_id', FILTER_SANITIZE_NUMBER_INT);
            $dados['data_inicio'] =  filter_input(INPUT_POST, 'data_inicio', FILTER_SANITIZE_STRING);
            $dados['data_fim'] =  filter_input(INPUT_POST, 'data_fim', FILTER_SANITIZE_STRING);
            $dados['valor'] = 0;
            $dados['porcentagem'] = 0;
            $dados['topico_id'] = filter_input(INPUT_POST, 'id_topico', FILTER_SANITIZE_NUMBER_INT);

            $responseDatas = DateHelper::getListaMesAno($dados['data_inicio'], $dados['data_fim']);
            $response = ModelOrcamentoFinanceiro::createPlanoContasValores($dados, $responseDatas);

            if ($response['tipo'] == 'success') {
                $auxPlanoContas = 0;
                $responseValoresPlanoContas = ModelOrcamentoFinanceiro::getValoresPlanoContasByVersaoAndPlanoContaId($dados['orcamento_id'], $dados['plano_conta_id']);
                if (!empty($responseValoresPlanoContas)) {
                    $responseNaturezas =  IPCF::getAll();
                    $naturezas = [];

                    foreach ($responseNaturezas as $resp) {
                        $naturezas[$resp['ID']] =  $resp['COD_N4'] . ' ' . $resp['N4'];
                    }
                    foreach ($responseValoresPlanoContas as $resp) {


                        $topicoId = $resp['orcamento_financeiro_topicos_id'];
                        $planoContasId = $resp['plano_contas_id'];
                        $mesAno =  \DateTime::createFromFormat('Y-m', $resp['data']);

                        $paiPlanoConta[$planoContasId]['id_topico'] = $topicoId;
                        $paiPlanoConta[$planoContasId]['id_plano_contas'] = $planoContasId;
                        $paiPlanoConta[$planoContasId]['name'] = $naturezas[$planoContasId];
                        $paiPlanoConta[$planoContasId]['percentage'] = $resp['aliquota'];
                        $paiPlanoConta[$planoContasId]['isAddAccountPlan'] = true;
                        $paiPlanoConta[$planoContasId]['sub'] = [];



                        $paiPlanoConta[$planoContasId]['months'][] = [
                            'value' => $resp['valor_mensal'],
                            'showCell' => false,
                            'id' => $resp['id'],
                            'topico_id' => $topicoId,
                            'table' => 'orcamento_financeiro_plano_contas_valores',
                            'month' => $mesAno->format('m'),
                            'year' => $mesAno->format('Y'),
                            'data' => $mesAno->format('Y-m')
                        ];

                        $auxPlanoContas++;
                    }
                }

                echo json_encode(array_values($paiPlanoConta));
                return;
            }
            http_response_code(500);
            echo json_encode($response['msg']);
            return;
        }
        http_response_code(500);
        echo json_encode('Erro: Dados incorretos.');
        return;
    }
    public static function deleteItens()
    {

        $itens = json_decode(file_get_contents("php://input"));

        mysql_query('begin');
        foreach ($itens as $i) {

            // $data[$i->id]['valor'] = $i->valor;
            // $data[$i->id]['porcentagem'] = $i->porcentagem;
            $data[$i->id]['id'] = $i->id;
            // $data[$i->id]['acao'] = 'delete';
            $tabela = $i->tabela;
            if ($tabela == 'orcamento_financeiro_topicos_valores') {
                $response =  ModelOrcamentoFinanceiro::deleteValoresTopico($data[$i->id]);
            } else {
                $response = ModelOrcamentoFinanceiro::deleteValoresPlanoContas($data[$i->id]);
            }

            if ($response['tipo'] == 'erro') {

                mysql_query('rollback');
                http_response_code(500);
                echo json_encode($response['msg']);
                return;
            }
        }
        mysql_query('commit');
        echo json_encode($response['msg']);


        //  echo json_decode($_POST);
    }


    public static function getRealizadoByData($inicio, $fim, $unidadeId, $versoaoEstrutura)
    {

        $responseRealizado = ModelOrcamentoFinanceiro::getRealizadoByData($inicio, $fim, $unidadeId);


        $planoContasRealizado = [];
        $classeRealizado = [];
        $identificacaoParcelas = [];
        // id naureza prolabore;
        $idProlabore = 108;
        $idJurosAtivo = 5;
        $idJurosEmprestimoFinanciameto = 133;
        $idJurosDeMora = 132;
        
        foreach ($responseRealizado as $resp) {

            $mesAno =  \DateTime::createFromFormat('Y-m-d', $resp['DATA_CONCILIADO'])->format('Y-m');
            $valorConciliado = round($resp['VALOR_PARCELA_CONCILIADO'], 2);
            $idParcela = $resp['PARCELA_ID'];
            // transformando em negativo caso seja conta de despesa
            if ($versoaoEstrutura == 2 && $resp['COD_N3'][0] == 2) {
                $valorConciliado = -1 * round($resp['VALOR_PARCELA_CONCILIADO'], 2);
            }
            $planoContasRealizado[$resp['ID_NATUREZA']][$mesAno] += $valorConciliado;
            if (!empty($resp['CLASSIFICACAO_CONVENIO']) && $resp['COD_N3'] == '1.0.1') {

                $classeRealizado[$resp['CLASSIFICACAO_CONVENIO']][$mesAno] += $valorConciliado;
            }
            $dataProcessada = \DateTime::createFromFormat('Y-m-d', $resp['DATA_PROCESSADO']);
            
            // atribuido valores da natureza 2.1.18 distribuição de licro para a natureza pro-labore
            if ($versoaoEstrutura == 2 && $resp['COD_N3'] == '2.1.18') {
                $valor = round($resp['VALOR_PARCELA_CONCILIADO'], 2);
                if(isset($parcelas[$resp['ID_NATUREZA']][$mesAno][$idParcela])){
                $valor = $parcelas[$resp['ID_NATUREZA']][$mesAno][$idParcela]['valor'] + round($resp['VALOR_PARCELA_CONCILIADO'], 2);
                } 
                $auxParcela = [
                    'tr' => $resp['NOTA_ID'],
                    'numero_nota' => $resp['numero_nota'],
                    'fornecedor' => $resp['fornecedor'],
                    'data' => $dataProcessada->format('d/m/Y'),
                    'valor' => $valor,
                    //'parcela' => $idParcela
    
                ];
                $planoContasRealizado[$idProlabore][$mesAno] += $planoContasRealizado[$resp['ID_NATUREZA']][$mesAno];
                $parcelas[$idProlabore][$mesAno][$idParcela] = $auxParcela;
            }else{
                $valor = round($resp['VALOR_PARCELA_CONCILIADO'], 2);
                if(isset($parcelas[$resp['ID_NATUREZA']][$mesAno][$idParcela])){
                $valor = $parcelas[$resp['ID_NATUREZA']][$mesAno][$idParcela]['valor'] + round($resp['VALOR_PARCELA_CONCILIADO'], 2);
                } 
                $auxParcela = [
                    'tr' => $resp['NOTA_ID'],
                    'numero_nota' => $resp['numero_nota'],
                    'fornecedor' => $resp['fornecedor'],
                    'data' => $dataProcessada->format('d/m/Y'),
                    'valor' => $valor,
                    //'parcela' => $idParcela
    
                ];
                $parcelas[$resp['ID_NATUREZA']][$mesAno][$idParcela] = $auxParcela;
            }

            // Atribuido juros de mora ou ativo
            if ($versoaoEstrutura == 2 && $resp['juros'] > 0 ) {
                $valor = round($resp['juros'], 2);
                $valorJuros = $resp['COD_N3'][0] == 2 ? $valor * -1 : $valor;
                $idNatureza =  $resp['COD_N3'][0] == 2 ? $idJurosDeMora : $idJurosAtivo;
                if(isset($parcelas[$idNatureza][$mesAno][$idParcela])){
                $valor = $parcelas[$idNatureza][$mesAno][$idParcela]['valor'] + round($resp['juros'], 2);
                } 
                if($idNatureza == 132 && $mesAno == '2021-05'){
                    $aux[] = $valor;
                }
               
                $auxParcela = [
                    'tr' => $resp['NOTA_ID'],
                    'numero_nota' => $resp['numero_nota'],
                    'fornecedor' => $resp['fornecedor'],
                    'data' => $dataProcessada->format('d/m/Y'),
                    'valor' => $valor
    
                ];
                $planoContasRealizado[$idNatureza][$mesAno] += $valorJuros;
                $parcelas[$idNatureza][$mesAno][$idParcela] = $auxParcela;
            }
            //Encargos financeiro
            if ($versoaoEstrutura == 2 && $resp['encargo'] > 0 && $resp['COD_N3'][0] == 2) {
                $valor = round($resp['encargo'], 2);
                //$idNatureza =  $resp['COD_N3'][0] == 2 ? $idJurosDeMora : $idJurosAtivo;
                if(isset($parcelas[$idJurosEmprestimoFinanciameto][$mesAno][$idParcela])){
                $valor = $parcelas[$idJurosEmprestimoFinanciameto][$mesAno][$idParcela]['valor'] + round($resp['encargo'], 2);
                } 
                $auxParcela = [
                    'tr' => $resp['NOTA_ID'],
                    'numero_nota' => $resp['numero_nota'],
                    'fornecedor' => $resp['fornecedor'],
                    'data' => $dataProcessada->format('d/m/Y'),
                    'valor' => $valor,
                    //'parcela' => $idParcela
    
                ];
                $planoContasRealizado[$idJurosEmprestimoFinanciameto][$mesAno] += (-1 * round($resp['encargo'], 2));
                $parcelas[$idJurosEmprestimoFinanciameto][$mesAno][$idParcela] = $auxParcela;
            }
        }
// echo '<pre>';
//         print_r($aux);
//         print_r($planoContasRealizado[132]);
//         die();

        return ['planoConatasRealizado' => $planoContasRealizado, 'classeRealizado' => $classeRealizado, 'parcelas' => $parcelas];
    }


    public static function relatorioModeloGerencialOrcamentoById($idOrcamento)
    {

        //Recuperando cabeçalho do orçamento finaceiro
        $responseVersaoOrcamento  =  current(ModelOrcamentoFinanceiro::getVersaoOrcamentoById($idOrcamento));
        $versoaoEstrutura = $responseVersaoOrcamento['orcamento_financeiro_versao_estrutura'];
        $inicioOrcamento = $responseVersaoOrcamento['inicio'];
        $fimOrcamento = $responseVersaoOrcamento['fim'];
        //Pegar meses e anos que o orçamento contempla, espera data no formato Y-m função retorna todos os meses entre o periodo
        $responseDatas = DateHelper::getListaMesAno($inicioOrcamento, $fimOrcamento);
        // Formatando meses para ser enviado para o front
        $arrayMonths = self::formatandoMesesParaEnvio($responseDatas);
        //Recuperando Itens do orçamento financeiro             
        $items = ModelOrcamentoFinanceiro::getOrcamentoAndValorGerencial($_GET['id'], $versoaoEstrutura);



        // retorna o array onde a chave é o pai e valores são seus filhos e netos
        //$paiFilhos = self::getFilhos($items);  


        // setando valor realizado nos pais 
        //$itemsWithRealizado = self::setRealizadoPais($items, $paiFilhos);
       // $itensAHCalc = self::_ahCalc($items);

        $itensGroupMonth = self::_groupItensMonth($items, $versoaoEstrutura);
        $itensGroupMonth = self::_ahCalcAnterior($itensGroupMonth);


        // $itensSum = self::_sumItensSon($itensGroupMonth);
        $itensRemoveKey = self::_removeKeyMounth($itensGroupMonth);
        $itensGroupFather = self::_groupItensFather($itensRemoveKey);
        $tipoavaliacao = $versoaoEstrutura == 1 ? 'AH' : 'AV';



        $response = [
            'items' => array_values($itensGroupFather),
            'months' => $arrayMonths,
            'id_budget' => $idOrcamento,
            'type' =>  $tipo == 'editar' ? 'edit' : 'view',
            'begin_date' => $inicioOrcamento,
            'end_date' => $fimOrcamento,
            'chart_of_accounts_add' => '',
            'units' => [],
            'unitId' => [],
            'availityType' => $tipoavaliacao
        ];




        echo json_encode($response);
        return;
    }




    public static function relatorioOrcamentoById($idOrcamento, $unidadeId)
    {
        $unidades = Filial::getUnidadesUsuario($_SESSION['empresa_user']);
        $unidadesArray = [];
        $i = 0;
        foreach ($unidades as $u) {

            $unidadesArray[$i]['id'] = $u['id'];
            $unidadesArray[$i]['name'] = $u['nome'];
            $i++;
        }



        $idOrcamento = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
        //Recuperando cabeçalho do orçamento finaceiro
        $responseVersaoOrcamento  =  current(ModelOrcamentoFinanceiro::getVersaoOrcamentoById($idOrcamento));
        $versoaoEstrutura = $responseVersaoOrcamento['orcamento_financeiro_versao_estrutura'];
        $inicioOrcamento = $responseVersaoOrcamento['inicio'];
        $fimOrcamento = $responseVersaoOrcamento['fim'];
        //Pegar meses e anos que o orçamento contempla, espera data no formato Y-m função retorna todos os meses entre o periodo
        $responseDatas = DateHelper::getListaMesAno($inicioOrcamento, $fimOrcamento);
        // Formatando meses para ser enviado para o front
        $arrayMonths = self::formatandoMesesParaEnvio($responseDatas);
        //Recuperando Itens do orçamento financeiro             
        $items = ModelOrcamentoFinanceiro::getOrcamentoFinanceiro($_GET['id'], $versoaoEstrutura);

        //Recuperando valores realizados dos itens
        $valoresRealizados = self::getRealizadoByData($inicioOrcamento, $fimOrcamento, $unidadeId, $versoaoEstrutura);

        //pegando as parcelas que somadas vão dar o valor realizado de cada item
        $parcelas = $valoresRealizados['parcelas'];

        //Setando valores realizados dos planos de custo
        $itemsWithRealizado = self::_setPlanoDeContaValorRealizado($items, $valoresRealizados['planoConatasRealizado'], $versoaoEstrutura, $parcelas);


        //Setando valores realizados dnos topicos tipo classe A,B, C, D de recebimento. 
        $itemsWithRealizado = self::_setTopicoClasse($itemsWithRealizado, $valoresRealizados['classeRealizado']);


        // retorna o array onde a chave é o pai e valores são seus filhos e netos
        $paiFilhos = self::getFilhos($itemsWithRealizado);


        // setando valor realizado nos pais 
        $itemsWithRealizado = self::setRealizadoPais($itemsWithRealizado, $paiFilhos);


        if ($versoaoEstrutura == 2) {
            // setando valor realizado nos pais 
            $itemsWithRealizado = self::setRealizadoTopicosDRE($itemsWithRealizado);
            //$itensAHCalc = self::_ahCalc($itensAHCalc);
        } else {
           // $itensAHCalc = self::_ahCalc($itemsWithRealizado);
        }

        $itensGroupMonth = self::_groupItensMonth($itemsWithRealizado, $versoaoEstrutura);
        $itensGroupMonth = self::_ahCalcAnterior($itensGroupMonth);


        // $itensSum = self::_sumItensSon($itensGroupMonth);
        $itensRemoveKey = self::_removeKeyMounth($itensGroupMonth);
        $itensGroupFather = self::_groupItensFather($itensRemoveKey);
        $tipoavaliacao = $versoaoEstrutura == 1 ? 'AH' : 'AV';



        $response = [
            'items' => array_values($itensGroupFather),
            'months' => $arrayMonths,
            'id_budget' => $idOrcamento,
            'type' =>  $tipo == 'editar' ? 'edit' : 'view',
            'begin_date' => $inicioOrcamento,
            'end_date' => $fimOrcamento,
            'chart_of_accounts_add' => '',
            'units' => array_values($unidadesArray),
            'unitId' => $unidadeId,
            'availityType' => $tipoavaliacao
        ];




        echo json_encode($response);
        return;
    }

    private function setRealizadoPais($itens, $filhos)
    {



        foreach ($filhos as $key => $filho) {


            $filhos[$key]['realizado'] =   array_reduce($filho, function ($aux, $f) use ($itens, $key) {


                foreach ($itens as $i) {
                    if ($i['id_topico'] == $f) {
                        $aux[$i['data']] += $i['realizado'];
                    }
                }



                return $aux;
            });
        }




        $auxItens = [];
        $topicosSum = [];
        foreach ($itens as $key => $i) {
            $auxItens[$key] = $i;
            $recebimento = !empty($filhos[1]['realizado'][$i['data']]) ? $filhos[1]['realizado'][$i['data']] : 0;
            $imposto =  !empty($filhos[4]['realizado'][$i['data']]) ? $filhos[4]['realizado'][$i['data']] : 0;
            $fornecedores =  !empty($filhos[7]['realizado'][$i['data']]) ? $filhos[7]['realizado'][$i['data']] : 0;
            $despesasOperacionais =  !empty($filhos[10]['realizado'][$i['data']]) ? $filhos[10]['realizado'][$i['data']] : 0;
            $despesasGeraisEAdm =  !empty($filhos[11]['realizado'][$i['data']]) ? $filhos[11]['realizado'][$i['data']] : 0;
            $vendasAtivos =  !empty($filhos[23]['realizado'][$i['data']]) ? $filhos[23]['realizado'][$i['data']] : 0;

            $CAPEX =  !empty($filhos[24]['realizado'][$i['data']]) ? $filhos[24]['realizado'][$i['data']] : 0;
            $captacaoEmprestimo =  !empty($filhos[26]['realizado'][$i['data']]) ? $filhos[26]['realizado'][$i['data']] : 0;
            $jurosEmprestimo =  !empty($filhos[27]['realizado'][$i['data']]) ? $filhos[27]['realizado'][$i['data']] : 0;
            $parcelamentoEmprestimo =  !empty($filhos[28]['realizado'][$i['data']]) ? $filhos[28]['realizado'][$i['data']] : 0;
            $parcelemantoTributario =  !empty($filhos[29]['realizado'][$i['data']]) ? $filhos[29]['realizado'][$i['data']] : 0;



            $fluxoCaixaOperacional = $recebimento - ($imposto + $fornecedores + $despesasOperacionais + $despesasGeraisEAdm);

            $fluxoCaixaInvestimento = $fluxoCaixaOperacional + $vendasAtivos - $CAPEX;

            $fluxoCaixaEndividamento = $fluxoCaixaInvestimento + $captacaoEmprestimo - ($jurosEmprestimo + $parcelamentoEmprestimo + $parcelemantoTributario);

            if ($i['id_topico'] == 22) {
                $auxItens[$key]['realizado'] = $fluxoCaixaOperacional;
            } elseif ($i['id_topico'] == 25) {
                $auxItens[$key]['realizado'] = $fluxoCaixaInvestimento;
            } elseif ($i['id_topico'] == 30) {
                $auxItens[$key]['realizado'] = $fluxoCaixaEndividamento;
            } elseif ($filhos[$i['id_topico']]['realizado'][$i['data']]) {
                $auxItens[$key]['realizado'] = $filhos[$i['id_topico']]['realizado'][$i['data']];
            }
        }



        return $auxItens;
    }

    private function setRealizadoTopicosDRE($itens)
    {


        $auxItens = [];
        $topicosSum = [];

        foreach ($itens as $key => $i) {
            if ($i['id_plano_de_conta'] == 0) {
                $auxItens[$i['id_topico']][$i['data']]['realizado'] = $i['realizado'];
                $auxItens[$i['id_topico']][$i['data']]['av']   = '';
            }
        }

        // pegando valores de Receita liquida topico 42 = Receitas Servicao 38 + Deducoes Vendas 39
        foreach ($auxItens[42] as $key => $valor) {
            $auxItens[42][$key]['realizado'] = $auxItens[38][$key]['realizado'] + $auxItens[39][$key]['realizado'];
        }


        // pegando valores de Custos dos servicos 43 = Custos variaveis 44 + custo fixo 50
        foreach ($auxItens[43] as $key => $valor) {
            $auxItens[43][$key]['realizado'] = $auxItens[44][$key]['realizado'] + $auxItens[50][$key]['realizado'];
        }

        // pegando valores de MArgem Contribuicao 49 = Receita liquida 42 + Custos variaveis 44
        foreach ($auxItens[49] as $key => $valor) {
            $auxItens[49][$key]['realizado'] = $auxItens[42][$key]['realizado'] + $auxItens[44][$key]['realizado'];
            $auxItens[49][$key]['av'] =  round($auxItens[49][$key]['realizado'] /  $auxItens[42][$key]['realizado'], 2);
        }

        // Lucro operacional bruto 53 =Receita Liquida 42 + ( Custo fixo 50 + Custos variaveis 44 )
        foreach ($auxItens[53] as $key => $valor) {
            $auxItens[53][$key]['realizado'] = $auxItens[42][$key]['realizado'] + ($auxItens[50][$key]['realizado']  + $auxItens[44][$key]['realizado']);
            $auxItens[53][$key]['av'] =  round($auxItens[53][$key]['realizado'] /  $auxItens[42][$key]['realizado'], 2);
        }

        // Despesa operacionais = Venda Marketing + Gerais e administrativa + Depreciação e Amortização

        // Ebitida 70 = Receita Liquida 42 + (Custo dos Serviços 43 + 54 Despesa Operacionais)
        foreach ($auxItens[70] as $key => $valor) {
            $auxItens[70][$key]['realizado'] = $auxItens[42][$key]['realizado'] + ($auxItens[43][$key]['realizado']  + $auxItens[54][$key]['realizado']);
            $auxItens[70][$key]['av'] =  round($auxItens[70][$key]['realizado'] /  $auxItens[42][$key]['realizado'], 2);
        }

        // Res. antes do IRPJ e CSLL 73 = EBTIDA 70 + Resultado Financeiro 71 + Outras Receitas 72
        foreach ($auxItens[73] as $key => $valor) {
            $auxItens[73][$key]['realizado'] = $auxItens[70][$key]['realizado']  + $auxItens[71][$key]['realizado']   + $auxItens[72][$key]['realizado'];
        }

        // Lucro Liquido 75   =  Res. antes do IRPJ e CSLL 73 + IRPJ e CSLL 74
        foreach ($auxItens[75] as $key => $valor) {


            $auxItens[75][$key]['realizado'] = $auxItens[73][$key]['realizado'] + $auxItens[74][$key]['realizado'];
            $auxItens[75][$key]['av'] =  round($auxItens[75][$key]['realizado'] /  $auxItens[42][$key]['realizado'], 2);
        }

        foreach ($auxItens[76] as $key => $valor) {

            $mesAtualOject =  \DateTime::createFromFormat('Y-m-d', $key . '-01');
            $mesAtual = $mesAtualOject->format('Y-m');

            $interval =  new \DateInterval('P1M');
            $mesAnteriorObjec = $mesAtualOject->sub($interval);
            $mesAnterior = $mesAnteriorObjec->format('Y-m');


            $auxItens[76][$key]['realizado'] = $auxItens[75][$key]['realizado'] + $auxItens[76][$mesAnterior]['realizado'];
        }



        $aux = [];

        foreach ($itens as $key => $i) {

            $aux[$key] = $i;

            if ($i['id_topico'] == 70) {
                $aux[$key]['name'] = "Sub Total";
            } else if ($i['id_topico'] == 47) {
                $aux[$key]['name'] = "Aquisição de Insumos";
            }


            if (isset($auxItens[$i['id_topico']][$i['data']])) {
                $aux[$key]['realizado']  = $auxItens[$i['id_topico']][$i['data']]['realizado'];
                $aux[$key]['AV']  = $auxItens[$i['id_topico']][$i['data']]['av'] * 100;
                $aux[$key]['AV_color'] = 'black';
            }
        }


        // echo "<pre>";
        // print_r($aux);
        // die();


        return $aux;
    }

    private function filhos($itemArray, $arrayPais)
    {
        $auxArray = [];

        return   $arrayChildren =  array_reduce($itemArray, function ($aux, $item) use ($arrayPais) {

            if (array_key_exists($item, $arrayPais)) {



                foreach ($arrayPais[$item] as $key => $father) {
                    $aux[] = $father;

                    if (array_key_exists($father, $arrayPais)) {

                        $ArrayNeto = [];
                        $ArrayNeto[$father] = $father;

                        $aux = array_merge(self::filhos($ArrayNeto, $arrayPais), $aux);
                    }
                }
            }



            return $aux;
        });
    }

    private function getFIlhos($itemRealizado)
    {
        $arrayPais =   array_reduce($itemRealizado, function ($aux, $item) {
            if ($item['id_pai']) {
                $aux[$item['id_pai']][$item['id_topico']] = $item['id_topico'];
                return $aux;
            }
        });





        $aux = $arrayPais;

        foreach ($arrayPais as $key => $pai) {


            $auxfilho = self::filhos($pai, $arrayPais);

            if (!empty($auxfilho)) {
                $aux[$key] = array_merge($arrayPais[$key], $auxfilho);
            }
        }




        return $aux;
    }

    public function _setPlanoDeContaValorRealizado($items, $planoContasRealizado, $versoaoEstrutura, $parcelas)
    {

        $response = [];
        // modelo tradicional
        if ($versoaoEstrutura == 1) {
            foreach ($items as $item) {
                if (array_key_exists($item['id_plano_de_conta'], $planoContasRealizado)) {
                    

                    $item['realizado'] = $planoContasRealizado[$item['id_plano_de_conta']][$item['data']];
                    $item['parcelas'] = $parcelas[$item['id_plano_de_conta']][$item['data']];
                    
                }
                array_push($response, $item);
            }
        } else {
            //modelo DRE
            $topicos[2] = [1, 2, 3, 4, 277]; // HOMECARE.            
            $topicos[77] = [279]; //teleconsulta
            $topicos[79] = [283]; // topicoServicoLogistico
            $topicos[80] = [284]; // APH
            $topicos[82] = [150]; // PIS
            $topicos[83] = [151]; //COFINS
            $topicos[84] = [149]; // ISS
            $topicos[85] = [167]; //DEVCancelamento
            $topicos[86] = [19]; // GLOSA
            $topicos[87] = [5, 6, 7, 8, 9]; // RECEITAFianceira
            $topicos[88] = [132, 133, 204, 134, 205, 274, 135]; // despFinanceira
            $topicos[89] = [10]; // VENDa IMOBILIZADO
            $topicos[90] = [11]; // RECUPERACAO de CREDITO
            $topicos[91] = [12]; //RECEITA ALUGUEL
            $topicos[96] = [152]; //IRPJ
            $topicos[97] = [153]; // CSLL
            $topicos[98] = [16]; // Reembolso Plano de Saude
            foreach ($items as $item) {
                if (array_key_exists($item['id_plano_de_conta'], $planoContasRealizado)) {
                    
                    $item['realizado'] = $planoContasRealizado[$item['id_plano_de_conta']][$item['data']];
                    $item['parcelas'] = array_values($parcelas[$item['id_plano_de_conta']][$item['data']]);
                } /* 16-06-2021 elseif (array_key_exists($item['id_topico'], $topicos)) {
                    $realizado = 0;
                    $auxParcela = [];
                    foreach ($topicos[$item['id_topico']] as $t) {
                        $realizado += $planoContasRealizado[$t][$item['data']];

                        if (!empty($parcelas[$t][$item['data']]))
                            $auxParcela = array_merge($auxParcela, $parcelas[$t][$item['data']]);
                    }
                    $item['realizado'] = $realizado;
                    $item['parcelas'] = $auxParcela;
                }*/
                array_push($response, $item);
            }
        }
        return $response;
    }



    public function _setTopicoClasse($items, $classeConvenioRealizado)
    {
        //criando array associadno id do topico ao tipo da classe para poder utilizar na rotina do relatório
        $classeTopico = [33 => 'A', 34 => 'B', 35 => 'C', 36 => 'D'];
        $response = [];

        foreach ($items as $item) {

            if (array_key_exists($item['id_topico'], $classeTopico)) {

                $item['realizado'] = $classeConvenioRealizado[$classeTopico[$item['id_topico']]][$item['data']];
            }
            array_push($response, $item);
        }
        return $response;
    }

    public function _ahCalc($items)
    {
        foreach ($items as $key => $item) {
            if ($item['value']) {
                $items[$key]['AH'] = round(($item['realizado'] / $item['value']) * 100 - 100, 2);
                $items[$key]['AH_color'] = 'black';
            }
        }

        return array_values($items);
    }

    public function _ahCalcAnterior($items)
    {   
        
        foreach ($items as $key => $item) {
            foreach($item['months'] as $monthKey => $month){
                $dataAtual = DateTime::createFromFormat('Y-m-d', $monthKey.'-10');
                if($dataAtual->format('m') != '01'){
                    $interval = new DateInterval('P1M');
                    $dataMesAnterior = $dataAtual->sub($interval)->format('Y-m');
                    $valorRealizadoAnterior = $items[$key]['months'][$dataMesAnterior]['realizado'];
                    if ($valorRealizadoAnterior) {
                        $items[$key]['months'][$monthKey]['AH'] = round(($month['realizado'] / $valorRealizadoAnterior) * 100 - 100, 2);
                        $items[$key]['months'][$monthKey]['AH_color'] = 'black';
                    }


                }               
                
            }
            
        }

        return $items;
    }

    public function _groupItensMonth($items, $versoaoEstrutura)
    {
        $response = [];
        $positivoTopicos = [1, 2, 3, 23, 26];

        foreach ($items as $item) {
            $valorRealizado = (float) $item['realizado'];
            if (in_array($item['id_topico'], [22, 25, 30])) {

                $colorRealizado = $valorRealizado < 0 ? 'red' : 'blue';
                $color = (float) $item['value'] < 0 ? 'red' : 'blue';
                $colorTopico = '';
                $colorTopicoRealizado = '';
            } elseif (in_array($item['id_topico'], $positivoTopicos)) {
                $color = 'blue';
                $colorRealizado = $color;
                $colorTopico = $color;
                $colorTopicoRealizado = $color;
            } else {
                $color = in_array($item['id_pai'], $positivoTopicos) ? 'blue' : 'red';
                $colorTopico = $color;
                $colorRealizado = $color;
                $colorTopicoRealizado = $color;
            }
            if (!array_key_exists($item['id_topico'],  $response)) {
                $response[$item['id_topico']]['id_topico'] = $item['id_topico'];
                $response[$item['id_topico']]['ordem'] = $item['ordem'];
                $response[$item['id_topico']]['id_pai'] = $item['id_pai'];
                $response[$item['id_topico']]['name'] = $item['name'];
                $response[$item['id_topico']]['percentage'] = (float) $item['aliquota'];
                $response[$item['id_topico']]['color'] = $colorTopico;
                $response[$item['id_topico']]['color_realized'] = $colorTopicoRealizado;
                $response[$item['id_topico']]['type_calc'] = (float) $item['aliquota'] == 0 ? "VALUE" : "PERCENTAGE";;
                //$response[$item['id_topico']]['id_plano_de_conta'] = $item['id_plano_de_conta'];
                $response[$item['id_topico']]['months'] = [];
                $response[$item['id_topico']]['sub'] = [];
            }
            $mesAno =  \DateTime::createFromFormat('Y-m-d', $item['data'] . '-01');
            // topicos que a cor depende do somatorio de outros itens.

            if ($versoaoEstrutura == 2) {
                $colorRealizado = $valorRealizado < 0 ? 'red' :  'blue';
                $color = $item['value'] < 0 ? 'red' :   'blue';
                if ($item['id_topico'] == 42) {
                    $totalRealizadoReceitaLiquida += $valorRealizado;
                    $totalOrcadoReceitaLiquida += (float) $item['value'];
                }

                if (in_array($item['id_topico'], [75, 70, 53, 49])) {
                    $response[$item['id_topico']]['total']['realizado'] += round($valorRealizado, 2);
                    $response[$item['id_topico']]['total']['value'] += (float) $item['value'];
                    $response[$item['id_topico']]['total']['AV'] = round(((float) $response[$item['id_topico']]['total']['realizado'] / (float) $totalRealizadoReceitaLiquida), 2) * 100;

                    // echo "<pre>";
                    // print_r($response[$item['id_topico']]['total']);
                    // print_r($totalRealizadoReceitaLiquida);
                    // print_r($response[$item['id_topico']]['total']['realizado'] / $totalReceitaLiquida);
                } else {
                    $response[$item['id_topico']]['total']['realizado'] += round($valorRealizado, 2);
                    $response[$item['id_topico']]['total']['value'] += (float) $item['value'];
                    $response[$item['id_topico']]['total']['AV'] = '';
                }
            }

            $colorBackGround = 'green';
            $yellowBackground = [42, 49, 53, 70, 71, 75, 76];

            if ($versoaoEstrutura != 2) {
                if ($item['id_topico'] == 1) {
                    $colorBackGround = 'blue';
                }
            } else {
                if ($item['id_topico'] == 38) {
                    $colorBackGround = 'blue';
                } elseif (in_array($item['id_topico'], $yellowBackground)) {

                    $colorBackGround = 'yellow';
                }
            }



            $response[$item['id_topico']]['color_background'] = $colorBackGround;


            $months = [
                'value' => (float) $item['value'],
                'showCell' => false,
                'id' => $item['id'],
                'topico_id' => $item['id_topico'],
                'table' => $item['tabela'],
                'month' => $mesAno->format('m'),
                'year' => $mesAno->format('Y'),
                'data' => $mesAno->format('Y-m'),
                'realizado' => round($valorRealizado, 2),
                'AH' => (float) $item['AH'],
                'AH_color' => $item['AH_color'],
                'AV' => (float) $item['AV'],
                'AV_color' => $item['AV_color'],
                'color' => $color,
                'color_realized' => $colorRealizado,
                'parcelas' => $item['parcelas']
            ];
            //array_push($response[$item['id_topico']]['months'],$months);
            $response[$item['id_topico']]['months'][$months['data']] = $months;
        }
        return $response;
    }
    //TODO somar

    public function _sumItensSon($items)
    {
        $teste = self::_groupItensFather($items);

        foreach ($items as $item) {
            if (array_key_exists($item['id_pai'], $items)) {
                foreach ($item['months'] as $mounth) {
                    $items[$item['id_pai']]['months'][$mounth['data']]['realizado'] += $mounth['realizado'];
                    $items[$item['id_pai']]['months'][$mounth['data']]['AH'] = ($items[$item['id_pai']]['months'][$mounth['data']]['realizado'] / $items[$item['id_pai']]['months'][$mounth['data']]['value']) * 100;
                    $items[$item['id_pai']]['months'][$mounth['data']]['AH'] ? $items[$item['id_pai']]['months'][$mounth['data']]['AH'] -= 100 : 0;
                }
            }
        }

        return $items;
    }

    public function _removeKeyMounth($items)
    {
        //TODO: implementar
        foreach ($items as $key => $item) {
            $items[$key]['months'] = array_values($items[$key]['months']);
        }

        return $items;
    }

    public function _groupItensFather($items)
    {
        $items = self::arrayToObject($items);
        $childs = array();
        /*
        CASO USUÁRIO TENHA RESTRIÇÃO DE NATUREZA NÃO DEVE VISUALIZAR OS FILHOS DESSES TOPICOS
            14 - PRO LABORE
            26 - Captação Empréstimos
            27 - Juros Empréstimo
            28 - Parcela Empréstimo
            financeiro_orcamento_financeiro_relatorio_restricao_natureza
        */
        $notViewChilds =  [];
        if ($_SESSION['adm_user'] != 1 && isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']['financeiro_orcamento_financeiro_relatorio_restricao_natureza'])) {
            $notViewChilds =   [14, 26, 27, 28];
        }


        foreach ($items as $item) {
            if (!in_array($item->id_pai, $notViewChilds))
                $childs[$item->id_pai][] = $item;
        }

        foreach ($items as $item) {
            isset($childs[$item->id_topico]) ? $item->sub = $childs[$item->id_topico] : '';
        }
        $tree = $childs[0];

        return $tree;
    }

    public static function arrayToObject($array)
    {
        $json = json_encode($array);
        $object = json_decode($json);
        return $object;
    }

    public static function formatandoMesesParaEnvio($responseDatas)
    {

        foreach ($responseDatas as $resp) {
            $mesAno =  \DateTime::createFromFormat('Y-m-d', $resp . '-01');
            $arrayMonths[] = [
                'month' => $mesAno->format('m'),
                'year' => $mesAno->format('Y'),
                'date' => $mesAno->format('Y-m')
            ];
        }
        return $arrayMonths;
    }

    public static function valoresGerenciaisByOrcamentoById()
    {


        if ((isset($_GET['id']) && !empty($_GET['id']))) {


            if ($_SESSION['adm_user'] <> 1) {

                if (!isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']['financeiro_orcamento_financeiro_editar'])) {
                    header('Status: 400');
                    echo json_encode('Permissao negadas');
                    return;
                }

                if (!isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']['financeiro_orcamento_financeiro_visualizar'])) {
                    header('Status: 400');
                    echo json_encode('Permissão negadas');
                    return;
                }
            }


            $idOrcamento = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
            $responseVersaoOrcamento  =  current(ModelOrcamentoFinanceiro::getVersaoOrcamentoById($idOrcamento));
            $versoaoEstrutura = $responseVersaoOrcamento['orcamento_financeiro_versao_estrutura'];
            $inicioOrcamento = $responseVersaoOrcamento['inicio'];
            $fimOrcamento = $responseVersaoOrcamento['fim'];

            $ok = self::criandoValorGerencialSeNaoExistir($idOrcamento);


            // passa data no formato Y-m função retorna todos os meses entre o periodo
            $responseDatas = DateHelper::getListaMesAno($inicioOrcamento, $fimOrcamento);
            $responseTopicosPais = ModelOrcamentoFinanceiro::getTopicosByIdPaiEVersao(0, $versoaoEstrutura);
            $responseEstruturaTopicos = ModelOrcamentoFinanceiro::getEstruturaByVersao($versoaoEstrutura);
            $responseTopicos = ModelOrcamentoFinanceiro::getTopicos();
            $responseValoresTopicos = ModelOrcamentoFinanceiro::getValoresGerenciaisTopicosByVersao($idOrcamento);
            $responseValoresPlanoContas = ModelOrcamentoFinanceiro::getValoresGerenciaisPlanoContasByVersao($idOrcamento);



            $valoresTopicos = [];
            $valoresPlanoContas = [];
            $auxPlanoContas = 0;
            if ($versoaoEstrutura != 2) {
                $planoContasAdicionados = [];
            } else {

                //modelo DRE
                /*16-06-2121
                $planoContasAdicionados  = [
                    '1',
                    '3',
                    '2',
                    '4',
                    '277',
                    '279',
                    '283',
                    '284',
                    '150',
                    '151',
                    '149',
                    '167',
                    '19',
                    '5',
                    '6',
                    '7',
                    '8',
                    '9',
                    '132',
                    '133',
                    '204',
                    '134',
                    '205',
                    '274',
                    '135',
                    '10',
                    '11',
                    '12',
                    '152',
                    '153',
                    '16',
                    '163',
                    '169',
                    '170',
                    '171',
                    '172',
                    '173',
                    '175',
                    '174',
                    '176',
                    '177',
                    '178',
                    '179',
                    '180',
                    '181',
                    '182',
                    '183',
                    '184',
                    '185',
                    '186',
                    '187',
                    '188',
                    '189',
                    '190',
                    '191',
                    '224'
                ];
                */
            }
            $porcentagemTopico = [];
            $porcentagemPlanoContas = [];




            if (!empty($responseValoresPlanoContas)) {
                $responseNaturezas =  IPCF::getAll();
                $naturezas = [];

                foreach ($responseNaturezas as $resp) {
                    $naturezas[$resp['ID']] =  $resp['COD_N4'] . ' ' . $resp['N4'];
                }
                $auxPlanoContas = 10000;
                foreach ($responseValoresPlanoContas as $resp) {

                    $topicoId = $resp['orcamento_financeiro_topicos_id'];
                    $planoContasId = $resp['plano_contas_id'];
                    $planoContasAdicionados[$auxPlanoContas] = $planoContasId;
                    $auxPlanoContas++;
                    $mesAno =  \DateTime::createFromFormat('Y-m', $resp['data']);

                    $paiPlanoConta[$topicoId][$planoContasId]['id_topico'] = $topicoId;
                    $paiPlanoConta[$topicoId][$planoContasId]['id_plano_contas'] = $planoContasId;
                    $paiPlanoConta[$topicoId][$planoContasId]['name'] = $naturezas[$planoContasId];
                    $paiPlanoConta[$topicoId][$planoContasId]['percentage'] = (float) $resp['aliquota'];
                    $paiPlanoConta[$topicoId][$planoContasId]['type_calc'] = (float) $resp['aliquota'] == 0 ? "VALUE" : "PERCENTAGE";
                    $paiPlanoConta[$topicoId][$planoContasId]['isAddAccountPlan'] = true;
                    $paiPlanoConta[$topicoId][$planoContasId]['sub'] = [];



                    $paiPlanoConta[$topicoId][$planoContasId]['months'][] = [
                        'value' => $resp['valor_gerencial'],
                        'showCell' => false,
                        'id' => $resp['id_valor_gerencial'],
                        'topico_id' => $topicoId,
                        'table' => 'orcamento_financeiro_plano_contas_valores_gerencial',
                        'month' => $mesAno->format('m'),
                        'year' => $mesAno->format('Y'),
                        'data' => $mesAno->format('Y-m')
                    ];
                }
            }


            foreach ($responseTopicos as $t) {
                $topicos[$t['id']] = $t['descricao'];
            }

            $auxValores = 0;
            foreach ($responseValoresTopicos as $vt) {
               

                $mesAno =  \DateTime::createFromFormat('Y-m', $vt['data']);
                $topicoId = $vt['orcamento_financeiro_topicos'];
                $porcentagemTopico[$topicoId] = $vt['aliquota'];
                $valoresTopicos[$topicoId][$auxValores]['value'] = $vt['valor_gerencial'];
                $valoresTopicos[$topicoId][$auxValores]['showCell'] = false;
                $valoresTopicos[$topicoId][$auxValores]['id'] = $vt['id_valor_gerencial'];
                $valoresTopicos[$topicoId][$auxValores]['topico_id'] = $topicoId;
                $valoresTopicos[$topicoId][$auxValores]['table'] = 'orcamento_financeiro_topicos_valores_gerencial';
                $valoresTopicos[$topicoId][$auxValores]['month'] = $mesAno->format('m');
                $valoresTopicos[$topicoId][$auxValores]['year'] = $mesAno->format('Y');
                $valoresTopicos[$topicoId][$auxValores]['data'] = $mesAno->format('Y-m');
                $auxValores++;
            }
            

            $arrayTopico = [];
            $aux01 = 0;


            foreach ($responseTopicosPais as $topico) {
                $topicoId = $topico['orcamento_finananceiro_topicos_id'];
                $data[$aux01]['id_topico'] = $topicoId;
                $data[$aux01]['name'] = $topicos[$topicoId];
                $data[$aux01]['percentage'] = (float) $porcentagemTopico[$topicoId];
                $data[$aux01]['type_calc'] = (float) $porcentagemTopico[$topicoId] == 0 ? "VALUE" : "PERCENTAGE";
                $data[$aux01]['months'] = array_values($valoresTopicos[$topicoId]);
                $colorBackGround = 'green';
                $yellowBackground = [42, 49, 53, 70, 71, 75, 76];
                if ($versoaoEstrutura != 2) {
                    if ($topicoId == 1) {
                        $colorBackGround = 'blue';
                    }
                } else {
                    if ($topicoId == 38) {
                        $colorBackGround = 'blue';
                    } elseif (in_array($topicoId, $yellowBackground)) {

                        $colorBackGround = 'yellow';
                    }
                }

                $data[$aux01]['color_background'] = $colorBackGround;
                $responseFilhos = ModelOrcamentoFinanceiro::getTopicosByIdPaiEVersao($topicoId, $versoaoEstrutura);

                if (!empty($responseFilhos)) {
                    if (isset($paiPlanoConta[$topicoId]) && !empty($paiPlanoConta[$topicoId])) {
                        $data[$aux01]['sub'] = array_values($paiPlanoConta[$topicoId]);
                        $aux02 = count($paiPlanoConta[$topicoId]);
                    } else {
                        $aux02 = 0;
                    }


                    foreach ($responseFilhos as $resp) {
                        $idFilho = $resp['orcamento_finananceiro_topicos_id'];
                        $data[$aux01]['sub'][$aux02]['id_topico'] = $idFilho;
                        $data[$aux01]['sub'][$aux02]['name'] = $topicos[$idFilho];
                        $data[$aux01]['sub'][$aux02]['percentage'] = (float) $porcentagemTopico[$idFilho];
                        $data[$aux01]['sub'][$aux02]['type_calc'] = (float) $porcentagemTopico[$idFilho] == 0 ? "VALUE" : "PERCENTAGE";
                        $data[$aux01]['sub'][$aux02]['months'] = array_values($valoresTopicos[$idFilho]);

                        $responseNetos = ModelOrcamentoFinanceiro::getTopicosByIdPaiEVersao($idFilho, $versoaoEstrutura);
                        if (!empty($responseNetos)) {
                            if (isset($paiPlanoConta[$idFilho]) && !empty($paiPlanoConta[$idFilho])) {
                                $data[$aux01]['sub'][$aux02]['sub'] = array_values($paiPlanoConta[$idFilho]);
                                $aux03 = count($paiPlanoConta[$idFilho]);
                            } else {
                                $aux03 = 0;
                            }
                            foreach ($responseNetos as $respNeto) {
                                $idNeto = $respNeto['orcamento_finananceiro_topicos_id'];
                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['id_topico'] = $idNeto;
                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['name'] = $topicos[$idNeto];
                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['percentage'] = (float) $porcentagemTopico[$idNeto];
                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['type_calc'] = (float) $porcentagemTopico[$idNeto] == 0 ? "VALUE" : "PERCENTAGE";

                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['months'] = array_values($valoresTopicos[$idNeto]);
                                $data[$aux01]['sub'][$aux02]['sub'][$aux03]['sub'] = [];
                                if (array_key_exists($idNeto, $paiPlanoConta)) {
                                    $data[$aux01]['sub'][$aux02]['sub'][$aux03]['sub'] = array_values($paiPlanoConta[$idNeto]);
                                }
                                $aux03++;
                            }
                        } else {
                            if (isset($paiPlanoConta[$idFilho]) && !empty($paiPlanoConta[$idFilho])) {
                                $data[$aux01]['sub'][$aux02]['sub'] = array_values($paiPlanoConta[$idFilho]);
                            } else {
                                $data[$aux01]['sub'][$aux02]['sub'] =  [];
                            }
                        }

                        if (array_key_exists($idFilho, $valoresPlanoContas)) {
                            $data[$aux01]['sub'][$aux02]['plano_contas'] = array_values($valoresPlanoContas[$idFilho]);
                        }
                        $aux02++;
                    }
                } else {
                    $data[$aux01]['sub'] = [];
                }
                if (array_key_exists($topicoId, $valoresPlanoContas)) {
                    $data[$aux01]['plano_contas'] = array_values($valoresPlanoContas[$topicoId]);
                }
                $aux01++;
            }

            $arrayMonths = [];

            foreach ($responseDatas as $resp) {
                $mesAno =  \DateTime::createFromFormat('Y-m', $resp);
                $arrayMonths[] = [
                    'month' => $mesAno->format('m'),
                    'year' => $mesAno->format('Y'),
                    'date' => $mesAno->format('Y-m')
                ];
            }


            $response = [
                'items' => array_values($data),
                'months' => array_values($arrayMonths),
                'id_budget' => $idOrcamento,
                'type' =>  $tipo == 'editar' ? 'edit' : 'view',
                'begin_date' => $inicioOrcamento,
                'end_date' => $fimOrcamento,
                'chart_of_accounts_add' => array_values($planoContasAdicionados),
            ];

            echo json_encode($response);
            return;
        }

        http_response_code(500);
        echo json_encode('Erro dados incorretos');
    }

    public static function criandoValorGerencialSeNaoExistir($versao)
    {
        $responseValoresTopicos = ModelOrcamentoFinanceiro::getValoresGerenciaisTopicosByVersao($versao);
        $responseValoresPlanoContas = ModelOrcamentoFinanceiro::getValoresGerenciaisPlanoContasByVersao($versao);

        foreach ($responseValoresTopicos as $top) {


            if ($top['id_valor_gerencial'] == null) {
                $insertTopico[] = "(
                   0,
                   {$top['orcamento_financeiro_topicos']},
                   '{$top['data']}',
                   now(),
                   {$_SESSION['id_user']}
                   )";
            }
        }




        foreach ($responseValoresPlanoContas as $plc) {
            if ($plc['id_valor_gerencial'] == null) {
                $insertPlanoContas[] = "(
                  0,
                  {$plc['plano_contas_id']},
                  '{$plc['data']}',
                  now(),
                  {$_SESSION['id_user']}
              )";
            }
        }

        

        if (count($insertPlanoContas) > 0) {
            $responsePlanoContas = ModelOrcamentoFinanceiro::createPlanoContasValoresGerenciais($insertPlanoContas);
        }

        if (count($insertTopico) > 0) {
            $responseTopicos = ModelOrcamentoFinanceiro::createTopicosValoresGerenciais($insertTopico);
        }

        if ($responseTopicos['tipo'] == 'erro' || $responsePlanoContas['tipo'] == 'erro') {
            return 'error';
        }
        return 'success';
    }

    public static function updateItemValorGerencial()
    {

        if (
            isset($_POST['id']) && !empty($_POST['id']) &&
            isset($_POST['tabela']) && !empty($_POST['tabela'])
        ) {

            $data['valor'] = filter_input(INPUT_POST, 'valor', FILTER_SANITIZE_STRING);
            $data['porcentagem'] = filter_input(INPUT_POST, 'porcentagem', FILTER_SANITIZE_STRING);
            $data['id'] = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $data['acao'] = 'update';
            $tabela = filter_input(INPUT_POST, 'tabela', FILTER_SANITIZE_STRING);

            if ($tabela == 'orcamento_financeiro_topicos_valores_gerencial') {
                $response =  ModelOrcamentoFinanceiro::updateValoresGerencialTopico($data);
            } else {
                $response = ModelOrcamentoFinanceiro::updateValoresGerencialPlanoContas($data);
            }

            if ($response['tipo'] == 'success') {                
                echo json_encode($response['msg']);
            } else {
                http_response_code(500);
                echo json_encode($response['msg']);
            }
            return;
        }
        http_response_code(500);
        echo json_encode('Erro dados incorretos');
    }
}
