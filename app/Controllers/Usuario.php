<?php

namespace App\Controllers;

use App\Helpers\Paginator;
use App\Models\Administracao\ConselhoRegional;
use App\Models\Administracao\Filial;
use App\Models\Administracao\Usuario AS UsuarioModel;
use App\Models\DB\ConnMysqli;
use App\Models\Sistema\Areas;
use App\Models\Sistema\Setores;

class Usuario
{
    public static function form()
    {
        $urs = Filial::getUnidadesAtivas();
        $areas = Areas::getAreas(0);
        $conselhos = ConselhoRegional::getAll();
        $setores = Setores::getAll();

        require ($_SERVER['DOCUMENT_ROOT'] . '/adm/usuario/templates/form.phtml');
    }

    public static function salvarUsuario()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];

            $data['nome'] = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
            $data['login'] = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING);
            $data['senha'] = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);
            $data['conselho_regional'] = filter_input(INPUT_POST, 'conselho_regional', FILTER_SANITIZE_STRING);
            $data['num_registro'] = filter_input(INPUT_POST, 'num_registro', FILTER_SANITIZE_NUMBER_INT);
            $data['empresa_principal'] = filter_input(INPUT_POST, 'empresa_principal', FILTER_SANITIZE_NUMBER_INT);
            $data['ur'] = $_POST['ur'];
            $data['perfil'] = filter_input(INPUT_POST, 'perfil', FILTER_SANITIZE_STRING);
            $data['acesso'] = $_POST['acesso'];
            $data['adm_ur'] = filter_input(INPUT_POST, 'adm_ur', FILTER_SANITIZE_STRING);
            $data['adm_geral'] = filter_input(INPUT_POST, 'adm_geral', FILTER_SANITIZE_NUMBER_INT);
            $data['padronizar'] = filter_input(INPUT_POST, 'padronizar', FILTER_SANITIZE_NUMBER_INT);
            $data['prestador'] = filter_input(INPUT_POST, 'prestador', FILTER_SANITIZE_NUMBER_INT);
            $data['setor'] = $_POST['setor'];
            $data['areas'] = $_POST['areas'];
            $data['permissoes'] = $_POST['permissoes'];

            $response = UsuarioModel::save($data);

            $msg = 'Houve um erro ao registrar o usuário! '.$response;
            $type = 'f';

            if($response === true) {
                $msg = 'Usuário registrado com sucesso!';
                $type = 's';
            }
            $urs = Filial::getUnidadesAtivas();
            $areas = Areas::getAreas(0);
            $conselhos = ConselhoRegional::getAll();
            $setores = Setores::getAll();

            require ($_SERVER['DOCUMENT_ROOT'] . '/adm/usuario/templates/form.phtml');
        }
    }

    public static function formEditar()
    {
        if(isset($_GET) && !empty($_GET)) {
            $id = filter_input(INPUT_GET, 'usuario', FILTER_SANITIZE_NUMBER_INT);
            $usuario = UsuarioModel::getById($id);
            $empresas = UsuarioModel::getEmpresasUsuario($id);
            foreach ($empresas as $empresa) {
                $usuario['empresas'][] = $empresa['empresas_id'];
            }
            $areas = UsuarioModel::getAreasUsuario($id);
            foreach ($areas as $area) {
                $usuario['areas'][] = $area['area_id'];
            }

            $permissoes = UsuarioModel::getPermissoesUsuario($id);
            foreach ($permissoes as $permissao) {
                $usuario['permissoes'][] = $permissao['permissao_id'];
            }
            $setoresUsuario = UsuarioModel::getSetoresUsuario($id);
            foreach ($setoresUsuario as $setor) {
                $usuario['setores'][] = $setor['setor_id'];
            }

            $urs = Filial::getUnidadesAtivas();
            $areas = Areas::getAreas(0);
            $conselhos = ConselhoRegional::getAll();
            $setores = Setores::getAll();

            require($_SERVER['DOCUMENT_ROOT'] . '/adm/usuario/templates/form-editar.phtml');
        }
    }

    public static function editarUsuario()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['id'] = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $data['nome'] = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
            $data['login'] = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING);
            $data['senha'] = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);
            $data['conselho_regional'] = filter_input(INPUT_POST, 'conselho_regional', FILTER_SANITIZE_STRING);
            $data['num_registro'] = filter_input(INPUT_POST, 'num_registro', FILTER_SANITIZE_NUMBER_INT);
            $data['empresa_principal'] = filter_input(INPUT_POST, 'empresa_principal', FILTER_SANITIZE_NUMBER_INT);
            $data['ur'] = $_POST['ur'];
            $data['perfil'] = filter_input(INPUT_POST, 'perfil', FILTER_SANITIZE_STRING);
            $data['acesso'] = $_POST['acesso'];
            $data['adm_ur'] = filter_input(INPUT_POST, 'adm_ur', FILTER_SANITIZE_STRING);
            $data['adm_geral'] = filter_input(INPUT_POST, 'adm_geral', FILTER_SANITIZE_NUMBER_INT);
            $data['padronizar'] = filter_input(INPUT_POST, 'padronizar', FILTER_SANITIZE_NUMBER_INT);
            $data['prestador'] = filter_input(INPUT_POST, 'prestador', FILTER_SANITIZE_NUMBER_INT);
            $data['setor'] = $_POST['setor'];
            $data['areas'] = $_POST['areas'];
            $data['permissoes'] = $_POST['permissoes'];

            $response = UsuarioModel::update($data);

            $msg = 'Houve um erro ao registrar o usuário!';
            $type = 'f';

            if($response === true) {
                $msg = 'Usuário registrado com sucesso!';
                $type = 's';
            }

            header("Location: /adm/usuario/?action=usuario-editar&usuario=" . $data['id'] . "&msg=" . $msg . "&type=" . $type);
        }
    }

    public static function listarForm()
    {
        $conn = ConnMysqli::getConnection();
        $query = UsuarioModel::getUsersListSQL();

        $limit = isset($_GET['limit']) ? $_GET['limit'] : 50;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $links = 5;

        $paginator = new Paginator($conn, $query);
        $orderBy = "ORDER BY nome";
        $response = $paginator->getData($limit, $page, $orderBy);
        $response->empresas = UsuarioModel::getUserEmpresas($response->data);

        $urs = Filial::getUnidadesAtivas();
        $conselhos = ConselhoRegional::getAll();

        require ($_SERVER['DOCUMENT_ROOT'] . '/adm/usuario/templates/listar-form.phtml');
    }

    public static function listarUsuarios()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['nome'] = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
            $data['login'] = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING);
            $data['status'] = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);
            $data['ur'] = isset($_POST['ur']) ? $_POST['ur'] : [];
            $data['perfis'] = isset($_POST['perfis']) ? $_POST['perfis'] : [];
            $data['conselho_regional'] = filter_input(INPUT_POST, 'conselho_regional', FILTER_SANITIZE_STRING);
            $data['padronizar'] = filter_input(INPUT_POST, 'padronizar', FILTER_SANITIZE_NUMBER_INT);
            $data['prestador'] = filter_input(INPUT_POST, 'prestador', FILTER_SANITIZE_STRING);

            $conn = ConnMysqli::getConnection();
            $query = UsuarioModel::getUsersListSQL($data);

            $limit = isset($_GET['limit']) ? $_GET['limit'] : 50;
            $page = isset($_GET['page']) ? $_GET['page'] : 1;
            $links = 5;

            $paginator = new Paginator($conn, $query);
            $orderBy = "ORDER BY nome";
            $response = $paginator->getData($limit, $page, $orderBy);
            $response->empresas = UsuarioModel::getUserEmpresas($response->data);

            $urs = Filial::getUnidadesAtivas();
            $conselhos = ConselhoRegional::getAll();

            require ($_SERVER['DOCUMENT_ROOT'] . '/adm/usuario/templates/listar-form.phtml');
        }
    }

    public static function bloquearUsuario()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data['usuario_id'] = filter_input(INPUT_POST, 'usuario_id', FILTER_SANITIZE_NUMBER_INT);
            $data['data_bloqueio'] = filter_input(INPUT_POST, 'data_bloqueio', FILTER_SANITIZE_STRING);
            $data['motivo_bloqueio'] = filter_input(INPUT_POST, 'motivo_bloqueio', FILTER_SANITIZE_STRING);

            echo $response = UsuarioModel::block($data);
        }
    }

    public static function ativarUsuario()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data['usuario_id'] = filter_input(INPUT_POST, 'usuario_id', FILTER_SANITIZE_NUMBER_INT);

            echo $response = UsuarioModel::activate($data);
        }
    }
}