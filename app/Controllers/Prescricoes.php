<?php
namespace app\Controllers;

use \App\Core\Config;
use \App\Models\Enfermagem\PrescricaoCurativo as Curativo;
use	\App\Models\Enfermagem\PrescricaoEnfermagem;
use	\App\Models\Enfermagem\Feridas;
use	\App\Models\Sistema\Frequencia;
use \App\Controllers\Administracao;
use	\App\Models\Administracao\Usuario;
use	\App\Models\Administracao\Paciente;
use	\App\Core\Log;
use \App\Models\Sistema\CaraterPrescricao;
use \App\Models\Sistema\Solicitacoes;
use \App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;
use \PhpOffice\PhpWord\IOFactory;
use \PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;


class Prescricoes
{
    const DIR = __DIR__;

    public static function listarPorFiltro()
    {
        $paciente   = filter_input(INPUT_GET, 'pacientes', FILTER_SANITIZE_NUMBER_INT);
        $inicio 	= filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
        $inicio		=	\DateTime::createFromFormat('d/m/Y', $inicio)->format('Y-m-d');
        $fim 		= filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
        $fim		=	\DateTime::createFromFormat('d/m/Y', $fim)->format('Y-m-d');

        return Curativo::getAllByFilters($paciente, $inicio, $fim);
    }

    public static function getItensPrescricao($id)
    {
        $itens = PrescricaoEnfermagem::getItensPrescricao($id);
        return $itens;
    }

    public static function getAllLocaisFeridas()
    {
        return Feridas::getLocaisFeridas();
    }

    public static function getAllFrequencias()
    {
        return Frequencia::getFrequenciasPrescricaoCurativo();
    }

    public static function getCarateresPrescricao()
    {
        return CaraterPrescricao::getByProfissional('e');
    }

    public static function getPacienteById($paciente)
    {
        return Paciente::getById($paciente);
    }

    public static function getLastPrescricaoCurativoByPeriodo()
    {
        $paciente = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
        $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
        $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
        return end(Curativo::getAllByFilters($paciente, $inicio, $fim));
    }

    public static function getLastPrescricaoCurativoByPaciente($paciente, $inicio, $fim)
    {
        $response = Curativo::getLastPrescricaoByPacienteIdAndPeriodo($paciente, $inicio, $fim);
        return end($response);
    }

    public static function existsPrescricaoCurativo()
    {
        $response = self::getLastPrescricaoCurativoByPeriodo();
        $count = count($response) > 0;
        if($_SERVER['REQUEST_METHOD'] === 'GET'){
            echo !$response ? '0' : '1';
        } else {
            return $count;
        }
    }

    public static function visualizarPrescricao()
    {
        $locais_ferida 	= self::getAllLocaisFeridas();
        $response 			= [];
        $prescricao 		= filter_input(INPUT_GET, 'prescricao', FILTER_SANITIZE_NUMBER_INT);

        $response['prescricao'] = Curativo::getById($prescricao);
        $response['feridas'] 		= Curativo::getFeridasByPrescricaoId($prescricao);

        $getLocalFerida = function($local) use ($locais_ferida){
            foreach ($locais_ferida as $local_ferida){
                if($local_ferida['ID'] === $local)
                    return $local_ferida['LOCAL'] . '<br>';
            }
        };

        $profissional = Usuario::getById($response['prescricao']['profissional']);
        $compTipo = $profissional['conselho_regional'];

        $response['prescricao']['profissional'] = $profissional['nome'] . $compTipo;

        $carater = CaraterPrescricao::getById($response['prescricao']['carater']);
        $response['prescricao']['carater'] = $carater['nome'];

        foreach($response['feridas'] as $key => $feridas){
            $response['feridas'][$key]['local'] 	= $getLocalFerida($feridas['local']);

            if($response['feridas'][$key]['lado'] == '1')
                $response['feridas'][$key]['lado'] = 'DIREITO';
            elseif($response['feridas'][$key]['lado'] == '2')
                $response['feridas'][$key]['lado'] = 'ESQUERDO';
            else
                $response['feridas'][$key]['lado'] = '';

            $response['cp_itens'][$feridas['id']] = Curativo::getCoberturasByFeridaId($feridas['id'], 'p');
            $response['cs_itens'][$feridas['id']] = Curativo::getCoberturasByFeridaId($feridas['id'], 's');
        }

        return $response;
    }

    public static function imprimirPrescricao($prescricao)
    {
        $locais_ferida = self::getAllLocaisFeridas();
        $response = [];

        $response['prescricao'] = Curativo::getById($prescricao);
        $response['feridas'] = Curativo::getFeridasByPrescricaoId($prescricao);

        $getLocalFerida = function($local) use ($locais_ferida){
            foreach ($locais_ferida as $local_ferida){
                if($local_ferida['ID'] === $local)
                    return $local_ferida['LOCAL'] . '<br>';
            }
        };

        $profissional = Usuario::getById($response['prescricao']['profissional']);
        $compTipo = $profissional['conselho_regional'];

        $response['prescricao']['profissional'] = $profissional['nome'] . $compTipo;
        $response['prescricao']['assinatura'] 	= $profissional['ASSINATURA'];

        $paciente = Paciente::getById($response['prescricao']['paciente']);
        $response['prescricao']['paciente_name']    = $paciente['nome'];
        $response['prescricao']['cuidador'] 	    = $paciente['cuidador'];
        $response['prescricao']['relacao'] 			= $paciente['relac'];

        $caraterPrescricao = CaraterPrescricao::getById($response['prescricao']['carater']);
        $response['prescricao']['carater_nome'] = $caraterPrescricao['nome'];

        foreach($response['feridas'] as $key => $feridas){
            $response['feridas'][$key]['local'] 	= $getLocalFerida($feridas['local']);

            if($response['feridas'][$key]['lado'] == '1')
                $response['feridas'][$key]['lado'] = 'DIREITO';
            elseif($response['feridas'][$key]['lado'] == '2')
                $response['feridas'][$key]['lado'] = 'ESQUERDO';
            else
                $response['feridas'][$key]['lado'] = '';

            $response['cp_itens'][$feridas['id']] = Curativo::getCoberturasByFeridaId($feridas['id'], 'p');
            $response['cs_itens'][$feridas['id']] = Curativo::getCoberturasByFeridaId($feridas['id'], 's');
        }

        return $response;
    }

    private static function prepareFichaPlanserv($prescricao)
    {
        $locais_ferida = self::getAllLocaisFeridas();
        $response = [];

        $response['prescricao'] = Curativo::getById($prescricao);
        $response['feridas'] = Curativo::getFeridasByPrescricaoId($prescricao);

        $admissao = Paciente::getDataAdmissaoPacienteById($response['prescricao']['paciente']);

        $getLocalFerida = function($local) use ($locais_ferida){
            foreach ($locais_ferida as $local_ferida){
                if($local_ferida['ID'] === $local)
                    return $local_ferida['LOCAL'] . '<br>';
            }
        };



        $getItemNames = function($itens){
            return $itens['item_name'];
        };

        $getQtd = function($itens){
            return sprintf("%s %s", $itens['qtd'], $itens['item_name']);
        };

        $getPeriod = function($itens){
            return $itens['frequencia_descricao'];
        };

        $profissional = Usuario::getById($response['prescricao']['profissional']);
        $compTipo = $profissional['conselho_regional'];

        $response['prescricao']['profissional'] = $profissional['nome'] . $compTipo;

        $num_feridas = count($response['feridas']);
        $response['prescricao']['num_paginas'] = ceil($num_feridas / 4);

        foreach($response['feridas'] as $key => $feridas){
            if($response['feridas'][$key]['lado'] == '1')
                $response['feridas'][$key]['lado'] = 'DIREITO';
            elseif($response['feridas'][$key]['lado'] == '2')
                $response['feridas'][$key]['lado'] = 'ESQUERDO';
            else
                $response['feridas'][$key]['lado'] = '';

            $response['ficha']['localizacao'][] 	= sprintf(
                "%s %s",
                strip_tags($getLocalFerida($feridas['local'])),
                strip_tags($response['feridas'][$key]['lado'])
            );
            //
            $response['ficha']['data_inicial'][]	= !isset($admissao['DATA_ADMISSAO']) && empty($admissao['DATA_ADMISSAO']) ? '' :
                \DateTime::createFromFormat('Y-m-d H:i:s', $admissao['DATA_ADMISSAO'])->format('d/m/Y');

            $response['ficha']['tamanho'][]			= " ";
            $response['ficha']['comprometimento'][]	= " ";
            $response['ficha']['tipo_tecido'][]		= " ";
            $response['ficha']['presenca_infeccao'][]	= " ";
            $response['ficha']['exsudato'][]			= " ";


            //CONDUTA ATUAL
            /*$cp = Curativo::getCondutaAtualByFeridaId($feridas['id'], 'p', $response['prescricao']['paciente']);
            if(count($cp) > 0) {
                $response['ficha']['conduta_atual']['produtos'][] = implode (' + ', array_map ($getItemNames, $cp));
                $response['ficha']['conduta_atual']['qtd'][] = implode (' + ', array_map ($getQtd, $cp));
                $response['ficha']['conduta_atual']['periodicidade'][] = implode(' + ', array_map($getPeriod, $cp));
                $response['ficha']['conduta_atual']['profissional'][] = $response['prescricao']['profissional'];
            } else {*/
            $response['ficha']['conduta_atual']['produtos'][] 		= " ";
            $response['ficha']['conduta_atual']['qtd'][] 			= " ";
            $response['ficha']['conduta_atual']['periodicidade'][] 	= " ";
            $response['ficha']['conduta_atual']['profissional'][] 	= " ";
            //}

            //COBERTURAS
            $cp = Curativo::getCoberturasByFeridaId($feridas['id'], 'p');
            $cs = Curativo::getCoberturasByFeridaId($feridas['id'], 's');

            $response['ficha']['cobertura_primaria']['produtos'][] 		= implode(' + ', array_map($getItemNames, $cp));
            $response['ficha']['cobertura_primaria']['qtd'][] 			= implode(' + ', array_map($getQtd, $cp));
            $response['ficha']['cobertura_primaria']['periodicidade'][] = implode(' + ', array_map($getPeriod, $cp));
            $response['ficha']['cobertura_primaria']['profissional'][] 	= $response['prescricao']['profissional'];

            $response['ficha']['cobertura_secundaria']['produtos'][] 	    = implode(' + ', array_map($getItemNames, $cs));
            $response['ficha']['cobertura_secundaria']['qtd'][] 			= implode(' + ', array_map($getQtd, $cs));
            $response['ficha']['cobertura_secundaria']['periodicidade'][]   = implode(' + ', array_map($getPeriod, $cs));
            $response['ficha']['cobertura_secundaria']['profissional'][] 	= $response['prescricao']['profissional'];
        }

        // SE A QUANTIDADE DE FERIDAS NÃO FOR SUFICIENTE PARA PREENCHER A TABELA
        for($i = 0; $i != 4 - ($num_feridas % 4); $i++){
            $response['ficha']['localizacao'][] 		= " ";
            $response['ficha']['data_inicial'][] 		= " ";
            $response['ficha']['tamanho'][]			= " ";
            $response['ficha']['comprometimento'][]	= " ";
            $response['ficha']['tipo_tecido'][]		= " ";
            $response['ficha']['presenca_infeccao'][]	= " ";
            $response['ficha']['exsudato'][]			= " ";

            $response['ficha']['conduta_atual']['produtos'][] 		= " ";
            $response['ficha']['conduta_atual']['qtd'][] 				= " ";
            $response['ficha']['conduta_atual']['periodicidade'][]	= " ";
            $response['ficha']['conduta_atual']['profissional'][] 	= " ";

            $response['ficha']['cobertura_primaria']['produtos'][] 		= " ";
            $response['ficha']['cobertura_primaria']['qtd'][] 			= " ";
            $response['ficha']['cobertura_primaria']['periodicidade'][] 	= " ";
            $response['ficha']['cobertura_primaria']['profissional'][] 	= " ";

            $response['ficha']['cobertura_secundaria']['produtos'][] 		= " ";
            $response['ficha']['cobertura_secundaria']['qtd'][] 			= " ";
            $response['ficha']['cobertura_secundaria']['periodicidade'][] = " ";
            $response['ficha']['cobertura_secundaria']['profissional'][] 	= " ";
        }

        return $response;
    }

    public static function fichaPlanserv ($prescricao = '')
    {

        if($_GET['action'] !== 'gerar-doc-prorrogacao-planserv'){
            $prescricao = filter_input(INPUT_GET, 'prescricao', FILTER_SANITIZE_NUMBER_INT);
        }

        $response   = self::prepareFichaPlanserv($prescricao);

        $paciente 	= self::getPacienteById($response['prescricao']['paciente']);
        $modalidade = Administracao::buscarModalidadePaciente($response['prescricao']['paciente']);

        $templateProcessor = new TemplateProcessor('../../enfermagem/prescricao/resources/planserv-template.docx');
        $templateProcessor->setValue('beneficiario', $paciente['nome']);

        $idade = \DateTime::createFromFormat('Y-m-d', $paciente['nascimento'])->diff(new \DateTime())->y;
        $templateProcessor->setValue('idade', sprintf('%d ano%s', $idade, ($idade > 1 ? "s" : "") ));

        $templateProcessor->setValue('diagnostico', $paciente['diagnostico']);

        $dataAdmissao = !isset($admissao['DATA_ADMISSAO']) && empty($admissao['DATA_ADMISSAO']) ? '' :
            \DateTime::createFromFormat('Y-m-d H:i:s', $admissao['DATA_ADMISSAO'])->format('d/m/Y');
        $templateProcessor->setValue('admissao', $dataAdmissao);

        $templateProcessor->setValue('regime', $modalidade['modalidade']);

        $inicio = \DateTime::createFromFormat('Y-m-d', $response['prescricao']['inicio'])->format('d/m/Y');
        $fim 	= \DateTime::createFromFormat('Y-m-d', $response['prescricao']['fim'])->format('d/m/Y');
        $templateProcessor->setValue('periodo', sprintf('%s a %s', $inicio, $fim));
        $templateProcessor->cloneRow('justificativa',count($response['feridas']));
        $i = 1;
        $justificativa = '';
        foreach($response['feridas'] as $ferida){
            $justificativa = $ferida['justificativa'];
            $templateProcessor->setValue("justificativa#{$i}",$justificativa);
            $i++;
        }

        $i = 0;
        array_reduce($response['ficha']['localizacao'], function ($buffer, $localizacao) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("local{$i}", $localizacao);
            $i++;
        });

        $i = 0;
        array_reduce($response['ficha']['data_inicial'], function ($buffer, $dataInicial) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("inicial{$i}", $dataInicial);
            $i++;
        });

        # CONDUTA ATUAL
        $i = 0;
        array_reduce($response['ficha']['conduta_atual']['produtos'], function ($buffer, $produto) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("condutaProduto{$i}", $produto);
            $i++;
        });
        $i = 0;
        array_reduce($response['ficha']['conduta_atual']['qtd'], function ($buffer, $qtd) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("condutaQtd{$i}", $qtd);
            $i++;
        });
        $i = 0;
        array_reduce($response['ficha']['conduta_atual']['periodicidade'], function ($buffer, $periodicidade) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("condutaPeriod{$i}", $periodicidade);
            $i++;
        });
        $i = 0;
        array_reduce($response['ficha']['conduta_atual']['profissional'], function ($buffer, $responsavel) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("condutaResp{$i}", $responsavel);
            $i++;
        });

        # COBERTURA PRIMARIA
        $i = 0;
        array_reduce($response['ficha']['cobertura_primaria']['produtos'], function ($buffer, $produto) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("primProduto{$i}", $produto);
            $i++;
        });
        $i = 0;
        array_reduce($response['ficha']['cobertura_primaria']['qtd'], function ($buffer, $qtd) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("primQtd{$i}", $qtd);
            $i++;
        });
        $i = 0;
        array_reduce($response['ficha']['cobertura_primaria']['periodicidade'], function ($buffer, $periodicidade) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("primPeriod{$i}", $periodicidade);
            $i++;
        });
        $i = 0;
        array_reduce($response['ficha']['cobertura_primaria']['profissional'], function ($buffer, $responsavel) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("primResp{$i}", $responsavel);
            $i++;
        });

        # COBERTURA SECUNDARIA
        $i = 0;
        array_reduce($response['ficha']['cobertura_secundaria']['produtos'], function ($buffer, $produto) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("secProduto{$i}", $produto);
            $i++;
        });
        $i = 0;
        array_reduce($response['ficha']['cobertura_secundaria']['qtd'], function ($buffer, $qtd) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("secQtd{$i}", $qtd);
            $i++;
        });
        $i = 0;
        array_reduce($response['ficha']['cobertura_secundaria']['periodicidade'], function ($buffer, $periodicidade) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("secPeriod{$i}", $periodicidade);
            $i++;
        });
        $i = 0;
        array_reduce($response['ficha']['cobertura_secundaria']['profissional'], function ($buffer, $responsavel) use (&$i, $templateProcessor){
            $templateProcessor->setValue ("secResp{$i}", $responsavel);
            $i++;
        });

        $templateProcessor->setValue('solicitante', $response['prescricao']['profissional']);
        $filename = sprintf("Curativo Beneficiario %s.docx", $paciente['nome']);

        return [$templateProcessor, $filename];
    }

    public static function preencherPrescricaoCurativo()
    {
        $response = [];
        $prescricao = filter_input(INPUT_GET, 'prescricao', FILTER_SANITIZE_NUMBER_INT);

        $response['prescricao'] = Curativo::getById($prescricao);
        $response['feridas'] = Curativo::getFeridasByPrescricaoId($prescricao);

        foreach($response['feridas'] as $key => $feridas){
            $response['cp_itens'][$feridas['id']] = Curativo::getCoberturasByFeridaId($feridas['id'], 'p');
            $response['cs_itens'][$feridas['id']] = Curativo::getCoberturasByFeridaId($feridas['id'], 's');
        }

        return $response;
    }

    public function salvar()
    {
        //Cabeçalho
        $paciente = $_POST['paciente'];
        $profissional = $_SESSION['id_user'];
        $empresa = $_SESSION['empresa_principal'];
        $carater = filter_input (INPUT_POST, 'carater', FILTER_SANITIZE_NUMBER_INT);

        $inicio = filter_input (INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
        $inicio = \DateTime::createFromFormat ('d/m/Y', $inicio)->format ('Y-m-d');
        $fim = filter_input (INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
        $fim = \DateTime::createFromFormat ('d/m/Y', $fim)->format ('Y-m-d');

        //Feridas
        $feridas 	= [];
        $lado 		= [];
        $i 				= 1;

        $concat_aprazamentos = function ($buffer, $value) {
            return $buffer .= $value . ' ';
        };


        foreach ($_POST['local_ferida'] as $id => $local) {

            $local = $id . "$" . $local . '$' . $_POST['lado_ferida'][$id];

            $feridas[$local]['lado'] = $_POST['lado_ferida'][$id];
            $feridas[$local]['justificativa'] = $_POST['justificativa'][$id];

            foreach ($_POST['cp_catalogo_id'][$i] as $increment => $catalogo_id) {
                $catalogo_id = current($catalogo_id);
                $feridas[$local]['cp_itens'][$increment]['frequencia'] = $_POST['cp_frequencia_item'][$i][$increment][$catalogo_id];
                $feridas[$local]['cp_itens'][$increment]['qtd'] = $_POST['cp_qtd'][$i][$increment][$catalogo_id];
                $feridas[$local]['cp_itens'][$increment]['obs'] = addslashes ($_POST['cp_obs_item'][$i][$increment][$catalogo_id]);
                $feridas[$local]['cp_itens'][$increment]['catalogo_id'] = $catalogo_id;

                $inicio_item 	= \DateTime::createFromFormat ('d/m/Y', $_POST['cp_inicio_item'][$i][$increment][$catalogo_id])->format ('Y-m-d');
                $fim_item 		= \DateTime::createFromFormat ('d/m/Y', $_POST['cp_fim_item'][$i][$increment][$catalogo_id])->format ('Y-m-d');

                $feridas[$local]['cp_itens'][$increment]['inicio'] 	= $inicio_item;
                $feridas[$local]['cp_itens'][$increment]['fim'] 		= $fim_item;

                $feridas[$local]['cp_itens'][$increment]['aprazamento'] = array_reduce ($_POST['cp_aprazamento'][$i][$increment][$catalogo_id] , $concat_aprazamentos);
            }

            foreach ($_POST['cs_catalogo_id'][$i] as $increment => $catalogo_id) {
                $catalogo_id = current($catalogo_id);
                $feridas[$local]['cs_itens'][$increment]['frequencia'] = $_POST['cs_frequencia_item'][$i][$increment][$catalogo_id];
                $feridas[$local]['cs_itens'][$increment]['qtd'] = $_POST['cs_qtd'][$i][$increment][$catalogo_id];
                $feridas[$local]['cs_itens'][$increment]['obs'] = addslashes ($_POST['cs_obs_item'][$i][$increment][$catalogo_id]);
                $feridas[$local]['cs_itens'][$increment]['catalogo_id'] = $catalogo_id;

                $inicio_item 	= \DateTime::createFromFormat ('d/m/Y', $_POST['cs_inicio_item'][$i][$increment][$catalogo_id])->format ('Y-m-d');
                $fim_item 		= \DateTime::createFromFormat ('d/m/Y', $_POST['cs_fim_item'][$i][$increment][$catalogo_id])->format ('Y-m-d');

                $feridas[$local]['cs_itens'][$increment]['inicio'] 	= $inicio_item;
                $feridas[$local]['cs_itens'][$increment]['fim'] 		= $fim_item;

                $feridas[$local]['cs_itens'][$increment]['aprazamento'] = array_reduce ($_POST['cs_aprazamento'][$i][$increment][$catalogo_id] , $concat_aprazamentos);
            }

            $i++;
        }

        try {
            @session_start();

            # ENVIA AS NOTIFICAÇÕES POR EMAIL E POR HIPCHAT
            $api_data = (object) Config::get('sendgrid');
            $gateway = new MailAdapter(
                new SendGridGateway(
                    new \SendGrid($api_data->user, $api_data->pass),
                    new \SendGrid\Email()
                )
            );
            $gateway->adapter->setSendData($paciente, 'enfermagem', 11, $empresa);
            $gateway->adapter->sendMail();

            $token = Config::get('TOKEN_HIPCHAT');
            $env = Config::get('APP_ENV');
            $hip = new \App\Controllers\HipChatSismederi($token, $env);
            $mensagem = utf8_decode($gateway->adapter->mail->html);
            $gateway->sendNotificationHipChat($hip, $mensagem);

            # SALVA E COLOCA COMO ENVIADO PARA SOLICITAÇÕES OU PARA ORÇAMENTO, CASO SEJA DE AVALIAÇÃO
            $prescricao = Curativo::save ($feridas, $paciente, $profissional, $inicio, $fim, $carater);

            //self::enviarPrescricaoCurativo($prescricao, $primeira_prescricao);
            return $prescricao;
        } catch (\Exception $e) {
            $msg = $e->getMessage ();
            $backtrace = [
                'text' => $msg,
                'module' => __NAMESPACE__,
                'file' => __FILE__,
                'function' => __FUNCTION__,
                'class' => __CLASS__,
                'line' => __LINE__
            ];
            Log::registerErrorLog ($backtrace);
            return $msg;
        }
    }

    public static function atualizar()
    {
        //Cabeçalho
        $prescricao = $_POST['prescricao'];
        $profissional = $_SESSION['id_user'];
        $carater = filter_input (INPUT_POST, 'carater', FILTER_SANITIZE_NUMBER_INT);
        $inicio	= filter_input (INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
        $inicio = \DateTime::createFromFormat ('d/m/Y', $inicio)->format ('Y-m-d');
        $fim = filter_input (INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
        $fim = \DateTime::createFromFormat ('d/m/Y', $fim)->format ('Y-m-d');

        //Feridas
        $feridas = [];
        $lado = [];
        $i = 1;

        $concat_aprazamentos = function ($buffer, $value) {
            return $buffer .= $value . ' ';
        };

        foreach ($_POST['local_ferida'] as $id => $local) {

            $local = $id . "$" . $local . '$' . $_POST['lado_ferida'][$id];

            $feridas[$local]['lado'] = $_POST['lado_ferida'][$id];
            $feridas[$local]['justificativa'] = $_POST['justificativa'][$id];

            foreach ($_POST['cp_catalogo_id'][$i] as $increment => $catalogo_id) {
                $catalogo_id = current($catalogo_id);
                $feridas[$local]['cp_itens'][$increment]['frequencia'] = $_POST['cp_frequencia_item'][$i][$increment][$catalogo_id];
                $feridas[$local]['cp_itens'][$increment]['qtd'] = $_POST['cp_qtd'][$i][$increment][$catalogo_id];
                $feridas[$local]['cp_itens'][$increment]['obs'] = addslashes ($_POST['cp_obs_item'][$i][$increment][$catalogo_id]);
                $feridas[$local]['cp_itens'][$increment]['catalogo_id'] = $catalogo_id;

                $inicio_item 	= \DateTime::createFromFormat ('d/m/Y', $_POST['cp_inicio_item'][$i][$increment][$catalogo_id])->format ('Y-m-d');
                $fim_item 		= \DateTime::createFromFormat ('d/m/Y', $_POST['cp_fim_item'][$i][$increment][$catalogo_id])->format ('Y-m-d');

                $feridas[$local]['cp_itens'][$increment]['inicio'] 	= $inicio_item;
                $feridas[$local]['cp_itens'][$increment]['fim'] 		= $fim_item;

                $feridas[$local]['cp_itens'][$increment]['aprazamento'] = array_reduce ($_POST['cp_aprazamento'][$i][$increment][$catalogo_id] , $concat_aprazamentos);
            }

            foreach ($_POST['cs_catalogo_id'][$i] as $increment => $catalogo_id) {
                $catalogo_id = current($catalogo_id);
                $feridas[$local]['cs_itens'][$increment]['frequencia'] = $_POST['cs_frequencia_item'][$i][$increment][$catalogo_id];
                $feridas[$local]['cs_itens'][$increment]['qtd'] = $_POST['cs_qtd'][$i][$increment][$catalogo_id];
                $feridas[$local]['cs_itens'][$increment]['obs'] = addslashes ($_POST['cs_obs_item'][$i][$increment][$catalogo_id]);
                $feridas[$local]['cs_itens'][$increment]['catalogo_id'] = $catalogo_id;

                $inicio_item 	= \DateTime::createFromFormat ('d/m/Y', $_POST['cs_inicio_item'][$i][$increment][$catalogo_id])->format ('Y-m-d');
                $fim_item 		= \DateTime::createFromFormat ('d/m/Y', $_POST['cs_fim_item'][$i][$increment][$catalogo_id])->format ('Y-m-d');

                $feridas[$local]['cs_itens'][$increment]['inicio'] 	= $inicio_item;
                $feridas[$local]['cs_itens'][$increment]['fim'] 		= $fim_item;

                $feridas[$local]['cs_itens'][$increment]['aprazamento'] = array_reduce ($_POST['cs_aprazamento'][$i][$increment][$catalogo_id] , $concat_aprazamentos);
            }

            $i++;
        }

        try {
            $response = Curativo::update ($feridas, $prescricao, $profissional, $inicio, $fim, $carater);
            //self::enviarPrescricaoCurativo($prescricao, $primeira_prescricao, 'update');
            return $response;
        } catch (\Exception $e) {
            $msg = $e->getMessage ();
            $backtrace = [
                'text' => $msg,
                'module' => __NAMESPACE__,
                'file' => __FILE__,
                'function' => __FUNCTION__,
                'class' => __CLASS__,
                'line' => __LINE__
            ];
            Log::registerErrorLog ($backtrace);
            return $msg;
        }
    }

    public static function cancelarPrescricaoCurativo()
    {
        $response = [];

        $prescricao 			= filter_input (INPUT_GET, 'prescricao', FILTER_SANITIZE_NUMBER_INT);
        $profissional 		= $_SESSION['id_user'];

        try {
            return self::verificaPrescricaoAuditada($prescricao) === 0 ? Curativo::cancel($prescricao, $profissional) : false;
        } catch (\Exception $e) {
            $msg = $e->getMessage ();
            $backtrace = [
                'text' => $msg,
                'module' => __NAMESPACE__,
                'file' => __FILE__,
                'function' => __FUNCTION__,
                'class' => __CLASS__,
                'line' => __LINE__
            ];
            Log::registerErrorLog ($backtrace);
            return $msg;
        }
    }

    public static function verificaPrescricaoAuditada($prescricao)
    {
        $haveAuditado = array_filter(Solicitacoes::getByPrescricaoCurativoId($prescricao), function($itens){
            return $itens['data_auditado'] !== '0000-00-00 00:00:00';
        });

        return count($haveAuditado);
    }
}
