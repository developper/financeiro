<?php
namespace app\Controllers;

use App\Helpers\StringHelper;
use \App\Models\Administracao\Filial;
use App\Models\Financeiro\OrcamentoGerencial as FinanceiroOrcamentoGerencial;

include_once('../../utils/codigos.php');
// ini_set('display_errors', 1);
class OrcamentoGerencial
{
    public static function visualizar()
    {
        $cores['cores'] = [
            'blue' => '#81F7F3',
            'red' => '#FA5858',
            'green' =>'#58FA58',
            'bold' => '#E6E6E6',
            'pink' => '#F8E0EC'
        ];

        $cores['panel'] = [
            'blue' => 'info',
            'red' => 'danger',
            'green' =>'success',
            'bold' => 'default',
            'pink' => 'warning'

        ];
        $responseTopicos = FinanceiroOrcamentoGerencial::getTopicosByIdPai(0);
        $versao = FinanceiroOrcamentoGerencial::getVersaoOrcamentoById(1);
        $responseValoresTopicos = FinanceiroOrcamentoGerencial::getValoresTopicosByVersao(1);
        foreach($responseValoresTopicos as $resp){
            $valoresTopicos[$resp['orcamento_gerencial_topicos_id']] = $resp;
        }

        $responseValoresPlanoContas = FinanceiroOrcamentoGerencial::getValoresPlanoContasByVersao(1);
        foreach($responseValoresPlanoContas as $resp){
            $valoresPlanoContas[$resp['orcamento_gerencial_topicos_id']][$resp['plano_contas_id']] = $resp;

        }

        foreach ($responseTopicos as $topico) {
            $data[$topico['id']] = $topico;
            $responseFilhos = FinanceiroOrcamentoGerencial::getTopicosByIdPai($topico['id']);
            if(!empty($responseFilhos)){
                foreach ($responseFilhos as $resp) {
                    $data[$topico['id']]['filhos'][$resp['id']] = $resp;
                    $data[$topico['id']]['filhos'][$resp['id']]['filhos'] = FinanceiroOrcamentoGerencial::getTopicosByIdPai($resp['id']);
                }
            }
        }

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/orcamento-gerencial/templates/visualizar.phtml');
    }

    public static function visualizarNovo()
    {
        $cores['cores'] = [
            'blue' => '#81F7F3',
            'red' => '#FA5858',
            'green' =>'#58FA58',
            'bold' => '#E6E6E6',
            'pink' => '#F8E0EC'
        ];

        $cores['panel'] = [
            'blue' => 'info',
            'red' => 'danger',
            'green' =>'success',
            'bold' => 'default',
            'pink' => 'warning'

        ];
        $responseTopicos = FinanceiroOrcamentoGerencial::getTopicosByIdPai(0);
        $versao = FinanceiroOrcamentoGerencial::getVersaoOrcamentoById(1);
        $responseValoresTopicos = FinanceiroOrcamentoGerencial::getValoresTopicosByVersao(1);
        foreach($responseValoresTopicos as $resp){
            $valoresTopicos[$resp['orcamento_gerencial_topicos_id']] = $resp;
        }

        $responseValoresPlanoContas = FinanceiroOrcamentoGerencial::getValoresPlanoContasByVersao(1);
        foreach($responseValoresPlanoContas as $resp){
            $valoresPlanoContas[$resp['orcamento_gerencial_topicos_id']][$resp['plano_contas_id']] = $resp;

        }

        foreach ($responseTopicos as $topico) {
            $data[$topico['id']] = $topico;
            $responseFilhos = FinanceiroOrcamentoGerencial::getTopicosByIdPai($topico['id']);
            if(!empty($responseFilhos)){
                foreach ($responseFilhos as $resp) {
                    $data[$topico['id']]['filhos'][$resp['id']] = $resp;
                    $data[$topico['id']]['filhos'][$resp['id']]['filhos'] = FinanceiroOrcamentoGerencial::getTopicosByIdPai($resp['id']);
                }
            }
        }

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/orcamento-gerencial/templates/visualizar.phtml');
    }
}