<?php

namespace app\Controllers;

use App\Models\Administracao\Paciente;
use App\Models\Administracao\Usuario;
use App\Models\SAC\ContratoEncaminhamento;
use App\Models\SAC\EncaminhamentoMedico;
use App\Models\SAC\TiposQueixa;

class SAC
{
    public static function menu()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/sac/templates/menu.phtml');
    }

    public static function buscarEncaminhamentoMedico()
    {
        $pacientes = Paciente::getPacientesByStatus();
        $contratos = ContratoEncaminhamento::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/sac/templates/encaminhamento/listar-encaminhamento-form.phtml');
    }

    public static function buscarListaEncaminhamentos()
    {
        if(isset($_POST) && !empty($_POST)) {
            $pacienteId = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $tipo = filter_input(INPUT_POST, 'select-tipo', FILTER_SANITIZE_STRING);
            $contratoId = filter_input(INPUT_POST, 'contrato', FILTER_SANITIZE_NUMBER_INT);
            $contratos = ContratoEncaminhamento::getAll();

            $filters = [
                'paciente' => $pacienteId,
                'inicio' => $inicio,
                'fim' => $fim,
                'contrato' => $contratoId,
                'tipo' => $tipo

            ];

            $response = EncaminhamentoMedico::listEncaminhamentos($filters);

            $pacientes = Paciente::getPacientesByStatus();

            require($_SERVER['DOCUMENT_ROOT'] . '/sac/templates/encaminhamento/listar-encaminhamento-form.phtml');
        }
    }

    public static function encaminhamentoMedicoForm()
    {
        if(isset($_GET['tipo']) && !empty($_GET['tipo'])) {
            $tipoEncaminhamento = $_GET['tipo'];
            if($tipoEncaminhamento == 'externo'){
                $contratos = ContratoEncaminhamento::getAll();

            }else{
                $pacientes = Paciente::getPacientesByStatus([1, 4]);
            }

            $usuariosMed = Usuario::getUsersByTipo('medico');
            $tipos_queixa = TiposQueixa::getAll();


            require($_SERVER['DOCUMENT_ROOT'] . '/sac/templates/encaminhamento/encaminhamento-novo-form.phtml');
        }else{
            $msgErro = 1;
            require($_SERVER['DOCUMENT_ROOT'] . '/sac/templates/encaminhamento/listar-encaminhamento-form.phtml');

        }
    }

    public static function gerarProtocoloEncaminhamento()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = $_POST;

            list($response['protocolo'], $response['erro']) = EncaminhamentoMedico::gerarProtocoloAtendimento($data);

            echo json_encode($response);
        }
    }

    public static function salvarCondutaEncaminhamento()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = $_POST;

            echo EncaminhamentoMedico::salvarCondutaEncaminhamento($data);
        }
    }

    public static function atualizarCondutaEncaminhamento()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = $_POST;

            echo $response = EncaminhamentoMedico::atualizarCondutaEncaminhamento($data);
        }
    }

    public static function visualizarEncaminhamentoMedico()
    {
        if(isset($_GET) && !empty($_GET)) {
            $id = filter_input(INPUT_GET, 'ecm', FILTER_SANITIZE_NUMBER_INT);

            $usuariosMed = Usuario::getUsersByTipo('medico');
            $response = EncaminhamentoMedico::viewEncaminhamento($id);

            require($_SERVER['DOCUMENT_ROOT'] . '/sac/templates/encaminhamento/encaminhamento-visualizar.phtml');
        }
    }

    public static function atualizarEncaminhamentoMedico()
    {
        if(isset($_GET) && !empty($_GET)) {
            $id = filter_input(INPUT_GET, 'ecm', FILTER_SANITIZE_NUMBER_INT);

            $usuariosMed = Usuario::getUsersByTipo('medico');
            $response = EncaminhamentoMedico::viewEncaminhamento($id);

            require($_SERVER['DOCUMENT_ROOT'] . '/sac/templates/encaminhamento/encaminhamento-editar-form.phtml');
        }
    }
}

