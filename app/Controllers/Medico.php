<?php

namespace app\Controllers;

use \App\Models\Administracao\Filial;
use \App\Models\Administracao\Paciente;
use \App\Models\Medico\Evolucao;
use \App\Models\Medico\Prescricao;

class Medico
{
	public static function formRelatorioEvolucoes()
	{
        $urs = Filial::getAll();
		require ($_SERVER['DOCUMENT_ROOT'] . '/medico/relatorios/templates/relatorio-evolucoes-form.phtml');
	}

    public static function relatorioEvolucoes()
    {
        if(isset($_POST) && !empty($_POST)){
            $periodoInicio = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $urBusca = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'ur'            => $_SESSION['empresa_principal'] == '11' || $_SESSION['empresa_principal'] == '1' ? $urBusca : $_SESSION['empresa_principal']
            ];

            $response 	= Evolucao::getRelatorioEvolucoes($filtros);
            $urs        = Filial::getAll();
            $urNome     = Filial::getById($urBusca)['nome'];
            $count      = count($response);

            require ($_SERVER['DOCUMENT_ROOT'] . '/medico/relatorios/templates/relatorio-evolucoes-form.phtml');
        }
    }

    public static function excelRelatorioEvolucoes()
    {
        if(isset($_GET) && !empty($_GET)){
            $periodoInicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $urBusca = filter_input(INPUT_GET, 'ur', FILTER_SANITIZE_NUMBER_INT);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'ur'            => $_SESSION['empresa_principal'] == '11' || $_SESSION['empresa_principal'] == '1' ? $urBusca : $_SESSION['empresa_principal']
            ];

            $response 	= Evolucao::getRelatorioEvolucoes($filtros);
            $count      = count($response);

            $extension = '.xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = '.xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $periodoInicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $periodoFim);
            $filename = sprintf(
                "relatorio_evolucoes_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}{$extension}");
            header("Pragma: no-cache");

            require ($_SERVER['DOCUMENT_ROOT'] . '/medico/relatorios/templates/relatorio-evolucoes-excel.phtml');
        }
    }

    public static function formSuspenderItensPrescricoes()
    {
        $pacientes = Paciente::getPacientesByStatus(4);
        require ($_SERVER['DOCUMENT_ROOT'] . '/medico/prescricao/templates/suspender-itens-prescricoes-form.phtml');
    }

    public static function buscarPrescricoesAtivas()
    {
        if(isset($_GET) && !empty($_GET)){
            $response = [];
            $itensSuspensos = [];
            $paciente = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);

            $itensSusp = Prescricao::getItensSuspensosPrescricoesAtivas($paciente);

            $i = 0;
            foreach ($itensSusp as $item) {
                $itensSuspensos[$item['tipo_item_origem']][$item['itemId']]['inicio'] = new \DateTime($item['inicio']);
                $itensSuspensos[$item['tipo_item_origem']][$item['itemId']]['fim']    = new \DateTime($item['fim']);
                $i++;
            }

            $prescricoes = Prescricao::getPrescricoesAtivas($paciente);
            //echo '<pre>'; die(print_r($itensSuspensos));

            foreach ($prescricoes as $data) {
                $inicioPrescricao = new \DateTime($data['inicioPrescricao']);
                $fimPrescricao = new \DateTime($data['fimPrescricao']);
                if(array_key_exists($data['tipo'], $itensSuspensos)) {
                    if (array_key_exists($data['itemId'], $itensSuspensos[$data['tipo']])) {
                        if (
                            !($itensSuspensos[$data['tipo']][$data['itemId']]['inicio'] >= $inicioPrescricao && $itensSuspensos[$data['tipo']][$data['itemId']]['inicio'] <= $fimPrescricao) &&
                            !($itensSuspensos[$data['tipo']][$data['itemId']]['fim'] >= $inicioPrescricao && $itensSuspensos[$data['tipo']][$data['itemId']]['fim'] <= $fimPrescricao)
                        ) {
                            $response[] = $data;
                        }
                    }else{
                        $response[] = $data;
                    }
                }else{
                    $response[] = $data;
                }
            }
            echo json_encode($response);
        }
    }

    public static function suspenderItensPrescricoesAtivas()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = $_POST;
            //die(var_dump($data));
            $prescricao = new Prescricao();
            try {
                $response = $prescricao->suspenderPrescricoesAtivas($data);
                $msg = "Suspensão(ões) efetuada(s) com sucesso! Para cada prescrição ativa que teve um item suspenso, foi criada uma nova prescrição aditiva!";
                header("Location: /medico/prescricao/?action=suspender-itens-prescricoes&msg=" . base64_encode($msg) . "&type=s");
            } catch (\Exception $e) {
                header("Location: /medico/prescricao/?action=suspender-itens-prescricoes&msg=" . base64_encode($e->getMessage()) . "&type=f");
            }
        }
    }

    public static function ultimaEvolucaoPaciente()
    {
        if(isset($_GET) && !empty($_GET)){
            $idPaciente = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $evolucao = Evolucao::getUltimoRelatorioPaciente($idPaciente);
            $problemasAtivos = Evolucao::getProblemasAtivosEvolucao($evolucao['ID']);
            echo json_encode($problemasAtivos);
        }
    }
}
