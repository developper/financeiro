<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 11/10/2017
 * Time: 09:48
 */

namespace app\Controllers;

use \App\Models\Logistica\Catalogo;
use \App\Models\Logistica\DevolucaoInterna as ModelDevolucaoInterna;
use \App\Models\Administracao\Filial;
use \App\Models\Administracao\Usuario;
use \App\Models\Sistema\Equipamentos;



class DevolucaoInterna
{
    public static function formSolicitarDevolucaoInterna()
    {
       require($_SERVER['DOCUMENT_ROOT'] . '/logistica/DevolucaoInterna/templates/formSolicitarDevolucao.phtml');
    }

    public static function salvarDevolucaoInterna()
    {
        if(isset($_POST) && !empty($_POST['catalogo_id'])){

            $arrayCatalogoId    = $_POST['catalogo_id'];
            $arrayTipo          = $_POST['tipo'];
            $arrayValidade      = $_POST['validade'];
            $arrayQuantidade    = $_POST['qtd'];
            $arrayObservacao    = $_POST['observacao'];
            $arrayLote          = $_POST['lote'];

            $arrayMotivo = $_POST['motivo'];

            $retorno = ModelDevolucaoInterna::salvarDevolucaoInterna($arrayCatalogoId,
                                                                        $arrayTipo,
                                                                        $arrayValidade,
                                                                        $arrayQuantidade,
                                                                        $arrayObservacao,
                                                                        $arrayLote,
                                                                        $arrayMotivo);
            echo $retorno;

        }
    }

    public static function pesquisarSolicitacaoDevolucaoInterna()
    {

        $pedidos = '';
        if(isset($_POST) && !empty($_POST)) {
            $empresaId = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_STRING);
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $solicitacoes = ModelDevolucaoInterna::getByFilters($empresaId,$inicio,$fim);

        }
        $empresas = Filial::getUnidadesAtivas();

        require($_SERVER['DOCUMENT_ROOT'] . '/logistica/DevolucaoInterna/templates/pesquisarSolicitacaoDevolucaoInterna.phtml');

    }

    public static function visualizarSolicitacaoDevolucaoInterna()
    {

        if (isset($_GET) && !empty($_GET['id'])) {
            $devolucaoInternaId = $_GET['id'];

            $pedido = ModelDevolucaoInterna::getPedidoById($devolucaoInternaId);
            $itensPedidos = ModelDevolucaoInterna::getItensByIdPedido($devolucaoInternaId);

            foreach ($itensPedidos as $r) {
                $arrayItensPedidos[$r['id']] = $r;
                if ($r['tipo'] == 5) {
                    $itemArray = Equipamentos::getById($r['catalogo_id']);
                    $item = $itemArray['item'];
                } else {
                    $itemArray = Catalogo::getById($r['catalogo_id']);
                    $item = $itemArray['principio'] . ' ' . $itemArray['apresentacao'] . ' <b>LAB: </b>' . $itemArray['lab_desc'];
                }
                $arrayItensPedidos[$r['id']]['item'] = $item;

            }
            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/DevolucaoInterna/templates/visualizarSolicitarDevolucao.phtml');


        }
    }

        public static function autorizarSolicitacaoDevolucaoInterna()
    {

        if (isset($_GET) && !empty($_GET['id'])) {
            $devolucaoInternaId = $_GET['id'];

            $pedido = ModelDevolucaoInterna::getPedidoById($devolucaoInternaId);
            $itensPedidos = ModelDevolucaoInterna::getItensByIdPedido($devolucaoInternaId);

            foreach ($itensPedidos as $r) {
                $arrayItensPedidos[$r['id']] = $r;
                if ($r['tipo'] == 5) {
                    $itemArray = Equipamentos::getById($r['catalogo_id']);
                    $item = $itemArray['item'];
                } else {
                    $itemArray = Catalogo::getById($r['catalogo_id']);
                    $item = $itemArray['principio'] . ' ' . $itemArray['apresentacao'] . ' <b>LAB: </b>' . $itemArray['lab_desc'];
                }
                $arrayItensPedidos[$r['id']]['item'] = $item;

            }
            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/DevolucaoInterna/templates/autorizarSolicitacaoDevolucao.phtml');


        }
    }

    public static function salvarAutorizacaoDevolucaoInterna()
    {

        if(isset($_POST) && !empty($_POST['pedido_id']) && !empty($_POST['quantidade_autorizada'])
            && !empty($_POST['devolucao_interna_item']) && !empty($_POST['ur_origem'])){


            $pedidoId    = $_POST['pedido_id'];
            $arrayQuantidadeAutorizada          = $_POST['quantidade_autorizada'];
            $arrayDevolucaoInternaItem          = $_POST['devolucao_interna_item'];
            $urOrigem = $_POST['ur_origem'];




            $retorno = ModelDevolucaoInterna::salvarAutorizacaoDevolucaoInterna($pedidoId,
                $arrayQuantidadeAutorizada,
                $arrayDevolucaoInternaItem,
                $urOrigem);

            echo $retorno;

        }else{
            echo 'Algo deu errado entre em contato com o TI';
        }

    }

    public static function enviarItensSolicitacaoDevolucaoInterna()
    {

        if (isset($_GET) && !empty($_GET['id'])) {
            $devolucaoInternaId = $_GET['id'];

            $pedido = ModelDevolucaoInterna::getPedidoById($devolucaoInternaId);
            $itensPedidos = ModelDevolucaoInterna::getItensByIdPedido($devolucaoInternaId);

            foreach ($itensPedidos as $r) {
                $arrayItensPedidos[$r['id']] = $r;
                if ($r['tipo'] == 5) {
                    $itemArray = Equipamentos::getById($r['catalogo_id']);
                    $item = $itemArray['item'];
                } else {
                    $itemArray = Catalogo::getById($r['catalogo_id']);
                    $item = $itemArray['principio'] . ' ' . $itemArray['apresentacao'] . ' <b>LAB: </b>' . $itemArray['lab_desc'];
                }
                $arrayItensPedidos[$r['id']]['item'] = $item;

            }
            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/DevolucaoInterna/templates/enviarItensSolicitacaoDevolucao.phtml');


        }
    }

    public static function salvarEnvioItensDevolucaoInterna()
    {

        if(isset($_POST) && !empty($_POST['pedido_id']) && !empty($_POST['quantidade_enviada'])
            && !empty($_POST['devolucao_interna_item']) && !empty($_POST['ur_origem'])
            && !empty($_POST['devolucao_interna_item_tipo']) && !empty($_POST['ur_destino'])
            && !empty($_POST['devolucao_interna_item_catalogo'])){


            $pedidoId    = $_POST['pedido_id'];
            $arrayQuantidadeEnviar          = $_POST['quantidade_enviada'];
            $arrayDevolucaoInternaItem          = $_POST['devolucao_interna_item'];
            $urOrigem = $_POST['ur_origem'];
            $urDestino = $_POST['ur_destino'];
            $arrayDevolucaoInternaItemTipo    = $_POST['devolucao_interna_item_tipo'];
            $arrayDevolucaoInternaItemCatalogo = $_POST['devolucao_interna_item_catalogo'];




            $retorno = ModelDevolucaoInterna::salvarEnvioItensDevolucaoInterna($pedidoId,
                $arrayQuantidadeEnviar,
                $arrayDevolucaoInternaItem,
                $urOrigem,
                $urDestino,
                $arrayDevolucaoInternaItemTipo,
                $arrayDevolucaoInternaItemCatalogo);

            echo $retorno;

        }else{
            echo 'Algo deu errado entre em contato com o TI';
        }

    }

    public static function confirmarEntregaItensSolicitacaoDevolucaoInterna()
    {

        if (isset($_GET) && !empty($_GET['id'])) {
            $devolucaoInternaId = $_GET['id'];

            $pedido = ModelDevolucaoInterna::getPedidoById($devolucaoInternaId);
            $itensPedidos = ModelDevolucaoInterna::getItensByIdPedido($devolucaoInternaId);
            $historicoConfirmacao = ModelDevolucaoInterna::getHistoricoConfirmacaoByPedido($devolucaoInternaId);


            foreach ($itensPedidos as $r) {
                $arrayItensPedidos[$r['id']] = $r;
                if ($r['tipo'] == 5) {
                    $itemArray = Equipamentos::getById($r['catalogo_id']);
                    $item = $itemArray['item'];
                } else {
                    $itemArray = Catalogo::getById($r['catalogo_id']);
                    $item = $itemArray['principio'] . ' ' . $itemArray['apresentacao'] . ' <b>LAB: </b>' . $itemArray['lab_desc'];
                }
                $arrayItensPedidos[$r['id']]['item'] = $item;

            }
            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/DevolucaoInterna/templates/confirmarEntregaItensSolicitacaoDevolucaoInterna.phtml');


        }
    }

    public static function salvarConfirmarcaoEntregaItensDevolucaoInterna()
    {

        if(isset($_POST) && !empty($_POST['pedido_id']) && !empty($_POST['quantidade_confirmada'])
            && !empty($_POST['devolucao_interna_item']) && !empty($_POST['ur_origem'])
            && !empty($_POST['devolucao_interna_item_tipo']) && !empty($_POST['ur_destino'])
            && !empty($_POST['devolucao_interna_item_catalogo'])
            && !empty($_POST['status'])){


            $pedidoId    = $_POST['pedido_id'];
            $arrayQuantidadeConfirmada          = $_POST['quantidade_confirmada'];
            $arrayDevolucaoInternaItem          = $_POST['devolucao_interna_item'];
            $urOrigem = $_POST['ur_origem'];
            $urDestino = $_POST['ur_destino'];
            $arrayDevolucaoInternaItemTipo    = $_POST['devolucao_interna_item_tipo'];
            $arrayDevolucaoInternaItemCatalogo = $_POST['devolucao_interna_item_catalogo'];
            $status = $_POST['status'];




            $retorno = ModelDevolucaoInterna::salvarConfirmarcaoEntregaItensDevolucaoInterna($pedidoId,
                $arrayQuantidadeConfirmada,
                $arrayDevolucaoInternaItem,
                $urOrigem,
                $urDestino,
                $arrayDevolucaoInternaItemTipo,
                $arrayDevolucaoInternaItemCatalogo,
                $status);

            echo $retorno;

        }else{
            echo 'Algo deu errado entre em contato com o TI';
        }

    }

    public static function cancelarPedidoDevolucaoInterna()
    {
        if(isset($_POST['justificativa-cancelar']) && !empty($_POST['pedido-id']) ){
            $justificativaCancelar = $_POST['justificativa-cancelar'];
            $pedidoId = $_POST['pedido-id'];
            $retorno = ModelDevolucaoInterna::cancelarPedidoDevolucaoInterna($pedidoId, $justificativaCancelar);
            echo $retorno;

        }else{
            echo 'Algo deu errado entre em contato com o TI';
        }
    }







}