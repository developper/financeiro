<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 17/12/2015
 * Time: 10:55
 */

namespace app\Controllers;

use App\Models\Logistica\PadronizarMedicamento as Padronizar;


class PadronizarMedicamento
{
    private $mensagem = null;
    public static function menu()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PadronizarMedicamento/templates/menu-form.phtml');
    }
    public static function principioAtivo()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PadronizarMedicamento/templates/principio-ativo-form.phtml');

    }
    public static function principioAtivoEditar()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PadronizarMedicamento/templates/editar-principio-ativo-form.phtml');

    }
    public static function inserirPrincipioAtivo()
    {
        $mensagem = null;
        $principioAtivo = filter_input(INPUT_POST,'novo-principio-ativo',FILTER_SANITIZE_STRING);
        $query = filter_input(INPUT_POST,'query',FILTER_SANITIZE_STRING);
        if(empty($principioAtivo) ||empty($query) ) {
            $mensagem['text'] = 'O campo não pode ser vazio.';
            $mensagem['style'] = 'text-red';
            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PadronizarMedicamento/templates/principio-ativo-form.phtml');
            return;
        }

        $data = Padronizar::getByPrincipio($principioAtivo,$query);
        if(empty($data)) {
            $mensagem['text'] = Padronizar::inserirPrincipioAtivo($principioAtivo);
            $mensagem['style'] = 'text-green';
            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PadronizarMedicamento/templates/principio-ativo-form.phtml');
            return;
        }
        $mensagem['text']= "O Princípio Ativo: {$principioAtivo}, já existe na base de dados.";
        $mensagem['style'] = 'text-red';
        require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PadronizarMedicamento/templates/principio-ativo-form.phtml');
        return;
    }
    public static function editarPrincipioAtivo()
    {
        $mensagem = null;
        $principioAtivo = filter_input(INPUT_POST,'principio_ativo',FILTER_SANITIZE_STRING);
        $idPrincipio = filter_input(INPUT_POST,'id_principio',FILTER_SANITIZE_NUMBER_INT);
        if(empty($idPrincipio)) {
            $mensagem['text'] = 'Você deve selecionar o Princípio Ativo na busca para poder edita-lo';
            $mensagem['style'] = 'text-red';
            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PadronizarMedicamento/templates/editar-principio-ativo-form.phtml');
            return;
        }
        if(empty($principioAtivo)) {
            $mensagem['text'] = 'O campo editar não pode ser vazio';
            $mensagem['style'] = 'text-red';
            require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PadronizarMedicamento/templates/editar-principio-ativo-form.phtml');
            return;

        }
        $mensagem['text']= Padronizar::updatePrincipioAtivoByID($principioAtivo, $idPrincipio);
        $mensagem['style'] = 'text-green';
        require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PadronizarMedicamento/templates/editar-principio-ativo-form.phtml');

    }

}