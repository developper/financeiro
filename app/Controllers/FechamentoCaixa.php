<?php
namespace app\Controllers;

use \App\Models\Administracao\Filial;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\IPCF;
use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\Notas;
use \App\Models\Financeiro\FechamentoCaixa AS FechamentoCaixaModel;

class FechamentoCaixa
{
    public static function indexFechamentoCaixa()
    {
        $fechamento = FechamentoCaixaModel::getFechamentoCaixaAtual();
        $naoConciliadas = [];
        if(isset($_GET['consultar']) && $_GET['consultar'] == 's') {
            $data_fechamento  = filter_input(INPUT_GET, 'data_fechamento', FILTER_SANITIZE_STRING);
            $naoConciliadas = FechamentoCaixaModel::consultarNaoConciliadas($data_fechamento);
            $naoProcessadas = FechamentoCaixaModel::consultarNaoProcessadas($data_fechamento);
            $transferenciaNaoConciliada = FechamentoCaixaModel::consultarTransferenciasNaoConciliadas($data_fechamento);
        }
        $historicoFechamento = FechamentoCaixaModel::getHistoricoFechamentoCaixa();
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/fechamento-caixa/templates/index-fechamento-caixa.phtml');
    }

    public static function salvarFechamentoCaixa()
    {
        if(isset($_POST) && !empty($_POST)){
            $data_fechamento  = filter_input(INPUT_POST, 'data_fechamento', FILTER_SANITIZE_STRING);
            $justificativa  = filter_input(INPUT_POST, 'justificativa', FILTER_SANITIZE_STRING);
            $data = [
                'data_fechamento' => $data_fechamento,
                'justificativa' => $justificativa,
            ];
            $response = FechamentoCaixaModel::save($data);
        }
        $msg = "Houve um problema ao atualizar o fechamento!";
        $type = 'f';
        if(!is_array($response) && $response) {
            $msg = "Fechamento de caixa atualizado com sucesso!";
            $type = 's';
            // FLAG PARA CONSULTAR NÃO-CONCILIADAS
            $consultar = 'n';
        }
        if(is_array($response)) {
            $msg = "Existem {$response['nao_processadas']} parcelas não processadas e {$response['nao_conciliadas']} 
                    parcelas processadas mas não conciliadas, existem {$response['transferencia_nao_conciliada']} tansferência(s) processada e não conciliada. Não é possível fechar o período na data informada. 
                    Regularize a situação dessas parcelas ou transferências.";
            $type = 'f';
            $consultar = 's';
        }

        header ("Location: /financeiros/fechamento-caixa/?action=fechamento-caixa&msg=" . base64_encode ($msg) . "&type={$type}&consultar={$consultar}&data_fechamento={$data_fechamento}");
    }

    public static function getFechamentoCaixa()
    {
        return FechamentoCaixaModel::getFechamentoCaixaAtual();
    }

    public static function jsonFechamentoCaixa()
    {
        echo json_encode(self::getFechamentoCaixa());
    }
}
