<?php

namespace app\Controllers;
require_once('../utils/codigos.php');
//ini_set('display_errors',1);
//ini_set('display_startup_erros',1);
//error_reporting(E_ALL);



use App\Helpers\StringHelper;
use App\Models\Administracao\Cidade;
use App\Models\Administracao\Filial;
use App\Models\Administracao\Paciente;
use App\Models\Administracao\Planos;
use App\Models\Administracao\Usuario;
use App\Models\EventoAPH\EventoAPH;
use App\Models\Financeiro\Fornecedor;
use \App\Models\DB\ConnMysqli;
use App\Services\Gateways;
use App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;
use App\Core\Config;
use App\Controllers\SendGrid;
use App\Models\Financeiro\ContaBancaria;
class EventosAPH
{
    private static function getLists()
    {
        return [

            'sim_nao'           => ['S' => 'Sim', 'N' => 'Não'],
            'tipo_servico'      => ['APH' => 'APH', 'EV' => 'Evento'],
            'publico_alvo'      =>  ['A' => 'Adulto', 'I' => 'Infantil'],
            'forma_pagamento'    => [
                'DIN' => 'Dinheiro',
                'CHQ' => 'Cheque',
                'CRT' => 'Cartão',
                'TRB' => 'Transação Bancária'
            ],
            'tipo_item'    => [
                '0' => 'Medicamento',
                '1' => 'Material',
                '3' => 'Dieta',
                '5' => 'Equipamento'
            ],
        ];
    }

    public static function menu()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/eventos_aph/templates/menu.phtml');
    }

    public static function form()
    {
        $cidades = Cidade::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/eventos_aph/templates/form.phtml');
    }



    public static function salvar()
    {
        if(isset($_POST) && !empty($_POST)){

            $solicitante            = filter_input(INPUT_POST, 'solicitante', FILTER_SANITIZE_STRING);
            $telefoneSolicitante    = filter_input(INPUT_POST, 'telefone-solicitante', FILTER_SANITIZE_STRING);
            $dataEventoAph             = filter_input(INPUT_POST, 'data-evento-aph', FILTER_SANITIZE_STRING);
            $dataEventoAph           = \DateTime::createFromFormat('d/m/Y', $dataEventoAph)->format('Y-m-d');
            $horaEventoAph              = filter_input(INPUT_POST, 'hora-evento-aph', FILTER_SANITIZE_STRING);
            $tipoServico                    = filter_input(INPUT_POST, 'tipo-servico', FILTER_SANITIZE_STRING);
            $publicoAlvo             = filter_input(INPUT_POST, 'publico-alvo', FILTER_SANITIZE_STRING);
            $endereco       = filter_input(INPUT_POST, 'endereco', FILTER_SANITIZE_STRING);
            $cidade               = filter_input(INPUT_POST, 'cidade', FILTER_SANITIZE_NUMBER_INT);
            $complemento           = filter_input(INPUT_POST, 'complemento', FILTER_SANITIZE_STRING);
            $observacao       = filter_input(INPUT_POST, 'observacao', FILTER_SANITIZE_STRING);

            $dados = [
                'solicitante' => $solicitante,
                'telefoneSolicitante' => $telefoneSolicitante ,
                'dataEventoAph' => $dataEventoAph,
                'horaEventoAph' => $horaEventoAph,
                'tipoServico' => $tipoServico,
                'publicoAlvo' => $publicoAlvo,
                'endereco' => $endereco,
                'cidade' => $cidade,
                'complemento' => $complemento,
                'observacao' => $observacao,

            ];

            $modelEventoAPH = EventoAPH::salvar($dados);


            echo $modelEventoAPH;
        }
    }

    public static function pesquisarForm()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/eventos_aph/templates/pesquisar-evento-aph.phtml');
    }

    public static function pesquisar()
    {
        if(isset($_POST) && !empty($_POST)){
            $solicitante  = filter_input(INPUT_POST, 'solicitante', FILTER_SANITIZE_STRING);
            $tipoServico = filter_input(INPUT_POST, 'tipo-servico', FILTER_SANITIZE_STRING);
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $inicio_BD = \DateTime::createFromFormat('d/m/Y', $inicio)->format('Y-m-d');
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $fim_BD = \DateTime::createFromFormat('d/m/Y', $fim)->format('Y-m-d');
            $dados = EventoAPH::getPreCadastroByFilters($solicitante,$tipoServico,$inicio_BD,$fim_BD);
            $lists = self::getLists();
            $dadosEvento = [];
            $i = 0;
            foreach($dados as $d){
                $cidade = Cidade::getById($d['cidade_id']);
                $usuario = Usuario::getById($d['criado_por']);
                $dadosEvento[$i] = $d;
                $dadosEvento[$i]['descricao_tipo_servico'] = $lists['tipo_servico'][$d['tipo_servico']];
                $dadosEvento[$i]['descricao_publico_alvo'] = $lists['publico_alvo'][$d['publico_alvo']];
                $dadosEvento[$i]['descricao_cidade'] = $cidade['NOME']." (".$cidade['UF'].")";
                $dadosEvento[$i]['data_BR'] = \DateTime::createFromFormat('Y-m-d', $d['data'])->format('d/m/Y');
                $dadosEvento[$i]['nome_criador'] =  $usuario['nome'];

                $i++;



            }



            require($_SERVER['DOCUMENT_ROOT'] . '/eventos_aph/templates/pesquisar-evento-aph.phtml');
        }
    }

    public static function finalizarForm()
    {
        if(isset($_GET) && !empty($_GET)){
            $id = $_GET['id'];

            $lists = self::getLists();
            $dados = EventoAPH::getById($id);
            $dados = $dados[0];

            $cidade = Cidade::getById($dados['cidade_id']);
            $usuario = Usuario::getById($dados['criado_por']);
            $dados['descricao_tipo_servico'] = $lists['tipo_servico'][$dados['tipo_servico']];
            $dados['descricao_publico_alvo'] = $lists['publico_alvo'][$dados['publico_alvo']];
            $dados['descricao_cidade'] = $cidade['NOME']." (".$cidade['UF'].")";

            $dados['data_BR'] = \DateTime::createFromFormat('Y-m-d', $dados['data'])->format('d/m/Y');
            $contasBancarias = $_SESSION['empresa_principal'] == 1 ? ContaBancaria::getAll() : ContaBancaria::getContasBancariasByUR($_SESSION['empresa_principal']);

            $dados['nome_criador'] =  $usuario['nome'];
            $fornecedores = Fornecedor::getAll();
            $empresas = Filial::getUnidadesAtivas();

            require($_SERVER['DOCUMENT_ROOT'] . '/eventos_aph/templates/finalizar-form.phtml');
        }
    }





    public static function cancelar()
    {
        if(isset($_POST['id']) && !empty($_POST['id']) && is_int(intval($_POST['id']))) {
            $id = $_POST['id'];
            $response = EventoAPH::cancelarById($id);
            return $response;
        }
    }



    public static function finalizar()
    {
        $fornecedor = [];
        $response = [];
        if(isset($_POST) && !empty($_POST)){
            $fornecedor_cliente = filter_input(INPUT_POST, 'fornecedor_cliente', FILTER_SANITIZE_NUMBER_INT);
            $fornecedorClienteNovo = 0;
            if($fornecedor_cliente == '') {
                $fornecedor['fisica_juridica'] = filter_input(INPUT_POST, 'fisica_juridica', FILTER_SANITIZE_STRING);
                $fornecedor['cpf'] = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING);
                $fornecedor['cnpj'] = filter_input(INPUT_POST, 'cnpj', FILTER_SANITIZE_STRING);
                $fornecedor['nome_fantasia'] = filter_input(INPUT_POST, 'nome_fantasia', FILTER_SANITIZE_STRING);
                $fornecedor['razao_social'] = filter_input(INPUT_POST, 'razao_social', FILTER_SANITIZE_STRING);
                $fornecedor['ies_rg'] = filter_input(INPUT_POST, 'ies_rg', FILTER_SANITIZE_STRING);
                $fornecedor['im'] = filter_input(INPUT_POST, 'im', FILTER_SANITIZE_STRING);
                $fornecedor['contato'] = filter_input(INPUT_POST, 'contato', FILTER_SANITIZE_STRING);
                $fornecedor['telefone'] = filter_input(INPUT_POST, 'telefone', FILTER_SANITIZE_STRING);
                $fornecedor['endereco'] = filter_input(INPUT_POST, 'endereco', FILTER_SANITIZE_STRING);
                $fornecedor['cidade'] = filter_input(INPUT_POST, 'id_cidade_cliente', FILTER_SANITIZE_STRING);
                $fornecedor['email'] = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);

                if($fornecedor['fisica_juridica']  == 1 ){
                    $responseCnpj = Fornecedor::checkIfExistsFornecedorByCNPJ($fornecedor['cnpj'],'C');
                    if(!empty($responseCnpj)){

                        echo "Já exite Cliente cadastrado com esse CNPJ {$fornecedor['cnpj']}, no Sitema.";
                        return;

                    }

                }else{
                    $responseCpf = Fornecedor::checkIfExistsFornecedorByCPF($fornecedor['cpf'],'C');
                    if(!empty($responseCpf)){

                        echo "Já exite Cliente cadastrado com esse CPF {$fornecedor['cpf']}, no Sitema.";
                        return;

                    }


                }
                $fornecedor_cliente = Fornecedor::createFornecedorByRemocao($fornecedor);
                $fornecedorClienteNovo = 1;
            }

            $idEventoAPH = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $data_termino         = filter_input(INPUT_POST, 'data-termino', FILTER_SANITIZE_STRING);
            $data_termino           =   \DateTime::createFromFormat('d/m/Y', $data_termino )->format('Y-m-d');
            $hora_termino         = filter_input(INPUT_POST, 'hora-termino', FILTER_SANITIZE_STRING);
            $valor              = filter_input(INPUT_POST, 'valor', FILTER_SANITIZE_STRING);
            $outros_custos      = filter_input(INPUT_POST, 'outros_custos', FILTER_SANITIZE_STRING);
            $medico             = filter_input(INPUT_POST, 'medico', FILTER_SANITIZE_STRING);
            $valor_medico       = filter_input(INPUT_POST, 'valor-medico', FILTER_SANITIZE_STRING);
            $tecnico            = filter_input(INPUT_POST, 'tecnico', FILTER_SANITIZE_STRING);
            $valor_tecnico      = filter_input(INPUT_POST, 'valor-tecnico', FILTER_SANITIZE_STRING);
            $condutor           = filter_input(INPUT_POST, 'condutor', FILTER_SANITIZE_STRING);
            $valor_condutor     = filter_input(INPUT_POST, 'valor-condutor', FILTER_SANITIZE_STRING);
            $recebido           = filter_input(INPUT_POST, 'recebido', FILTER_SANITIZE_STRING);
            $forma_pagamento    = filter_input(INPUT_POST, 'forma_pagamento', FILTER_SANITIZE_STRING);
            $observacao         = filter_input(INPUT_POST, 'observacao', FILTER_SANITIZE_STRING);
            $empresa          = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_NUMBER_INT);
            $enfermeiro           = filter_input(INPUT_POST, 'enfermeiro', FILTER_SANITIZE_STRING);
            $valor_enfermeiro      = filter_input(INPUT_POST, 'valor-enfermeiro', FILTER_SANITIZE_STRING);
            $seraCobrada = filter_input(INPUT_POST, 'sera_cobrada', FILTER_SANITIZE_STRING);
            $justificativaNaoCobrar = filter_input(INPUT_POST, 'justificativa_nao_cobrar', FILTER_SANITIZE_STRING);
            $contaRecebido =   filter_input(INPUT_POST, 'conta_recebido', FILTER_SANITIZE_STRING);
            $recebidoEm         = filter_input(INPUT_POST, 'recebido_em', FILTER_SANITIZE_STRING);

            $itens              = [
                'catalogo_id' => $_POST['catalogo_id'],
                'tipo' => $_POST['tipo'],
                'tiss' => $_POST['tiss'],
                'qtd' => $_POST['qtd'],
            ];



            $x = EventoAPH::finalizar(
                $idEventoAPH,
                $fornecedor_cliente,
                $data_termino,
                $hora_termino,
                $valor,
                $outros_custos,
                $medico,
                $valor_medico,
                $tecnico,
                $valor_tecnico,
                $condutor,
                $valor_condutor,
                $recebido,
                $forma_pagamento,
                $observacao,
                $itens,
                $empresa,
                $enfermeiro,
                $valor_enfermeiro,
                $seraCobrada,
                $justificativaNaoCobrar,
                $contaRecebido,
                $recebidoEm
            );

            // $fornecedorClienteNovo recebe 1 caso tenha sido inserido agora, recurso para fazer o roolback



            if($x != 1 && $fornecedorClienteNovo == 1){
                $f = Fornecedor::deleteByID($fornecedor_cliente);

            }

            if($x == 1 ){
                $eventoAPH = EventoAPH::getById($idEventoAPH); 
                $solicitante = $eventoAPH[0]['solicitante'];
                $tipoServico = $eventoAPH[0]['tipo_servico'] == 'APH' ? 'APH' : 'Evento';
                $nomeUsuario = $_SESSION['nome_user'];
                $dataEvento = \DateTime::createFromFormat('Y-m-d', $eventoAPH[0]['data'])->format('d/m/Y');
                $finalizadoEm = \DateTime::createFromFormat('Y-m-d H:i:s', $eventoAPH[0]['finalizado_em'])->format('d/m/Y');
                $titulo = "Aviso de Evento/APH finalizado";
                $texto = "<h2 style='text-align:center'><strong>Aviso de Finaliza&ccedil;&atilde;o de Evento/APH</strong></h2>

<p>&nbsp;</p>

<h2>O evento/aph de c&oacute;digo&nbsp;<strong>#$idEventoAPH</strong>&nbsp;foi <strong>finalizado</strong> por <strong>$nomeUsuario</strong> em <strong>$finalizadoEm</strong>.&nbsp;</h2>

<h2>&nbsp;</h2>

<h3><strong>#INFORMA&Ccedil;&Otilde;ES COMPLEMENTARES:</strong><br />
&nbsp;</h3>

<h3><strong>Solicitado por:</strong> $solicitante </h3>

<h3><strong>Tipo:</strong> $tipoServico </h3>

<h3><strong>Data do evento:</strong> $dataEvento </h3>;";

                $para[] = ['email' => 'lancamentos@mederi.com.br', 'nome'=> 'Lancamentos'];
                $para[] = ['email' => 'financeiro02@mederi.com.br', 'nome'=> 'Financeiro 02'];
                SendGrid::enviarEmailSimples($titulo, $texto, $para);
            }

            echo $x;
        }
    }

    public static function visualizar()
    {
        if(isset($_GET) && !empty($_GET)){
            $id = $_GET['id'];

                $dados = EventoAPH::getCadastroCompletoById($id);
                $lists = self::getLists();
                $dadosEvento = [];
                $i = 0;
                foreach($dados as $d){
                    $cidade = Cidade::getById($d['cidade_id']);
                    $usuario = Usuario::getById($d['criado_por']);
                    $dadosEvento = $d;
                    $dadosEvento['descricao_tipo_servico'] = $lists['tipo_servico'][$d['tipo_servico']];
                    $dadosEvento['descricao_publico_alvo'] = $lists['publico_alvo'][$d['publico_alvo']];
                    $dadosEvento['descricao_sera_cobrada'] = $lists['sim_nao'][$d['sera_cobrada']];
                    $dadosEvento['descricao_cidade'] = $cidade['NOME']." (".$cidade['UF'].")";
                    $dadosEvento['data_BR'] = \DateTime::createFromFormat('Y-m-d', $d['data'])->format('d/m/Y');
                    $dadosEvento['data_termino_BR'] = \DateTime::createFromFormat('Y-m-d', $d['data_termino'])->format('d/m/Y');
                    $dadosEvento['data_recebido_BR'] = !empty($d['recebido_em']) ? \DateTime::createFromFormat('Y-m-d', $d['recebido_em'])->format('d/m/Y') : '';
                    $dadosEvento['nome_criador'] =  $usuario['nome'];
                    $dadosFornecedor = Fornecedor::getById($d['fornecedor_cliente']);
                }

                $resultContaBancaria = ContaBancaria::getById($dados[0]['conta_recebido']);

                $itens = EventoAPH::getItensEventosAPHById($id);
                $empresa = '';
                if(!empty($dadosEvento['empresa_id'])){
                    $empresa = Filial::getById($dadosEvento['empresa_id']);
                }
            $dados = $dadosEvento;

            }


            require($_SERVER['DOCUMENT_ROOT'] . '/eventos_aph/templates/visualizar-evento-aph.phtml');

    }

    public static function relatorioFinalizadosForm()
    {
        $empresas = Filial::getUnidadesAtivas();
        $fornecedores = Fornecedor::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/eventos_aph/templates/relatorios/relatorio-eventos-aph-realizados.phtml');
    }

    public static function buscarRelatorioEventosAPHFinalizados()
    {
        if(isset($_POST) && !empty($_POST)){

            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $inicio_BD = \DateTime::createFromFormat('d/m/Y', $inicio)->format('Y-m-d');
            $fim_BD = \DateTime::createFromFormat('d/m/Y', $fim)->format('Y-m-d');
            $fornecedorCliente = filter_input(INPUT_POST, 'fornecedor_cliente', FILTER_SANITIZE_STRING);
            $empresaId = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_STRING);
            $tipoServico = filter_input(INPUT_POST, 'tipo-servico', FILTER_SANITIZE_STRING);
            $empresas = Filial::getUnidadesAtivas();
            $fornecedores = Fornecedor::getAll();
            $dados = EventoAPH::getEventosAPHFinalizados($inicio_BD, $fim_BD, $fornecedorCliente, $empresaId,$tipoServico);
            $lists = self::getLists();
            require($_SERVER['DOCUMENT_ROOT'] . '/eventos_aph/templates/relatorios/relatorio-eventos-aph-realizados.phtml');
        }
    }

    public static function relatorioFinalizadosExcel()
    {
        if(isset($_GET) && !empty($_GET)){
            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $fornecedorCliente = filter_input(INPUT_GET, 'fornecedor_cliente', FILTER_SANITIZE_STRING);
            $empresaId = filter_input(INPUT_GET, 'empresa', FILTER_SANITIZE_STRING);
            $tipoServico = filter_input(INPUT_GET, 'tipo-servico', FILTER_SANITIZE_STRING);

            $filterFornecedorCliente = "TODOS";
            if(!empty($fornecedorCliente)){
                $resultFornecedor = Fornecedor::getById($fornecedorCliente);
                $filterFornecedorCliente =  !empty($resultFornecedor['nome']) ? $resultFornecedor['nome'] : $resultFornecedor['razaoSocial'];

            }
            $filterEmpresa = 'TODAS';
            if(!empty($empresaId)){
                $responseEmpresa = Filial::getEmpresaById($empresaId);
                $filterEmpresa = $responseEmpresa['nome'];

            }

            $lists = self::getLists();
            $filterTipoServico = 'TODOS';
            if(!empty($tipoServico)){
                $filterTipoServico = $lists['tipo_servico'][$tipoServico];
            }


            $dados = EventoAPH::getEventosAPHFinalizados($inicio,$fim, $fornecedorCliente, $empresaId, $tipoServico);


            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $inicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $fim);
            $filename = sprintf(
                "relatorio_eventos_aph_finalizados_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require($_SERVER['DOCUMENT_ROOT'] . '/eventos_aph/templates/relatorios/relatorio-eventos-aph-realizados-excel.phtml');
        }
    }

//    public static function enviarEmailSimples($titulo, $texto, $para)
//    {
//        $api_data = (object) Config::get('sendgrid');
//
//        $gateway = new MailAdapter(
//            new SendGridGateway(
//                new \SendGrid($api_data->user, $api_data->pass),
//                new \SendGrid\Email()
//            )
//        );
//
//        $gateway->adapter->sendSimpleMail($titulo, $texto, $para);
//    }
}

