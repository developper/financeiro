<?php

namespace app\Controllers;

use \App\Models\Administracao\Filial;
use App\Models\Administracao\Paciente;
use \App\Models\Enfermagem\Evolucao;
use App\Models\Enfermagem\Feridas;
use App\Models\Enfermagem\ProcedimentosGtm;

class Enfermagem
{
	public static function formRelatorioEvolucoes()
	{
        $urs = Filial::getAll();
		require ($_SERVER['DOCUMENT_ROOT'] . '/enfermagem/relatorios/templates/relatorio-evolucoes-form.phtml');
	}

    public static function relatorioEvolucoes()
    {
        if(isset($_POST) && !empty($_POST)){
            $periodoInicio = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $urBusca = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'ur'            => $_SESSION['empresa_principal'] == '11' || $_SESSION['empresa_principal'] == '1' ? $urBusca : $_SESSION['empresa_principal']
            ];

            $response 	= Evolucao::getRelatorioEvolucoes($filtros);
            $urs        = Filial::getAll();
            $count      = count($response);

            require ($_SERVER['DOCUMENT_ROOT'] . '/enfermagem/relatorios/templates/relatorio-evolucoes-form.phtml');
        }
    }

    public static function excelRelatorioEvolucoes()
    {
        if(isset($_GET) && !empty($_GET)){
            $periodoInicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $urBusca = filter_input(INPUT_GET, 'ur', FILTER_SANITIZE_NUMBER_INT);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'ur'            => $_SESSION['empresa_principal'] == '11' || $_SESSION['empresa_principal'] == '1' ? $urBusca : $_SESSION['empresa_principal']
            ];

            $response 	= Evolucao::getRelatorioEvolucoes($filtros);
            $count      = count($response);

            $extension = '.xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = '.xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $periodoInicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $periodoFim);
            $filename = sprintf(
                "relatorio_evolucoes_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require ($_SERVER['DOCUMENT_ROOT'] . '/enfermagem/relatorios/templates/relatorio-evolucoes-excel.phtml');
        }
    }

    public static function listarProcedimentosGtm()
    {
        $idPaciente = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
        $cabecalho = Paciente::getCabecalho($idPaciente);
        $response = ProcedimentosGtm::getProcedimentosGtmByPacienteId($idPaciente);
        $count = count($response);
        require $_SERVER['DOCUMENT_ROOT'] . '/enfermagem/relatorios/templates/procedimentos-gtm/procedimentos-gtm-list.phtml';
    }

    public static function viewProcedimentosGtm()
    {
        $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        $response = ProcedimentosGtm::getByIdPrescricao($id);
        $cabecalho = Paciente::getCabecalho($response[0]['paciente']);
        $tipos = [
            '1' => 'BUTTON',
            '2' => 'SONDA DE FOLEY',
            '3' => 'SONDA DE GTM'
        ];
        $motivos = [
            '1' => 'MANUSEIO INDEVIDO',
            '2' => 'ALTERAÇÃO CLÍNICA DO PACIENTE',
            '3' => 'TRAÇÃO',
            '4' => 'OUTRO'
        ];
        require $_SERVER['DOCUMENT_ROOT'] . '/enfermagem/relatorios/templates/procedimentos-gtm/procedimentos-gtm-view.phtml';
    }

    public static function editarProcedimentosGtm()
    {
        $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        $response = ProcedimentosGtm::getByIdPrescricao($id);
        $cabecalho = Paciente::getCabecalho($response[0]['paciente']);
        require $_SERVER['DOCUMENT_ROOT'] . '/enfermagem/relatorios/templates/procedimentos-gtm/procedimentos-gtm-edit.phtml';
    }

    public static function atualizarProcedimentosGtm()
    {
        foreach ($_POST as $key => $value) {
            foreach ($value as $k => $v) {
                $data[$k][$key] = $v;
            }
        }

        $response = ProcedimentosGtm::updateProcedimentosGtm($data);

        if($response) {
            $msg = "Procedimento atualizado com sucesso!";
            $type = 's';
            header ("Location: /enfermagem/relatorios/?action=procedimentos-gtm&paciente=" . $data[key($data)]['paciente'] . "&msg=" . base64_encode ($msg) . "&type=" . $type);
        } else {
            $msg = "Houve um problema ao atualizar os dados desse procedimento!";
            $type = 'f';
            header ("Location: /enfermagem/relatorios/?action=procedimentos-gtm&paciente=" . $data[key($data)]['paciente'] . "&msg=" . base64_encode ($msg) . "&type=" . $type);
        }
    }

    public static function ultimaEvolucaoPaciente()
    {
        if(isset($_GET) && !empty($_GET)){
            $idPaciente = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $evolucao = Evolucao::getUltimoRelatorioPaciente($idPaciente);
            echo json_encode($evolucao);
        }
    }

    public static function feridasUltimaEvolucaoPaciente()
    {
        if(isset($_GET) && !empty($_GET)){
            $idPaciente = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $evolucao = Evolucao::getUltimoRelatorioPaciente($idPaciente);
            $feridas = Feridas::getFeridasByEvolucao($evolucao['ID'], 5);
            echo json_encode($feridas);
        }
    }
}