<?php

namespace app\Controllers;

use App\Models\Administracao\Filial;
use App\Models\Administracao\Planos;
use App\Models\Enfermagem\Aditivo;
use App\Models\Enfermagem\Alta;
use App\Models\Enfermagem\Avaliacao;
use App\Models\Enfermagem\Deflagrado;
use App\Models\Enfermagem\Evolucao;
use App\Models\Enfermagem\PrescricaoCurativo;
use App\Models\Enfermagem\PrescricaoEnfermagem;
use App\Models\Enfermagem\Prorrogacao;
use App\Models\Medico\Evolucao as EvolucaoMedica;
use App\Models\Medico\Avaliacao as AvaliacaoMedica;
use App\Models\Medico\Deflagrado as DeflagradoMedico;
use App\Models\Medico\Intercorrencia;
use App\Models\Medico\Prorrogacao as ProrrogacaoMedica;
use App\Models\Medico\Alta as AltaMedica;
use App\Models\Medico\Prescricao;
use App\Models\Medico\Aditivo as AditivoMedico;

class RelatoriosMedicoEnfermagemUr
{
    private static function getLists()
    {
        return [
            'tipo_relatorio' => [
                'ADT'   => 'Aditivo',
                'AL'    => 'Alta',
                'AV'    => 'Avaliação',
                'DEF'   => 'Deflagrado',
                'EV'    => 'Evolução',
                'INT'   => 'Intercorrência',
                'PRE'   => 'Prescrição',
                'PRO'   => 'Prorrogação'
            ],
            'area' => ['M' => 'Médico', 'E' => 'Enfermagem', 'N' => 'Nutrição'],

        ];
    }
    public static function form(){
        $lista= self::getLists();
        $empresas = Filial::getUnidadesAtivas();
        $planos =Planos::getPlanosAtivos();
        require($_SERVER['DOCUMENT_ROOT'] . '/relatorios/templates/medico-enfermagem-ur/form.phtml');
    }

    public static function pesquisar()
    {
        session_start();
        $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
        $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
        $area = filter_input(INPUT_POST, 'area', FILTER_SANITIZE_STRING);
        $empresa = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_STRING);
        $tipo_relatorio = isset($_POST['tipo']) ? $_POST['tipo'] : '';
        $plano = filter_input(INPUT_POST, 'plano', FILTER_SANITIZE_STRING);

        $data = [
            'inicio' => $inicio ,
            'fim' => $fim ,
            'area' => $area,
            'empresa' => $empresa ,
            'tipo' => $tipo_relatorio,
            'plano' => $plano
        ];

        $response ='';

        if(!empty($inicio) && !empty($fim) && !empty($area) && !empty($empresa) ){
            if($area == 'E'){
                $response = self::getRelatorioEnfermagem($data,$response,$tipo_relatorio);
            }elseif($area == 'M'){
                $response = self::getRelatorioMedico($data,$response,$tipo_relatorio);
            }else{
                $response = self::getRelatorioNutricao($data,$response,$tipo_relatorio);

            }
        }else{
            if($_SESSION['empresa_principal'] == 1){
                $erroEmpresa = 'e empresa';
            }
            $msgErro = "Os campos início, fim, área {$erroEmpresa} são obrigatórios.";
        }

        $lista= self::getLists();
        $empresas = Filial::getUnidadesAtivas();
        $planos =Planos::getPlanosAtivos();

        require($_SERVER['DOCUMENT_ROOT'] . '/relatorios/templates/medico-enfermagem-ur/form.phtml');
    }

    public static function getRelatorioEnfermagem($data,$response,$tipo_relatorio)
    {
        if(empty($tipo_relatorio)){
            $avaliacao = Avaliacao::getByUR($data);
            if(!empty($avaliacao))
                $response['avaliacao'] = $avaliacao;

            $evolucao = Evolucao::getByUR($data);
            if(!empty($evolucao))
                $response['evolucao'] = $evolucao;

            $prorrogacao = Prorrogacao::getByUR($data);
            if(!empty($prorrogacao))
                $response['prorrogacao'] = $prorrogacao;

            $alta = Alta::getByUR($data);
            if(!empty($alta))
                $response['alta'] = $alta;

            $aditivo = Aditivo::getByUR($data);
            if(!empty($aditivo))
                $response['aditivo'] = $aditivo;

            $deflagrado = Deflagrado::getByUR($data);
            if(!empty($deflagrado))
                $response['deflagrado'] = $deflagrado;

            $prescricao_curativo = PrescricaoCurativo::getByUR($data);
            if(!empty($prescricao_curativo))
                $response['prescricao_curativo'] = $prescricao_curativo;


            $prescricao_enf = PrescricaoEnfermagem::getByUR($data);
            if(!empty($prescricao_enf))
                $response['prescricao_enf'] = $prescricao_enf;

        }else{
            foreach($tipo_relatorio as $tipo){

                if($tipo == "AV" ) {
                    $avaliacao = Avaliacao::getByUR($data);
                    if(!empty($avaliacao))
                        $response['avaliacao'] = $avaliacao;

                }elseif($tipo == "EV"){
                    $evolucao = Evolucao::getByUR($data);
                    if(!empty($evolucao))
                        $response['evolucao'] = $evolucao;

                }elseif($tipo == 'PRO') {
                    $prorrogacao = Prorrogacao::getByUR($data);
                    if(!empty($prorrogacao))
                        $response['prorrogacao'] = $prorrogacao;

                }elseif($tipo == 'AL'){
                    $alta = Alta::getByUR($data);
                    if(!empty($alta))
                        $response['alta'] = $alta;

                }elseif($tipo == 'PRE'){
                    $prescricao_curativo = PrescricaoCurativo::getByUR($data);
                    if(!empty($prescricao_curativo))
                        $response['prescricao_curativo'] = $prescricao_curativo;


                    $prescricao_enf = PrescricaoEnfermagem::getByUR($data);
                    if(!empty($prescricao_enf))
                        $response['prescricao_enf'] = $prescricao_enf;

                }elseif($tipo == 'ADT'){
                    $aditivo = Aditivo::getByUR($data);
                    if(!empty($aditivo))
                        $response['aditivo'] = $aditivo;
                }elseif ($tipo == 'DEF'){
                    $deflagrado = Deflagrado::getByUR($data);
                    if(!empty($deflagrado))
                        $response['deflagrado'] = $deflagrado;
                }
            }
        }

        return $response;
    }

    public static function getRelatorioMedico($data, $response, $tipo_relatorio)
    {
        if(empty($tipo_relatorio)){
            $avaliacao = AvaliacaoMedica::getByUR($data);
            if(!empty($avaliacao))
                $response['avaliacao'] = $avaliacao;

            $evolucao = EvolucaoMedica::getByUR($data);
            if(!empty($evolucao))
                $response['evolucao'] = $evolucao;

            $prorrogacao = ProrrogacaoMedica::getByUR($data);
            if(!empty($prorrogacao))
                $response['prorrogacao'] = $prorrogacao;

            $alta = AltaMedica::getByUR($data);
            if(!empty($alta))
                $response['alta'] = $alta;

            $intercorrencia = Intercorrencia::getByUR($data);
            if(!empty($intercorrencia))
                $response['encaminhamento'] = $intercorrencia;

            $prescricao = Prescricao::getByUR($data);
            if(!empty($prescricao))
                $response['prescricao'] = $prescricao;

            $aditivo = AditivoMedico::getByUR($data,3);
            if(!empty($aditivo))
                $response['aditivo'] = $aditivo;

             $deflagrado = DeflagradoMedico::getByUR($data);
            if(!empty($deflagrado))
                $response['deflagrado'] = $deflagrado;

        }else{
            foreach($tipo_relatorio as $tipo){
                if($tipo == "AV" ) {
                    $avaliacao = AvaliacaoMedica::getByUR($data);
                    if(!empty($avaliacao))
                        $response['avaliacao'] = $avaliacao;

                }elseif($tipo == "EV"){
                    $evolucao = EvolucaoMedica::getByUR($data);
                    if(!empty($evolucao))
                        $response['evolucao'] = $evolucao;

                }elseif($tipo == 'PRO') {
                    $prorrogacao = ProrrogacaoMedica::getByUR($data);
                    if(!empty($prorrogacao))
                        $response['prorrogacao'] = $prorrogacao;

                }elseif($tipo == 'AL'){
                    $alta = AltaMedica::getByUR($data);
                    if(!empty($alta))
                        $response['alta'] = $alta;

                }elseif($tipo == 'INT'){
                    $intercorrencia = Intercorrencia::getByUR($data);
                    if(!empty($intercorrencia))
                        $response['encaminhamento'] = $intercorrencia;

                }elseif($tipo == 'PRE'){
                    $prescricao = Prescricao::getByUR($data,'1,2,3,4,7');
                    if(!empty($prescricao))
                        $response['prescricao'] = $prescricao;
                }elseif($tipo == 'ADT'){
                    $aditivo = AditivoMedico::getByUR($data,3);
                    if(!empty($aditivo))
                        $response['aditivo'] = $aditivo;
                }elseif ($tipo == 'DEF'){
                    $deflagrado = DeflagradoMedico::getByUR($data);
                    if(!empty($deflagrado))
                        $response['deflagrado'] = $deflagrado;
                }
            }
        }

        return $response;
    }

    public static function getRelatorioNutricao($data, $response, $tipo_relatorio)
    {
        foreach($tipo_relatorio as $tipo){
            if($tipo == 'PRE'){
                $prescricao = Prescricao::getByUR($data,'5,6');


                if(!empty($prescricao))
                    $response['prescricao'] = $prescricao;
            }
        }
        return $response;

    }

    public static function exportarExcel()
    {
        session_start();

        $_GET['tipo'] = explode('x',$_GET['tipo']);

        $inicio = $_GET['inicio'];
        $fim = $_GET['fim'];
        $area = $_GET['area'];
        $empresa = $_GET['ur'];
        $plano = $_GET['plano'];
        $tipo_relatorio = isset($_GET['tipo']) && !empty($_GET['tipo'][0]) ? $_GET['tipo'] : '';




        $data = [
            'inicio' => $inicio ,
            'fim' => $fim ,
            'area' => $area,
            'empresa' => $empresa ,
            'tipo' => $tipo_relatorio,
            'plano' => $plano
        ];




        $response ='';
        $lista= self::getLists();
        $empresas = Filial::getUnidadesAtivas();
        $planos =Planos::getPlanosAtivos();

        if(!empty($inicio) && !empty($fim) && !empty($area) && !empty($empresa) ){

            if($area == 'E'){
                $response = self::getRelatorioEnfermagem($data,$response,$tipo_relatorio);
            }elseif($area == 'M'){
                $response = self::getRelatorioMedico($data,$response,$tipo_relatorio);
            }else{
                $response = self::getRelatorioNutricao($data,$response,$tipo_relatorio);
            }

        }else{
            if($_SESSION['empresa_principal'] == 1){
                $erroEmpresa = 'e empresa';
            }

            $msgErro = "Os campos início, fim, área {$erroEmpresa} são obrigatórios.";
            require($_SERVER['DOCUMENT_ROOT'] . '/relatorios/templates/medico-enfermagem-ur/form.phtml');
        }
        $extension = '.xls';

        if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
            $extension = '.xlsx';
        }

        $inicioUS = \DateTime::createFromFormat('d/m/Y', $inicio);
        $fimUS =  \DateTime::createFromFormat('d/m/Y', $fim);
        if($empresa == 'T'){
            $urNome = 'Todos';
        }else{
            $ur = Filial::getById($empresa);
            $urNome = $ur['nome'];
        }

        if(empty($plano)){
            $labelPlano = "Todos";
        }else{
            $plano = Planos::getById($plano);
            $labelPlano = $plano['nome'];
        }

        $areaNome = $lista['area'][$area];
        $i=0;
        foreach($data['tipo'] as $t){
            $tipo[$i++] = $lista['tipo_relatorio'][$t];
        }
        $tipoNome = implode(', ',$tipo);
        if(empty($tipoNome)){
            $tipoNome = "Todos";
        }
        $dataHora = date('d/m/Y H:i');
        $usuario = $_SESSION['nome_user'];

       $filename = sprintf(
            "relatorio_assistencial_%s_%s.%s",
            $inicioUS->format('d-m-Y'),
            $fimUS->format('d-m-Y'),
            $extension
        );


        header("Content-type: application/x-msexce; charset=ISO-8859-1");
        header("Content-Disposition: attachment; filename={$filename}");
        header("Pragma: no-cache");

        require($_SERVER['DOCUMENT_ROOT'] . '/relatorios/templates/medico-enfermagem-ur/exportar-excel.phtml');

    }

}