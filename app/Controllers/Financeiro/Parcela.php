<?php
namespace app\Controllers\Financeiro;

use App\Models\Financeiro\Parcela AS ParcelaModel;

class Parcela
{
    public static function estornarParcela()
    {
        if(isset($_POST) && !empty($_POST)) {
            $parcela = filter_input(INPUT_POST, 'parcela', FILTER_SANITIZE_NUMBER_INT);
            $valor = filter_input(INPUT_POST, 'valor_estorno', FILTER_SANITIZE_STRING);
            $conta = filter_input(INPUT_POST, 'conta', FILTER_SANITIZE_NUMBER_INT);
            $dataAgendamento = filter_input(INPUT_POST, 'data_agendamento', FILTER_SANITIZE_STRING);
            $dataEstorno = filter_input(INPUT_POST, 'data_estorno', FILTER_SANITIZE_STRING);

            echo ParcelaModel::estornarParcelaFornecedores($parcela, $conta, $dataAgendamento, $dataEstorno, $valor);
        }
    }

    public static function desfazerEstornoParcela()
    {
        if(isset($_POST) && !empty($_POST)) {
            $estorno = filter_input(INPUT_POST, 'estorno_id', FILTER_SANITIZE_NUMBER_INT);

            echo ParcelaModel::desfazerEstornoParcela($estorno);
        }
    }
}
