<?php

namespace App\Controllers\Financeiro\TOTVS;

use App\Models\Financeiro\TOTVS\Arquivo;

class Integracao
{
    public static function index()
    {
        $filtros = [
            'inicio' => '2019-11-01',
            'fim' => '2019-11-30',
        ];
        require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/totvs/templates/totvs-form.phtml');
    }

    public static function pesquisarLancamentos()
    {
        if(isset($_POST) && !empty($_POST)) {
            $filtros = [];
            $filtros['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $filtros['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $filtros['tipo'] = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING);

            $arquivo = new Arquivo();
            $response = $arquivo->createTextFromData($filtros);

            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);

            $filename = sprintf(
                "TOTVS-AssisteVida_%s_%s",
                $inicio,
                $fim
            );

            header("Content-type: application/text; charset=utf-8");
            header("Content-Disposition: attachment; filename={$filename}.txt");
            header("Pragma: no-cache");

            echo $response;
        }
    }
}
