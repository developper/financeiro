<?php

namespace app\Controllers;



use App\Models\Administracao\Paciente;
use App\Models\Faturamento\Fatura;
use App\Models\Faturamento\PlanoSaude;
use \App\Models\Financeiro\Financeiro as F;
use \App\Models\Administracao\Filial;
use \App\Models\Financeiro\Fornecedor;

class Financeiro
{
	public static function formRelatorioRetencoes()
	{
        $fornecedores = Fornecedor::getAll();
        $urs = Filial::getAll();
		require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/relatorio-retencoes-form.phtml');
	}

	public static function relatorioRetencoes()
	{
		if(isset($_POST) && !empty($_POST)){
            $periodoInicio = filter_input(INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_POST, 'periodo_fim', FILTER_SANITIZE_STRING);
            $fornecedorBusca = filter_var($_POST['filters']['fornecedor'], FILTER_SANITIZE_NUMBER_INT);
            $urBusca = filter_var($_POST['filters']['ur'], FILTER_SANITIZE_NUMBER_INT);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'fornecedor'    => $fornecedorBusca,
                'ur'            => $urBusca
            ];

            $response 	= F::getRelatorioRetencoes($filtros);

            $fornecedores = Fornecedor::getAll();
            $urs = Filial::getAll();
			require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/relatorio-retencoes-form.phtml');
		}
	}

	public static function excelRelatorioRetencoes()
	{
		if(isset($_GET) && !empty($_GET)){
            $periodoInicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
			$fornecedor = filter_input(INPUT_GET, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
			$ur = filter_input(INPUT_GET, 'ur', FILTER_SANITIZE_NUMBER_INT);

			$filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'fornecedor'    => $fornecedor,
                'ur'            => $ur
            ];

			$response 	= F::getRelatorioRetencoes($filtros);

            $inicio = \DateTime::createFromFormat('Y-m-d', $periodoInicio);
            $fim = \DateTime::createFromFormat('Y-m-d', $periodoFim);
            $filename = sprintf(
                "relatorio_retencoes_%s_%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y')
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}.xls");
            header("Pragma: no-cache");

			require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/relatorio-retencoes-excel.phtml');
		}
	}

    public static function formRelatorioSimulacoes()
    {
        $pacientes = Paciente::getAll();
        $convenios = PlanoSaude::getAll();
        $urs = Filial::getAll();
        require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/relatorio-simulacoes-realizadas.phtml');
    }

    public static function relatorioSimulacoes()
    {
        if(isset($_POST) && !empty($_POST)){
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $pacienteBusca = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $convenioBusca = filter_input(INPUT_POST, 'convenio', FILTER_SANITIZE_NUMBER_INT);
            $urBusca = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'paciente'      => $pacienteBusca,
                'convenio'      => $convenioBusca,
                'ur'            => $urBusca,
            ];

            $data = F::getRelatorioSimulacoes($filtros);



            $response = [];
            $simulacaoId = 0;
            $valorTotal = 0;
            $custoTotal = 0;
            foreach ($data as $row) {
                if($row['simulacao_id'] != $simulacaoId){
                    if($simulacaoId != 0) {
                        $response[$simulacaoId]['valor_total'] = $valorTotal;
                        $response[$simulacaoId]['custo_total'] = $custoTotal;
                    }
                    $simulacaoId = $row['simulacao_id'];
                    $valorTotal = 0;
                    $custoTotal = 0;
                }
                $response[$simulacaoId]['paciente'] = $row['paciente'];
                $response[$simulacaoId]['convenio'] = $row['convenio'];
                $response[$simulacaoId]['ur'] = $row['ur'];
                $response[$simulacaoId]['fatura'] = $row['fatura'];
                $quantidade = $row['qtd'];
                $custoItemCalculo = $row['custo'];
                $desconto_acrescimo = ($quantidade * $row['preco']) * ($row['acrescimo_desconto'] / 100);
                $valorTotal += ($quantidade * $row['preco']) + $desconto_acrescimo;
                $custoTotal += $quantidade * $custoItemCalculo;
            }
            $response[$simulacaoId]['valor_total'] = $valorTotal;
            $response[$simulacaoId]['custo_total'] = $custoTotal;

            $data = $response;

            $response = [];
            $pissCofins = 4.73;
            $ir = 2;
            $i = 0;
            if(count(current($data)) > 2) {
                foreach ($data as $row) {
                    if ($i == 0) {
                        $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['laranja'] = 0;
                        $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['vermelho'] = 0;
                        $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['verde'] = 0;
                    }
                    $iss = (Fatura::getIssByFatura($row['fatura']));
                    if ($row['valor_total'] > 0) {
                        $custoTotal = ($row['valor_total'] * ($pissCofins + $ir + $iss['imposto_iss']) / 100) + $row['custo_total'];
                        $lucroReais = $row['valor_total'] - $custoTotal;
                        $lucroPorcento = ($row['valor_total'] - $custoTotal) / $row['valor_total'];
                        $lucroPorcento *= 100;
                        if ($lucroPorcento < 25 && $lucroReais < 1000) {
                            $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['vermelho']++;
                        } elseif ($lucroPorcento >= 25 && $lucroReais >= 1000) {
                            $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['verde']++;
                        } elseif ($lucroPorcento >= 25 || $lucroReais >= 1000) {
                            $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['laranja']++;
                        }
                        $i++;
                    }
                }
            }

            $pacientes = Paciente::getAll();
            $convenios = PlanoSaude::getAll();
            $urs = Filial::getAll();
            require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/relatorio-simulacoes-realizadas.phtml');
        }
    }

    public static function excelRelatorioSimulacoes()
    {
        if(isset($_GET) && !empty($_GET)){
            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $pacienteBusca = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $convenioBusca = filter_input(INPUT_GET, 'convenio', FILTER_SANITIZE_NUMBER_INT);
            $urBusca = filter_input(INPUT_GET, 'ur', FILTER_SANITIZE_NUMBER_INT);

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'paciente'      => $pacienteBusca,
                'convenio'      => $convenioBusca,
                'ur'            => $urBusca,
            ];

            $data = F::getRelatorioSimulacoes($filtros);

            $response = [];
            $simulacaoId = 0;
            $valorTotal = 0;
            $custoTotal = 0;
            foreach ($data as $row) {
                if($row['simulacao_id'] != $simulacaoId){
                    if($simulacaoId != 0) {
                        $response[$simulacaoId]['valor_total'] = $valorTotal;
                        $response[$simulacaoId]['custo_total'] = $custoTotal;
                    }
                    $simulacaoId = $row['simulacao_id'];
                    $valorTotal = 0;
                    $custoTotal = 0;
                }
                $response[$simulacaoId]['paciente'] = $row['paciente'];
                $response[$simulacaoId]['convenio'] = $row['convenio'];
                $response[$simulacaoId]['ur'] = $row['ur'];
                $response[$simulacaoId]['fatura'] = $row['fatura'];
                $quantidade = $row['qtd'];
                $custoItemCalculo = $row['custo'];
                $desconto_acrescimo = ($quantidade * $row['preco']) * ($row['acrescimo_desconto'] / 100);
                $valorTotal += ($quantidade * $row['preco']) + $desconto_acrescimo;
                $custoTotal += $quantidade * $custoItemCalculo;
            }
            $response[$simulacaoId]['valor_total'] = $valorTotal;
            $response[$simulacaoId]['custo_total'] = $custoTotal;

            $data = $response;

            $response = [];
            $pissCofins = 4.73;
            $ir = 2;
            $i = 0;
            foreach ($data as $row) {
                if($i == 0) {
                    $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['laranja'] = 0;
                    $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['vermelho'] = 0;
                    $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['verde'] = 0;
                }
                $iss = (Fatura::getIssByFatura($row['fatura']));
                if($row['valor_total'] > 0) {
                    $custoTotal = ($row['valor_total'] * ($pissCofins + $ir + $iss['imposto_iss']) / 100) + $row['custo_total'];
                    $lucroReais = $row['valor_total'] - $custoTotal;
                    $lucroPorcento = ($row['valor_total'] - $custoTotal) / $row['valor_total'];
                    $lucroPorcento *= 100;
                    if ($lucroPorcento < 25 && $lucroReais < 1000) {
                        $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['vermelho']++;
                    } elseif ($lucroPorcento >= 25 && $lucroReais >= 1000) {
                        $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['verde']++;
                    } elseif ($lucroPorcento >= 25 || $lucroReais >= 1000) {
                        $response[$row['paciente'] . '-' . $row['convenio'] . '-' . $row['ur']]['laranja']++;
                    }
                    $i++;
                }
            }

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $inicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $fim);
            $filename = sprintf(
                "relatorio_simulacoes_realizadas_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/relatorio-simulacoes-realizadas-excel.phtml');
        }
    }

    public static function fornecedorEquipamentoMenu()
    {
        require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/fornecedor/templates/fornecedor-equipamento-menu.phtml');

    }
    public static function equipamentosFornecedorForm()
    {
        $fornecedores = Fornecedor::getFornecedoresEquipmentResponsible();
        require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/fornecedor/templates/equipamentos-fornecedor-form.phtml');
    }


    public static function gerenciarEmailFornecedorEquipamentoForm()
    {
        $fornecedores = Fornecedor::getFornecedoresEquipmentResponsible();
        require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/fornecedor/templates/gerenciar-email-fornecedor-equipamento-form.phtml');
    }

    public static function equipamentosFornecedorJSON()
    {
        $fornecedor = filter_input(INPUT_GET, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
        echo Fornecedor::getFornecedorEquipmentCosts($fornecedor);
    }

    public static function getEmailfornecedorEquipamentoJSON()
    {
        $fornecedor = filter_input(INPUT_GET, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
        echo Fornecedor::getEmailFornecedorEquipmento($fornecedor);
    }
    public static function salvarEmailFornecedorEquipamento()
    {

        $naoEhEmail ='';
        $erro = '';
        $fornecedor = filter_input(INPUT_POST, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
        $novoEmail = $_POST['novo-email'];
        foreach ($novoEmail as $email){
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $naoEhEmail .= $email."; ";
                }
        }

        if(!empty($naoEhEmail)){
            $erro = "Erro: Não segue o padrão de e-mail: ".$naoEhEmail;
            echo $erro;
            return;
        }

        if(empty($fornecedor) || empty($novoEmail)){
            $erro = "Erro: O campo fornecedor é obrigatório e no minimo um novo e-mail deve ser adicionado";
            echo $erro;
            return;
        }

        $response = Fornecedor::salvarEmailFornecedorEquipamento($fornecedor, $novoEmail);

        if($response == 1){
            echo Fornecedor::getEmailFornecedorEquipmento($fornecedor);
        }else{
            echo $response;
        }

    }

    public static function deletarEmailFornecedorEquipamento()
    {
        $fornecedorEquipamentoEmailId = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            if(empty($fornecedorEquipamentoEmailId)){
            echo "Erro: Não foi passado o Email para ser deletado.";
            return ;
            }
            $response = Fornecedor::deletarEmailFornecedorEquipamento($fornecedorEquipamentoEmailId);

            echo $response;

    }

    public static function salvarEquipamentosFornecedor()
    {
        $data = $_POST['dados'];
        $fornecedor = filter_input(INPUT_POST, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
        echo Fornecedor::saveFornecedorEquipmentCosts($data, $fornecedor);
    }

    public static function consultarCPFCNPJCliente()
    {
        if(
            (isset($_GET['cpf']) && $_GET['cpf'] != '') ||
            (isset($_GET['cnpj']) && $_GET['cnpj'] != '')
        ) {
            $CPFCNPJ = isset($_GET['cpf']) ?
                filter_input(INPUT_GET, 'cpf', FILTER_SANITIZE_STRING) :
                filter_input(INPUT_GET, 'cnpj', FILTER_SANITIZE_STRING);
            $CPFCNPJFiltrado = strpos($CPFCNPJ, '.') !== false ?
                str_replace('.', '', str_replace('-', '', str_replace('/', '', $CPFCNPJ))) :
                $CPFCNPJ;
            $response = Fornecedor::getFornecedorByCPFCNPJ($CPFCNPJ, $CPFCNPJFiltrado);
            echo isset($_GET['cpf']) ? $response['CPF'] : $response['cnpj'];
        }
    }

    
}
