<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 07/06/2017
 * Time: 08:25
 */

namespace App\Controllers;

//ini_set('display_errors',1);
//ini_set('display_startup_erros',1);
//error_reporting(E_ALL);


use App\Models\Financeiro\CustosExtras as ModelCustoExtras;



class CustosExtras
{
    public static function form()
    {
        $custosExtras = ModelCustoExtras::getAll();
        require ($_SERVER['DOCUMENT_ROOT'] . '/financeiros/custos-extras/templates/custos-extras-form.phtml');
    }

    public static function salvar()
    {
        if(isset($_POST) && !empty($_POST)) {
            $custo = filter_input(INPUT_POST, 'custo', FILTER_SANITIZE_STRING);
        }
        $response = ModelCustoExtras::salvar($custo);
    }
    public static function desativar()
    {
        if(isset($_POST) && !empty($_POST)) {
            $id = filter_input(INPUT_POST, 'idCusto',FILTER_SANITIZE_NUMBER_INT );
        }
        $response = ModelCustoExtras::desativarById($id);
    }

    public static function ativar()
    {
        if(isset($_POST) && !empty($_POST)) {
            $id = filter_input(INPUT_POST, 'idCusto', FILTER_SANITIZE_NUMBER_INT);
        }
        $response = ModelCustoExtras::ativarById($id);
    }

}