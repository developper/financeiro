<?php
namespace app\Controllers;

use \App\Models\Administracao\Filial;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\IPCF;
use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\Notas;
use App\Helpers\StringHelper;
use App\Models\Financeiro\ImportarContaPagarIW as ModelContaPagarIW;
use Zend\Stdlib\DateTime;
use App\Models\Financeiro\Assinatura;
use App\Helpers\DateHelper;

include_once('../../utils/codigos.php');

// ini_set('display_errors',1);

class ImportarContaPagarIW
{
    static $planoContasNaturezaIW = [
        // 2.0 Despesas operacionais -> 2.0.4 Material Aplicado           
        2090 => 38, // 2.0.4.01 materiais
        2091 => 39, // 2.0.4.02 Medicamentos
        2092 => 40, // 2.0.4.03 Dietas
        2093 => 41, // 2.0.4.04 Curativos
        2094 => 42, // 2.0.4.05 Fraldas
    ];
    private static $numeroCaracteresLinha = 642; // cada linha tem 642 caracteres 

    public static function index()
    {      ini_set('display_errors',1);
        $filiais = Filial::getUnidadesAtivas();
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/importar-conta-pagar-iw/templates/index.phtml');
    }

    public static function importarArquivo()
    {
        $filiais = Filial::getUnidadesAtivas();


        if(isset($_FILES) && !empty($_FILES['conta_pagar_iw']['size']) && !empty($_POST['empresa'])){
            // salvando arquivo ofx na pasta ofx do financeiro
            $responseMoveTxt = self::moveTxtToTemporaryPath($_FILES);
            $empresa = $_POST['empresa'];

            

            if(!empty($responseMoveTxt['acerto'])){
                // lendo arquivo ofx
                $nomeArquivo = $_FILES['conta_pagar_iw']['name'];
                $caminhoArquivo = $responseMoveTxt['acerto'];
                $responseGetTxt = self::getTxt($caminhoArquivo);                
                if(!empty($responseGetTxt['itensTxt'])){
                    $responseInsert = ModelContaPagarIW::inserirNotas($responseGetTxt['itensTxt'],$empresa);
                }
                unlink($caminhoArquivo);
            }else{
                $response['error'][] = $responseMoveTxt['erro'];
            }

            if(!empty($responseGetTxt['error'])){
                foreach($responseGetTxt['error'] as $erro){
                    $response['error'][] = $erro;
                }
            }
            if(!empty($responseGetTxt['jaCadastrada'])){
                foreach($responseGetTxt['jaCadastrada'] as $jaCadastrada){
                    $response['jaCadastrada'][] = $jaCadastrada;
                }
            }

            if(!empty($responseInsert['error'])){
                foreach($responseInsert['error'] as $erro){
                    $response['error'][] = $erro;
                }
            }

            if(!empty($responseInsert['insert'])){
                foreach($responseInsert['insert'] as $insert){
                    $response['insert'][] = $insert;
                }
            }

            if(!empty($responseMoveTxt['erro'])){
                $response['error'][] = $responseMoveTxt['erro'];
            }
        }else{
            if(empty($_POST['empresa'])){
                $response['error'][] = "Selecione a Filial antes de fazer Upload";
            }else{
                $response['error'][] = "Selecione um arquivo antes de fazer Upload";
            }

        }

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/importar-conta-pagar-iw/templates/index.phtml');
    }

    public static function moveTxtToTemporaryPath($file)
    {
        $errors = [];

        $path = $_SERVER['DOCUMENT_ROOT'] . '/financeiros/arquivos-iw/contas-receber/';
        $size = 1024 * 1024 *1024 * 1024 *1024 * 1024 * 2;
        $array['erro'] = '';
        $array['acerto'] = '';

        $errors[1] = 'O arquivo no upload é maior do que o permitido';
        $errors[2] = 'O arquivo ultrapassa o limite de tamanho especifiado';
        $errors[3] = 'O upload do arquivo foi feito parcialmente';
        $errors[4] = 'Não foi feito o upload do arquivo!';

        if ($file['conta_pagar_iw']['error'] != 0) {
            $array['erro'] = "Não foi possível fazer o upload, erro:<br />" . $errors[$file['conta_pagar_iw']['error']];
            return $array;
        }

        $ext = explode('.', $file['conta_pagar_iw']['name']);

        $extensao = end($ext);

        $type = $file['conta_pagar_iw']['type'];

        if ($extensao != 'txt' && $extensao != 'TXT' && $type != 'text/plain') {
            $array['erro'] = "Por favor, envie apenas arquivos com no formato .Txt!";
            return $array;
        }

        if ($size < $file['conta_pagar_iw']['size']) {
            $array['erro'] = "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
            return $array;
        }

        $agora= date('Y_m_d_H_i_s_');

        $name = $file['conta_pagar_iw']['name'];
        $fullPath = $path .$agora.$name;
        $tmp = $file['conta_pagar_iw']['tmp_name'];

       

        if (move_uploaded_file($tmp, $fullPath)) {
            $array['acerto'] = $fullPath;
            return $array;
        }

        $array['erro'] = "Não foi possível enviar o arquivo, tente novamente! Caso o erro persista, entre em contato com o setor de TI.";
        return $array['erro'];
    }

    public static function formatDate($data){

        $date = DateTime::createFromFormat('d/m/Y', $data);

        return  $date->format('Y-m-d');
    }

    public static function validarData($data){
        
         if(strlen(str_replace(" ", '',$data)) == 8){
            $data = \DateTime::createFromFormat('dmY', $data);
            return $data == false ? $data : $data->format('Y-m-d');
         }
             return null;
        
    }


    public static function getTxt($caminhoArquivo)
    {
        $data_fechamento_obj = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);

        // $responseFilial =  Filial::getEmpresas();
        // foreach($responseFilial as $resp){
        //     $codEmpresaIW[$resp['iw_id']] = $resp['id'];
        // }

        $responseAssinatura = Assinatura::getAll();
        // o arquivo Txt traz o codigo da dominio
        foreach($responseAssinatura as $resp){
            $codigoDominioAssinatura[$resp['CODIGO_DOMINIO']] = $resp['ID'];
        }
        $file = fopen($caminhoArquivo, "r");

        $header = true;
        $TxtToArray = [];
        $arrayErro = [];
        $contLinhas = 1;
        $auxFile = $file;
        $primeiroRegistro = 1;

        // verificando se arquivo é vazio.
        // if(!empty($primeiroRegistro)){


        while  (($txtLine = fgets($file)) !== FALSE) {
            $errorLinha = false;

            if(strlen($txtLine) >=  self::$numeroCaracteresLinha){
                $arrayParcelas = [];
                $arrayRateio= [];
            
            $linha['codNota'] = substr($txtLine, 0, 20);
            $linha['cpfCNPJFornecedor'] = substr($txtLine, 20, 14);
            $linha['codNota2'] = substr($txtLine, 34, 20);
            $linha['naoPrecisaImportar'] = substr($txtLine, 54, 11);
            $linha['dataEmissao'] = substr($txtLine, 65, 8);
            $linha['simbolo'] = substr($txtLine, 73, 3);
            $linha['valor'] = StringHelper::parseFloat(substr($txtLine, 76, 15));
            $linha['tipo'] = substr($txtLine, 91, 2);
            $linha['observacao'] = substr($txtLine, 93, 60);
            $linha['dataEntrada'] = substr($txtLine, 153, 8);
            $linha['validadeP1'] = substr($txtLine, 161, 8);
            $linha['valorP1'] = StringHelper::parseFloat(substr($txtLine, 169, 15));
            
            $linha['validadeP2'] = substr($txtLine, 184, 8);            
            $linha['valorP2'] = StringHelper::parseFloat(substr($txtLine, 192, 15));
            
            $linha['validadeP3'] = substr($txtLine, 207, 8);
            $linha['valorP3'] = StringHelper::parseFloat(substr($txtLine, 215, 15));
            
            $linha['validadeP4'] = substr($txtLine, 230, 8);
            $linha['valorP4'] = StringHelper::parseFloat(substr($txtLine, 238, 15));
            
            $linha['validadeP5'] = substr($txtLine, 253, 8);
            $linha['valorP5'] =  StringHelper::parseFloat(substr($txtLine, 261, 15));
            
            $linha['validadeP6'] = substr($txtLine, 276, 8);
            $linha['valorP6'] = StringHelper::parseFloat(substr($txtLine, 284, 15));
            
            $linha['codigoCurativo'] = substr($txtLine, 299, 10);
            $linha['valorCurativo'] = StringHelper::parseFloat(substr($txtLine, 309, 15));
            $arrayRateio[] = ['natureza' => $linha['codigoCurativo'],
            'valor' => $linha['valorCurativo']
            ];
            $linha['codigoFralda'] = substr($txtLine, 324, 10);
            $linha['valorFralda'] = StringHelper::parseFloat(substr($txtLine, 334, 15));
            $arrayRateio[] = ['natureza' => $linha['codigoFralda'],
            'valor' => $linha['valorFralda']
            ];
            $linha['codigoMateriais'] = substr($txtLine, 349, 10);
            $linha['valorMateriais'] = StringHelper::parseFloat(substr($txtLine, 359, 15));
            $arrayRateio[] = ['natureza' => $linha['codigoMateriais'],
            'valor' => $linha['valorMateriais']
            ];
            $linha['codigoMedicamento'] = substr($txtLine, 374, 10);
            $linha['valorMedicamento'] = StringHelper::parseFloat(substr($txtLine, 384, 15));
            $arrayRateio[] = ['natureza' => $linha['codigoMedicamento'],
            'valor' => $linha['valorMedicamento']
            ];
            $linha['codigoDieta'] = substr($txtLine, 399, 10);
            $linha['valorDieta'] = StringHelper::parseFloat(substr($txtLine, 409, 15));
            $arrayRateio[] = ['natureza' => $linha['codigoDieta'],
            'valor' => $linha['valorDieta']
            ];
            $linha['centroCusto'] = substr($txtLine, 424, 3);
            $linha['naoPrecisa1'] = substr($txtLine, 427, 7);
            $linha['portador'] = substr($txtLine, 434, 3);
            $linha['naoPrecisa2'] = substr($txtLine, 437, 3);
            $linha['descricao'] = substr($txtLine, 440, 202);  
            
            $arrayParcelas[] =['validade' => $linha['validadeP1'],
            'valor' => $linha['valorP1'] ];
            $arrayParcelas[] =['validade' => $linha['validadeP2'],
            'valor' => $linha['valorP2'] ];
            $arrayParcelas[] =['validade' => $linha['validadeP3'],
            'valor' => $linha['valorP3'] ];
            $arrayParcelas[] =['validade' => $linha['validadeP4'],
            'valor' => $linha['valorP4'] ];
            $arrayParcelas[] =['validade' => $linha['validadeP5'],
            'valor' => $linha['valorP5'] ];
            $arrayParcelas[] =['validade' => $linha['validadeP6'],
            'valor' => $linha['valorP6'] ];
            
            
           

           // verificando se a unidade regional do Txt existe no sistema.
            $cnpj = ltrim(rtrim($linha['cpfCNPJFornecedor']));
            $numeroDocumento = ltrim(rtrim($linha['codNota'])); 
            if(!array_key_exists($linha['centroCusto'], $codigoDominioAssinatura)){
                $arrayErro[] = "Linha {$contLinhas}; Num. doc. = {$numeroDocumento}; CNPJ/CPF = {$cnpj}. Erro: 
                                                não foi encontrada a assinatura para o código: {$txtLine[34]}.";
                $errorLinha = true;
            }
            // vericaando se fornecedor já existe no sistema
            $fornecedorCadastrado = Fornecedor::checkIfExistsFornecedorByCNPJ($cnpj, 'F');

            if(empty($fornecedorCadastrado)){
                $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ/CPF = {$cnpj}. Erro: 
                                            Fornecedor não possui cadastro no sistema.";

                $errorLinha = true;
            }else{
                $fornecedorCadastrado = Fornecedor::getById($fornecedorCadastrado);
            }

            // verificar se pode por cpf Fornecedor::checkIfExistsFornecedorByCPF($cnpj);
            $notaJaCadastrada = ModelContaPagarIW::verificarNFJaExiste($cnpj, $numeroDocumento);
            
            if(!empty($notaJaCadastrada)){
                $arrayJaCadastrada[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ/CPF = {$cnpj}.
                                Conta já possui cadastro no sistema TR {$notaJaCadastrada['idNotas']}.";

                $existeCadastro = true;
            }

            $dataEmissao = self::validarData($linha['dataEmissao']);
            if( $dataEmissao == false ||  $dataEmissao == null){
                $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ/CPF = {$cnpj}. Erro: 
                                Data da emissão não é válida.";
                $errorLinha = true;
            } else {
                
                $data_emissao_obj = \DateTime::createFromFormat('Y-m-d',  $dataEmissao);
                
                if($data_emissao_obj->format('Y-m-d') <= $data_fechamento_obj->format('Y-m-d')) {
                    $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ/CPF = {$cnpj}. Erro: 
                                            Data da emissão é menor que a data de fechamento do período: " . $data_fechamento_obj->format('d/m/Y');
                    $errorLinha = true;
                }
            }

            // gerando parcelas
            
                $parcelas = [];
                foreach($arrayParcelas as $p){
                    $dataParcela = self::validarData($p['validade']);
                                   

                    if($p['valor'] != 0){
                        // validando data fechamento  
                                           
                        if($dataParcela == false &&  $dataParcela == null){
                           
                            $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ/CPF = {$cnpj}. Erro: 
                            Data de Validade da Parcela não é válida.";
                                $errorLinha = true;
                        }else{
                            
                            $data_parcela_obj = \DateTime::createFromFormat('Y-m-d',  $dataParcela);
                         
                            if($data_parcela_obj <= $data_fechamento_obj) {
                              
                                $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ/CPF = {$cnpj}. Erro: 
                                                        Data da validade é menor que a data de fechamento do período: " . $data_fechamento_obj->format('d/m/Y');
                                $errorLinha = true;
                            }else{
                                $parcelas[] = [
                                    'vencimento' => $dataParcela,
                                    'valor' => $p['valor'],
                                    'vencimento_real'=>self::formatDate(DateHelper::diaUtilVencimentoReal($data_parcela_obj->format('d/m/Y')))
                                ];
                            } 
                        }                      
                       
                    }

                }

                if(empty($parcelas)){
                    $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ/CPF = {$cnpj}. Erro: 
                                            Registro sem parcela valida.";
                    $errorLinha = true;
                    
                }
               

                //Gerando rateio
                $valorTotal = $linha['valor'];
                $valorTotalRateio = 0;
                $auxMaiorRateio = 0;
                $naturezaPrincipal = 0;
              
               

                $rateio = [];
               
                foreach($arrayRateio as $r){
                    if($r['valor'] != 0){                                       
                        $valorRateio =  $r['valor'];
                        $percenentualRateio = round(($valorRateio * 100)/$valorTotal,5);
                        $valorTotalRateio += $valorRateio;
                        
                        if(!array_key_exists((int) $r['natureza'], self::$planoContasNaturezaIW)){
                            $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ = {$cnpj}. Erro: 
                                                                Não foi encontrada a natureza para o código: {$r['natureza']}.";

                            $errorLinha = true;
                        }
                        $rateio[] = [
                            'natureza' => self::$planoContasNaturezaIW[(int)$r['natureza']],
                            'valor' => $valorRateio,
                            'assinatura' => $codigoDominioAssinatura[$linha['centroCusto']],
                            // 'empresa' =>$codEmpresaIW[$txtLine[38]],
                            'percentual' =>$percenentualRateio
                        ];

                        $naturezaPrincipal = (float) $auxMaiorRateio >  (float) $valorRateio ? (int) $naturezaPrincipal : (int) $r['natureza'];
                        $auxMaiorRateio = $valorRateio;                          
                    }
                }

                if(!$errorLinha){
                    if(!$existeCadastro){                        
                        $TxtToArray[] = [
                            'linha' => $contLinhas,
                            'num_documento' => ltrim(rtrim($numeroDocumento)),
                            'cnpj' => $cnpj,
                            'fornecedor_id' => $fornecedorCadastrado['idFornecedores'],
                            'data_emissao' => $dataEmissao,
                            'data_competencia' => $data_emissao_obj->format("Y-m"),
                            'valor_total' => $valorTotal,
                            'valor_servico' => $valorTotal,                            
                            'tipo_documento_financeiro' => $txtLine[9], 
                            'parcelas' => $parcelas,
                            'rateio' => $rateio,
                            'descricao' => rtrim($linha['descricao']),
                            'natureza_principal' => self::$planoContasNaturezaIW[$naturezaPrincipal],
                            // 'empresa' =>$codEmpresaIW[$txtLine[38]]
                        ];
                    }
                }
              

            
            
            } else {

                $arrayErro[] =  " Linha {$contLinhas}: Foram encontradas ".strlen($txtLine)." caracteres em vez de ".self::$numeroCaracteresLinha.".";
                $errorLinha = true;

            }         
            
            $contLinhas++;
        }
        
        $response = [
            'error' => $arrayErro,
            'itensTxt' => $TxtToArray,
            'jaCadastrada' => $arrayJaCadastrada ];
            
            return $response;

    }
}
