<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 27/10/2017
 * Time: 08:17
 */

namespace app\Controllers;

use App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;
use App\Core\Config;
class SendGrid 
{
    public static function enviarEmailSimples($titulo, $texto, $para)
    {
        $api_data = (object) Config::get('sendgrid');

        $gateway = new MailAdapter(
            new SendGridGateway(
                new \SendGrid($api_data->user, $api_data->pass),
                new \SendGrid\Email()
            )
        );

        $gateway->adapter->sendSimpleMail($titulo, $texto, $para);

    }




}