<?php
namespace app\Controllers;

use App\Models\Financeiro\Fornecedor;

class Fornecedores
{
    public static function indexRelatorioPosicaoFornecedores()
    {
        $fornecedores = Fornecedor::getAllByTipo('F');
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/posicao-fornecedores/index.phtml');
    }

    public static function relatorioPosicaoFornecedores()
    {
        $tipoBusca = 'F';
        if(isset($_POST) && !empty($_POST)){
            $tipoBusca  = filter_input(INPUT_POST, 'tipo_busca', FILTER_SANITIZE_STRING);
            $fornecedoresClientes  = isset($_POST['fornecedores_clientes']) ? $_POST['fornecedores_clientes'] : [];
            $filtros = [
                'tipo_busca' => $tipoBusca,
                'fornecedores_clientes' => $fornecedoresClientes,
                'modo' => 'a',
            ];
            

            $response = Fornecedor::getPosicaoFornecedores($filtros);
        }
        $fornecedores = Fornecedor::getAll();
        $mask = function ($val, $mask)
        {
            $maskared = '';
            $k = 0;
            $val = preg_replace("/[^0-9]/", "", $val);
            for($i = 0; $i <= strlen($mask)-1; $i++) {
                if($mask[$i] == '#') {
                    if(isset($val[$k]))
                        $maskared .= $val[$k++];
                } else {
                    if(isset($mask[$i])) {
                        $maskared .= $mask[$i];
                    }
                }
            }
            return $maskared;
        };
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/posicao-fornecedores/index.phtml');
    }

    public static function excelRelatorioPosicaoFornecedores()
    {
        $tipoBusca = 'F';
        $modo = 'a';
        if(isset($_GET) && !empty($_GET)){
            $tipoBusca = filter_input(INPUT_GET, 'tipo_busca', FILTER_SANITIZE_STRING);
            $modo = filter_input(INPUT_GET, 'modo', FILTER_SANITIZE_STRING);
            $fornecedoresClientes = isset($_GET['fornecedores_clientes']) ? explode(' ', $_GET['fornecedores_clientes']) : [];
            $filtros = [
                'tipo_busca' => $tipoBusca,
                'fornecedores_clientes' => array_filter($fornecedoresClientes),
                'modo' => $modo,
            ];

            $response = Fornecedor::getPosicaoFornecedores($filtros);
        }

        $mask = function ($val, $mask)
        {
            $maskared = '';
            $k = 0;
            $val = preg_replace("/[^0-9]/", "", $val);
            for($i = 0; $i <= strlen($mask)-1; $i++) {
                if($mask[$i] == '#') {
                    if(isset($val[$k]))
                        $maskared .= $val[$k++];
                } else {
                    if(isset($mask[$i])) {
                        $maskared .= $mask[$i];
                    }
                }
            }
            return $maskared;
        };
        $filename = "posicao_fornecedores_clientes_" . ($modo == 'a' ? 'analitico' : 'sintetico');

        header("Content-type: application/x-msexce; charset=ISO-8859-1");
        header("Content-Disposition: attachment; filename={$filename}.xls");
        header("Pragma: no-cache");

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/posicao-fornecedores/excel.phtml');
    }
}
