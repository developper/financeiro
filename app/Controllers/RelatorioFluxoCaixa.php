<?php

namespace app\Controllers;

use \App\Models\Administracao\Filial;
use \App\Models\Financeiro\FluxoCaixa;
use \App\Models\Financeiro\FluxoCaixaCompleto;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\RelatorioNotasEmitidas;


class RelatorioFluxoCaixa
{
    public static function index()
    {

        $unidades = Filial::getUnidadesAtivas($_SESSION['empresa_user']);

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/fluxo-de-caixa/index.phtml');

    }

    public static function pesquisar()
    {

//        var_dump($_POST); die();
        $unidades = Filial::getUnidadesAtivas($_SESSION['empresa_user']);
        if (
            isset($_POST['tipo']) && !empty($_POST['tipo']) &&
            isset($_POST['fim']) && !empty($_POST['fim']) &&
            isset($_POST['inicio']) && !empty($_POST['inicio'])) {
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $data['tipo'] = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING);
            $data['filiais'] = $_POST["filiais"];
            $filtros = [
                'inicio' => $data['inicio'],
                'fim' => $data['fim'],
                'tipo' => $data['tipo'],
                'filiais' => $data['filiais']
            ];
            $parcelas = [];
            $responseSaldo = FluxoCaixa::getSaldoByUR($data);
           
            
            $saldoEntrada = 0;
            $saldoSaida = 0;
            $saldosTipoConta = [];
            if (count($responseSaldo) > 0) {
                foreach($responseSaldo as $saldo){  
                    if($saldo['conta_bancaria_id'] == 39){
                        $saldosTipoConta[39]['nome'] = 'Crédito Tributário (PERDCOMP)';

                    if($saldo['tipo_nota'] == 'Saida'){
                        $saldoSaida += $saldo['VALOR_PAGO'];
                        $saldosTipoConta[39]['saldo_saida'] += $saldo['VALOR_PAGO'];
                    }else{
                        $saldoEntrada += $saldo['VALOR_PAGO'];
                        $saldosTipoConta[39]['saldo_entrada'] += $saldo['VALOR_PAGO'];
                    } 

                    }else{
                        $saldosTipoConta[$saldo['tipo_conta_id']]['nome'] = $saldo['tipo_conta'];

                    if($saldo['tipo_nota'] == 'Saida'){
                        $saldoSaida += $saldo['VALOR_PAGO'];
                        $saldosTipoConta[$saldo['tipo_conta_id']]['saldo_saida'] += $saldo['VALOR_PAGO'];
                    }else{
                        $saldoEntrada += $saldo['VALOR_PAGO'];
                        $saldosTipoConta[$saldo['tipo_conta_id']]['saldo_entrada'] += $saldo['VALOR_PAGO'];
                    } 
                    }   
                                      
                    

                }

            }
            

            $saldoAcumulado =  round($saldoEntrada - $saldoSaida, 2);           

            $response = FluxoCaixa::getFluxoDeCaixa($data);
            $saidasPeriodo = 0;
            $entradasPeriodo = 0;
                        
            $aux = 0;
            if (count($response) > 0) {

                $responseArray = [];
                
               
                foreach ($response as $key => $r) {
                    $keyArray = $data['tipo'] == 'diario' ? $r['vencimento_real'] : $r['ano'].'-'.$r['mes'];
                    if (!isset($responseArray[$keyArray])) {
                        $responseArray[$keyArray]['saldo_entrada'] = 0;
                        $responseArray[$keyArray]['saldo_saida'] = 0;
                    }

                    if($aux == 0){
                        $saldoAcumuladoDia = $saldoAcumulado;
                        $aux++;
                    }

                    

                    if ($r['tipo_nota'] == 'Entrada') {                        
                        $entradasPeriodo += $r['valor'];
                        $saldoAcumuladoDia += $r['valor']; 
                        $responseArray[$keyArray]['saldo_entrada'] += $r['valor'];
                        $responseArray[$keyArray]['entrada'][$key]['tipo_documento'] = $r['tipo_documento'];
                        $responseArray[$keyArray]['entrada'][$key]['valor'] = $r['valor'];
                        $responseArray[$keyArray]['entrada'][$key]['tr'] = $r['tr'];
                        $responseArray[$keyArray]['entrada'][$key]['cod_nota'] = $r['cod_nota'];
                        $responseArray[$keyArray]['entrada'][$key]['tipo_nota'] = $r['tipo_nota'];
                        $responseArray[$keyArray]['entrada'][$key]['fornecedor'] = $r['fornecedor'];
                        $responseArray[$keyArray]['entrada'][$key]['cpf_cnpj'] = $r['cpf_cnpj'];
                        $responseArray[$keyArray]['entrada'][$key]['mes'] = $r['mes'];
                        $responseArray[$keyArray]['entrada'][$key]['ano'] = $r['ano'];
                        $responseArray[$keyArray]['entrada'][$key]['vencimento_br'] = $r['vencimento_br'];
                        $responseArray[$keyArray]['entrada'][$key]['vencimento_real'] = $r['vencimento_real'];
                        $responseArray[$keyArray]['entrada'][$key]['empresa'] = $r['empresa'];
                        $responseArray[$keyArray]['entrada'][$key]['natureza'] = $r['natureza'];
                    } else {
                        $saidasPeriodo += ($r['valor'] * -1);
                        $saldoAcumuladoDia += ($r['valor'] * -1);
                        $responseArray[$keyArray]['saldo_saida'] += ($r['valor'] * -1);
                        $responseArray[$keyArray]['saida'][$key]['tipo_documento'] = $r['tipo_documento'];
                        $responseArray[$keyArray]['saida'][$key]['valor'] = $r['valor'];
                        $responseArray[$keyArray]['saida'][$key]['tr'] = $r['tr'];
                        $responseArray[$keyArray]['saida'][$key]['cod_nota'] = $r['cod_nota'];
                        $responseArray[$keyArray]['saida'][$key]['tipo_nota'] = $r['tipo_nota'];
                        $responseArray[$keyArray]['saida'][$key]['fornecedor'] = $r['fornecedor'];
                        $responseArray[$keyArray]['saida'][$key]['cpf_cnpj'] = $r['cpf_cnpj'];
                        $responseArray[$keyArray]['saida'][$key]['mes'] = $r['mes'];
                        $responseArray[$keyArray]['saida'][$key]['ano'] = $r['ano'];
                        $responseArray[$keyArray]['saida'][$key]['vencimento_br'] = $r['vencimento_br'];
                        $responseArray[$keyArray]['saida'][$key]['vencimento_real'] = $r['vencimento_real'];
                        $responseArray[$keyArray]['saida'][$key]['empresa'] = $r['empresa'];
                        $responseArray[$keyArray]['saida'][$key]['natureza'] = $r['natureza'];
                    }

                    $responseArray[$keyArray]['saldo_dia'] = $responseArray[$keyArray]['saldo_entrada'] + $responseArray[$keyArray]['saldo_saida'];                  
                    $responseArray[$keyArray]['saldo_acumulado'] = $saldoAcumuladoDia;

                }

            }
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/fluxo-de-caixa/index.phtml');

        }


    }


    public static function excel()
    {

        if (isset($_GET['inicio']) && !empty($_GET['inicio']) && isset($_GET['fim']) && !empty($_GET['fim'])) {
            $data['inicio'] = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $data['tipo'] = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING);
            $filiais = [];
            $data['filiais'] = empty($_GET["filiais"]) ? $_GET["filiais"] : explode(' ', $_GET["filiais"]);
                     
            
            $filtros = [
                'inicio' => $data['inicio'],
                'fim' => $data['fim'],
                'tipo' => $data['tipo'],
                'filiais' => $data['filiais']
            ];
            $parcelas = [];
            $responseSaldo = FluxoCaixa::getSaldoByUR($data);           
            
            $saldoEntrada = 0;
            $saldoSaida =0;
            $saidasPeriodo = 0;
            $entradasPeriodo = 0;
            $saldosTipoConta = [];
            if (count($responseSaldo) > 0) {
                foreach($responseSaldo as $saldo){  
                    if($saldo['conta_bancaria_id'] == 39){
                        $saldosTipoConta[39]['nome'] = 'Crédito Tributário (PERDCOMP)';

                    if($saldo['tipo_nota'] == 'Saida'){
                        $saldoSaida += $saldo['VALOR_PAGO'];
                        $saldosTipoConta[39]['saldo_saida'] += $saldo['VALOR_PAGO'];
                    }else{
                        $saldoEntrada += $saldo['VALOR_PAGO'];
                        $saldosTipoConta[39]['saldo_entrada'] += $saldo['VALOR_PAGO'];
                    } 

                    }else{
                        $saldosTipoConta[$saldo['tipo_conta_id']]['nome'] = $saldo['tipo_conta'];

                        if($saldo['tipo_nota'] == 'Saida'){
                            $saldoSaida += $saldo['VALOR_PAGO'];
                            $saldosTipoConta[$saldo['tipo_conta_id']]['saldo_saida'] += $saldo['VALOR_PAGO'];
                        }else{
                            $saldoEntrada += $saldo['VALOR_PAGO'];
                            $saldosTipoConta[$saldo['tipo_conta_id']]['saldo_entrada'] += $saldo['VALOR_PAGO'];
                        }    
                    }               
                }

            }
            $saldoAcumulado =  round($saldoEntrada - $saldoSaida, 2);           

            $response = FluxoCaixa::getFluxoDeCaixa($data);
                        
            $aux = 0;
            if (count($response) > 0) {

                $responseArray = [];
//                if ($data['tipo'] == 'diario') {
                foreach ($response as $key => $r) {
                    $keyArray = $data['tipo'] == 'diario' ? $r['vencimento_real'] : $r['mes'].'-'.$r['ano'];

                    if (!isset($responseArray[$keyArray])) {
                        $responseArray[$keyArray]['saldo_entrada'] = 0;
                        $responseArray[$keyArray]['saldo_saida'] = 0;
                    }

                    if($aux == 0){
                        $saldoAcumuladoDia = $saldoAcumulado;
                        $aux++;
                    }

                    if ($r['tipo_nota'] == 'Entrada') {

                        $entradasPeriodo += ($r['valor'] * -1); 
                        $saldoAcumuladoDia += $r['valor']; 
                        $responseArray[$keyArray]['saldo_entrada'] += $r['valor'];
                        $responseArray[$keyArray]['entrada'][$key]['tipo_documento'] = $r['tipo_documento'];
                        $responseArray[$keyArray]['entrada'][$key]['valor'] = $r['valor'];
                        $responseArray[$keyArray]['entrada'][$key]['tr'] = $r['tr'];
                        $responseArray[$keyArray]['entrada'][$key]['cod_nota'] = $r['cod_nota'];
                        $responseArray[$keyArray]['entrada'][$key]['tipo_nota'] = $r['tipo_nota'];
                        $responseArray[$keyArray]['entrada'][$key]['fornecedor'] = $r['fornecedor'];
                        $responseArray[$keyArray]['entrada'][$key]['cpf_cnpj'] = $r['cpf_cnpj'];
                        $responseArray[$keyArray]['entrada'][$key]['mes'] = $r['mes'];
                        $responseArray[$keyArray]['entrada'][$key]['ano'] = $r['ano'];
                        $responseArray[$keyArray]['entrada'][$key]['vencimento_br'] = $r['vencimento_br'];
                        $responseArray[$keyArray]['entrada'][$key]['vencimento_real'] = $r['vencimento_real'];
                        $responseArray[$keyArray]['entrada'][$key]['empresa'] = $r['empresa'];
                        $responseArray[$keyArray]['entrada'][$key]['natureza'] = $r['natureza'];

                    } else {  

                        $saidasPeriodo += ($r['valor'] * -1);                 
                        $saldoAcumuladoDia += ($r['valor'] * -1); 
                        $responseArray[$keyArray]['saldo_saida'] += ($r['valor'] * -1);
                        $responseArray[$keyArray]['saida'][$key]['tipo_documento'] = $r['tipo_documento'];
                        $responseArray[$keyArray]['saida'][$key]['valor'] = $r['valor'];
                        $responseArray[$keyArray]['saida'][$key]['tr'] = $r['tr'];
                        $responseArray[$keyArray]['saida'][$key]['cod_nota'] = $r['cod_nota'];
                        $responseArray[$keyArray]['saida'][$key]['tipo_nota'] = $r['tipo_nota'];
                        $responseArray[$keyArray]['saida'][$key]['fornecedor'] = $r['fornecedor'];
                        $responseArray[$keyArray]['saida'][$key]['cpf_cnpj'] = $r['cpf_cnpj'];
                        $responseArray[$keyArray]['saida'][$key]['mes'] = $r['mes'];
                        $responseArray[$keyArray]['saida'][$key]['ano'] = $r['ano'];
                        $responseArray[$keyArray]['saida'][$key]['vencimento_br'] = $r['vencimento_br'];
                        $responseArray[$keyArray]['saida'][$key]['vencimento_real'] = $r['vencimento_real'];
                        $responseArray[$keyArray]['saida'][$key]['empresa'] = $r['empresa'];
                        $responseArray[$keyArray]['saida'][$key]['natureza'] = $r['natureza'];
                    }

                    $responseArray[$keyArray]['saldo_dia'] = $responseArray[$keyArray]['saldo_entrada'] + $responseArray[$keyArray]['saldo_saida'];                  
                    $responseArray[$keyArray]['saldo_acumulado'] = $saldoAcumuladoDia;

                }

            }

            $extension = '.xls';
            if (strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = '.xlsx';
            }

            $filename = sprintf(
                "relatorio_fluxo_caixa_%s_%s",
                $data['inicio'],
                $data['fim']
            );

            $inicio = \DateTime::createFromFormat('Y-m-d', $data['inicio'])->format('d/m/Y');
            $fim = \DateTime::createFromFormat('Y-m-d', $data['fim'])->format('d/m/Y');
            date_default_timezone_set('America/Sao_Paulo');
            $emitidoEm = date('d/m/Y H:i');
            $emitidoPor = $_SESSION['nome_user'];
            $tipoNota = $tipo ='diario' ? 'Diário' : 'Mensal';


            header("Content-type: application/x-msexce");
            header("Content-type: application/force-download;");
            header("Content-Disposition: attachment; filename={$filename}" . $extension);
            header("Pragma: no-cache");
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/fluxo-de-caixa/excel.phtml');

        }


    }

    public static function indexCompleto()
    {

        $unidades = Filial::getUnidadesAtivas($_SESSION['empresa_user']);

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/fluxo-de-caixa/index_completo.phtml');

    }

    public static function pesquisarCompleto()
    {

      // var_dump($_POST); die();
        $unidades = Filial::getUnidadesAtivas($_SESSION['empresa_user']);
       
        if (
            isset($_POST['tipo']) && !empty($_POST['tipo']) &&
            isset($_POST['fim']) && !empty($_POST['fim']) &&
            isset($_POST['inicio']) && !empty($_POST['inicio'])) {
        
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $data['tipo'] = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING);
            $data['filiais'] = $_POST["filiais"];
            $data['status'] = $_POST["status"]; 

            $filtros = [
                'inicio' => $data['inicio'],
                'fim' => $data['fim'],
                'tipo' => $data['tipo'],
                'filiais' => $data['filiais'],
                'status' => $data['status']
            ];

            $parcelas = [];

            $responseSaldo = FluxoCaixaCompleto::getSaldoByUR($data);          
            
            $saldoEntrada = 0;
            $saldoSaida = 0;
            $saldosTipoConta = [];
            if (count($responseSaldo) > 0) {
                foreach($responseSaldo as $saldo){  
                    if($saldo['conta_bancaria_id'] == 39){
                        $saldosTipoConta[39]['nome'] = 'Crédito Tributário (PERDCOMP)';

                    if($saldo['tipo_nota'] == 'Saida'){
                        $saldoSaida += $saldo['VALOR_PAGO'];
                        $saldosTipoConta[39]['saldo_saida'] += $saldo['VALOR_PAGO'];
                    }else{
                        $saldoEntrada += $saldo['VALOR_PAGO'];
                        $saldosTipoConta[39]['saldo_entrada'] += $saldo['VALOR_PAGO'];
                    } 

                    }else{   
                        $saldosTipoConta[$saldo['tipo_conta_id']]['nome'] = $saldo['tipo_conta'];

                        if($saldo['tipo_nota'] == 'Saida'){
                            $saldoSaida += $saldo['VALOR_PAGO'];
                            $saldosTipoConta[$saldo['tipo_conta_id']]['saldo_saida'] += $saldo['VALOR_PAGO'];
                        }else{
                            $saldoEntrada += $saldo['VALOR_PAGO'];
                            $saldosTipoConta[$saldo['tipo_conta_id']]['saldo_entrada'] += $saldo['VALOR_PAGO'];
                        }     
                    }              
                    

                }

            }
            

            $saldoAcumulado =  round($saldoEntrada - $saldoSaida, 2);           

            $response = FluxoCaixaCompleto::getFluxoDeCaixaCompleto($data);
            $saidasPeriodo = 0;
            $entradasPeriodo = 0;
                        
            $aux = 0;
            if (count($response) > 0) {

                $responseArray = [];
                
               
                foreach ($response as $key => $r) {
                    $keyArray = $data['tipo'] == 'diario' ? $r['vencimento_real'] : $r['ano'].'-'.$r['mes'];
                    if (!isset($responseArray[$keyArray])) {
                        $responseArray[$keyArray]['saldo_entrada'] = 0;
                        $responseArray[$keyArray]['saldo_saida'] = 0;
                    }

                    if($aux == 0){
                        $saldoAcumuladoDia = $saldoAcumulado;
                        $aux++;
                    }

                    

                    if ($r['tipo_nota'] == 'Entrada') {                        
                        $entradasPeriodo += $r['valor'];
                        $saldoAcumuladoDia += $r['valor']; 
                        $responseArray[$keyArray]['saldo_entrada'] += $r['valor'];
                        $responseArray[$keyArray]['entrada'][$key]['tipo_documento'] = $r['tipo_documento'];
                        $responseArray[$keyArray]['entrada'][$key]['valor'] = $r['valor'];
                        $responseArray[$keyArray]['entrada'][$key]['tr'] = $r['tr'];
                        $responseArray[$keyArray]['entrada'][$key]['cod_nota'] = $r['cod_nota'];
                        $responseArray[$keyArray]['entrada'][$key]['tipo_nota'] = $r['tipo_nota'];
                        $responseArray[$keyArray]['entrada'][$key]['fornecedor'] = $r['fornecedor'];
                        $responseArray[$keyArray]['entrada'][$key]['cpf_cnpj'] = $r['cpf_cnpj'];
                        $responseArray[$keyArray]['entrada'][$key]['mes'] = $r['mes'];
                        $responseArray[$keyArray]['entrada'][$key]['ano'] = $r['ano'];
                        $responseArray[$keyArray]['entrada'][$key]['vencimento_br'] = $r['vencimento_br'];
                        $responseArray[$keyArray]['entrada'][$key]['vencimento_real'] = $r['vencimento_real'];
                        $responseArray[$keyArray]['entrada'][$key]['empresa'] = $r['empresa'];
                        $responseArray[$keyArray]['entrada'][$key]['natureza'] = $r['natureza'];
                        $responseArray[$keyArray]['entrada'][$key]['status_item'] = $r['status_item'];
                    } else {
                        $saidasPeriodo += ($r['valor'] * -1);
                        $saldoAcumuladoDia += ($r['valor'] * -1);
                        $responseArray[$keyArray]['saldo_saida'] += ($r['valor'] * -1);
                        $responseArray[$keyArray]['saida'][$key]['tipo_documento'] = $r['tipo_documento'];
                        $responseArray[$keyArray]['saida'][$key]['valor'] = $r['valor'];
                        $responseArray[$keyArray]['saida'][$key]['tr'] = $r['tr'];
                        $responseArray[$keyArray]['saida'][$key]['cod_nota'] = $r['cod_nota'];
                        $responseArray[$keyArray]['saida'][$key]['tipo_nota'] = $r['tipo_nota'];
                        $responseArray[$keyArray]['saida'][$key]['fornecedor'] = $r['fornecedor'];
                        $responseArray[$keyArray]['saida'][$key]['cpf_cnpj'] = $r['cpf_cnpj'];
                        $responseArray[$keyArray]['saida'][$key]['mes'] = $r['mes'];
                        $responseArray[$keyArray]['saida'][$key]['ano'] = $r['ano'];
                        $responseArray[$keyArray]['saida'][$key]['vencimento_br'] = $r['vencimento_br'];
                        $responseArray[$keyArray]['saida'][$key]['vencimento_real'] = $r['vencimento_real'];
                        $responseArray[$keyArray]['saida'][$key]['empresa'] = $r['empresa'];
                        $responseArray[$keyArray]['saida'][$key]['natureza'] = $r['natureza'];
                        $responseArray[$keyArray]['saida'][$key]['status_item'] = $r['status_item'];
                    }

                    $responseArray[$keyArray]['saldo_dia'] = $responseArray[$keyArray]['saldo_entrada'] + $responseArray[$keyArray]['saldo_saida'];                  
                    $responseArray[$keyArray]['saldo_acumulado'] = $saldoAcumuladoDia;

                }

            }
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/fluxo-de-caixa/index_completo.phtml');

        }


    }

    public static function excelCompleto()
    {

        if (isset($_GET['inicio']) && !empty($_GET['inicio']) && isset($_GET['fim']) && !empty($_GET['fim'])) {
            $data['inicio'] = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $data['tipo'] = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING);
            $filiais = [];
            $data['filiais'] = empty($_GET["filiais"]) ? $_GET["filiais"] : explode(' ', $_GET["filiais"]);
            $data['status'] = $_GET["status"];
                     
            
            $filtros = [
                'inicio' => $data['inicio'],
                'fim' => $data['fim'],
                'tipo' => $data['tipo'],
                'filiais' => $data['filiais'],
                'status' => $data['status'] 
            ];
            $parcelas = [];
            $responseSaldo = FluxoCaixaCompleto::getSaldoByUR($data);           
            
            $saldoEntrada = 0;
            $saldoSaida =0;
            $saidasPeriodo = 0;
            $entradasPeriodo = 0;
            $saldosTipoConta = [];
            if (count($responseSaldo) > 0) {
                foreach($responseSaldo as $saldo){ 
                    if($saldo['conta_bancaria_id'] == 39){
                        $saldosTipoConta[39]['nome'] = 'Crédito Tributário (PERDCOMP)';

                    if($saldo['tipo_nota'] == 'Saida'){
                        $saldoSaida += $saldo['VALOR_PAGO'];
                        $saldosTipoConta[39]['saldo_saida'] += $saldo['VALOR_PAGO'];
                    }else{
                        $saldoEntrada += $saldo['VALOR_PAGO'];
                        $saldosTipoConta[39]['saldo_entrada'] += $saldo['VALOR_PAGO'];
                    } 

                    }else{ 
                        $saldosTipoConta[$saldo['tipo_conta_id']]['nome'] = $saldo['tipo_conta'];

                        if($saldo['tipo_nota'] == 'Saida'){
                            $saldoSaida += $saldo['VALOR_PAGO'];
                            $saldosTipoConta[$saldo['tipo_conta_id']]['saldo_saida'] += $saldo['VALOR_PAGO'];
                        }else{
                            $saldoEntrada += $saldo['VALOR_PAGO'];
                            $saldosTipoConta[$saldo['tipo_conta_id']]['saldo_entrada'] += $saldo['VALOR_PAGO'];
                        } 
                    }                  
                }

            }
            $saldoAcumulado =  round($saldoEntrada - $saldoSaida, 2);           

            $response = FluxoCaixaCompleto::getFluxoDeCaixaCompleto($data);
                        
            $aux = 0;
            if (count($response) > 0) {

                $responseArray = [];
//                if ($data['tipo'] == 'diario') {
                foreach ($response as $key => $r) {
                    $keyArray = $data['tipo'] == 'diario' ? $r['vencimento_real'] : $r['mes'].'-'.$r['ano'];

                    if (!isset($responseArray[$keyArray])) {
                        $responseArray[$keyArray]['saldo_entrada'] = 0;
                        $responseArray[$keyArray]['saldo_saida'] = 0;
                    }

                    if($aux == 0){
                        $saldoAcumuladoDia = $saldoAcumulado;
                        $aux++;
                    }

                    if ($r['tipo_nota'] == 'Entrada') {

                        $entradasPeriodo += ($r['valor'] * -1); 
                        $saldoAcumuladoDia += $r['valor']; 
                        $responseArray[$keyArray]['saldo_entrada'] += $r['valor'];
                        $responseArray[$keyArray]['entrada'][$key]['tipo_documento'] = $r['tipo_documento'];
                        $responseArray[$keyArray]['entrada'][$key]['valor'] = $r['valor'];
                        $responseArray[$keyArray]['entrada'][$key]['tr'] = $r['tr'];
                        $responseArray[$keyArray]['entrada'][$key]['cod_nota'] = $r['cod_nota'];
                        $responseArray[$keyArray]['entrada'][$key]['tipo_nota'] = $r['tipo_nota'];
                        $responseArray[$keyArray]['entrada'][$key]['fornecedor'] = $r['fornecedor'];
                        $responseArray[$keyArray]['entrada'][$key]['cpf_cnpj'] = $r['cpf_cnpj'];
                        $responseArray[$keyArray]['entrada'][$key]['mes'] = $r['mes'];
                        $responseArray[$keyArray]['entrada'][$key]['ano'] = $r['ano'];
                        $responseArray[$keyArray]['entrada'][$key]['vencimento_br'] = $r['vencimento_br'];
                        $responseArray[$keyArray]['entrada'][$key]['vencimento_real'] = $r['vencimento_real'];
                        $responseArray[$keyArray]['entrada'][$key]['empresa'] = $r['empresa'];
                        $responseArray[$keyArray]['entrada'][$key]['natureza'] = $r['natureza'];
                        $responseArray[$keyArray]['entrada'][$key]['status_item'] = $r['status_item'];

                    } else {  

                        $saidasPeriodo += ($r['valor'] * -1);                 
                        $saldoAcumuladoDia += ($r['valor'] * -1); 
                        $responseArray[$keyArray]['saldo_saida'] += ($r['valor'] * -1);
                        $responseArray[$keyArray]['saida'][$key]['tipo_documento'] = $r['tipo_documento'];
                        $responseArray[$keyArray]['saida'][$key]['valor'] = $r['valor'];
                        $responseArray[$keyArray]['saida'][$key]['tr'] = $r['tr'];
                        $responseArray[$keyArray]['saida'][$key]['cod_nota'] = $r['cod_nota'];
                        $responseArray[$keyArray]['saida'][$key]['tipo_nota'] = $r['tipo_nota'];
                        $responseArray[$keyArray]['saida'][$key]['fornecedor'] = $r['fornecedor'];
                        $responseArray[$keyArray]['saida'][$key]['cpf_cnpj'] = $r['cpf_cnpj'];
                        $responseArray[$keyArray]['saida'][$key]['mes'] = $r['mes'];
                        $responseArray[$keyArray]['saida'][$key]['ano'] = $r['ano'];
                        $responseArray[$keyArray]['saida'][$key]['vencimento_br'] = $r['vencimento_br'];
                        $responseArray[$keyArray]['saida'][$key]['vencimento_real'] = $r['vencimento_real'];
                        $responseArray[$keyArray]['saida'][$key]['empresa'] = $r['empresa'];
                        $responseArray[$keyArray]['saida'][$key]['natureza'] = $r['natureza'];
                        $responseArray[$keyArray]['saida'][$key]['status_item'] = $r['status_item'];
                    }

                    $responseArray[$keyArray]['saldo_dia'] = $responseArray[$keyArray]['saldo_entrada'] + $responseArray[$keyArray]['saldo_saida'];                  
                    $responseArray[$keyArray]['saldo_acumulado'] = $saldoAcumuladoDia;

                }

            }
            

            $extension = '.xls';
            if (strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = '.xlsx';
            }

            $filename = sprintf(
                "relatorio_fluxo_caixa_completo%s_%s",
                $data['inicio'],
                $data['fim']
            );

            $inicio = \DateTime::createFromFormat('Y-m-d', $data['inicio'])->format('d/m/Y');
            $fim = \DateTime::createFromFormat('Y-m-d', $data['fim'])->format('d/m/Y');
            date_default_timezone_set('America/Sao_Paulo');
            $emitidoEm = date('d/m/Y H:i');
            $emitidoPor = $_SESSION['nome_user'];
            $tipoNota = $tipo ='diario' ? 'Diário' : 'Mensal';
            $status = $data['status'];

            

            header("Content-type: application/x-msexce");
            header("Content-type: application/force-download;");
            header("Content-Disposition: attachment; filename={$filename}" . $extension);
            header("Pragma: no-cache");
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/fluxo-de-caixa/excel_completo.phtml');

        }


    }


}