<?php
namespace app\Controllers;

use \App\Models\Administracao\Filial;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\IPCF;
use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\Notas;
use App\Helpers\StringHelper;
use App\Models\Financeiro\ImportarContaPagarIW as ModelContaPagarIW;
use Zend\Stdlib\DateTime;
use App\Models\Financeiro\Assinatura;
use App\Helpers\DateHelper;

include_once('../../utils/codigos.php');

//ini_set('display_errors',1);

class ImportarContaPagarIW
{
    static $planoContasNaturezaIW = [
        // 2.0 Despesas operacionais -> 2.0.4 Material Aplicado           
        2090 => 38, // 2.0.4.01 materiais
        2091 => 39, // 2.0.4.02 Medicamentos
        2092 => 40, // 2.0.4.03 Dietas
        2093 => 41, // 2.0.4.04 Curativos
        2094 => 42, // 2.0.4.05 Fraldas
    ];
    private static $numeroColunas = 39; // tamanho do array =39 colunas  

    public static function index()
    {      ini_set('display_errors',1);
        $filiais = Filial::getUnidadesAtivas();
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/importar-conta-pagar-iw/templates/index.phtml');
    }

    public static function importarArquivo()
    {
        $filiais = Filial::getUnidadesAtivas();

        if(isset($_FILES) && !empty($_FILES['conta_pagar_iw']['size']) && !empty($_POST['empresa'])){
            // salvando arquivo ofx na pasta ofx do financeiro
            $responseMoveCSV = self::moveTxtToTemporaryPath($_FILES);
            $empresa = $_POST['empresa'];

            if(!empty($responseMoveCSV['acerto'])){
                // lendo arquivo ofx
                $nomeArquivo = $_FILES['conta_pagar_iw']['name'];
                $caminhoArquivo = $responseMoveCSV['acerto'];
                $responseGetCSV = self::getCSV($caminhoArquivo);
                //echo '<pre>'; die(print_r($responseGetCSV['itensCSV']));
                if(!empty($responseGetCSV['itensCSV'])){
                    $responseInsert = ModelContaPagarIW::inserirNotas($responseGetCSV['itensCSV'],$empresa);
                }
                unlink($caminhoArquivo);
            }else{
                $response['error'][] = $responseMoveCSV['erro'];
            }

            if(!empty($responseGetCSV['error'])){
                foreach($responseGetCSV['error'] as $erro){
                    $response['error'][] = $erro;
                }
            }
            if(!empty($responseGetCSV['jaCadastrada'])){
                foreach($responseGetCSV['jaCadastrada'] as $jaCadastrada){
                    $response['jaCadastrada'][] = $jaCadastrada;
                }
            }

            if(!empty($responseInsert['error'])){
                foreach($responseInsert['error'] as $erro){
                    $response['error'][] = $erro;
                }
            }

            if(!empty($responseInsert['insert'])){
                foreach($responseInsert['insert'] as $insert){
                    $response['insert'][] = $insert;
                }
            }

            if(!empty($responseMoveCSV['erro'])){
                $response['error'][] = $responseMoveCSV['erro'];
            }
        }else{
            if(empty($_POST['empresa'])){
                $response['error'][] = "Selecione a Filial antes de fazer Upload";
            }else{
                $response['error'][] = "Selecione um arquivo antes de fazer Upload";
            }

        }

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/importar-conta-pagar-iw/templates/index.phtml');
    }

    public static function moveTxtToTemporaryPath($file)
    {
        $errors = [];

        $path = $_SERVER['DOCUMENT_ROOT'] . '/financeiros/arquivos-iw/contas-pagar/';
        $size = 1024 * 1024 *1024 * 1024 *1024 * 1024 * 2;
        $array['erro'] = '';
        $array['acerto'] = '';

        $errors[1] = 'O arquivo no upload é maior do que o permitido';
        $errors[2] = 'O arquivo ultrapassa o limite de tamanho especifiado';
        $errors[3] = 'O upload do arquivo foi feito parcialmente';
        $errors[4] = 'Não foi feito o upload do arquivo!';

        if ($file['conta_pagar_iw']['error'] != 0) {
            $array['erro'] = "Não foi possível fazer o upload, erro:<br />" . $errors[$file['conta_pagar_iw']['error']];
            return $array;
        }

        $ext = explode('.', $file['conta_pagar_iw']['name']);

        $extensao = end($ext);

        $type = $file['conta_pagar_iw']['type'];
        if ($extensao != 'csv' && $extensao != 'CSV' && $type != 'application/vnd.ms-excel') {
            $array['erro'] = "Por favor, envie apenas arquivos com no formato .csv!";
            return $array;
        }

        if ($size < $file['conta_pagar_iw']['size']) {
            $array['erro'] = "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
            return $array;
        }

        $agora= date('Y_m_d_H_i_s_');

        $name = $file['conta_pagar_iw']['name'];
        $fullPath = $path .$agora.$name;
        $tmp = $file['conta_pagar_iw']['tmp_name'];

        if (move_uploaded_file($tmp, $fullPath)) {
            $array['acerto'] = $fullPath;
            return $array;
        }

        $array['erro'] = "Não foi possível enviar o arquivo, tente novamente! Caso o erro persista, entre em contato com o setor de TI.";
        return $array['erro'];
    }

    public static function formatDate($data){

        $date = DateTime::createFromFormat('d/m/Y', $data);

        return  $date->format('Y-m-d');
    }


    public static function getCsv($caminhoArquivo)
    {
        $data_fechamento_obj = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);

        // $responseFilial =  Filial::getEmpresas();
        // foreach($responseFilial as $resp){
        //     $codEmpresaIW[$resp['iw_id']] = $resp['id'];
        // }

        $responseAssinatura = Assinatura::getAll();
        // o arquivo csv traz o codigo da dominio
        foreach($responseAssinatura as $resp){
            $codigoDominioAssinatura[$resp['CODIGO_DOMINIO']] = $resp['ID'];
        }
        $file = fopen($caminhoArquivo, "r");

        $header = true;
        $csvToArray = [];
        $arrayErro = [];
        $contLinhas = 1;
        $auxFile = $file;
        $primeiroRegistro = 1;

        // verificando se arquivo é vazio.
        // if(!empty($primeiroRegistro)){


        while (($csvLine = fgetcsv($file, 1000, ";")) !== FALSE) {

            if($csvLine[0] != "Numero do Titulo" && $contLinhas == 1){
                $response['error'][] = "Erro: Cabeçalho do arquivo incorreto";
                return $response;

            }
            if(count($csvLine) == 1){
                $contLinhas++;
                continue;
            }
            // verificando se o array tem o tamanho esperado
            if(count($csvLine) != self::$numeroColunas) {
                $arrayErro[] =  " Linha {$contLinhas}: Foram encontradas ".count($csvLine)." colunas em vez de ".self::$numeroColunas.".";
                if ($header) {
                    $header = false;
                }
            }else{
                if ($header) {
                    $header = false;
                }else {
                    if(count($csvLine) != 1){
                        $inicioKeyParcela = 12;
                        $arrayParcelas = [];
                        $inicioKeyRateio = 24;
                        $arrayRateio = [];
                        $valorTotalRateio = 0;
                        $numeroDocumento = $csvLine[0];
                        $cnpj = $csvLine[1];
                        $errorLinha = false;
                        $existeCadastro = false;
                        $arrayContaCadastrada = [];
                        $valorTotal = StringHelper::moneyStringToFloat($csvLine[8]);
                        // verificando se a unidade regional do csv existe no sistema.
                        // $empresaLineIW = $csvLine[38];
                        // if(!array_key_exists($empresaLineIW, $codEmpresaIW)){
                        //     $arrayErro[] = " Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ = {$cnpj}. Erro:
                        //                     não foi encontrada a empresa para o código: {$csvLine[38]}.";
                        //     $errorLinha = true;
                        // }

                        if(!array_key_exists($csvLine[34], $codigoDominioAssinatura)){
                            $arrayErro[] = "Linha {$contLinhas}; Num. doc. = {$numeroDocumento}; CNPJ = {$cnpj}. Erro: 
                                                            não foi encontrada a assinatura para o código: {$csvLine[34]}.";
                            $errorLinha = true;
                        }
                        // vericaando se fornecedor já existe no sistema
                        $fornecedorCadastrado = Fornecedor::checkIfExistsFornecedorByCNPJ($cnpj, 'F');

                        if(empty($fornecedorCadastrado)){
                            $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ = {$cnpj}. Erro: 
                                                        Fornecedor não possui cadastro no sistema.";

                            $errorLinha = true;
                        }else{
                            $fornecedorCadastrado = Fornecedor::getById($fornecedorCadastrado);
                        }

                        // verificar se pode por cpf Fornecedor::checkIfExistsFornecedorByCPF($cnpj);
                        $notaJaCadastrada = ModelContaPagarIW::verificarNFJaExiste($cnpj, $numeroDocumento);
                        if(!empty($notaJaCadastrada)){
                            $arrayJaCadastrada[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ = {$cnpj}.
                                            Conta já possui cadastro no sistema TR {$notaJaCadastrada['idNotas']}.";

                            $existeCadastro = true;
                        }

                        if(!\DateTime::createFromFormat('d/m/Y', $csvLine[6])){
                            $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ = {$cnpj}. Erro: 
                                            Data da emissão não é válida.";
                            $errorLinha = true;
                        } else {
                            $data_emissao_obj = \DateTime::createFromFormat('d/m/Y', $csvLine[$inicioKeyParcela]);
                            if($data_emissao_obj <= $data_fechamento_obj) {
                                $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ = {$cnpj}. Erro: 
                                                        Data da emissão é menor que a data de fechamento do período: " . $data_fechamento_obj->format('d/m/Y');
                                $errorLinha = true;
                            }
                        }

                        // pegando vencimento e valor das parcelas
                        for ($i = 12; $i <= 22; ) {
                            if(!empty($csvLine[$inicioKeyParcela])){
                                // verificando se data de emissão é válida
                                if(!\DateTime::createFromFormat('d/m/Y', $csvLine[$inicioKeyParcela])){
                                    $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ = {$cnpj}. Erro: 
                                                    Data da parcela não é válida.";
                                    $errorLinha = true;
                                }else{
                                    $data_parcela_obj = \DateTime::createFromFormat('d/m/Y', $csvLine[$inicioKeyParcela]);
                                    if($data_parcela_obj <= $data_fechamento_obj) {
                                        $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ = {$cnpj}. Erro: 
                                                        Data da parcela (" . $data_parcela_obj->format('d/m/Y') . ") é menor que a data de fechamento do período: " . $data_fechamento_obj->format('d/m/Y');
                                        $errorLinha = true;
                                    }
                                    $arrayParcelas[] = [
                                        'vencimento' => self::formatDate($csvLine[$inicioKeyParcela]),
                                        'valor' => StringHelper::moneyStringToFloat($csvLine[$inicioKeyParcela + 1]),
                                        'vencimento_real'=>self::formatDate(DateHelper::diaUtilVencimentoReal($csvLine[$inicioKeyParcela]))
                                    ];
                                }
                            }
                            $i = $i+2;
                            $inicioKeyParcela = $i;
                        }

                        // gerando rateios
                        $auxMaiorRateio = 0;
                        $naturezaPrincipal = 0;


                        for ($i = 24; $i <= 33; ) {

                            if(!empty($csvLine[$inicioKeyRateio + 1])){
                                $valorRateio =  StringHelper::moneyStringToFloat($csvLine[$inicioKeyRateio + 1]) ;
                                $percenentualRateio = round(($valorRateio * 100)/$valorTotal,5);
                                $valorTotalRateio += $valorRateio;
                                if(!array_key_exists($csvLine[$inicioKeyRateio], self::$planoContasNaturezaIW)){
                                    $arrayErro[] = "Linha {$contLinhas}; Num. Doc. = {$numeroDocumento}; CNPJ = {$cnpj}. Erro: 
                                                                        Não foi encontrada a natureza para o código: {$csvLine[$inicioKeyRateio]}.";

                                    $errorLinha = true;
                                }
                                $arrayRateio[] = [
                                    'natureza' => self::$planoContasNaturezaIW[$csvLine[$inicioKeyRateio]],
                                    'valor' => $valorRateio,
                                    'assinatura' => $codigoDominioAssinatura[$csvLine[34]],
                                    // 'empresa' =>$codEmpresaIW[$csvLine[38]],
                                    'percentual' =>$percenentualRateio
                                ];
                                $naturezaPrincipal = (float) $auxMaiorRateio >  (float) $valorRateio ? $naturezaPrincipal : $csvLine[$inicioKeyRateio];
                                $auxMaiorRateio = $valorRateio;
                            }

                            $i = $i+2;
                            $inicioKeyRateio = $i;
                        }

                        if(!$errorLinha){
                            if(!$existeCadastro){
                                $valorFrete = $valorTotal - $valorTotalRateio;
                                $valorServico = $valorTotal - $valorFrete;
                                $csvToArray[] = [
                                    'linha' => $contLinhas,
                                    'num_documento' => $csvLine[0],
                                    'cnpj' => $csvLine[1],
                                    'fornecedor_id' => $fornecedorCadastrado['idFornecedores'],
                                    'data_emissao' => self::formatDate($csvLine[6]),
                                    'valor_total' => $valorTotal,
                                    'valor_servico' => $valorServico,
                                    'valor_frete' => $valorFrete,
                                    'tipo_documento_financeiro' => $csvLine[9],
                                    'observacao' => $csvLine[10],
                                    'data_entrada' => self::formatDate($csvLine[11]),
                                    'parcelas' => $arrayParcelas,
                                    'rateio' => $arrayRateio,
                                    'descricao' => $csvLine[36],
                                    'natureza_principal' => self::$planoContasNaturezaIW[$naturezaPrincipal],
                                    // 'empresa' =>$codEmpresaIW[$csvLine[38]]
                                ];
                            }
                        }
                    }
                }
            }
            $contLinhas++;
        }
        $response = [
            'error' => $arrayErro,
            'itensCSV' => $csvToArray,
            'jaCadastrada' => $arrayJaCadastrada ];

        return $response;



    }
}
