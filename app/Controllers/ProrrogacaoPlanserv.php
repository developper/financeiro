<?php

namespace App\Controllers;

use App\Models\Administracao\ProrrogacaoPlanserv as Prorrogacao;
use App\Models\Administracao\Paciente as Paciente;
use \PhpOffice\PhpWord\IOFactory;
use \PhpOffice\PhpWord\PhpWord;
use \PhpOffice\PhpWord\TemplateProcessor;
use App\Controllers\Prescricoes;

class ProrrogacaoPlanserv
{
    public $listaProrrogacao;
    public $cabecalhoProrogacao;
    public static function listarProrrogacao ()
    {
        $pacienteId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

        if(empty($pacienteId) || $pacienteId <= 0 ) {
            $error = 'Parametro inválido. Entre em contato com o TI!';
            return require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/prorrogacao-planserv-listar.phtml');
        }

        $listaProrrogacao = Prorrogacao::getProrrogacoes($pacienteId);
        $cabecalho = Paciente::getCabecalho($pacienteId);

        if(empty($listaProrrogacao)) {
            $error = "Paciente não possui prorrogação cadastrada";
        }
        return require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/prorrogacao-planserv-listar.phtml');

    }

    public static function formProrrogacao ()
    {
        $mode = 'new';
        if(isset($_GET['mode']) && !empty($_GET['mode'])) {
            $mode = filter_input (INPUT_GET, 'mode', FILTER_SANITIZE_STRING);
            $prorrogacaoId = filter_input(INPUT_GET, 'prorrogacao', FILTER_SANITIZE_NUMBER_INT);
        }

        if($mode === 'new') {
            $pacienteId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

            if(empty($pacienteId) || $pacienteId <= 0 ) {
                $error = 'Parametro inválido. Entre em contato com o TI!';
                header("Location: " . $_SERVER['HTTP_REFERER']);
            }
            $inicio     = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim        = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);

            $cabecalho  = Paciente::getCabecalho($pacienteId);
            $logo 		= Administracao::buscarLogoPeloIdPaciente($pacienteId);
            $modalidade = Administracao::buscarModalidadePaciente($pacienteId);
            $admissao	= Administracao::buscarDataAdmissaoPaciente($pacienteId);
            $admissao   = isset($admissao['DATA_ADMISSAO']) && !empty($admissao['DATA_ADMISSAO']) ?
                \DateTime::createFromFormat('Y-m-d H:i:s', $admissao['DATA_ADMISSAO'])->format('Y-m-d') :
                '0000-00-00';
            $just_med   = Paciente::getUltimaProrrogacaoMedica ($pacienteId, $inicio, $fim);
            $justMedProfissionais = '';
            if($just_med){
                $justMedProfissionais = Paciente::getProfissionaisUltimaProrrogacaoMedica($just_med[0]);
            }


            if(empty($justMedProfissionais)){
                $justMedProfissionais ='';
            }else{
                foreach($justMedProfissionais as $justMedProf){
                    $just_med['justificativa'] .= "\n - ".$justMedProf['DESCRICAO'].", ".$justMedProf['frequencia'].", ".$justMedProf['JUSTIFICATIVA'].". ";
                    $just_med['quadro_clinico'] .= "\n - ".$justMedProf['DESCRICAO'].", ".$justMedProf['frequencia'].", ".$justMedProf['JUSTIFICATIVA'].". ";
                }
            }

            $just_enf   = Paciente::getUltimaProrrogacaoEnfermagem ($pacienteId, $inicio, $fim);

            require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/prorrogacao-planserv-novo.phtml');
        } else {
            $response   = Prorrogacao::getById($prorrogacaoId);

            $cabecalho  = Paciente::getCabecalho($response['paciente_id']);
            $logo 		= Administracao::buscarLogoPeloIdPaciente($response['paciente_id']);
            $inicio     = \DateTime::createFromFormat('Y-m-d', $response['inicio'])->format('d/m/Y');
            $fim        = \DateTime::createFromFormat('Y-m-d', $response['fim'])->format('d/m/Y');

            if($mode == 'toNew'){
                $just_med = Paciente::getUltimaProrrogacaoMedica ($response['paciente_id'], $inicio, $fim);
                $just_enf = Paciente::getUltimaProrrogacaoEnfermagem ($response['paciente_id'], $inicio, $fim);

                $response['evolucao_medica'] = '';
                $response['quadro_clinico'] = $just_med['quadro_clinico'];
                $response['regime_atual'] = $just_med['regime_atual'];
                $response['indicacao_tecnica'] = $just_med['indicacao_tecnica'];
                $response['prorrogacao_medica'] = $just_med['ID'];
                $response['evolucao_enfermagem'] = $just_enf['EVOLUCAO_CLINICA'];
                $response['prorrogacao_enfermagem'] = $just_enf['ID'];
            }

            require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/prorrogacao-planserv-editar.phtml');
        }
    }

    public static function visualizarProrrogacao ()
    {
        $prorrogacaoId = filter_input(INPUT_GET, 'prorrogacao', FILTER_SANITIZE_NUMBER_INT);
        $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
        $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);

        $response   = Prorrogacao::getById($prorrogacaoId);
        $cabecalho  = Paciente::getCabecalho($response['paciente_id']);
        $logo 		= Administracao::buscarLogoPeloIdPaciente($response['paciente_id']);

        require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/prorrogacao-planserv-visualizar.phtml');
    }

    public static function gerarDocProrrogacao ()
    {
        $prorrogacaoId = filter_input(INPUT_GET, 'prorrogacao', FILTER_SANITIZE_NUMBER_INT);

        $response   = Prorrogacao::getById($prorrogacaoId);

        $cabecalho  = Paciente::getCabecalho($response['paciente_id']);
        $modalidade = Administracao::buscarModalidadePaciente($response['paciente_id']);

        if($response['evolucao_medica'] != '') {
            $templateProcessor = new TemplateProcessor('../../adm/secmed/resources/planserv-template.docx');
        } else {
            $templateProcessor = new TemplateProcessor('../../adm/secmed/resources/planserv-template-v2.docx');
        }

        # ----------------------- PERÍODO ----------------------------
        $periodo = sprintf(
            '%s a %s',
            \DateTime::createFromFormat('Y-m-d', $response['inicio'])->format('d/m/Y'),
            \DateTime::createFromFormat('Y-m-d', $response['fim'])->format('d/m/Y')
        );
        $templateProcessor->setValue('periodo', $periodo);

        # ----------------------- CABEÇALHO --------------------------
        $templateProcessor->setValue('beneficiario', $cabecalho['paciente']);
        $templateProcessor->setValue('matricula', $cabecalho['matricula']);
        $templateProcessor->setValue('admissao',
            isset($response['admissao']) && !empty($response['admissao']) ?
            \DateTime::createFromFormat('Y-m-d', $response['admissao'])->format('d/m/Y') :
            ""
        );

        $templateProcessor->setValue('idade', $cabecalho['idade']);
        $templateProcessor->setValue('modalidade', $response['modalidade']);
        $templateProcessor->setValue('criterio', $response['criterio']);

        # ----------------------- TERAPIAS ---------------------------
        $fonoterapia = $response['terapia_fonoterapia'] != 0 ?
            "☑ FONOTERAPIA ({$response['terapia_fonoterapia']} X/SEMANA)" :
            "□ FONOTERAPIA (____X/SEMANA)"
        ;

        $fisioterapia = $response['terapia_fisioterapia'] != 0 ?
            "☑ FISIOTERAPIA ({$response['terapia_fisioterapia']} X/SEMANA)" :
            "□ FISIOTERAPIA (____X/SEMANA)"
        ;
        $o2 = $response['terapia_o2'] == 's' ? "☑ O2" : "□ O2";

        $terapias = sprintf('%s %s %s', $fonoterapia, $fisioterapia, $o2);

        $templateProcessor->setValue('terapias', $terapias);

        # ----------------------- DIETA ------------------------------
        $dieta = sprintf('%s %s %s',
            $response['dieta'] == 'art' ? "☑ Artesanal" : "□ Artesanal",
            $response['dieta'] == 'ind' ? "☑ Industrializada" : "□ Industrializada",
            $response['dieta'] == 'mis' ? "☑ Mista" : "□ Mista"
        );

        $templateProcessor->setValue('dieta', $dieta);

        # ----------------------- VIA --------------------------------
        $via = sprintf('%s %s %s %s %s %s',
            $response['via'] == 'oral' ? "☑ Oral" : "□ Oral",
            $response['via'] == 'sne' ? "☑ SNE" : "□ SNE",
            $response['via'] == 'gtm' ? "☑ GTM" : "□ GTM",
            $response['via'] == 'jej' ? "☑ Jejuno" : "□ Jejuno",
            $response['via'] == 'bi' ? "☑ BI" : "□ BI",
            $response['via'] == 'grav' ? "☑ Gravitacional" : "□ Gravitacional"
        );

        $templateProcessor->setValue('via', $via);

        # ----------------------- DEAMBULA ---------------------------
        $deambula = sprintf('%s %s %s',
            $response['deambula'] == 'ss' ? "☑ SIM (sem auxílio)" : "□ SIM (sem auxílio)",
            $response['deambula'] == 'sc' ? "☑ SIM (com auxílio de {$response['deambula_auxilio']})" : "□ SIM (com auxílio de ________________)",
            $response['deambula'] == 'n' ? "☑ NÃO" : "□ NÃO"
        );

        $templateProcessor->setValue('deambula', $deambula);

        # ----------------------- DRENO ------------------------------
        $drenos = sprintf('%s %s',
            $response['drenos'] == 'n' ? "☑ Ausentes" : "□ Ausentes",
            $response['drenos'] == 's' ? "☑ Presentes" : "□ Presentes"
        );

        $templateProcessor->setValue('drenos', $drenos);

        # ----------------------- FAMILIA APTA -----------------------
        $familiaApta = sprintf('%s %s',
            $response['familia_apta'] == 's' ? "☑ SIM" : "□ SIM",
            $response['familia_apta'] == 'n' ? "☑ NÃO" : "□ NÃO"
        );

        $templateProcessor->setValue('familia_apta', $familiaApta);

        # ----------------------- INTEGRIDADE CULTANEA ---------------
        $integridadeCultanea = sprintf('%s %s',
            $response['integridade_cultanea'] == 'n' ? "☑ Íntegra" : "□ Íntegra",
            $response['integridade_cultanea'] == 's' ? "☑ Com lesões: " : "□ Com lesões: "
        );

        $templateProcessor->setValue('integridade_cultanea', $integridadeCultanea);
        $templateProcessor->setValue('lesoes', $response['lesoes'] != "" ? stripslashes($response['lesoes']) : "");

        # ----------------------- ATB --------------------------------
        $atb = sprintf('%s %s',
            $response['atb'] == 'n' ? "☑ NÃO" : "□ NÃO",
            $response['atb'] == 's' ? "☑ SIM {$response['atb_descricao']}" : "□ SIM ___________"
        );

        $templateProcessor->setValue('atb', $atb);
        $templateProcessor->setValue(
            'previsao_termino_atb',
            $response['previsao_termino_atb'] != '0000-00-00' ?
                \DateTime::createFromFormat('Y-m-d', $response['previsao_termino_atb'])->format('d/m/Y') :
                ""
        );

        # ----------------------- ANTIFUNGICO ------------------------
        $antifungico = sprintf('%s %s',
            $response['antifungico'] == 'n' ? "☑ NÃO" : "□ NÃO",
            $response['antifungico'] == 's' ? "☑ SIM {$response['antifungico_descricao']}" : "□ SIM ___________"
        );

        $templateProcessor->setValue('antifungico', $antifungico);
        $templateProcessor->setValue(
            'previsao_termino_antifungico',
            $response['previsao_termino_antifungico'] != '0000-00-00' ?
                \DateTime::createFromFormat('Y-m-d', $response['previsao_termino_antifungico'])->format('d/m/Y') :
                ""
        );

        # ----------------------- MEDICO -----------------------------
        if($response['evolucao_medica'] != '') {
            $templateProcessor->setValue('evolucao_medica', stripslashes(htmlspecialchars($response['evolucao_medica'])));
        } else {
            $templateProcessor->setValue('quadro_clinico', stripslashes(htmlspecialchars($response['quadro_clinico'])));
            $templateProcessor->setValue('regime_atual', stripslashes(htmlspecialchars($response['regime_atual'])));
            $templateProcessor->setValue('indicacao_tecnica', stripslashes(htmlspecialchars($response['indicacao_tecnica'])));
        }

        $templateProcessor->setValue(
            'data_visita_medica',
            $response['data_visita_medica'] != '0000-00-00' ?
                \DateTime::createFromFormat('Y-m-d', $response['data_visita_medica'])->format('d/m/Y') :
                ""
        );
        $templateProcessor->setValue('medico', $response['medico']);
        $templateProcessor->setValue('crm', $response['crm']);

        # ----------------------- ENFERMAGEM -------------------------
        $templateProcessor->setValue('evolucao_enfermagem', stripslashes(htmlspecialchars($response['evolucao_enfermagem'])));
        $templateProcessor->setValue(
            'data_visita_enfermagem',
            $response['data_visita_enfermagem'] != '0000-00-00' ?
                \DateTime::createFromFormat('Y-m-d', $response['data_visita_enfermagem'])->format('d/m/Y') :
                ""
        );
        $templateProcessor->setValue('enfermeiro', $response['enfermeiro']);
        $templateProcessor->setValue('coren', $response['coren']);


        # ----------------------- NUTRICAO ---------------------------
        $templateProcessor->setValue('evolucao_nutricao',stripslashes(htmlspecialchars($response['evolucao_nutricao'])));

        $templateProcessor->setValue(
            'data_visita_nutricao',
            $response['data_visita_nutricao'] != '0000-00-00' ?
                \DateTime::createFromFormat('Y-m-d', $response['data_visita_nutricao'])->format('d/m/Y') :
                ""
        );
        $templateProcessor->setValue('nutricionista', $response['nutricionista']);
        $templateProcessor->setValue('crn', $response['crn']);


        # ----------------------- FISIOTERAPIA -----------------------
        $templateProcessor->setValue('evolucao_fisioterapia', stripslashes(htmlspecialchars($response['evolucao_fisioterapia'])));
        $templateProcessor->setValue(
            'data_visita_fisioterapia',
            $response['data_visita_fisioterapia'] != '0000-00-00' ?
                \DateTime::createFromFormat('Y-m-d', $response['data_visita_fisioterapia'])->format('d/m/Y') :
                ""
        );
        $templateProcessor->setValue('fisioterapeuta', $response['fisioterapeuta']);
        $templateProcessor->setValue('crefitto', $response['crefitto']);


        # ----------------------- FONOAUDIOLOGIA ---------------------
        $templateProcessor->setValue('evolucao_fonoaudiologia', stripslashes(htmlspecialchars($response['evolucao_fonoaudiologia'])));
        $templateProcessor->setValue(
            'data_visita_fonoaudiologia',
            $response['data_visita_fonoaudiologia'] != '0000-00-00' ?
                \DateTime::createFromFormat('Y-m-d', $response['data_visita_fonoaudiologia'])->format('d/m/Y') :
                ""
        );
        $templateProcessor->setValue('fonoaudiologo', $response['fonoaudiologo']);
        $templateProcessor->setValue('crfa', $response['crfa']);

        # ----------------------- SOLICITACOES -----------------------
        $templateProcessor->setValue('solicitacoes_modalidade',     stripslashes(htmlspecialchars($response['solicitacoes_modalidade'])));
        $templateProcessor->setValue('solicitacoes_fisioterapia',   stripslashes(htmlspecialchars($response['solicitacoes_fisioterapia'])));
        $templateProcessor->setValue('solicitacoes_fonoterapia',    stripslashes(htmlspecialchars($response['solicitacoes_fonoterapia'])));
        $templateProcessor->setValue('solicitacoes_dietas',         stripslashes(htmlspecialchars($response['solicitacoes_dietas'])));
        $templateProcessor->setValue('solicitacoes_curativos',      stripslashes(htmlspecialchars($response['solicitacoes_curativos'])));
        $templateProcessor->setValue('solicitacoes_oxigenoterapia', stripslashes(htmlspecialchars($response['solicitacoes_oxigenoterapia'])));
        $templateProcessor->setValue('solicitacoes_equipamentos',   stripslashes(htmlspecialchars($response['solicitacoes_equipamentos'])));
        $templateProcessor->setValue('solicitacoes_atb_med',        stripslashes(htmlspecialchars($response['solicitacoes_atb_med'])));
        $templateProcessor->setValue('solicitacoes_enfermagem',     stripslashes(htmlspecialchars($response['solicitacoes_enfermagem'])));
        $templateProcessor->setValue('solicitacoes_nutricao',       stripslashes(htmlspecialchars($response['solicitacoes_nutricao'])));
        $templateProcessor->setValue('solicitacoes_medico',         stripslashes(htmlspecialchars($response['solicitacoes_medico'])));

        $fileName = sprintf("Prorrogacao Beneficiario %s.docx", $response['paciente']);
        $path   = '../../adm/secmed/temp/' . $fileName;
        $mimeType   = 'msword';

        $templateProcessor->saveAs($path);


        $prescricao = Prescricoes::getLastPrescricaoCurativoByPaciente($response['paciente_id'], $response['inicio'], $response['fim']);

        if($prescricao !== false) {
            list($curativoPlanservProcessor, $curativoFileName) = Prescricoes::fichaPlanserv($prescricao['id']);
            $curativoPath = '../../enfermagem/prescricao/temp/' . $curativoFileName;


            $curativoPlanservProcessor->saveAs($curativoPath);

            // MONTANDO O ARQUIVO .ZIP, JÁ QUE EXISTE PRESCRIÇÃO DE CURATIVO
            $zipName = sprintf("Beneficiario %s.zip", $response['paciente']);
            $zip = new \ZipArchive();
            $zipPath = '../../adm/secmed/temp/' . $zipName;
            if( $zip->open($zipPath , \ZipArchive::CREATE) === true){
                $zip->addFile($path, $fileName);
                $zip->addFile($curativoPath, $curativoFileName);
                $zip->close();
            }
            $path = $zipPath;
            $mimeType = 'zip';
        }


        ob_start();
        header('Content-Type: application/' . $mimeType);
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename=' . basename($path));
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: cache');
        ob_clean();
        flush();
        readfile($path);
        if($mimeType === 'zip'){
            unlink($path);
            unlink($curativoPath);
            unlink('../../adm/secmed/temp/' . $fileName);
        } else {
            unlink($path);
        }
    }


    public static function salvarProrrogacao ()
    {
        $data = $_POST;
        $inicio = \DateTime::createFromFormat('Y-m-d', $data['inicio'])->format('d/m/Y');
        $fim = \DateTime::createFromFormat('Y-m-d', $data['fim'])->format('d/m/Y');

        list($prorrogacaoId, $response) = Prorrogacao::saveProrrogacaoPlanserv($data);

        if(!$response){
            $msg = "Houve um problema ao salvar essa prorrogação! Entre em contato com o suporte.";
            $msg = base64_encode($msg);
           header("Location: ?action=nova-prorrogacao-planserv&id={$data['paciente']}&inicio={$inicio}&fim={$fim}&msg={$msg}");
        }
        header("Location: ?action=visualizar-prorrogacao-planserv&prorrogacao={$prorrogacaoId}&inicio={$inicio}&fim={$fim}"
        );
    }

    public static function editarProrrogacao ()
    {
        $data = $_POST;
        $inicio = \DateTime::createFromFormat('Y-m-d', $data['inicio'])->format('d/m/Y');
        $fim = \DateTime::createFromFormat('Y-m-d', $data['fim'])->format('d/m/Y');

        $response = Prorrogacao::updateProrrogacaoPlanserv($data);

        if(!$response){
            $msg = "Houve um problema ao editar essa prorrogação! Entre em contato com o suporte.";
            $msg = base64_encode($msg);
            header("Location: ?action=listar-prorrogacao-planserv&id={$data['paciente']}&inicio={$inicio}&fim={$fim}&msg={$msg}");
        }
        header("Location: ?action=visualizar-prorrogacao-planserv&prorrogacao={$data['prorrogacao']}&inicio={$inicio}&fim={$fim}");
    }
}
