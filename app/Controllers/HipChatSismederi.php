<?php
namespace App\Controllers;

use \GorkaLaucirica\HipchatAPIv2Client\Auth\OAuth2;
use \GorkaLaucirica\HipchatAPIv2Client\Client;
use \GorkaLaucirica\HipchatAPIv2Client\API\UserAPI;
use \GorkaLaucirica\HipchatAPIv2Client\API\RoomAPI;

class HipChatSismederi {

	private $auth;
	private $client;
	private $rooms;
	private $roomId;
	private $user;
	private $env;



	function __construct ($token, $env)
	{
		$this->AuthApi($token);
		$this->env = $env;
	}

	private function AuthApi($token)
	{
		$this->auth = new OAuth2($token);
		$this->client = new Client($this->auth);
	}

	/**
	 * Gets user by id, email or mention name
	 * More info: https://www.hipchat.com/docs/apiv2/method/view_user
	 *
	 * @param string $id The id, email address, or mention name (beginning with an '@') of the user to view
	 *
	 * @return User
	 */

	public function getOneUser ($person)
	{
		$userAPI     = new UserAPI($this->client);
		$this->user  = $userAPI->getUser($person);
		return $this->user;
	}

	public function getAllRooms ()
	{
		$roomAPI     = new RoomAPI($this->client );
		$this->rooms = $roomAPI->getRooms(array('max-results' => 30) );
		return $this->rooms;
	}

	/*
	* Gets room by id or name
	* More info: https://www.hipchat.com/docs/apiv2/method/get_room
	*
	* @param string $name The id or name of the room
	*
	* @return Room
	*/
	public function getIdRoomByName ($name)
	{
		$name = str_replace(" ","%20",$name);
		$roomAPI = new RoomAPI($this->client);
		$this->roomId = $roomAPI->getRoom($name);
		return $this->roomId->getId();
	}

	public function getRecentHistoryRoom ($id=null)
	{
		$roomAPI      = new RoomAPI($this->client);
		$HistoryRoom  = $roomAPI->getRecentHistory($id);
		return $HistoryRoom;
	}

	/*
	 * @string id receive the id or name of the room
	 * @string text receive a plan text for be sent like message
	 * @Array args accept the follows values: (color='yellow',notify=true)
	 *
	 * These are colors acceptable by args(color=>" ")
	 *  yellow, green, red, purple, gray, random
	 * */
	public function sendNotificationRoom ($id, $text, $args=array())
	{
		if ($this->env == 'prd') {
			if (empty($args)) {
				$args['color'] = 'yellow';
				$args['notify'] = false;
			} else {
				if (empty($args['color']) || !array_key_exists('color',$args)) {
					$args['color'] = 'yellow';
				}
				if (empty($args['notify']) || !array_key_exists('notify',$args)) {
					$args['notify'] = false;
				}
			}

			$objMessage = new \GorkaLaucirica\HipchatAPIv2Client\Model\Message();
			$objMessage->setColor( $args['color']);
			$objMessage->setNotify( $args['notify']);
			$text       = utf8_encode(strip_tags($text));
			$msg        = $objMessage->setMessage( $text);
			$roomAPI    = new RoomAPI( $this->client);
			try {
				$roomAPI->sendRoomNotification ($id, $msg);
				return true;
			} catch(\GorkaLaucirica\HipchatAPIv2Client\Exception\RequestException $e){
			    //print_r($e);
				return false;
			}
		}
	}
}