<?php
namespace app\Controllers;

use App\Helpers\StringHelper;
use App\Models\Financeiro\Banco;
use App\Models\Financeiro\BorderoTipo;
use App\Models\Financeiro\ContaBancaria;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\Notas;
use \App\Models\Financeiro\Bordero as BorderoModel;
use App\Models\Financeiro\IPCF;
use App\Models\Financeiro\Previsao as FinanceiroPrevisao;
use \GuzzleHttp\Client;
use Dompdf\Dompdf;
use Dompdf\Options;

include_once('../../utils/codigos.php');
ini_set('display_errors', 1);

class Previsao
{
    public static function index()
    {      
        $naturezaPrincipal = IPCF::getAll();
        
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/previsao/templates/index.phtml');
    }

    public static function pesquisarParcelaPrevisao()
    {
        // print_r($_POST);
        // die();
        if(isset($_POST) && !empty($_POST)){
            $tipoDocumentoId = 25; // Id da PR
            $inicio  = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim     = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $naturezaPrincipal = filter_input(INPUT_POST,'natureza-principal', FILTER_SANITIZE_NUMBER_INT);
            $filtros = [
                'inicio' => $inicio,
                'fim' => $fim,
                'natureza-principal' => $naturezaPrincipal,
                'tipo-documento' => $tipoDocumentoId
            ];
            

           $response =  FinanceiroPrevisao::getParcelaPrevisao($filtros);
          
            
        }
        $naturezaPrincipal = IPCF::getAll();
       
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/previsao/templates/index.phtml');
    }

    public static function pesquisarParcelaNota()
    {
         
        if(isset($_GET) && !empty($_GET)){
            $parcelaIdPrevisao = filter_input(INPUT_GET,'parcelaId', FILTER_SANITIZE_NUMBER_INT); 
            $responseParcelaPrevisao = Parcela::getDadosParcelaById($parcelaIdPrevisao); 
            $dadosParcelaPrevisao = [
                'vencimento_real' => $responseParcelaPrevisao['vencimento_real'],
                'natureza_movimentacao'=> $responseParcelaPrevisao['natureza_movimentacao'],
                'valor'=> $responseParcelaPrevisao['valor'],
                'tipo_documento' => 25, // id PR
                'id_parcela' => $parcelaIdPrevisao,
                'id_nota'=> $responseParcelaPrevisao['idNota']
                
            ];
            $response['parcela_previsao'] = $dadosParcelaPrevisao;
           $response['parcelas'] = FinanceiroPrevisao::getParcelaByDadosPrevisao($dadosParcelaPrevisao);
                               
            
        }
        echo json_encode($response);
       
       
    }

    public static function baixarParcelaPrevisao()
    {
        
        if(isset($_POST) && !empty($_POST)){

            $tipoDocumentoId = 25; // Id da PR
            $parcelaIdPrevisao  = filter_input(INPUT_POST, 'parcela-previsao', FILTER_SANITIZE_NUMBER_INT);
            $notaIdPrevisao    = filter_input(INPUT_POST, 'nota-previsao', FILTER_SANITIZE_NUMBER_INT);
            $parcelas = $_POST['parcelas'];            
            $arrayParcelas = [];
            $totalBaixar=0;
            foreach($parcelas as $key => $value){
                $value = StringHelper::moneyStringToFloat($value);
                $insert[] = [
                    'parcela_previsao_id' => $parcelaIdPrevisao,
                    'parcela_nota_id' => $key,
                    'valor' => $value,
                    'created_by'    => $_SESSION['id_user'],
                    'created_at' => date("Y-m-d H:i:s"),
                ];

                $arrayInsert[] = "(
                    {$parcelaIdPrevisao},
                    {$key},
                    {$value},
                    {$_SESSION['id_user']},
                    '".date('Y-m-d H:i:s')."')";

                $totalBaixar += $value;
            }

           
            
            $dados = [
                'parcelaIdPrevisao' => $parcelaIdPrevisao,
                'notaIdPrevisao' => $notaIdPrevisao,
                'insert' => implode(',',$arrayInsert),
                'valorBaixar' => $totalBaixar
            ];
            

           $response =  FinanceiroPrevisao::baixarParcelaPrevisao($dados);
          
           echo json_encode($response);   
        }
    }

    public static function cancelarParcelaPrevisao()
    {
        
        if(isset($_POST) && !empty($_POST)){

           
            $parcelas = $_POST['parcelas'];            
            $arrayParcelas = [];
            $totalBaixar=0;
            foreach($parcelas as $key => $value){
               $dadosNota = Parcela::getNotaByParcelaId($key);
                             
                $arrayParcelas[] = [
                    'parcela_previsao_id' => $key,                    
                    'valor' => $value,
                    'nota_id' => $dadosNota['idNota']
                    
                ];

               
            }

        //    print_r($_SESSION);
        //    print_r($arrayParcelas);
        //    die();

           
            
            
            

           $response =  FinanceiroPrevisao::cancelarParcelaPrevisao($arrayParcelas);
          
           echo json_encode($response);   
        }
    }
       


   
}
