<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 21/11/2017
 * Time: 11:07
 */

namespace App\Controllers;
use App\Models\Administracao\Filial;
use App\Models\Logistica\Estoque;


class RelatorioAjusteEstoque
{
    public static function form()
    {
        $urs            = Filial::getUnidadesAtivas();
        $erro = '';
        if(isset($_POST) && !empty($_POST['inicio']) && !empty($_POST['fim'])){
            $urBusca = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_STRING);
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            if($inicio > $fim){
                $erro = "A data Inicio não pode ser maior que a data Fim.";
            }else{
                $response = Estoque::ajusteEstoque($urBusca, $inicio , $fim);
                $quantidade = count($response);
            }


        }
        require ($_SERVER['DOCUMENT_ROOT'] . '/logistica/relatorios/templates/ajuste-estoque/ajusteEstoque.phtml');
    }

}