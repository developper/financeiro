<?php

namespace App\Controllers;

use \App\Models\Faturamento as Fatu;
use \App\Core\Url;
use App\Helpers\StringHelper as String;

class GuiaTissXML
{

    public static function novaGuia($capaLote, $tipo)
    {

        $response = array();

        $data['tipoSelected']           = $tipo;
        $data['tipos']                  = [Fatu\Guia::SP_SADT => 'SP/SADT', Fatu\Guia::RESUMO_INTERNACAO => 'Resumo de Internação'];
        $data['acomodacoes']            = Fatu\Acomodacao::getAll();
        $data['tiposInternacao']        = Fatu\TipoInternacao::getAll();
        $data['regimesInternacao']      = Fatu\RegimeInternacao::getAll();
        $data['motivosSaidaInternacao'] = Fatu\MotivoSaidaInternacao::getAll();
        $data['tiposFaturamento']       = Fatu\TipoFaturamento::getAll();
        $data['contratados']            = Fatu\Contratado::getAll();
        $data['profissionais']          = Fatu\Profissional::getAll();
        $data['planos']                 = Fatu\PlanoSaude::getAll();
        $data['lote']                   = Fatu\LoteFatura::getDetalhe($capaLote);
        $data['cid10Lista']             = Fatu\Cid10::getAll();
        $data['tiposAtendimento']       = Fatu\TipoAtendimento::getAll();
        $data['indicadoresAcidente']    = Fatu\IndicadorAcidente::getAll();
        $data['tiposSaida']             = Fatu\TipoSaida::getAll();
        $data['tipoOutrasDespesas']     = Fatu\TipoOutrasDespesas::getAll();
        $data['tipoCodigoInterno']      = $tipoCodigoInterno = Fatu\TipoOutrasDespesas::getAllComCodigoInterno();
        $data['tabelasOutrasDespesas']  = Fatu\TabelaTiss::getAll();
        $data['unidadesMedida']         = Fatu\UnidadeMedida::getAll();

        $data['tipoTitulo'] = strcmp($tipo, Fatu\Guia::SP_SADT)  === 0 ?
            sprintf(' (%s)', $data['tipos'][Fatu\Guia::SP_SADT]) :
            sprintf(' (%s)', $data['tipos'][Fatu\Guia::RESUMO_INTERNACAO])
        ;

        $data['faturas']     = Fatu\Fatura::getByCapaLote($capaLote);
        $data['itensFatura'] = array();
        foreach ($data['faturas'] as $fatura) {
            if (empty($fatura['NUM_MATRICULA_CONVENIO']) || empty($fatura['NOME_PLANO_CONVENIO']))
                $response['mensagemTemp']['errors'][] = "Paciente '{$fatura['nomePaciente']}' está com alguns campos vazios: Número da Matrícula ou Tipo de Plano no Convênio.";

            $data['itensFatura'] = array_map(function($item) use ($tipoCodigoInterno, $fatura) {
                $descontoAcrescimo = Fatu\Fatura::calculaDescontoAcrescimo(
                    $item['VALOR_FATURA'],
                    $item['DESCONTO_ACRESCIMO']
                );

                $item['valorFaturaComAcrescimoDesconto'] = $item['VALOR_FATURA'] + $descontoAcrescimo;
                $item['valorTotal'] = $item['VALOR_FATURA'] * $item['QUANTIDADE'];
                $item['incluido'] = $item['INCLUSO_PACOTE'] == 0;
                $item['valorTotalComAcrescimoDesconto'] = $item['valorTotal'] + ($descontoAcrescimo * $item['QUANTIDADE']);

                $item['principio'] = trim($item['principio']);
                $codigoInterno     = $item['TIPO'];
                $item['tiss_tipo'] = array_filter($tipoCodigoInterno, function($itemTipo) use ($codigoInterno) {
                    return $itemTipo['codigo_interno'] == $codigoInterno;
                });

                /**
                 * tipo 0 é Medicamento
                 * tipo 1 é Material
                 * tipo 2 é Procedimentos
                 * tipo 3 é Dieta
                 * tipo 5 é Equipamentos
                 * tipo 6 é Gasoterapia
                 */
                $item['tiss_tabela'] = 1; // Tabela Própria da Operadora
                $item['NUMERO_TISS'] = $item['NUMERO_TISS'];
                $item['NUMERO_TISS'] = str_replace('.', '', $item['NUMERO_TISS']);
                if($codigoInterno == 0 || $codigoInterno == 1 || $codigoInterno == 3){
                    $item['NUMERO_TISS'] = String::stringPad($item['NUMERO_TISS'], 10);
                }

                if (isset($item['TUSS']) && !empty($item['TUSS']) && ($codigoInterno == 0||
                        $codigoInterno == 1 || $codigoInterno == 3)) {
                    $item['tiss_tabela'] = $codigoInterno ==0 ? 6 : $item['tiss_tabela']; // TUSS - Medicamentos

                    $item['NUMERO_TISS'] = String::stringPad($item['TUSS'], 8);
                }
                $item['tiss_data'] = \DateTime::createFromFormat('Y-m-d', $fatura['DATA_INICIO']);

                return $item;
            }, Fatu\Fatura::getItensByFaturaId($fatura['ID']));

            $data['itens'][$fatura['ID']]['fatura']                   = $fatura;
            $data['itens'][$fatura['ID']]['procedimentosRealizados']  = array_filter($data['itensFatura'], function($item) { return $item['TIPO'] == 2; });
            $data['itens'][$fatura['ID']]['outrasDespesas']           = array_filter($data['itensFatura'], function($item) { return $item['TIPO'] != 2; });

            $data['itens'][$fatura['ID']]['valorTotal']['procedimentos'] = array_reduce(
                $data['itens'][$fatura['ID']]['procedimentosRealizados'],
                function($buffer, $item) {
                    return $buffer += ($item['valorTotal']);
                }
            );
            $data['itens'][$fatura['ID']]['valorTotal']['procedimentosComAcrescimoDesconto'] = array_reduce(
                $data['itens'][$fatura['ID']]['procedimentosRealizados'],
                function($buffer, $item) {
                    return $buffer += ($item['valorTotalComAcrescimoDesconto']);
                }
            );
            $data['itens'][$fatura['ID']]['valorTotal']['outrasDespesas'] = array_reduce(
                $data['itens'][$fatura['ID']]['outrasDespesas'],
                function($buffer, $item) {
                    return $buffer += ($item['valorTotal']);
                }
            );
            $data['itens'][$fatura['ID']]['valorTotal']['outrasDespesasComAcrescimoDesconto'] = array_reduce(
                $data['itens'][$fatura['ID']]['outrasDespesas'],
                function($buffer, $item) {
                    return $buffer += ($item['valorTotalComAcrescimoDesconto']);
                }
            );
        }
        $response['data'] = $data;

        return $response;
    }

    public static function salvarGuia($data)
    {
        $response = array();
        $guiaSADT = new Fatu\GuiaSADT($data);

        if ($guiaSADT->save()) {
            $response['lote'] = Fatu\GuiaSADT::getLastLoteId();
            $response['mensagemTemp']['success'][] = "Sua Guia SADT foi salva com sucesso!";
        } else {
            $response['mensagemTemp']['errors'][] = "Ocorreu um erro inesperado e sua guia não pode ser salva!";
        }
        return $response;
    }

    public static function processarXML($lote, $versao = '3.03.01', $tipoTransacao = 'ENVIO_LOTE_GUIAS')
    {
        $response  = array();
        $capaLote  = ! isset($lote['numero_lote']) ?: $lote['numero_lote'];
        $loteId    = $lote['id'];

        $guias     = Fatu\GuiaSADT::getGuiasByLoteId($loteId);
        foreach ($guias as &$guia) {
            $guia = current(array_map(Fatu\GuiaSADT::fillAll(), [$guia]));

            $cabecalho = array(
                'tipoGuia'     => $guia['tipo_guia'],
                'numeroLote'   => $loteId,
                'seqTransacao' => $guia['guiaId'],
                'tpTransacao'  => $tipoTransacao,
                'origem'       => array('tipoIdentificacao' =>$guia['identificacaoPrestador'],
                    'codigoIdentificacao' => $guia['codigoNaOperadora']),
                'destino'      => array('registroANS' => $guia['planoSaude']['registro_ans']),
            );
        }

        $data = Fatu\XML\GuiaTiss::createData($cabecalho, $guias, $loteId); //v3
        


        $guiaTissXML = new Fatu\XML\GuiaTissXML;
        $xml = $guiaTissXML->importFromData($data, $versao,$guia['planosdesaude_id']);


        if ($guiaTissXML->isValid()) {
            $version = Fatu\XML\IdentificacaoSoftwareGerador::VERSION_030301;
            if($versao == '3.03.00'){
                $version = Fatu\XML\IdentificacaoSoftwareGerador::VERSION_030300;
            } elseif($versao == '3.02.00'){
                $version = Fatu\XML\IdentificacaoSoftwareGerador::VERSION_030200;
            }elseif($versao == '3.03.02'){
                $version = Fatu\XML\IdentificacaoSoftwareGerador::VERSION_030302;
            }



            $epilogo = $guiaTissXML->getEpilogo();

            $xmlId = Fatu\GuiaSADT::saveXML($loteId, $guiaTissXML->xml->asXML(), 'Content-type: text/xml; charset=ISO-8859-1', $epilogo, $version);

            if ($xmlId) {
                $urlXML = Url::get('tiss-baixar-xml', ['xmlId' => $xmlId, 'capalote' => $capaLote]);
                $response['mensagemTemp']['success'][] =
                    'O arquivo XML da Guia foi gerado com sucesso. <a href="' . $urlXML . '">Clique aqui</a> para baixá-lo!';
            } else {
                $response['mensagemTemp']['errors'][] = 'Ocorreu algum erro ao tentar salvar o arquivo XML!';
            }
        } else {
            $response['mensagemTemp']['errors'][] = 'XML gerado não é válido!';
        }

        return $response;
    }

    public static function baixarXML($xmlId)
    {
        $xml = Fatu\GuiaSADT::getXMLById($xmlId);
        $nomeArquivo = str_pad($xml[0]['id'], 20, '0', STR_PAD_LEFT) . '_' . $xml[0]['epilogo'];
        if (isset($xml) && !empty($xml)) {
            header($xml[0]['header']);
            header('Content-Disposition: attachment; filename="' . $nomeArquivo . '.xml"');
            echo $xml[0]['plan_text'];
        }
    }

    public static function versaoImpressao($guiaId)
    {
        $response = array();
        $response['guiaSadt'] = array_map(Fatu\GuiaSADT::fillAll(), Fatu\GuiaSADT::getByGuiaSadtId($guiaId));
        $cabecalho = array(
            'tipoGuia'      => $response['guiaSadt'][0]['tipoGuia'],
            'numeroLote'    => null,
            'seqTransacao'  => $guiaId,
            'tpTransacao'   => isset($tipoTransacao) ? $tipoTransacao : null,
            'origem'        => array('cnpj' => $response['guiaSadt'][0]['contratado']['cnpj']),
            'destino'       => array('registroANS' => $response['guiaSadt'][0]['planoSaude']['registro_ans']),
        );
        $response['data'] = Fatu\XML\GuiaTiss::createData($cabecalho, $response['guiaSadt'], null);
        return $response;
    }

    public static function guiasListar($loteId)
    {
        $response = array();
        $response['guias'] = array_map(Fatu\GuiaSADT::fillAll(), Fatu\GuiaSADT::getSpSadtByLoteId($loteId));
        return $response;
    }

}