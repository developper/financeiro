<?php

namespace app\Controllers;

require_once('../utils/codigos.php');

use App\Helpers\StringHelper;
use App\Models\Administracao\Filial;
use App\Models\Administracao\Paciente;
use App\Models\Administracao\Planos;
use App\Models\Financeiro\ContaBancaria;
use App\Models\Financeiro\Fornecedor;
use App\Models\Remocao\Remocao as ModelRemocao;
use \App\Models\DB\ConnMysqli;
use App\Models\Sistema\AtendimentoPaciente;
use App\Services\Gateways\SendGridGateway;
use App\Services\MailAdapter;
use App\Core\Config;
use JansenFelipe\CpfGratis\CpfGratis;

class Remocao
{
    private static function getLists()
    {
        return [
            'tipo_remocao'      => ['C' => 'Interna', 'P' => 'Externa'],
            'target'            => ['C' => 'convenio', 'P' => 'particular'],
            'tipo_ambulancia'   => ['S' => 'Simples', 'U' => 'UTI'],
            'perimetro'         => ['D' => 'Dentro', 'F' => 'Fora'],
            'sim_nao'           => ['S' => 'Sim', 'N' => 'Não'],
            'dentro_fora'           => ['D' => 'Dentro', 'F' => 'Fora'],
            'tipo_item'    => [
                '0' => 'Medicamento',
                '1' => 'Material',
                '3' => 'Dieta',
                '5' => 'Equipamento'
            ],
            'motivo_remocao'    => [
                'ADM' => 'Admissão',
                'EXA' => 'Exame',
                'INT' => 'Intercorrência',
                'TRA' => 'Transferência'
            ],
            'forma_pagamento'    => [
                'DIN' => 'Dinheiro',
                'CHQ' => 'Cheque',
                'CRT' => 'Cartão',
                'TRB' => 'Transação Bancária'
            ],
            'modo_cobranca'      => ['F' => 'Fatura', 'NF' => 'Nota Fiscal'],
        ];
    }

    public static function menu()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/menu.phtml');
    }

    public static function convenioForm()
    {
        $planos = Planos::getPlanosAtivos();
        $pacientes = Paciente::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/convenio-form.phtml');
    }

    public static function dadosPaciente()
    {
        if(isset($_GET) && !empty($_GET)){
            $idPaciente = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $paciente = Paciente::getById($idPaciente);
            echo json_encode(array('idPlano'=>$paciente['convenio'],
                                    'cpf'=>$paciente['cpf'],
                                    'nascimento'=>$paciente['nascimento'],
                                    'matricula'=>$paciente['NUM_MATRICULA_CONVENIO'],
                                    'telefone_paciente'=>$paciente['TEL_DOMICILIAR']));
        }
    }

    public static function salvarRemocaoConvenio()
    {
        if(isset($_POST) && !empty($_POST)){

            $solicitante            = filter_input(INPUT_POST, 'solicitante', FILTER_SANITIZE_STRING);
            $telefoneSolicitante    = filter_input(INPUT_POST, 'telefone-solicitante', FILTER_SANITIZE_STRING);
            $pacienteId             = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $matricula              = filter_input(INPUT_POST, 'matricula', FILTER_SANITIZE_STRING);
            $cpf                    = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING);
            $nascimento             = filter_input(INPUT_POST, 'nascimento', FILTER_SANITIZE_STRING);
            $telefonePaciente       = filter_input(INPUT_POST, 'telefone-paciente', FILTER_SANITIZE_STRING);
            $convenio               = filter_input(INPUT_POST, 'convenio', FILTER_SANITIZE_NUMBER_INT);
            $localOrigem            = filter_input(INPUT_POST, 'local-origem', FILTER_SANITIZE_STRING);
            $enderecoOrigem       = filter_input(INPUT_POST, 'endereco-origem', FILTER_SANITIZE_STRING);
            $localDestino           = filter_input(INPUT_POST, 'local-destino', FILTER_SANITIZE_STRING);
            $enderecoDestino         = filter_input(INPUT_POST, 'endereco-destino', FILTER_SANITIZE_STRING);
            $motivoRemocao          = filter_input(INPUT_POST, 'motivo-remocao', FILTER_SANITIZE_STRING);
            $tipoAmbulancia         = filter_input(INPUT_POST, 'tipo-ambulancia', FILTER_SANITIZE_STRING);
            $retorno                = filter_input(INPUT_POST, 'retorno', FILTER_SANITIZE_STRING);
            $dataSolicitacao        = filter_input(INPUT_POST, 'data-solicitacao', FILTER_SANITIZE_STRING);
            $dadosPaciente          = Paciente::getById($pacienteId);
            $nomePaciente           = $dadosPaciente['nome'];

            $remocao = new ModelRemocao(ConnMysqli::getConnection());

            $x = $remocao->salvarRemocaoConvenio(
                $solicitante,
                $telefoneSolicitante,
                $telefonePaciente,
                $pacienteId,
                $matricula,
                $cpf,
                $nascimento,
                $convenio,
                $localOrigem,
                $enderecoOrigem,
                $enderecoDestino,
                $localDestino,
                $motivoRemocao,
                $tipoAmbulancia,
                $retorno,
                $nomePaciente,
                $dataSolicitacao
            );

            echo $x;
        }
    }

    public static function pesquisarForm()
    {
        $pacientes = Paciente::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/pesquisar-form.phtml');
    }

    public static function pesquisarRemocao()
    {
        if(isset($_POST) && !empty($_POST)){
            $pacienteId  = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_STRING);
            $tipoRemocao = filter_input(INPUT_POST, 'tipo-remocao', FILTER_SANITIZE_STRING);
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $remocoes = ModelRemocao::getRemocaoByFilter($pacienteId,$tipoRemocao,$inicio,$fim);
            $pacientes = Paciente::getAll();
            $tipoRemocaoList = self::getLists()['tipo_remocao'];
            $targetList = self::getLists()['target'];

            require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/pesquisar-form.phtml');
        }
    }

    public static function finalizarRemocaoConvenioForm()
    {
        if(isset($_GET) && !empty($_GET)){
            $remocaoId = $_GET['remocao'];
            $remocao = ModelRemocao::getRemocaoById($remocaoId);
            $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFaturaPlano($remocao['convenio_id']);
            $tabelaReferenciaFatura = 'valorescobranca';


            if(empty($tipoRemocaFatura)){
                $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFatura();
                $tabelaReferenciaFatura = 'cobrancaplanos';
            }
            $empresas = Filial::getUnidadesAtivas();
            $lists = self::getLists();

            require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/convenio-finalizar-form.phtml');
        }
    }

    public static function finalizarRemocaoConvenio()
    {
        if(isset($_POST) && !empty($_POST)){
            $autorizacao        = filter_input(INPUT_POST, 'autorizacao', FILTER_SANITIZE_STRING);
            $remocao_realizada  = filter_input(INPUT_POST, 'remocao-realizada', FILTER_SANITIZE_STRING);
            $motivo             = filter_input(INPUT_POST, 'motivo', FILTER_SANITIZE_STRING);
            $km                 = filter_input(INPUT_POST, 'km', FILTER_SANITIZE_STRING);
            $perimetro          = filter_input(INPUT_POST, 'perimetro', FILTER_SANITIZE_STRING);
            $data_saida         = filter_input(INPUT_POST, 'data-saida', FILTER_SANITIZE_STRING);
            $hora_saida         = filter_input(INPUT_POST, 'hora-saida', FILTER_SANITIZE_STRING);
            $data_chegada       = filter_input(INPUT_POST, 'data-chegada', FILTER_SANITIZE_STRING);
            $hora_chegada       = filter_input(INPUT_POST, 'hora-chegada', FILTER_SANITIZE_STRING);
            $tabela_propria     = filter_input(INPUT_POST, 'tabela-propria', FILTER_SANITIZE_STRING);
            $valor_remocao      = filter_input(INPUT_POST, 'valor-remocao', FILTER_SANITIZE_STRING);
            $medico             = filter_input(INPUT_POST, 'medico', FILTER_SANITIZE_STRING);
            $valor_medico       = filter_input(INPUT_POST, 'valor-medico', FILTER_SANITIZE_STRING);
            $tecnico            = filter_input(INPUT_POST, 'tecnico', FILTER_SANITIZE_STRING);
            $valor_tecnico      = filter_input(INPUT_POST, 'valor-tecnico', FILTER_SANITIZE_STRING);
            $condutor           = filter_input(INPUT_POST, 'condutor', FILTER_SANITIZE_STRING);
            $valor_condutor     = filter_input(INPUT_POST, 'valor-condutor', FILTER_SANITIZE_STRING);
            $remocao_fatura     = filter_input(INPUT_POST, 'remocao-fatura', FILTER_SANITIZE_STRING);
            $outrasDespesas     = filter_input(INPUT_POST, 'outras-despesas', FILTER_SANITIZE_STRING);
            $remocaoId          = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $tabelaOrigemItemFatura     = filter_input(INPUT_POST, 'tabela-origem-item-fatura', FILTER_SANITIZE_STRING);
            $empresa          = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_NUMBER_INT);
            $enfermeiro           = filter_input(INPUT_POST, 'enfermeiro', FILTER_SANITIZE_STRING);
            $valor_enfermeiro      = filter_input(INPUT_POST, 'valor-enfermeiro', FILTER_SANITIZE_STRING);
            $remocaoSeraCobrada = filter_input(INPUT_POST, 'remocao_sera_cobrada', FILTER_SANITIZE_STRING);
            $justificativaNaoCobrar = filter_input(INPUT_POST, 'justificativa_nao_cobrar', FILTER_SANITIZE_STRING);

            $itens              = [
                'catalogo_id' => $_POST['catalogo_id'],
                'tipo' => $_POST['tipo'],
                'tiss' => $_POST['tiss'],
                'qtd' => $_POST['qtd'],
            ];



            $remocao = new ModelRemocao(ConnMysqli::getConnection());

            $x = $remocao->finalizarRemocaoConvenio(
                $autorizacao,
                $remocao_realizada,
                $motivo,
                $km,
                $perimetro,
                $data_saida,
                $hora_saida,
                $data_chegada,
                $hora_chegada,
                $tabela_propria,
                $valor_remocao,
                $medico,
                $valor_medico,
                $tecnico,
                $valor_tecnico,
                $condutor,
                $valor_condutor,
                $remocao_fatura,
                $remocaoId,
                $outrasDespesas,
                $itens,
                $tabelaOrigemItemFatura,
                $empresa,
                $enfermeiro,
                $valor_enfermeiro,
                $remocaoSeraCobrada,
                $justificativaNaoCobrar

            );

            echo $x;
        }
    }

    public static function visualizarRemocaoConvenio()
    {
        if(isset($_GET) && !empty($_GET)){
            $remocaoId = $_GET['remocao'];
            $remocao = ModelRemocao::getRemocaoById($remocaoId);
            $itens = ModelRemocao::getItensRemocao($remocaoId);
            $tipoRemocaFatura ='';
            if($remocao['tabela_origem_item_fatura'] == 'valorescobranca'){
                if(!empty($remocao['valorescobranca_id']))
                    $itemValoresCobranca =  ModelRemocao::getItemValoresCobrancaById($remocao['valorescobranca_id']);
                    $tipoRemocaFatura = $itemValoresCobranca[0]['DESC_COBRANCA_PLANO'];

            }else{

                if(!empty($remocao['cobrancaplano_id'])){
                    $itemCobrancaPlano =  ModelRemocao::getItemCobrancaPlanoById($remocao['cobrancaplano_id']);
                    $tipoRemocaFatura = $itemCobrancaPlano[0]['item'];
                }

            }

            $lists = self::getLists();
            $empresa = '';
            if(!empty($remocao['empresa_id'])){
                $empresa = Filial::getById($remocao['empresa_id']);
            }

            require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/convenio-visualizar.phtml');
        }
    }

    public static function excluirRemocao()
    {
        if(isset($_POST['id']) && !empty($_POST['id']) && is_int(intval($_POST['id']))) {
            $remocao = new ModelRemocao(ConnMysqli::getConnection());
            $idRemocao = $_POST['id'];
            try {
                $response = $remocao->excluirRemocao($idRemocao);
                echo $response;
                $responseAtendimentoProcedimento = AtendimentoPaciente::getAtendimentoByTabelaId('remocoes',$idRemocao);
                $status = 'F';
                if(!empty($responseAtendimentoProcedimento)){
                    $finalizado = AtendimentoPaciente::getAtendimentoPacienteFinalizadoByfilter($responseAtendimentoProcedimento['atendimento_paciente_id']);
                    if(empty($finalizado)){
                        AtendimentoPaciente::finalizar($responseAtendimentoProcedimento['atendimento_paciente_id'],$status);
                    }
                }
                return true;
            } catch (\Exception $e) {
                $msg = base64_encode($e->getMessage());
            }
        }

        return false;
    }

    public static function particularForm()
    {
        $planos = Planos::getPlanosAtivos();
        $pacientes = Paciente::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/particular-form.phtml');
    }

    public static function salvarRemocaoParticular()
    {
        if(isset($_POST) && !empty($_POST)){

            $solicitante            = filter_input(INPUT_POST, 'solicitante', FILTER_SANITIZE_STRING);
            $telefoneSolicitante    = filter_input(INPUT_POST, 'telefone-solicitante', FILTER_SANITIZE_STRING);
            $paciente               = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_STRING);
            $matricula              = filter_input(INPUT_POST, 'matricula', FILTER_SANITIZE_STRING);
            $cpf                    = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING);
            $nascimento             = filter_input(INPUT_POST, 'nascimento', FILTER_SANITIZE_STRING);
            $telefonePaciente       = filter_input(INPUT_POST, 'telefone-paciente', FILTER_SANITIZE_STRING);
            $convenio               = filter_input(INPUT_POST, 'convenio', FILTER_SANITIZE_NUMBER_INT);
            $localOrigem            = filter_input(INPUT_POST, 'local-origem', FILTER_SANITIZE_STRING);
            $enderecoOrigem       = filter_input(INPUT_POST, 'endereco-origem', FILTER_SANITIZE_STRING);
            $localDestino           = filter_input(INPUT_POST, 'local-destino', FILTER_SANITIZE_STRING);
            $enderecoDestino         = filter_input(INPUT_POST, 'endereco-destino', FILTER_SANITIZE_STRING);
            $motivoRemocao          = filter_input(INPUT_POST, 'motivo-remocao', FILTER_SANITIZE_STRING);
            $tipoAmbulancia         = filter_input(INPUT_POST, 'tipo-ambulancia', FILTER_SANITIZE_STRING);
            $retorno                = filter_input(INPUT_POST, 'retorno', FILTER_SANITIZE_STRING);
            $dataSolicitacao        = filter_input(INPUT_POST, 'data-solicitacao', FILTER_SANITIZE_STRING);
            $pacienteExiste         = filter_input(INPUT_POST, 'paciente-existe', FILTER_SANITIZE_STRING );
            $nomePaciente           = filter_input(INPUT_POST, 'nome-paciente', FILTER_SANITIZE_STRING );
            $sexo           = filter_input(INPUT_POST, 'sexo', FILTER_SANITIZE_STRING );
            $modalidade = 13;
            $empresa = null;

            if($pacienteExiste =='N'){
                $paciente  = Paciente::insertPacienteRemocao($nomePaciente,
                    $convenio,
                    $cpf,
                    $nascimento,
                    $matricula,
                    $telefonePaciente,
                    $sexo,
                    $empresa);

                if(!is_int($paciente)){
                    echo $paciente;
                    return;
                }
            }

            $dadosPaciente          = Paciente::getById($paciente);
            $nomePaciente           = $dadosPaciente['nome'];

            $remocao = new ModelRemocao(ConnMysqli::getConnection());

            $responseRemocao = $remocao->salvarRemocaoParticular(
                $solicitante,
                $telefoneSolicitante,
                $telefonePaciente,
                $paciente,
                $matricula,
                $convenio,
                $localOrigem,
                $enderecoOrigem,
                $enderecoDestino,
                $localDestino,
                $motivoRemocao,
                $tipoAmbulancia,
                $retorno,
                $dataSolicitacao,
                $nomePaciente
            );

            if(!is_int($responseRemocao)){
                if($pacienteExiste =='N'){
                    $deletePaciente = Paciente::deletePacienteRemocao($paciente);
                    if($deletePaciente <> 1){
                        echo $deletePaciente;
                    }
                }
                echo $responseRemocao;
                return;
            }

            $responseAtendimento = AtendimentoPaciente::salvar($paciente,$modalidade,$convenio,$matricula,$empresa);

            if(!is_int($responseAtendimento)){
                if($pacienteExiste =='N'){
                    $deletePaciente = Paciente::deletePacienteRemocao($paciente);
                    if($deletePaciente <> 1){
                        echo $deletePaciente;
                    }
                }

                $deleteRemocao = \app\Models\Remocao\Remocao::deleteRemocao($responseRemocao);
                if($deleteRemocao <> 1){
                    echo $deleteRemocao;
                }
                echo $responseAtendimento;
                return;

            }
            $atendimentoId = $responseAtendimento;
            $tabelaId = $responseRemocao;
            $tabela = 'remocoes';

            $responseAtendimentoProcedimento = AtendimentoPaciente::salvarProcedimento($atendimentoId,$tabela,$tabelaId);
            if(!is_int($responseAtendimentoProcedimento)){
                if($pacienteExiste =='N'){
                    $deletePaciente = Paciente::deletePacienteRemocao($paciente);
                    if($deletePaciente <> 1){
                        echo $deletePaciente;
                    }
                }
                $deleteRemocao = \app\Models\Remocao\Remocao::deleteRemocao($responseRemocao);
                if($deleteRemocao <> 1){
                    echo $deleteRemocao;
                }
                $deleteAtendimento = AtendimentoPaciente::deleteRemocao($responseAtendimento);
                if( $deleteAtendimento <> 1){
                    echo  $deleteAtendimento;
                }
                echo $responseAtendimentoProcedimento;
                return;

            }

            echo 1;
        }
    }

    public static function finalizarRemocaoParticularForm()
    {
        if(isset($_GET) && !empty($_GET)){
            $remocaoId = $_GET['remocao'];
            $remocao = ModelRemocao::getRemocaoById($remocaoId);
            $lists = self::getLists();
            $fornecedores = Fornecedor::getAll();
            $empresas = Filial::getUnidadesAtivas();

            $contasBancarias = $_SESSION['empresa_principal'] == 1 ? ContaBancaria::getAll() : ContaBancaria::getContasBancariasByUR($_SESSION['empresa_principal']);

            $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFaturaPlano($remocao['convenio_id']);
            $tabelaReferenciaFatura = 'valorescobranca';

            if(empty($tipoRemocaFatura)){
                $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFatura();
                $tabelaReferenciaFatura = 'cobrancaplanos';
            }

            require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/particular-finalizar-form.phtml');
        }
    }

    public static function finalizarRemocaoParticular()
    {
        $fornecedor = [];
        $response = [];
        if(isset($_POST) && !empty($_POST)){
            $fornecedor_cliente = filter_input(INPUT_POST, 'fornecedor_cliente', FILTER_SANITIZE_NUMBER_INT);
            if($fornecedor_cliente == '') {
                $fornecedor['fisica_juridica'] = filter_input(INPUT_POST, 'fisica_juridica', FILTER_SANITIZE_STRING);
                $fornecedor['cpf'] = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING);
                $fornecedor['cnpj'] = filter_input(INPUT_POST, 'cnpj', FILTER_SANITIZE_STRING);
                $fornecedor['nome_fantasia'] = filter_input(INPUT_POST, 'nome_fantasia', FILTER_SANITIZE_STRING);
                $fornecedor['razao_social'] = filter_input(INPUT_POST, 'razao_social', FILTER_SANITIZE_STRING);
                $fornecedor['ies_rg'] = filter_input(INPUT_POST, 'ies_rg', FILTER_SANITIZE_STRING);
                $fornecedor['im'] = filter_input(INPUT_POST, 'im', FILTER_SANITIZE_STRING);
                $fornecedor['contato'] = filter_input(INPUT_POST, 'contato', FILTER_SANITIZE_STRING);
                $fornecedor['telefone'] = filter_input(INPUT_POST, 'telefone', FILTER_SANITIZE_STRING);
                $fornecedor['endereco'] = filter_input(INPUT_POST, 'endereco', FILTER_SANITIZE_STRING);
                $fornecedor['cidade'] = filter_input(INPUT_POST, 'id_cidade_cliente', FILTER_SANITIZE_NUMBER_INT);
                $fornecedor['email'] = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
                $fornecedor['validado_receita'] = filter_input(INPUT_POST, 'cpf_receita_validado', FILTER_SANITIZE_STRING);

                if($fornecedor['fisica_juridica']  == 1 ){
                    $responseCnpj = Fornecedor::checkIfExistsFornecedorByCNPJ($fornecedor['cnpj'],'C');
                    if(!empty($responseCnpj)){
                        echo "Já existe Cliente cadastrado com esse CNPJ {$fornecedor['cnpj']}, no Sitema.";
                        return;
                    }
                }else{
                    $responseCpf = Fornecedor::checkIfExistsFornecedorByCPF($fornecedor['cpf'],'C');
                    if(!empty($responseCpf)){
                        echo "Já existe Cliente cadastrado com esse CPF {$fornecedor['cpf']}, no Sitema.";
                        return;
                    }
                }

                $fornecedor_cliente = Fornecedor::createFornecedorByRemocao($fornecedor);
            }

            $km                 = filter_input(INPUT_POST, 'km', FILTER_SANITIZE_STRING);
            $perimetro          = filter_input(INPUT_POST, 'perimetro', FILTER_SANITIZE_STRING);
            $data_saida         = filter_input(INPUT_POST, 'data-saida', FILTER_SANITIZE_STRING);
            $hora_saida         = filter_input(INPUT_POST, 'hora-saida', FILTER_SANITIZE_STRING);
            $data_chegada       = filter_input(INPUT_POST, 'data-chegada', FILTER_SANITIZE_STRING);
            $hora_chegada       = filter_input(INPUT_POST, 'hora-chegada', FILTER_SANITIZE_STRING);
            $tabela_propria     = filter_input(INPUT_POST, 'tabela-propria', FILTER_SANITIZE_STRING);
            $valor_remocao      = filter_input(INPUT_POST, 'valor-remocao', FILTER_SANITIZE_STRING);
            $outros_custos      = filter_input(INPUT_POST, 'outros_custos', FILTER_SANITIZE_STRING);
            $medico             = filter_input(INPUT_POST, 'medico', FILTER_SANITIZE_STRING);
            $valor_medico       = filter_input(INPUT_POST, 'valor-medico', FILTER_SANITIZE_STRING);
            $tecnico            = filter_input(INPUT_POST, 'tecnico', FILTER_SANITIZE_STRING);
            $valor_tecnico      = filter_input(INPUT_POST, 'valor-tecnico', FILTER_SANITIZE_STRING);
            $condutor           = filter_input(INPUT_POST, 'condutor', FILTER_SANITIZE_STRING);
            $valor_condutor     = filter_input(INPUT_POST, 'valor-condutor', FILTER_SANITIZE_STRING);
            $recebido           = filter_input(INPUT_POST, 'recebido', FILTER_SANITIZE_STRING);
            $forma_pagamento    = filter_input(INPUT_POST, 'forma_pagamento', FILTER_SANITIZE_STRING);
            $observacao         = filter_input(INPUT_POST, 'observacao', FILTER_SANITIZE_STRING);
            $remocaoId          = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $empresa          = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_NUMBER_INT);
            $enfermeiro           = filter_input(INPUT_POST, 'enfermeiro', FILTER_SANITIZE_STRING);
            $valor_enfermeiro      = filter_input(INPUT_POST, 'valor-enfermeiro', FILTER_SANITIZE_STRING);
            $remocaoSeraCobrada = filter_input(INPUT_POST, 'remocao_sera_cobrada', FILTER_SANITIZE_STRING);
            $modoCobranca = filter_input(INPUT_POST, 'modo_cobranca', FILTER_SANITIZE_STRING);
            $justificativaNaoCobrar = filter_input(INPUT_POST, 'justificativa_nao_cobrar', FILTER_SANITIZE_STRING);
            $remocaoFatura = filter_input(INPUT_POST, 'remocao-fatura', FILTER_SANITIZE_STRING);
            $tabelaOrigemItemFatura     = filter_input(INPUT_POST, 'tabela-origem-item-fatura', FILTER_SANITIZE_STRING);
            $contaRecebido =   filter_input(INPUT_POST, 'conta_recebido', FILTER_SANITIZE_STRING);
            $recebidoEm =   filter_input(INPUT_POST, 'recebido_em', FILTER_SANITIZE_STRING);

            $itens              = [
                'catalogo_id' => $_POST['catalogo_id'],
                'tipo' => $_POST['tipo'],
                'tiss' => $_POST['tiss'],
                'qtd' => $_POST['qtd'],
            ];

            $remocao = new ModelRemocao(ConnMysqli::getConnection());

            $x = $remocao->finalizarRemocaoParticular(
                $fornecedor_cliente,
                $km,
                $perimetro,
                $data_saida,
                $hora_saida,
                $data_chegada,
                $hora_chegada,
                $tabela_propria,
                $valor_remocao,
                $outros_custos,
                $medico,
                $valor_medico,
                $tecnico,
                $valor_tecnico,
                $condutor,
                $valor_condutor,
                $recebido,
                $forma_pagamento,
                $observacao,
                $remocaoId,
                $itens,
                $empresa,
                $enfermeiro,
                $valor_enfermeiro,
                $remocaoSeraCobrada,
                $modoCobranca,
                $justificativaNaoCobrar,
                $remocaoFatura,
                $tabelaOrigemItemFatura,
                $contaRecebido,
                $recebidoEm
            );

            if(!empty($x)){

                $response = ModelRemocao::getRemocaoById($remocaoId);
                if($response['convenio_id'] == 1 || $response['modo_cobranca'] == 'NF'  ){
                    $nomePaciente = $response['paciente'];
                    $valorRemocao = StringHelper::currencyFormat($response['valor_remocao']);
                    $nomeUsuario = $_SESSION['nome_user'];
                    $finalizadoEm = \DateTime::createFromFormat('Y-m-d H:i:s', $response['finalized_at'])->format('d/m/Y');
                    $titulo = "Aviso de Remoção Externa  finalizada";
                    $texto = "Uma Remoção Externa  do paciente {$nomePaciente}, no valor de {$valorRemocao} foi finalizada
                    por {$nomeUsuario} em {$finalizadoEm}";
                    $para[] = ['email' => 'lancamentos@mederi.com.br', 'nome'=> 'Lancamentos'];
                    $para[] = ['email' => 'financeiro02@mederi.com.br', 'nome'=> 'Financeiro 02'];


                    self::enviarEmailSimples($titulo, $texto, $para);
                }elseif ($response['modo_cobranca'] == 'F'){
                    $nomePaciente = $response['paciente'];
                    $valorRemocao = StringHelper::currencyFormat($response['valor_remocao']);
                    $nomeUsuario = $_SESSION['nome_user'];
                    $finalizadoEm = \DateTime::createFromFormat('Y-m-d H:i:s', $response['finalized_at'])->format('d/m/Y');
                    $titulo = "Aviso de Remoção Finalizada para ser cobrada por Fatura";
                    $texto = "Uma Remoção  do paciente {$nomePaciente}, no valor de {$valorRemocao} foi finalizada
                    por {$nomeUsuario} em {$finalizadoEm}";
                    $para[] = ['email' => 'faturamento@mederi.com.br', 'nome'=> 'Faturamento'];

                }


            }

            $responseAtendimentoProcedimento = AtendimentoPaciente::getAtendimentoByTabelaId('remocoes',$remocaoId);
            $responseAtendimento = AtendimentoPaciente::getById($responseAtendimentoProcedimento['atendimento_paciente_id']);
            $dadosPaciente = Paciente::getById($responseAtendimento['paciente_id']);

            

            if(!empty($responseAtendimentoProcedimento)){
                if(!empty($empresa)){
                    AtendimentoPaciente::updateEmpresa($empresa,$responseAtendimentoProcedimento['atendimento_paciente_id']);
                    if(empty($dadosPaciente['empresa'])){
                        Paciente::updateEmpresaById($dadosPaciente['idClientes'], $empresa);
                    }

                }
                $status = 'F';
                AtendimentoPaciente::finalizar($responseAtendimentoProcedimento['atendimento_paciente_id'],$status);
            }  

            echo $x;
        }
    }

    public static function visualizarRemocaoParticular()
    {

        if(isset($_GET) && !empty($_GET)){
            $remocaoId = $_GET['remocao'];
            $remocao = ModelRemocao::getRemocaoById($remocaoId);
            $itens = ModelRemocao::getItensRemocao($remocaoId);
            $cliente = Fornecedor::getById($remocao['fornecedor_cliente'])['nome'];
            $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFatura();
            $lists = self::getLists();
            $empresa = '';
            $resultContaBancaria = ContaBancaria::getById($remocao['conta_recebido']);
            $remocao['data_recebido_BR'] = !empty($remocao['recebido_em']) ? \DateTime::createFromFormat('Y-m-d', $remocao['recebido_em'])->format('d/m/Y') : '';



            if(!empty($remocao['empresa_id'])){
                $empresa = Filial::getById($remocao['empresa_id']);
            }

            require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/particular-visualizar.phtml');
        }
    }

    public static function relatorioRemocoesRealizadas()
    {
        $pacientes = Paciente::getAll();
        $empresas = Filial::getUnidadesAtivas();
        $fornecedores = Fornecedor::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/relatorios/relatorio-remocoes-realizadas.phtml');
    }

    public static function buscarRelatorioRemocoesRealizadas()
    {
        if(isset($_POST) && !empty($_POST)){
            $from = filter_input(INPUT_POST, 'from', FILTER_SANITIZE_STRING);
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $filtrarPor = filter_input(INPUT_POST, 'filtrar_por', FILTER_SANITIZE_STRING);
            $pacienteId = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_STRING);
            $fornecedorCliente = filter_input(INPUT_POST, 'fornecedor_cliente', FILTER_SANITIZE_STRING);
            $empresaId = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_STRING);


            $tipoRemocao = $from == 'faturamento' ? 'C' : 'P';
            $exibirNaoCobrada = filter_input(INPUT_POST, 'exibir_nao_cobrada', FILTER_SANITIZE_STRING);
            $remocoes = ModelRemocao::getRemocoesRealizadas($inicio,
                $fim,
                $tipoRemocao,
                $filtrarPor,
                $exibirNaoCobrada,
                $pacienteId,
                $fornecedorCliente,
                $empresaId);
            $tipoAmbulanciaList = self::getLists()['tipo_ambulancia'];
            $simNao = self::getLists()['sim_nao'];
            $pacientes = Paciente::getAll();
            $empresas = Filial::getUnidadesAtivas();
            $fornecedores = Fornecedor::getAll();

            require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/relatorios/relatorio-remocoes-realizadas.phtml');
        }
    }

    public static function excelRelatorioRemocoesRealizadas()
    {
        if(isset($_GET) && !empty($_GET)){
            $from = filter_input(INPUT_GET, 'from', FILTER_SANITIZE_STRING);
            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
            $filtrarPor = filter_input(INPUT_GET, 'filtrarpor', FILTER_SANITIZE_STRING);
            $pacienteId = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_STRING);
            $fornecedorCliente = filter_input(INPUT_GET, 'fornecedor_cliente', FILTER_SANITIZE_STRING);
            $empresaId = filter_input(INPUT_GET, 'empresa', FILTER_SANITIZE_STRING);
            $exibirNaoCobrada = filter_input(INPUT_POST, 'exibir_nao_cobrada', FILTER_SANITIZE_STRING);

            $tipoRemocao = $from == 'faturamento' ? 'C' : 'P';


            $remocoes = ModelRemocao::getRemocoesRealizadas($inicio,
                                                            $fim,
                                                            $tipoRemocao,
                                                            $filtrarPor,
                                                            $exibirNaoCobrada,
                                                            $pacienteId,
                                                            $fornecedorCliente,
                                                            $empresaId);
            $tipoAmbulanciaList = self::getLists()['tipo_ambulancia'];

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $inicio = \DateTime::createFromFormat('Y-m-d', $inicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $fim);
            $filename = sprintf(
                "relatorio_remocoes_realizadas_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/relatorios/relatorio-remocoes-realizadas-excel.phtml');
        }
    }

    public static function enviarEmailSimples($titulo, $texto, $para)
    {
        $api_data = (object) Config::get('sendgrid');

        $gateway = new MailAdapter(
            new SendGridGateway(
                new \SendGrid($api_data->user, $api_data->pass),
                new \SendGrid\Email()
            )
        );

        $gateway->adapter->sendSimpleMail($titulo, $texto, $para);
    }

    public static function getCPFCaptchaJson()
    {

        if(isset($_GET) && !empty($_GET)) {
            $params = CpfGratis::getParams();

            try {
                $params = CpfGratis::getParams();

                if(strpos($params['captchaToken'], 'DOCTYPE') === false) {
                    echo json_encode($params);
                    exit;
                }
                throw new \Exception('Serviço Indisponível!');
            } catch (\Exception $exception) {
                echo json_encode(
                    [
                        'message' => $exception->getMessage(),
                        'errono' => 1
                    ]
                );
            }
        }
    }

    public static function getDadosPessoaCPF()
    {
        if(isset($_GET) && !empty($_GET)) {
            $cpf = filter_input(INPUT_GET, 'cpf', FILTER_SANITIZE_STRING);
            $dataNasc = filter_input(INPUT_GET, 'dataNasc', FILTER_SANITIZE_STRING);
            $captcha = filter_input(INPUT_GET, 'captcha', FILTER_SANITIZE_STRING);
            $cookie = filter_input(INPUT_GET, 'cookie', FILTER_SANITIZE_STRING);
            $token = filter_input(INPUT_GET, 'token', FILTER_SANITIZE_STRING);

            if($dataNasc != '') {
                $dataNasc = \DateTime::createFromFormat('Y-m-d', $dataNasc)->format('dmY');
                try {
                    $dadosPessoa = CpfGratis::consulta(
                        $cpf,
                        $dataNasc, //DDMMYYYY
                        $captcha,
                        $cookie,
                        $token
                    );

                    if (!isset($dadosPessoa['errono'])) {
                        echo json_encode($dadosPessoa);
                        exit;
                    } else {
                        throw new \Exception($dadosPessoa['message']);
                    }
                } catch (\Exception $exception) {
                    $msg = $exception->getMessage();
                    $msgError = mb_strtolower(strip_tags($msg), 'UTF8');
                    if(strpos($msgError, 'nascimento') !== false) {
                        $msg = 'A data de nascimento não corresponde a encontrada nos dados da Receita Federal!';
                    }
                    echo json_encode(
                        [
                            'message' => $msg,
                            'errono' => 2
                        ]
                    );
                }
            }
        }
    }

    public static function editarFormRemocaoConvenio()
    {
        if(isset($_GET) && !empty($_GET)){
            $remocaoId = $_GET['remocao'];
            $remocao = ModelRemocao::getRemocaoById($remocaoId);
            $itens = ModelRemocao::getItensRemocao($remocaoId);
            $tipoRemocaFatura ='';
            $idRemocaFatura = '';
            if($remocao['tabela_origem_item_fatura'] == 'valorescobranca'){
                if(!empty($remocao['valorescobranca_id']))
                    $itemValoresCobranca =  ModelRemocao::getItemValoresCobrancaById($remocao['valorescobranca_id']);
                $itemRemocaFaturaRemocaFatura = $itemValoresCobranca[0]['DESC_COBRANCA_PLANO'];
                $idRemocaFatura = $remocao['valorescobranca_id'];
                $tabelaReferenciaFatura = 'valorescobranca';
                $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFaturaPlano($remocao['convenio_id']);

            }elseif($remocao['tabela_origem_item_fatura'] == 'cobrancaplanos'){
                if(!empty($remocao['cobrancaplano_id'])){
                    $itemCobrancaPlano =  ModelRemocao::getItemCobrancaPlanoById($remocao['cobrancaplano_id']);
                    $itemRemocaFatura = $itemCobrancaPlano[0]['item'];
                    $idRemocaFatura = $remocao['cobrancaplano_id'];
                    $tabelaReferenciaFatura = 'cobrancaplanos';
                    $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFatura();
                }

            }else{

                $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFaturaPlano($remocao['convenio_id']);
                $tabelaReferenciaFatura = 'valorescobranca';

                if(empty($tipoRemocaFatura)){
                    $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFatura();
                    $tabelaReferenciaFatura = 'cobrancaplanos';
                }
            }
             $lists = self::getLists();
            $empresa = '';
            if(!empty($remocao['empresa_id'])){
                $empresa = Filial::getById($remocao['empresa_id']);
            }
            $planos = Planos::getPlanosAtivos();
            $pacientes = Paciente::getAll();
            $empresas = Filial::getUnidadesAtivas();

            require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/convenio-editar-form.phtml');
        }
    }

    public static function updateRemocaoConvenioFinalizada()
    {
        // ini_set('display_errors', 1);
        if(isset($_POST) && !empty($_POST)){


            $solicitante            = filter_input(INPUT_POST, 'solicitante', FILTER_SANITIZE_STRING);
            $telefoneSolicitante    = filter_input(INPUT_POST, 'telefone-solicitante', FILTER_SANITIZE_STRING);
            $pacienteId             = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $matricula              = filter_input(INPUT_POST, 'matricula', FILTER_SANITIZE_STRING);
            $cpf                    = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING);
            $nascimento             = filter_input(INPUT_POST, 'nascimento', FILTER_SANITIZE_STRING);
            $telefonePaciente       = filter_input(INPUT_POST, 'telefone-paciente', FILTER_SANITIZE_STRING);
            $convenio               = filter_input(INPUT_POST, 'convenio', FILTER_SANITIZE_NUMBER_INT);
            $localOrigem            = filter_input(INPUT_POST, 'local-origem', FILTER_SANITIZE_STRING);
            $enderecoOrigem       = filter_input(INPUT_POST, 'endereco-origem', FILTER_SANITIZE_STRING);
            $localDestino           = filter_input(INPUT_POST, 'local-destino', FILTER_SANITIZE_STRING);
            $enderecoDestino         = filter_input(INPUT_POST, 'endereco-destino', FILTER_SANITIZE_STRING);
            $motivoRemocao          = filter_input(INPUT_POST, 'motivo-remocao', FILTER_SANITIZE_STRING);
            $tipoAmbulancia         = filter_input(INPUT_POST, 'tipo-ambulancia', FILTER_SANITIZE_STRING);
            $retorno                = filter_input(INPUT_POST, 'retorno', FILTER_SANITIZE_STRING);
            $dataSolicitacao        = filter_input(INPUT_POST, 'data-solicitacao', FILTER_SANITIZE_STRING);
            $dadosPaciente          = Paciente::getById($pacienteId);
            $nomePaciente           = $dadosPaciente['nome'];
            $autorizacao        = filter_input(INPUT_POST, 'autorizacao', FILTER_SANITIZE_STRING);
            $remocao_realizada  = filter_input(INPUT_POST, 'remocao-realizada', FILTER_SANITIZE_STRING);
            $motivo             = filter_input(INPUT_POST, 'motivo', FILTER_SANITIZE_STRING);
            $km                 = filter_input(INPUT_POST, 'km', FILTER_SANITIZE_STRING);
            $perimetro          = filter_input(INPUT_POST, 'perimetro', FILTER_SANITIZE_STRING);
            $data_saida         = filter_input(INPUT_POST, 'data-saida', FILTER_SANITIZE_STRING);
            $hora_saida         = filter_input(INPUT_POST, 'hora-saida', FILTER_SANITIZE_STRING);
            $data_chegada       = filter_input(INPUT_POST, 'data-chegada', FILTER_SANITIZE_STRING);
            $hora_chegada       = filter_input(INPUT_POST, 'hora-chegada', FILTER_SANITIZE_STRING);
            $tabela_propria     = filter_input(INPUT_POST, 'tabela-propria', FILTER_SANITIZE_STRING);
            $valor_remocao      = filter_input(INPUT_POST, 'valor-remocao', FILTER_SANITIZE_STRING);
            $medico             = filter_input(INPUT_POST, 'medico', FILTER_SANITIZE_STRING);
            $valor_medico       = filter_input(INPUT_POST, 'valor-medico', FILTER_SANITIZE_STRING);
            $tecnico            = filter_input(INPUT_POST, 'tecnico', FILTER_SANITIZE_STRING);
            $valor_tecnico      = filter_input(INPUT_POST, 'valor-tecnico', FILTER_SANITIZE_STRING);
            $condutor           = filter_input(INPUT_POST, 'condutor', FILTER_SANITIZE_STRING);
            $valor_condutor     = filter_input(INPUT_POST, 'valor-condutor', FILTER_SANITIZE_STRING);
            $remocao_fatura     = filter_input(INPUT_POST, 'remocao-fatura', FILTER_SANITIZE_STRING);
            $outrasDespesas     = filter_input(INPUT_POST, 'outras-despesas', FILTER_SANITIZE_STRING);
            $remocaoId          = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $tabelaOrigemItemFatura     = filter_input(INPUT_POST, 'tabela-origem-item-fatura', FILTER_SANITIZE_STRING);
            $empresa          = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_NUMBER_INT);
            $enfermeiro           = filter_input(INPUT_POST, 'enfermeiro', FILTER_SANITIZE_STRING);
            $valor_enfermeiro      = filter_input(INPUT_POST, 'valor-enfermeiro', FILTER_SANITIZE_STRING);
            $remocaoSeraCobrada = filter_input(INPUT_POST, 'remocao_sera_cobrada', FILTER_SANITIZE_STRING);
            $justificativaNaoCobrar = filter_input(INPUT_POST, 'justificativa_nao_cobrar', FILTER_SANITIZE_STRING);

            $itens              = [
                'catalogo_id' => $_POST['catalogo_id'],
                'tipo' => $_POST['tipo'],
                'tiss' => $_POST['tiss'],
                'qtd' => $_POST['qtd'],
            ];

            $remocao = new ModelRemocao(ConnMysqli::getConnection());
            $data = [
                'solicitante' => $solicitante,
                'telefoneSolicitante' => $telefoneSolicitante,
                'telefonePaciente' => $telefonePaciente,
                'pacienteId' => $pacienteId,
                'matricula' => $matricula,
                'cpf' => $cpf,
                'nascimento' => $nascimento,
                'convenio' => $convenio,
                'localOrigem' => $localOrigem,
                'enderecoOrigem' => $enderecoOrigem,
                'enderecoDestino' => $enderecoDestino,
                'localDestino' => $localDestino,
                'motivoRemocao' => $motivoRemocao,
                'tipoAmbulancia' => $tipoAmbulancia,
                'retorno' => $retorno,
                'nomePaciente' => $nomePaciente,
                'dataSolicitacao' => $dataSolicitacao,
                'autorizacao' => $autorizacao,
                'remocao_realizada' => $remocao_realizada,
                'motivo' => $motivo,
                'km' => $km,
                'perimetro' => $perimetro,
                'data_saida' => $data_saida,
                'hora_saida' => $hora_saida,
                'data_chegada' => $data_chegada,
                'hora_chegada' => $hora_chegada,
                'tabela_propria' => $tabela_propria,
                'valor_remocao' => $valor_remocao,
                'medico' => $medico,
                'valor_medico' => $valor_medico,
                'tecnico' => $tecnico,
                'valor_tecnico' => $valor_tecnico,
                'condutor' => $condutor,
                'valor_condutor' => $valor_condutor,
                'remocao_fatura' => $remocao_fatura,
                'remocaoId' => $remocaoId,
                'outrasDespesas' => $outrasDespesas,
                'itens' => $itens,
                'tabelaOrigemItemFatura' => $tabelaOrigemItemFatura,
                'empresa' => $empresa,
                'enfermeiro' => $enfermeiro,
                'valor_enfermeiro' => $valor_enfermeiro,
                'remocaoSeraCobrada' => $remocaoSeraCobrada,
                'justificativaNaoCobrar' => $justificativaNaoCobrar,
                'itens' => $itens

            ];

            $x = $remocao->updateRemocaoConvenioFinalizada($data);

            echo $x;

        }


    }

    public static function updateRemocaoConvenioNaoFinalizada()
    {
         //ini_set('display_errors', 1);
        if(isset($_POST) && !empty($_POST)){


            $solicitante            = filter_input(INPUT_POST, 'solicitante', FILTER_SANITIZE_STRING);
            $telefoneSolicitante    = filter_input(INPUT_POST, 'telefone-solicitante', FILTER_SANITIZE_STRING);
            $pacienteId             = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $matricula              = filter_input(INPUT_POST, 'matricula', FILTER_SANITIZE_STRING);
            $cpf                    = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING);
            $nascimento             = filter_input(INPUT_POST, 'nascimento', FILTER_SANITIZE_STRING);
            $telefonePaciente       = filter_input(INPUT_POST, 'telefone-paciente', FILTER_SANITIZE_STRING);
            $convenio               = filter_input(INPUT_POST, 'convenio', FILTER_SANITIZE_NUMBER_INT);
            $localOrigem            = filter_input(INPUT_POST, 'local-origem', FILTER_SANITIZE_STRING);
            $enderecoOrigem       = filter_input(INPUT_POST, 'endereco-origem', FILTER_SANITIZE_STRING);
            $localDestino           = filter_input(INPUT_POST, 'local-destino', FILTER_SANITIZE_STRING);
            $enderecoDestino         = filter_input(INPUT_POST, 'endereco-destino', FILTER_SANITIZE_STRING);
            $motivoRemocao          = filter_input(INPUT_POST, 'motivo-remocao', FILTER_SANITIZE_STRING);
            $tipoAmbulancia         = filter_input(INPUT_POST, 'tipo-ambulancia', FILTER_SANITIZE_STRING);
            $retorno                = filter_input(INPUT_POST, 'retorno', FILTER_SANITIZE_STRING);
            $dataSolicitacao        = filter_input(INPUT_POST, 'data-solicitacao', FILTER_SANITIZE_STRING);
            $dadosPaciente          = Paciente::getById($pacienteId);
            $nomePaciente           = $dadosPaciente['nome'];
            $remocaoId          = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);



            $remocao = new ModelRemocao(ConnMysqli::getConnection());
            $data = [
                'solicitante' => $solicitante,
                'telefoneSolicitante' => $telefoneSolicitante,
                'telefonePaciente' => $telefonePaciente,
                'pacienteId' => $pacienteId,
                'matricula' => $matricula,
                'cpf' => $cpf,
                'nascimento' => $nascimento,
                'convenio' => $convenio,
                'localOrigem' => $localOrigem,
                'enderecoOrigem' => $enderecoOrigem,
                'enderecoDestino' => $enderecoDestino,
                'localDestino' => $localDestino,
                'motivoRemocao' => $motivoRemocao,
                'tipoAmbulancia' => $tipoAmbulancia,
                'retorno' => $retorno,
                'dataSolicitacao' => $dataSolicitacao,
                'remocaoId' => $remocaoId,

            ];

            $x = $remocao->updateRemocaoConvenioNaoFinalizada($data);

            echo $x;

        }


    }

    public static function editarFormRemocaoParticular()
    {
        if(isset($_GET) && !empty($_GET)){
            $remocaoId = $_GET['remocao'];
            $remocao = ModelRemocao::getRemocaoById($remocaoId);
            $itens = ModelRemocao::getItensRemocao($remocaoId);
            $fornecedores = Fornecedor::getAll();
            $tipoRemocaFatura ='';
            $idRemocaFatura = '';
            $contasBancarias = $_SESSION['empresa_principal'] == 1 ? ContaBancaria::getAll() : ContaBancaria::getContasBancariasByUR($_SESSION['empresa_principal']);


            if($remocao['tabela_origem_item_fatura'] == 'valorescobranca'){
                if(!empty($remocao['valorescobranca_id']))
                    $itemValoresCobranca =  ModelRemocao::getItemValoresCobrancaById($remocao['valorescobranca_id']);
                $itemRemocaFaturaRemocaFatura = $itemValoresCobranca[0]['DESC_COBRANCA_PLANO'];
                $idRemocaFatura = $remocao['valorescobranca_id'];
                $tabelaReferenciaFatura = 'valorescobranca';
                $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFaturaPlano($remocao['convenio_id']);

            }elseif($remocao['tabela_origem_item_fatura'] == 'cobrancaplanos' && !empty($remocao['cobrancaplano_id'])){

                    $itemCobrancaPlano =  ModelRemocao::getItemCobrancaPlanoById($remocao['cobrancaplano_id']);
                    $itemRemocaFatura = $itemCobrancaPlano[0]['item'];
                    $idRemocaFatura = $remocao['cobrancaplano_id'];
                    $tabelaReferenciaFatura = 'cobrancaplanos';
                    $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFatura();


            }else{

                $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFaturaPlano($remocao['convenio_id']);
                $tabelaReferenciaFatura = 'valorescobranca';

                if(empty($tipoRemocaFatura)){
                    $tipoRemocaFatura = ModelRemocao::getTipoRemocaoFatura();
                    $tabelaReferenciaFatura = 'cobrancaplanos';
                }
            }

            $lists = self::getLists();
            $empresa = '';
            if(!empty($remocao['empresa_id'])){
                $empresa = Filial::getById($remocao['empresa_id']);
            }
            $planos = Planos::getPlanosAtivos();
            $pacientes = Paciente::getAll();
            $empresas = Filial::getUnidadesAtivas();

            require($_SERVER['DOCUMENT_ROOT'] . '/remocao/templates/particular-editar-form.phtml');
        }
    }

    public static function updateRemocaoParticularNaoFinalizada()
    {

        if(isset($_POST) && !empty($_POST)){


            $solicitante            = filter_input(INPUT_POST, 'solicitante', FILTER_SANITIZE_STRING);
            $telefoneSolicitante    = filter_input(INPUT_POST, 'telefone-solicitante', FILTER_SANITIZE_STRING);
            $pacienteId             = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $matricula              = filter_input(INPUT_POST, 'matricula', FILTER_SANITIZE_STRING);
            $cpf                    = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING);
            $nascimento             = filter_input(INPUT_POST, 'nascimento', FILTER_SANITIZE_STRING);
            $telefonePaciente       = filter_input(INPUT_POST, 'telefone-paciente', FILTER_SANITIZE_STRING);
            $convenio               = filter_input(INPUT_POST, 'convenio', FILTER_SANITIZE_NUMBER_INT);
            $localOrigem            = filter_input(INPUT_POST, 'local-origem', FILTER_SANITIZE_STRING);
            $enderecoOrigem       = filter_input(INPUT_POST, 'endereco-origem', FILTER_SANITIZE_STRING);
            $localDestino           = filter_input(INPUT_POST, 'local-destino', FILTER_SANITIZE_STRING);
            $enderecoDestino         = filter_input(INPUT_POST, 'endereco-destino', FILTER_SANITIZE_STRING);
            $motivoRemocao          = filter_input(INPUT_POST, 'motivo-remocao', FILTER_SANITIZE_STRING);
            $tipoAmbulancia         = filter_input(INPUT_POST, 'tipo-ambulancia', FILTER_SANITIZE_STRING);
            $retorno                = filter_input(INPUT_POST, 'retorno', FILTER_SANITIZE_STRING);
            $dataSolicitacao        = filter_input(INPUT_POST, 'data-solicitacao', FILTER_SANITIZE_STRING);
            $dadosPaciente          = Paciente::getById($pacienteId);
            $nomePaciente           = $dadosPaciente['nome'];
            $remocaoId          = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $sexo           = filter_input(INPUT_POST, 'sexo', FILTER_SANITIZE_STRING );
            $nomePaciente           = filter_input(INPUT_POST, 'nome-paciente', FILTER_SANITIZE_STRING );
            $pacienteExiste         = filter_input(INPUT_POST, 'paciente-existe', FILTER_SANITIZE_STRING );
            $modalidade = 13;
            $empresa = null;


            if($pacienteExiste == 'N'){
                $pacienteId  = Paciente::insertPacienteRemocao($nomePaciente,
                    $convenio,
                    $cpf,
                    $nascimento,
                    $matricula,
                    $telefonePaciente,
                    $sexo,
                    $empresa);

                if(!is_int($pacienteId)){
                    echo $pacienteId;
                    return;
                }
            }



            $remocao = new ModelRemocao(ConnMysqli::getConnection());
            $data = [
                'solicitante' => $solicitante,
                'telefoneSolicitante' => $telefoneSolicitante,
                'telefonePaciente' => $telefonePaciente,
                'pacienteId' => $pacienteId,
                'matricula' => $matricula,
                'cpf' => $cpf,
                'nascimento' => $nascimento,
                'convenio' => $convenio,
                'localOrigem' => $localOrigem,
                'enderecoOrigem' => $enderecoOrigem,
                'enderecoDestino' => $enderecoDestino,
                'localDestino' => $localDestino,
                'motivoRemocao' => $motivoRemocao,
                'tipoAmbulancia' => $tipoAmbulancia,
                'retorno' => $retorno,
                'nomePaciente' => $nomePaciente,
                'dataSolicitacao' => $dataSolicitacao,
                'remocaoId' => $remocaoId


            ];

            $x = $remocao->updateRemocaoConvenioNaoFinalizada($data);

            echo $x;

        }


    }

    public static function updateRemocaoParticularFinalizada()
    {

        if(isset($_POST) && !empty($_POST)){


            $solicitante            = filter_input(INPUT_POST, 'solicitante', FILTER_SANITIZE_STRING);
            $telefoneSolicitante    = filter_input(INPUT_POST, 'telefone-solicitante', FILTER_SANITIZE_STRING);
            $pacienteId             = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $matricula              = filter_input(INPUT_POST, 'matricula', FILTER_SANITIZE_STRING);
            $cpf                    = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING);
            $nascimento             = filter_input(INPUT_POST, 'nascimento', FILTER_SANITIZE_STRING);
            $telefonePaciente       = filter_input(INPUT_POST, 'telefone-paciente', FILTER_SANITIZE_STRING);
            $convenio               = filter_input(INPUT_POST, 'convenio', FILTER_SANITIZE_NUMBER_INT);
            $localOrigem            = filter_input(INPUT_POST, 'local-origem', FILTER_SANITIZE_STRING);
            $enderecoOrigem       = filter_input(INPUT_POST, 'endereco-origem', FILTER_SANITIZE_STRING);
            $localDestino           = filter_input(INPUT_POST, 'local-destino', FILTER_SANITIZE_STRING);
            $enderecoDestino         = filter_input(INPUT_POST, 'endereco-destino', FILTER_SANITIZE_STRING);
            $motivoRemocao          = filter_input(INPUT_POST, 'motivo-remocao', FILTER_SANITIZE_STRING);
            $tipoAmbulancia         = filter_input(INPUT_POST, 'tipo-ambulancia', FILTER_SANITIZE_STRING);
            $retorno                = filter_input(INPUT_POST, 'retorno', FILTER_SANITIZE_STRING);
            $dataSolicitacao        = filter_input(INPUT_POST, 'data-solicitacao', FILTER_SANITIZE_STRING);
            $dadosPaciente          = Paciente::getById($pacienteId);
            $nomePaciente           = $dadosPaciente['nome'];
            $remocaoId          = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $sexo           = filter_input(INPUT_POST, 'sexo', FILTER_SANITIZE_STRING );
            $nomePaciente           = filter_input(INPUT_POST, 'nome-paciente', FILTER_SANITIZE_STRING );
            $pacienteExiste         = filter_input(INPUT_POST, 'paciente-existe', FILTER_SANITIZE_STRING );
            $modalidade = 13;
            $empresa = null;

            $km                 = filter_input(INPUT_POST, 'km', FILTER_SANITIZE_STRING);
            $perimetro          = filter_input(INPUT_POST, 'perimetro', FILTER_SANITIZE_STRING);
            $data_saida         = filter_input(INPUT_POST, 'data-saida', FILTER_SANITIZE_STRING);
            $hora_saida         = filter_input(INPUT_POST, 'hora-saida', FILTER_SANITIZE_STRING);
            $data_chegada       = filter_input(INPUT_POST, 'data-chegada', FILTER_SANITIZE_STRING);
            $hora_chegada       = filter_input(INPUT_POST, 'hora-chegada', FILTER_SANITIZE_STRING);
            $tabela_propria     = filter_input(INPUT_POST, 'tabela-propria', FILTER_SANITIZE_STRING);
            $valor_remocao      = filter_input(INPUT_POST, 'valor-remocao', FILTER_SANITIZE_STRING);
            $medico             = filter_input(INPUT_POST, 'medico', FILTER_SANITIZE_STRING);
            $valor_medico       = filter_input(INPUT_POST, 'valor-medico', FILTER_SANITIZE_STRING);
            $tecnico            = filter_input(INPUT_POST, 'tecnico', FILTER_SANITIZE_STRING);
            $valor_tecnico      = filter_input(INPUT_POST, 'valor-tecnico', FILTER_SANITIZE_STRING);
            $condutor           = filter_input(INPUT_POST, 'condutor', FILTER_SANITIZE_STRING);
            $valor_condutor     = filter_input(INPUT_POST, 'valor-condutor', FILTER_SANITIZE_STRING);
            $remocao_fatura     = filter_input(INPUT_POST, 'remocao-fatura', FILTER_SANITIZE_STRING);
            $outrasDespesas     = filter_input(INPUT_POST, 'outras-despesas', FILTER_SANITIZE_STRING);
            $remocaoId          = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $tabelaOrigemItemFatura     = filter_input(INPUT_POST, 'tabela-origem-item-fatura', FILTER_SANITIZE_STRING);
            $empresa          = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_NUMBER_INT);
            $enfermeiro           = filter_input(INPUT_POST, 'enfermeiro', FILTER_SANITIZE_STRING);
            $valor_enfermeiro      = filter_input(INPUT_POST, 'valor-enfermeiro', FILTER_SANITIZE_STRING);
            $modoCobranca = filter_input(INPUT_POST, 'modo_cobranca', FILTER_SANITIZE_STRING);
            $remocaoSeraCobrada = filter_input(INPUT_POST, 'remocao_sera_cobrada', FILTER_SANITIZE_STRING);
            $justificativaNaoCobrar = filter_input(INPUT_POST, 'justificativa_nao_cobrar', FILTER_SANITIZE_STRING);
            $recebido           = filter_input(INPUT_POST, 'recebido', FILTER_SANITIZE_STRING);
            $forma_pagamento    = filter_input(INPUT_POST, 'forma_pagamento', FILTER_SANITIZE_STRING);
            $contaRecebido =   filter_input(INPUT_POST, 'conta_recebido', FILTER_SANITIZE_STRING);
            $recebidoEm =   filter_input(INPUT_POST, 'recebido_em', FILTER_SANITIZE_STRING);
            $observacao =  filter_input(INPUT_POST, 'observacao', FILTER_SANITIZE_STRING);
            $itens              = [
                'catalogo_id' => $_POST['catalogo_id'],
                'tipo' => $_POST['tipo'],
                'tiss' => $_POST['tiss'],
                'qtd' => $_POST['qtd'],
            ];
            $fornecedor_cliente = filter_input(INPUT_POST, 'fornecedor_cliente', FILTER_SANITIZE_NUMBER_INT);
            if($fornecedor_cliente == '') {
                $fornecedor['fisica_juridica'] = filter_input(INPUT_POST, 'fisica_juridica', FILTER_SANITIZE_STRING);
                $fornecedor['cpf'] = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING);
                $fornecedor['cnpj'] = filter_input(INPUT_POST, 'cnpj', FILTER_SANITIZE_STRING);
                $fornecedor['nome_fantasia'] = filter_input(INPUT_POST, 'nome_fantasia', FILTER_SANITIZE_STRING);
                $fornecedor['razao_social'] = filter_input(INPUT_POST, 'razao_social', FILTER_SANITIZE_STRING);
                $fornecedor['ies_rg'] = filter_input(INPUT_POST, 'ies_rg', FILTER_SANITIZE_STRING);
                $fornecedor['im'] = filter_input(INPUT_POST, 'im', FILTER_SANITIZE_STRING);
                $fornecedor['contato'] = filter_input(INPUT_POST, 'contato', FILTER_SANITIZE_STRING);
                $fornecedor['telefone'] = filter_input(INPUT_POST, 'telefone', FILTER_SANITIZE_STRING);
                $fornecedor['endereco'] = filter_input(INPUT_POST, 'endereco', FILTER_SANITIZE_STRING);
                $fornecedor['cidade'] = filter_input(INPUT_POST, 'id_cidade_cliente', FILTER_SANITIZE_NUMBER_INT);
                $fornecedor['email'] = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
                $fornecedor['validado_receita'] = filter_input(INPUT_POST, 'cpf_receita_validado', FILTER_SANITIZE_STRING);

                if($fornecedor['fisica_juridica']  == 1 ){
                    $responseCnpj = Fornecedor::checkIfExistsFornecedorByCNPJ($fornecedor['cnpj'],'C');
                    if(!empty($responseCnpj)){
                        echo "Já existe Cliente cadastrado com esse CNPJ {$fornecedor['cnpj']}, no Sitema.";
                        return;
                    }
                }else{
                    $responseCpf = Fornecedor::checkIfExistsFornecedorByCPF($fornecedor['cpf'],'C');
                    if(!empty($responseCpf)){
                        echo "Já existe Cliente cadastrado com esse CPF {$fornecedor['cpf']}, no Sitema.";
                        return;
                    }
                }

                $fornecedor_cliente = Fornecedor::createFornecedorByRemocao($fornecedor);
            }


            if($pacienteExiste == 'N'){
                $pacienteId  = Paciente::insertPacienteRemocao($nomePaciente,
                    $convenio,
                    $cpf,
                    $nascimento,
                    $matricula,
                    $telefonePaciente,
                    $sexo,
                    $empresa);

                if(!is_int($pacienteId)){
                    echo $pacienteId;
                    return;
                }
            }



            $remocao = new ModelRemocao(ConnMysqli::getConnection());
            $data = [
                'solicitante' => $solicitante,
                'telefoneSolicitante' => $telefoneSolicitante,
                'telefonePaciente' => $telefonePaciente,
                'pacienteId' => $pacienteId,
                'matricula' => $matricula,
                'cpf' => $cpf,
                'nascimento' => $nascimento,
                'convenio' => $convenio,
                'localOrigem' => $localOrigem,
                'enderecoOrigem' => $enderecoOrigem,
                'enderecoDestino' => $enderecoDestino,
                'localDestino' => $localDestino,
                'motivoRemocao' => $motivoRemocao,
                'tipoAmbulancia' => $tipoAmbulancia,
                'retorno' => $retorno,
                'nomePaciente' => $nomePaciente,
                'dataSolicitacao' => $dataSolicitacao,
                'km' => $km,
                'perimetro' => $perimetro,
                'data_saida' => $data_saida,
                'hora_saida' => $hora_saida,
                'data_chegada' => $data_chegada,
                'hora_chegada' => $hora_chegada,
                'tabela_propria' => $tabela_propria,
                'valor_remocao' => $valor_remocao,
                'medico' => $medico,
                'valor_medico' => $valor_medico,
                'tecnico' => $tecnico,
                'valor_tecnico' => $valor_tecnico,
                'condutor' => $condutor,
                'valor_condutor' => $valor_condutor,
                'remocao_fatura' => $remocao_fatura,
                'remocaoId' => $remocaoId,
                'outrasDespesas' => $outrasDespesas,
                'tabelaOrigemItemFatura' => $tabelaOrigemItemFatura,
                'empresa' => $empresa,
                'enfermeiro' => $enfermeiro,
                'valor_enfermeiro' => $valor_enfermeiro,
                'remocaoSeraCobrada' => empty($remocaoSeraCobrada) ? NULL : $remocaoSeraCobrada,
                'modoCobranca' => $modoCobranca,
                'justificativaNaoCobrar' => $justificativaNaoCobrar,
                'recebido' => $recebido,
                'forma_pagamento' => $forma_pagamento,
                'fornecedor_cliente' => $fornecedor_cliente,
                'itens' => $itens,
                'conta_recebido' => $contaRecebido,
                'recebido_em' => $recebidoEm,
                'observacao' => $observacao
            ];

             $x = $remocao->updateRemocaoParticularFinalizada($data);

            echo $x;

        }


    }

}

