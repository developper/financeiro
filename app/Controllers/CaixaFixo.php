<?php
namespace app\Controllers;

use App\Helpers\StringHelper;
use \App\Models\Administracao\Filial;
use App\Models\Financeiro\AplicacaoFundos;
use App\Models\Financeiro\Assinatura;
use App\Models\Financeiro\BaixarNotaAntecipada;
use App\Models\Financeiro\ContaBancaria;
use App\Models\Financeiro\Fornecedor;
use App\Models\Financeiro\IPCF;
use App\Models\Financeiro\Parcela;
use App\Models\Financeiro\TipoDocumentoFinanceiro;
use App\Models\Financeiro\Notas;
use App\Models\Financeiro\Compensacao;

include_once('../../utils/codigos.php');
// ini_set('display_errors', 1);
class CaixaFixo
{
    public static function listar()
        {       
            $filtros = [
                'notIn'=>'23, 25, 26',
            ];
            $fornecedores = Fornecedor::getAll();
            $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getByFiltros($filtros);
            $urs = $_SESSION['empresa_user'];
            $origens= ContaBancaria::getContasBancariasByURs($urs,null);
            $aplicacoes = AplicacaoFundos::getAll();
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/caixa-fixo/templates/index-caixa-fixo.phtml');
    }

    public static function criarConta()
    {
        $tipo = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING);
        $tipo_fornecedor = $tipo == 'S' ? 'F' : 'C';
        $tipo_titulo = $tipo == 'S' ? 'Saída' : 'Entrada';
        $acaoTitulo ='Criar';
        if(!isset($_GET['tipo']) || empty($tipo)) {
            header ("Location: /financeiros/caixa-fixo/?action=index-caixa-fixo");
            exit();
        }
        
        $filtros = [
            'notIn'=>'23, 25, 26',
        ];

        $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();
        $interval = new \DateInterval('P1D');
        $data_fechamento_obj = \DateTime::createFromFormat('Y-m-d', $fechamento_caixa['data_fechamento'])->add($interval);
        $data_fechamento = $data_fechamento_obj->format('Y-m-d');
        if($tipo_fornecedor == 'C') {
            $fornecedores = Fornecedor::getAllByTipo($tipo_fornecedor);
        } else{
            $fornecedores[0] = Fornecedor::getById(4979);
        }
        $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getByFiltros($filtros);
        $unidades = Filial::getUnidadesAtivas($_SESSION['empresa_user']);
        $naturezas = IPCF::getAllNatureza($tipo);
        $hoje = (new \DateTime())->format('Y-m-d');
        $urs = $_SESSION['empresa_user'];
        $origens= ContaBancaria::getContasBancariasByURs($urs, 4);
        $aplicacoes = AplicacaoFundos::getAll();
        $assinaturas = Assinatura::getAll();
                

        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/caixa-fixo/templates/criar-conta.phtml');
    }
    
    
    public static function pesquisarNotas()
    {
       
       
        if(isset($_POST)){
            $inicio  = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim     = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $competencia_inicio  = filter_input(INPUT_POST, 'competencia_inicio', FILTER_SANITIZE_STRING);
            $competencia_fim     = filter_input(INPUT_POST, 'competencia_fim', FILTER_SANITIZE_STRING);
            
            if(!empty($_POST['competencia_inicio'])){
                $competencia_inicio_db = \DateTime::createFromFormat('m/Y',$competencia_inicio)->format('Y-m');
            }
            if(!empty($_POST['competencia_fim'])){
                $competencia_fim_db =  \DateTime::createFromFormat('m/Y',$competencia_fim)->format('Y-m');
            }
            $inicioPagamento  = filter_input(INPUT_POST, 'inicioPagamento', FILTER_SANITIZE_STRING);
            $fimPagamento     = filter_input(INPUT_POST, 'fimPagamento', FILTER_SANITIZE_STRING);
            $fornecedor  = filter_input(INPUT_POST, 'fornecedor', FILTER_SANITIZE_NUMBER_INT);
            $tipo_documento_financeiro        = filter_input(INPUT_POST, 'tipo_documento', FILTER_SANITIZE_NUMBER_INT);
            $tr = filter_input(INPUT_POST, 'tr', FILTER_SANITIZE_NUMBER_INT);
            $descricaoNota  = filter_input(INPUT_POST, 'descricaoNota', FILTER_SANITIZE_STRING);
            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'competencia_inicio' => $competencia_inicio,
                'competencia_fim' => $competencia_fim,
                'competencia_inicio_db' => $competencia_inicio_db,
                'competencia_fim_db' => $competencia_fim_db,
                'fornecedor'         => $fornecedor,
                'tipo_documento_financeiro'  => $tipo_documento_financeiro,
                'empresa_user' => $_SESSION['empresa_user'],
                'inicioPagamento' => $inicioPagamento,
                'fimPagamento' => $fimPagamento,
                'tr'=> $tr,
                'descricaoNota' => $descricaoNota,
            ];
            $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();
           // $interval = new \DateInterval('P1D');
            $data_fechamento_obj = \DateTime::createFromFormat('Y-m-d', $fechamento_caixa['data_fechamento']);
            $data_fechamento = $data_fechamento_obj->format('Y-m-d');  
                           
            $response 	= Notas::getNotasCaixaFixo($filtros);
                     
         }
        $filtros['notIn'] = '23, 25, 26';
        $fornecedores = Fornecedor::getAll();
        $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getByFiltros($filtros);
        $urs = $_SESSION['empresa_user'];
        $origens= ContaBancaria::getContasBancariasByURs($urs, 4);
        $aplicacoes = AplicacaoFundos::getAll();
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/caixa-fixo/templates/index-caixa-fixo.phtml');

    }


    public static function salvar()
    { 
        if(isset($_POST) && !empty($_POST)){
           
            $data = [];
            $competencia  = filter_input(INPUT_POST, 'competencia', FILTER_SANITIZE_STRING);
            $data['competencia'] = \DateTime::createFromFormat('m/Y', $competencia)->format('Y-m');
            $data['data_emissao']  = filter_input(INPUT_POST, 'data_emissao', FILTER_SANITIZE_STRING);
            $data['data_pagamento']  = filter_input(INPUT_POST, 'data_pagamento', FILTER_SANITIZE_STRING);
            $data['descricao']  = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);
            $data['descricao'] = mb_strtoupper($data['descricao'], 'UTF-8');
            $data['forma_pagamento']  = filter_input(INPUT_POST, 'forma_pagamento', FILTER_SANITIZE_STRING);
            $data['fornecedor_nota']  = filter_input(INPUT_POST, 'fornecedor_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['natureza_nota']  = filter_input(INPUT_POST, 'natureza_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['numero_nota']  = filter_input(INPUT_POST, 'numero_nota', FILTER_SANITIZE_STRING);
            $data['origem']  = filter_input(INPUT_POST, 'origem', FILTER_SANITIZE_STRING);
            $data['tipo_documento_nota']  = filter_input(INPUT_POST, 'tipo_documento_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['tipo_nota']  = filter_input(INPUT_POST, 'tipo_nota', FILTER_SANITIZE_STRING);         
            $data['ur_nota']  = filter_input(INPUT_POST, 'ur_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['valor']  = filter_input(INPUT_POST, 'valor', FILTER_SANITIZE_STRING);
            $data['cod_barras']  = filter_input(INPUT_POST, 'cod_barras', FILTER_SANITIZE_STRING);           
            $data['valor'] = str_replace('.','',$data['valor']);
            $data['valor'] = str_replace(',','.',$data['valor']);
            $data['assinatura']  = filter_input(INPUT_POST, 'assinatura', FILTER_SANITIZE_NUMBER_INT);
            $data['forma_pagamento'] = 4;
             

            $fechamentoCaixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);

            $dataEntradaObj = new \DateTime($data['data_emissao']);
            $dataPagamentoOj = new \DateTime($data['data_pagamento']);
            if($dataEntradaObj <= $fechamentoCaixa) {
                $response = ['tipo' => 'erro',
                'msg'=>"Data de entrada ".$dataEntradaObj->format('d/m/Y')." menor qua a data de fechamento do período ".$fechamentoCaixa->format('d/m/Y')

            ];
             echo json_encode($response);
                return;
            }  

            if($dataPagamentoOj <= $fechamentoCaixa) {
                $response = ['tipo' => 'erro',
                'msg'=>"Data de dagamento ".$dataPagamentoOj->format('d/m/Y')." menor qua a data de fechamento do período ".$fechamentoCaixa->format('d/m/Y')

            ];
             echo json_encode($response);
                return;
            }  
            $notaJaCadastrada = Notas::verificarNFJaExiste($data['fornecedor_nota'], $data['numero_nota']);
            if(!empty($notaJaCadastrada)){
                $response = ['tipo' => 'erro',
                'msg'=>"Conta já possui cadastro no sistema TR {$notaJaCadastrada['idNotas']}."

                ];                
                echo json_encode($response);
                return;
            }         
            
            $response = Notas::criarNotaCaixaFixo($data);              
                     
            
            if($response['tipo'] == 'acerto') {
                $response = ['tipo' => 'acerto',
                'msg'=>$response['msg']

                ];
                echo json_encode($response);
                return;
            } else {
                $response = ['tipo' => 'erro',
                'msg'=> $response['msg']
                ];
                echo json_encode($response);
                return;
               
            }
        }
    }

    public static function visualizar()
    {
        if(isset($_GET) && !empty($_GET)){
            $notaId  = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
            if(!isset($_GET['id']) || empty($notaId)) {
                header ("Location: /financeiros/caixa-fixo/?action=index-caixa-fixo");
                exit();
            }
            
            
            $filtros = ['tr'=> $notaId];  
            $response 	= Notas::getNotasCaixaFixo($filtros); 
            $response = $response[0];
            $tipoMovimentacao = $response['tipo'] == 1 ? 'S' : 'E'; 
            $tipo_fornecedor = $response['tipo'] == 1 ? 'F' : 'C';
            $tipo_titulo = $response['tipo'] == 1 ? 'Saída' : 'Entrada';
            $data_emissao_obj = \DateTime::createFromFormat('Y-m-d', $response['dataEntrada']);
            $data_emissao = $data_emissao_obj->format('d/m/Y');
            $fornecedores = Fornecedor::getById($response['codFornecedor']);
            $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getById($response['tipo_documento_financeiro_id']);
            $unidades = Filial::getEmpresaById($response['empresa']);
            $naturezas = IPCF::getById($response['natureza_movimentacao']);
            $origens= ContaBancaria::getById($response['origem']);
            $aplicacoes = AplicacaoFundos::getById($response["aplicacao"]);
            $assinaturas = Assinatura::getById($response["ASSINATURA"]);
            $response['valorFormatoBr'] = StringHelper::currencyFormat($response['valor']);
            
            $response['data_emissao'] = $data_emissao;
            $response['competencia'] = \DateTime::createFromFormat('Y-m', $response['data_competencia'] )->format('m/Y');
            
            $response['fornecedor'] = $fornecedores['nome'];
            $response['tipoDocFin'] = $tiposDocumentoFinanceiro[0]['descricao'];
            $response['empresa'] = $unidades['nome'];
            $response['natureza'] =$naturezas['COD_N4']." ".$naturezas['N4'];
            $response['origem'] = $origens['nome_conta'];
            $response['assinatura'] = $assinaturas[0]['DESCRICAO'];
            $response['aplicacao'] = $aplicacoes[0]['forma'];
            $response['data_conciliado'] = '';
            if($response['CONCILIADO'] == 'S'){
                $data_conciliado_obj = \DateTime::createFromFormat('Y-m-d', $response['DATA_CONCILIADO']);
                $data_conciliado = $data_conciliado_obj->format('d/m/Y');
                $response['data_conciliado_BR'] = $data_conciliado;
            }

            $fechamentoCaixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
            $dataFechamentoBd = $fechamentoCaixa->format('Y-m-d'); 
                       
            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/caixa-fixo/templates/visualizar-conta.phtml');
        }
    }

    public static function editar()
    {
        if(isset($_GET) && !empty($_GET)){
            $notaId  = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
            if(!isset($_GET['id']) || empty($notaId)) {
                header ("Location: /financeiros/caixa-fixo/?action=index-caixa-fixo");
                exit();
            }

            $acaoTitulo ='Editar';
            $filtros = ['tr'=> $notaId];  
            $response 	= Notas::getNotasCaixaFixo($filtros); 
            $response = $response[0];            
            $tipo = $response['tipo'] == 1 ? 'S' : 'E';
            $filtrosTipoDoc = [
                'notIn'=>'23, 25, 26',
            ];             
            $tipo_fornecedor = $response['tipo'] == 1 ? 'F' : 'C';
            $tipo_titulo = $response['tipo'] == 1 ? 'Saída' : 'Entrada';     
            $fechamento_caixa = \App\Controllers\FechamentoCaixa::getFechamentoCaixa();
            $interval = new \DateInterval('P1D');
            $data_fechamento_obj = \DateTime::createFromFormat('Y-m-d', $fechamento_caixa['data_fechamento'])->add($interval);
            $data_fechamento = $data_fechamento_obj->format('Y-m-d');
            if($tipo_fornecedor == 'C') {
                $fornecedores = Fornecedor::getAllByTipo($tipo_fornecedor);
            }
            else{
                $fornecedores[0] = Fornecedor::getById(4979);
            } 
            
            $tiposDocumentoFinanceiro = TipoDocumentoFinanceiro::getByFiltros($filtrosTipoDoc);
            $unidades = Filial::getUnidadesAtivas($_SESSION['empresa_user']);
            $naturezas = IPCF::getAllNatureza($tipo);
            $hoje = (new \DateTime())->format('Y-m-d');
            $urs = $_SESSION['empresa_user'];
            $origens= ContaBancaria::getContasBancariasByURs($urs,4);
            $aplicacoes = AplicacaoFundos::getAll();
            $assinaturas = Assinatura::getAll();  
               

            require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/caixa-fixo/templates/criar-conta.phtml');
    

         
        }
    }

    public static function update()
    { 
        if(isset($_POST) && !empty($_POST)){
           
            $data = [];
            $competencia  = filter_input(INPUT_POST, 'competencia', FILTER_SANITIZE_STRING);
            $data['competencia'] = \DateTime::createFromFormat('m/Y', $competencia)->format('Y-m');
            $data['data_emissao']  = filter_input(INPUT_POST, 'data_emissao', FILTER_SANITIZE_STRING);
            $data['data_pagamento']  = filter_input(INPUT_POST, 'data_pagamento', FILTER_SANITIZE_STRING);
            $data['descricao']  = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);
            $data['descricao'] = mb_strtoupper($data['descricao'], 'UTF-8');
            $data['forma_pagamento']  = filter_input(INPUT_POST, 'forma_pagamento', FILTER_SANITIZE_STRING);
            $data['fornecedor_nota']  = filter_input(INPUT_POST, 'fornecedor_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['natureza_nota']  = filter_input(INPUT_POST, 'natureza_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['numero_nota']  = filter_input(INPUT_POST, 'numero_nota', FILTER_SANITIZE_STRING);
            $data['origem']  = filter_input(INPUT_POST, 'origem', FILTER_SANITIZE_STRING);
            $data['tipo_documento_nota']  = filter_input(INPUT_POST, 'tipo_documento_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['tipo_nota']  = filter_input(INPUT_POST, 'tipo_nota', FILTER_SANITIZE_STRING);         
            $data['ur_nota']  = filter_input(INPUT_POST, 'ur_nota', FILTER_SANITIZE_NUMBER_INT);
            $data['valor']  = filter_input(INPUT_POST, 'valor', FILTER_SANITIZE_STRING);
            $data['cod_barras']  = filter_input(INPUT_POST, 'cod_barras', FILTER_SANITIZE_STRING);           
            $data['valor'] = str_replace('.','',$data['valor']);
            $data['valor'] = str_replace(',','.',$data['valor']);
            $data['nota_id'] = filter_input(INPUT_POST, 'nota_id', FILTER_SANITIZE_NUMBER_INT); 
            $data['assinatura']  = filter_input(INPUT_POST, 'assinatura', FILTER_SANITIZE_NUMBER_INT);  
            
           

                    

            $fechamentoCaixa = new \DateTime(\App\Controllers\FechamentoCaixa::getFechamentoCaixa()['data_fechamento']);
           
            $dataEntradaObj = new \DateTime($data['data_emissao']);
            $dataPagamentoOj = new \DateTime($data['data_pagamento']);
            if($dataEntradaObj <= $fechamentoCaixa) {
                $response = ['tipo' => 'erro',
                'msg'=>"Data de entrada ".$dataEntradaObj->format('d/m/Y')." menor qua a data de fechamento do período ".$fechamentoCaixa->format('d/m/Y')

            ];
             echo json_encode($response);
                return;
            } 
            if($dataPagamentoOj <= $fechamentoCaixa) {
                $response = ['tipo' => 'erro',
                'msg'=>"Data de dagamento ".$dataPagamentoOj->format('d/m/Y')." menor qua a data de fechamento do período ".$fechamentoCaixa->format('d/m/Y')

            ];
             echo json_encode($response);
                return;
            }              
            
            
            $response = Notas::editarNotaCaixaFixo($data);            
            
            if($response['tipo'] == 'acerto') {
                $response = ['tipo' => 'acerto',
                'msg'=>$response['msg']

                ];
                echo json_encode($response);
                return;
            }  else {
                $response = ['tipo' => 'erro',
                'msg'=>$response['msg']

                ];
                echo json_encode($response);
                return;
               
            }
        }
    }

    public static function cancelarNota()
    {
        if(isset($_POST) && !empty($_POST)){
            $data['id']  = filter_input(INPUT_POST, 'id_nota_cancelar', FILTER_SANITIZE_NUMBER_INT);
            $data['motivo'] =   filter_input(INPUT_POST, 'motivo', FILTER_SANITIZE_STRING);

            $response= Notas::cancelar($data);
            if($response != false) {
                $response = ['tipo' => 'acerto',
                'msg'=>$response

                ];
                echo json_encode($response);
                return;
            } else {
                $response = ['tipo' => 'erro',
                'msg'=>$response
                ];
                echo json_encode($response);
                return;
               
            }
        }
    }
    
    
}