<?php
namespace app\Controllers;

use App\Models\Financeiro\ContaBancaria;

class ContasBancarias
{
    public static function indexSaldosContasBancarias()
    {
        $hoje = (new \DateTime())->format('Y-m-d');
        $contas = ContaBancaria::getContasBancariasByURs($_SESSION['empresa_user']);
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/saldo-contas-bancarias/index.phtml');
    }

    public static function saldosContasBancarias()
    {
        if(isset($_POST) && !empty($_POST)){
            $data_saldo  = filter_input(INPUT_POST, 'data_saldo', FILTER_SANITIZE_STRING);
            $contas  = isset($_POST['contas']) ? $_POST['contas'] : [];
            $filtros = [
                'data_saldo' => $data_saldo,
                'contas' => $contas,
                'empresa_user' => $_SESSION['empresa_user']
            ];

            $response['anterior'] = ContaBancaria::getSaldoAnteriorContasBancarias($filtros);
            $response['atual'] = ContaBancaria::getSaldoAtualContasBancarias($filtros);
        }
        $contas = ContaBancaria::getContasBancariasByURs($_SESSION['empresa_user']);
        require($_SERVER['DOCUMENT_ROOT'] . '/financeiros/relatorios/templates/saldo-contas-bancarias/index.phtml');
    }

    public static function salvarSaldosContasBancarias()
    {
        if (isset($_POST) && !empty($_POST)) {
            $data = $_POST;
            $data_saldo = $data['data_saldo'];
            unset($data['data_saldo']);
            echo ContaBancaria::salvarHistoricoSaldoContasBancarias($data, $data_saldo);
        }
    }
}
