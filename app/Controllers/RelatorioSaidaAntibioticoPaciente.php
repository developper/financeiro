<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 28/04/2017
 * Time: 10:36
 */

namespace app\Controllers;


use App\Models\Logistica\SaidaAntibioticoPaciente;

class RelatorioSaidaAntibioticoPaciente
{
    public static function form()
    {
        require($_SERVER['DOCUMENT_ROOT'] . '/relatorios/templates/saida-antibiotico-paciente/form.phtml');
    }

    public static function pesquisar()
    {
        session_start();
        $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
        $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
        $paciente = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_STRING);

        $filters = [
            'inicio' => $inicio ,
            'fim' => $fim ,
            'paciente' => $paciente,
            'inicioUS' => \DateTime::createFromFormat('d/m/Y', $inicio)->format('Y-m-d'),
            'fimUS' => \DateTime::createFromFormat('d/m/Y', $fim)->format('Y-m-d'),

        ];

        $response = SaidaAntibioticoPaciente::getSaida($filters);

        require($_SERVER['DOCUMENT_ROOT'] . '/relatorios/templates/saida-antibiotico-paciente/form.phtml');


    }

    public static function exportarExcel()
    {
        session_start();

        $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
        $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);
        $paciente = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_STRING);

        $filters = [
            'inicio' => $inicio ,
            'fim' => $fim ,
            'paciente' => $paciente,
            'inicioUS' => \DateTime::createFromFormat('d/m/Y', $inicio)->format('Y-m-d'),
            'fimUS' => \DateTime::createFromFormat('d/m/Y', $fim)->format('Y-m-d'),

        ];


        $response = SaidaAntibioticoPaciente::getSaida($filters);
        $extension = '.xls';

        if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
            $extension = '.xlsx';
        }

        $inicioUS = \DateTime::createFromFormat('d/m/Y', $inicio);
        $fimUS =  \DateTime::createFromFormat('d/m/Y', $fim);

        $i=0;

        $dataHora = date('d/m/Y H:i');
        $usuario = $_SESSION['nome_user'];

        $filename = sprintf(
            "relatorio_saida_antibiotico_paciente_%s_%s.%s",
            $inicioUS->format('d-m-Y'),
            $fimUS->format('d-m-Y'),
            $extension
        );


        header("Content-type: application/x-msexce; charset=ISO-8859-1");
        header("Content-Disposition: attachment; filename={$filename}");
        header("Pragma: no-cache");


        require($_SERVER['DOCUMENT_ROOT'] . '/relatorios/templates/saida-antibiotico-paciente/exportar-excel.phtml');


    }

}