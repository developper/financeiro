<?php

namespace app\Controllers;

use App\Helpers\DateHelper;
use App\Helpers\StringHelper;
use \App\Models\Administracao\Filial;
use App\Models\Financeiro\AplicacaoFundos;
use App\Models\Financeiro\Assinatura;
use App\Models\Financeiro\IPCF;
use App\Models\Financeiro\OrcamentoFinanceiro as ModelOrcamentoFinanceiro;
use DateInterval;
use DateTime;

include_once('../../utils/codigos.php');
//ini_set('display_errors',1);

class OrcamentoFinanceiroRelatorioGerencial
{


    public static function listarNaturezas()
    {
        $responseNaturezas =  IPCF::getAll();
        $naturezas = [];

        foreach ($responseNaturezas as $resp) {
            $naturezas[] = ['id' => $resp['ID'], 'name' => $resp['COD_N4'] . ' ' . $resp['N4']];
        }


        print_r(json_encode(array_values($naturezas)));
    }

    public static function getRealizadoByData($inicio, $fim, $versoaoEstrutura)
    {
        

        $responseRealizado = ModelOrcamentoFinanceiro::getGerencialAutomatizadoByData($inicio, $fim, $unidadeId = null);
       
        $planoContasRealizado = [];
        $classeRealizado = [];
        $identificacaoParcelas = [];
        // natureazas que devemos pegar da contabilidade.
        $estoque = [38,
        39,
        40,
        41,
        42];

        $avlogInvestimento = [298]; 
        
        $remuneracao = [109];
        // $remuneracao = [
        //                 107,
        //                 108,
        //                 109,
        //                 110,
        //                 111,
        //                 112,
        //                 113,
        //                 114,
        //                 115,
        //                 116,
        //                 117,
        //                 118,
        //                 119,
        //                 121,
        //                 122,
        //                 123,
        //                 124];
         $naturezaQueNaoDevemSerUtilizadas = array_merge($remuneracao,$estoque,$avlogInvestimento);
         
        // id naureza prolabore;
        $idProlabore = 108;
        // id natureza 13 salario
        $id13Salario = 109;
        $valorSalario = 0;
        foreach ($responseRealizado as $resp) {
             if(in_array($resp['ID_NATUREZA'], $naturezaQueNaoDevemSerUtilizadas)){
              
             }else{
            
            $idNota = $resp['NOTA_ID'];           
            
          
            $mesAno =  \DateTime::createFromFormat('Y-m-d', $resp['DATA_CONCILIADO'])->format('Y-m');
            
            $valorNota = round($resp['VALOR'], 2);
            // transformando em negativo caso seja conta de despesa
            if ($versoaoEstrutura == 2 && $resp['COD_N3'][0] == 2) {
                $valorNota = -1 * round($resp['VALOR'], 2);
            }
           
            if ($versoaoEstrutura == 2 && $resp['COD_N3'] == '2.1.18') {
                $valor = round($resp['VALOR'], 2);
                if(isset($parcelas[$resp['ID_NATUREZA']][$mesAno][$idNota])){
                    $valor = $parcelas[$resp['ID_NATUREZA']][$mesAno][$idNota]['valor'] + round($resp['VALOR'], 2);
                } 
                $auxParcela = [
                    'tr' => $resp['NOTA_ID'],
                    'numero_nota' => $resp['numero_nota'],
                    'fornecedor' => $resp['fornecedor'],
                    'data' => $mesAno,
                    'valor' => $valor,
                    //'parcela' => $idParcela
    
                ];
                $planoContasRealizado[$idProlabore][$mesAno] += $valorNota;
                $parcelas[$idProlabore][$mesAno][$idNota] = $auxParcela;;
            }else{
                $valor = round($resp['VALOR'], 2);
                if(isset($parcelas[$resp['ID_NATUREZA']][$mesAno][$idNota])){
                $valor = $parcelas[$resp['ID_NATUREZA']][$mesAno][$idNota]['valor'] + round($resp['VALOR'], 2);
                } 
                $auxParcela = [
                    'tr' => $resp['NOTA_ID'],
                    'numero_nota' => $resp['numero_nota'],
                    'fornecedor' => $resp['fornecedor'],
                    'data' => $mesAno,
                    'valor' => $valor,
                    //'parcela' => $idParcela
    
                ];
                $parcelas[$resp['ID_NATUREZA']][$mesAno][$idNota] = $auxParcela;
                $planoContasRealizado[$resp['ID_NATUREZA']][$mesAno] += $valorNota;
                if($resp['ID_NATUREZA'] == 107){
                    $planoContasRealizado[$id13Salario][$mesAno] = round($planoContasRealizado[$resp['ID_NATUREZA']][$mesAno]/12,2);
                   
                }
            }
            
            
        }       
            
           
        }
        
        
        return ['planoContasRealizado' => $planoContasRealizado, 'parcelas' => $parcelas];
    }

    public static function valorRealizadoRemuneracao($inicio, $fim, $valoresRealizados){
        //id plano de contas remuneração.
       
        $remuneracao = [
            107,
            108,
            109,
            110,
            111,
            112,
            113,
            114,
            115,
            116,
            117,
            118,
            119,
            121,
            122,
            123,
            124];
            $remuneracaoRealizado = [];
            $responseRemuneracao = ModelOrcamentoFinanceiro::getValoresPlanoContasGerencial($inicio, $fim, $remuneracao);
            foreach ($responseRemuneracao as $resp) {               
    
               
                $valor = round($resp['valor_mensal'], 2);             
                
                $remuneracaoRealizado[$resp['plano_contas_id']][$resp['data']] += $valor;
                
               
            }
            $valoresRealizados['planoContasRealizado'] = $valoresRealizados['planoContasRealizado']+$remuneracaoRealizado;
           return $valoresRealizados;
    }

    public static function valorRealizadoDepreciacaoAmortizacao($inicio, $fim, $itens){
        //id topico depreciação e Amortizacao.
       
            $depreciacao = [69];
            
            $responseDepreciacao = ModelOrcamentoFinanceiro::getValoresTopicosGerencial($inicio, $fim, $depreciacao);
            $response = [];
            foreach ($responseDepreciacao as $resp) {     
                
              
    
               
                $valor = round($resp['valor_mensal'], 2);   
                foreach($itens as $key => $i){
                    // echo "<pre>";
                    // var_dump($i);
                    // die();
                    if($i['id_topico'] == $resp['orcamento_financeiro_topicos']
                     && $i['data'] == $resp['data']
                     && $i['tabela'] == "orcamento_financeiro_topicos_valores"){
                        $i['realizado'] = $valor;
                    }
                    $itens[$key] = $i;
                }          
                
               
                
               
            }
           return $itens;
    }

    public static function relatorioModeloGerencialAutomatico($idOrcamento)
    {   
        // Regras de calculo.
        /* Topicos
            2 - Home Care - Pegar do IW 
            86 - golsa pegar do IW
            82	PIS (0,65%) Aplicar em cima de Receita do Serviço
            83	COFINS (3%) Aplicar em cima de Receita do Serviço
            84	ISS (2%)    Aplicar em cima de Receita do serviço
            94	IRPJ 
            95	CSLL
            96	IRPJ Diferido
            97	CSLL Diferido
        */
        
        //Recuperando cabeçalho do orçamento finaceiro
        $responseVersaoOrcamento  =  current(ModelOrcamentoFinanceiro::getVersaoOrcamentoById($idOrcamento));
        $versoaoEstrutura = $responseVersaoOrcamento['orcamento_financeiro_versao_estrutura'];
        $inicioOrcamento = $responseVersaoOrcamento['inicio'];
        $fimOrcamento = $responseVersaoOrcamento['fim'];
        $mesAnterior = date('Y-m', strtotime("-1 month", strtotime($inicioOrcamento)));
       // die($mesAnterior);
        //Pegar meses e anos que o orçamento contempla, espera data no formato Y-m função retorna todos os meses entre o periodo
        $responseDatas = DateHelper::getListaMesAno($inicioOrcamento, $fimOrcamento);
        // Formatando meses para ser enviado para o front
        $arrayMonths = self::formatandoMesesParaEnvio($responseDatas);
        //Recuperando Itens do orçamento financeiro             
        $itens = ModelOrcamentoFinanceiro::getOrcamentoAndValorGerencialAutomatico($_GET['id'], $versoaoEstrutura,$mesAnterior);
       
        // Pegar valor Depreciação informado no gerencial.
        //$itens = self::valorRealizadoDepreciacaoAmortizacao($inicioOrcamento, $fimOrcamento, $itens);
        
        //Recuperando valores realizados dos itens
        $valoresRealizados = self::getRealizadoByData($mesAnterior, $fimOrcamento, $versoaoEstrutura);
        
        //pegando as parcelas que somadas vão dar o valor realizado de cada item
        $parcelas = $valoresRealizados['parcelas'];
        
        
        
        // pegando valores realizados gerenciais de remuneracao
        //$valoresRealizados = self::valorRealizadoRemuneracao($inicioOrcamento, $fimOrcamento, $valoresRealizados);
          
       
       
         //Setando valores realizados dos planos de custo
        $itemsWithRealizado = self::_setPlanoDeContaValorRealizado($itens, $valoresRealizados['planoContasRealizado'], $versoaoEstrutura, $parcelas);

        
        

        // retorna o array onde a chave é o pai e valores são seus filhos e netos
        $paiFilhos = self::getFilhos($itemsWithRealizado);


        // setando valor realizado nos pais 
        $itemsWithRealizado = self::setRealizadoPais($itemsWithRealizado, $paiFilhos);      
         
        // setando valor realizado nos pais 
        $itensAHCalc = self::setRealizadoTopicosDRE($itemsWithRealizado, $inicioOrcamento);
       // $itensAHCalc = self::_ahCalc($itensAHCalc);

        
       

        $itensGroupMonth = self::_groupItensMonth($itensAHCalc, $versoaoEstrutura);
        
        $itensGroupMonth = self::_ahCalcAnterior($itensGroupMonth, $mesAnterior);
       //   die();


        // $itensSum = self::_sumItensSon($itensGroupMonth);
        $itensRemoveKey = self::_removeKeyMounth($itensGroupMonth);
        $itensGroupFather = self::_groupItensFather($itensRemoveKey);
        $tipoavaliacao = $versoaoEstrutura == 1 ? 'AH' : 'AV';



        $response = [
            'items' => array_values($itensGroupFather),
            'months' => $arrayMonths,
            'id_budget' => $idOrcamento,
            'type' =>  $tipo == 'editar' ? 'edit' : 'view',
            'begin_date' => $inicioOrcamento,
            'end_date' => $fimOrcamento,
            'chart_of_accounts_add' => '',
            'units' => [],
            'unitId' => [],
            'availityType' => $tipoavaliacao
        ];




        echo json_encode($response);
        return;
    }

    private function setRealizadoPais($itens, $filhos)
    {



        foreach ($filhos as $key => $filho) {


            $filhos[$key]['realizado'] =   array_reduce($filho, function ($aux, $f) use ($itens, $key) {


                foreach ($itens as $i) {
                    if ($i['id_topico'] == $f) {
                        $aux[$i['data']] += $i['realizado'];
                    }
                }



                return $aux;
            });
        }




        $auxItens = [];
        $topicosSum = [];
        foreach ($itens as $key => $i) {
            $auxItens[$key] = $i;
            $recebimento = !empty($filhos[1]['realizado'][$i['data']]) ? $filhos[1]['realizado'][$i['data']] : 0;
            $imposto =  !empty($filhos[4]['realizado'][$i['data']]) ? $filhos[4]['realizado'][$i['data']] : 0;
            $fornecedores =  !empty($filhos[7]['realizado'][$i['data']]) ? $filhos[7]['realizado'][$i['data']] : 0;
            $despesasOperacionais =  !empty($filhos[10]['realizado'][$i['data']]) ? $filhos[10]['realizado'][$i['data']] : 0;
            $despesasGeraisEAdm =  !empty($filhos[11]['realizado'][$i['data']]) ? $filhos[11]['realizado'][$i['data']] : 0;
            $vendasAtivos =  !empty($filhos[23]['realizado'][$i['data']]) ? $filhos[23]['realizado'][$i['data']] : 0;

            $CAPEX =  !empty($filhos[24]['realizado'][$i['data']]) ? $filhos[24]['realizado'][$i['data']] : 0;
            $captacaoEmprestimo =  !empty($filhos[26]['realizado'][$i['data']]) ? $filhos[26]['realizado'][$i['data']] : 0;
            $jurosEmprestimo =  !empty($filhos[27]['realizado'][$i['data']]) ? $filhos[27]['realizado'][$i['data']] : 0;
            $parcelamentoEmprestimo =  !empty($filhos[28]['realizado'][$i['data']]) ? $filhos[28]['realizado'][$i['data']] : 0;
            $parcelemantoTributario =  !empty($filhos[29]['realizado'][$i['data']]) ? $filhos[29]['realizado'][$i['data']] : 0;



            $fluxoCaixaOperacional = $recebimento - ($imposto + $fornecedores + $despesasOperacionais + $despesasGeraisEAdm);

            $fluxoCaixaInvestimento = $fluxoCaixaOperacional + $vendasAtivos - $CAPEX;

            $fluxoCaixaEndividamento = $fluxoCaixaInvestimento + $captacaoEmprestimo - ($jurosEmprestimo + $parcelamentoEmprestimo + $parcelemantoTributario);

            if ($i['id_topico'] == 22) {
                $auxItens[$key]['realizado'] = $fluxoCaixaOperacional;
            } elseif ($i['id_topico'] == 25) {
                $auxItens[$key]['realizado'] = $fluxoCaixaInvestimento;
            } elseif ($i['id_topico'] == 30) {
                $auxItens[$key]['realizado'] = $fluxoCaixaEndividamento;
            } elseif ($filhos[$i['id_topico']]['realizado'][$i['data']]) {
                $auxItens[$key]['realizado'] = $filhos[$i['id_topico']]['realizado'][$i['data']];
            }
        }



        return $auxItens;
    }

    private function setRealizadoTopicosDRE($itens, $inicioOrcamento = null)
    {

        
        $auxItens = [];
        $topicosSum = [];

        foreach ($itens as $key => $i) {
            if ($i['id_plano_de_conta'] == 0) {
                $auxItens[$i['id_topico']][$i['data']]['realizado'] = $i['realizado'];
                $auxItens[$i['id_topico']][$i['data']]['av']   = '';
            }
        }



        //pegando valor de  82	PIS (0,65%) Aplicar em cima de 38 Receita do Serviço
        foreach ($auxItens[82] as $key => $valor) {
            
            $auxItens[82][$key]['realizado'] = -1 *($auxItens[38][$key]['realizado'] * 0.0065);
            $auxItens['40'][$key]['realizado'] = $auxItens['40'][$key]['realizado'] + $auxItens[82][$key]['realizado'];
            $auxItens['39'][$key]['realizado'] = $auxItens['39'][$key]['realizado'] + $auxItens[82][$key]['realizado'];

        }
        //pegando  valor de    83	COFINS (3%) Aplicar em cima de Receita do Serviço
        foreach ($auxItens[83] as $key => $valor) {
            $auxItens[83][$key]['realizado'] = -1 *($auxItens[38][$key]['realizado'] * 0.03);
            $auxItens['40'][$key]['realizado'] = $auxItens['40'][$key]['realizado'] + $auxItens[83][$key]['realizado'];
            $auxItens['39'][$key]['realizado'] = $auxItens['39'][$key]['realizado'] + $auxItens['83'][$key]['realizado'];

        }

        //pegando  valor de    84	ISS (2%)    Aplicar em cima de Receita do serviço
        foreach ($auxItens[84] as $key => $valor) {
            $auxItens[84][$key]['realizado'] = -1 *($auxItens[38][$key]['realizado'] * 0.02);
            $auxItens['40'][$key]['realizado'] = $auxItens['40'][$key]['realizado'] + $auxItens[84][$key]['realizado'];
            $auxItens['39'][$key]['realizado'] = $auxItens['39'][$key]['realizado'] + $auxItens[84][$key]['realizado'];

        }

        // pegando valores de Receita liquida topico 42 = Receitas Servicao 38 + Deducoes Vendas 39
        foreach ($auxItens[42] as $key => $valor) {
            $auxItens[42][$key]['realizado'] = $auxItens[38][$key]['realizado'] + $auxItens[39][$key]['realizado'];
        }


        // pegando valores de Custos dos servicos 43 = Custos variaveis 44 + custo fixo 50
        foreach ($auxItens[43] as $key => $valor) {
            $auxItens[43][$key]['realizado'] = $auxItens[44][$key]['realizado'] + $auxItens[50][$key]['realizado'];
        }

        // pegando valores de MArgem Contribuicao 49 = Receita liquida 42 + Custos variaveis 44
        foreach ($auxItens[49] as $key => $valor) {
            $auxItens[49][$key]['realizado'] = $auxItens[42][$key]['realizado'] + $auxItens[44][$key]['realizado'];
            $auxItens[49][$key]['av'] =  round($auxItens[49][$key]['realizado'] /  $auxItens[42][$key]['realizado'], 2);
        }

        // Lucro operacional bruto 53 =Receita Liquida 42 + ( Custo fixo 50 + Custos variaveis 44 )
        foreach ($auxItens[53] as $key => $valor) {
            $auxItens[53][$key]['realizado'] = $auxItens[42][$key]['realizado'] + ($auxItens[50][$key]['realizado']  + $auxItens[44][$key]['realizado']);
            $auxItens[53][$key]['av'] =  round($auxItens[53][$key]['realizado'] /  $auxItens[42][$key]['realizado'], 2);
        }

        // Despesa operacionais = Venda Marketing + Gerais e administrativa + Depreciação e Amortização

        // Ebitida 70 = Receita Liquida 42 + (Custo dos Serviços 43 + 54 Despesa Operacionais)
        foreach ($auxItens[70] as $key => $valor) {
            $auxItens[70][$key]['realizado'] = $auxItens[42][$key]['realizado'] + ($auxItens[43][$key]['realizado']  + $auxItens[54][$key]['realizado']);
            $auxItens[70][$key]['av'] =  round($auxItens[70][$key]['realizado'] /  $auxItens[42][$key]['realizado'], 2);
        }

        // Res. antes do IRPJ e CSLL 73 = EBTIDA 70 + Resultado Financeiro 71 + Outras Receitas 72 + 69 Depreciação e Amortização
        foreach ($auxItens[73] as $key => $valor) {
            $auxItens[73][$key]['realizado'] = $auxItens[70][$key]['realizado']  + $auxItens[71][$key]['realizado']   + $auxItens[72][$key]['realizado'] +$auxItens[69][$key]['realizado'];
        }
        // IRPJ 94 = Res. antes do IRPJ e CSLL 73  * 24%
        foreach ($auxItens[94] as $key => $valor) {
            if($auxItens[73][$key]['realizado'] < 0){
                $auxItens[94][$key]['realizado'] = 0;   
            }else{
                $auxItens[94][$key]['realizado'] = -1 * ($auxItens[73][$key]['realizado'] * 0.25);
            }
            
        }

        // CSLL 95 = Res. antes do IRPJ e CSLL 73  * 9%
        foreach ($auxItens[95] as $key => $valor) {
            if($auxItens[73][$key]['realizado'] < 0){
                $auxItens[95][$key]['realizado'] = 0;   
            }else{
            $auxItens[95][$key]['realizado'] = -1 * ($auxItens[73][$key]['realizado'] * 0.09);
            }
        }
        // IRPJ e CSLL 74 = IRPJ 94 + CSLL 95
        foreach ($auxItens[74] as $key => $valor) {
            $auxItens[74][$key]['realizado'] = $auxItens[94][$key]['realizado'] + $auxItens[95][$key]['realizado'];
        }

        // Lucro Liquido 75   =  Res. antes do IRPJ e CSLL 73 + IRPJ e CSLL 74
        foreach ($auxItens[75] as $key => $valor) {

            $auxItens[75][$key]['realizado'] = $auxItens[73][$key]['realizado'] + $auxItens[74][$key]['realizado'];
            $auxItens[75][$key]['av'] =  round($auxItens[75][$key]['realizado'] /  $auxItens[42][$key]['realizado'], 2);
        }

        $auxCont = 0;

        foreach ($auxItens[76] as $key => $valor) {

            $mesAtualOject =  \DateTime::createFromFormat('Y-m-d', $key . '-01');
            $mesAtual = $mesAtualOject->format('Y-m');           

            $interval =  new \DateInterval('P1M');
            $mesAnteriorObjec = $mesAtualOject->sub($interval);
            $mesAnterior = $mesAnteriorObjec->format('Y-m');
            $auxItens[76][$key]['realizado'] = $auxItens[75][$key]['realizado'] + $auxItens[76][$mesAnterior]['realizado'];
            if($inicioOrcamento == $mesAtual){
                $auxItens[76][$key]['realizado'] = $auxItens[75][$key]['realizado'];
            }
            $auxCont++;
        }



        $aux = [];
  

        foreach ($itens as $key => $i) {

            $aux[$key] = $i;            


            if (isset($auxItens[$i['id_topico']][$i['data']])) {
                $aux[$key]['realizado']  = $auxItens[$i['id_topico']][$i['data']]['realizado'];
                $aux[$key]['AV']  = $auxItens[$i['id_topico']][$i['data']]['av'] * 100;
                $aux[$key]['AV_color'] = 'black';
            }
        }


         


        return $aux;
    }

    private function filhos($itemArray, $arrayPais)
    {
        $auxArray = [];

        return   $arrayChildren =  array_reduce($itemArray, function ($aux, $item) use ($arrayPais) {

            if (array_key_exists($item, $arrayPais)) {



                foreach ($arrayPais[$item] as $key => $father) {
                    $aux[] = $father;

                    if (array_key_exists($father, $arrayPais)) {

                        $ArrayNeto = [];
                        $ArrayNeto[$father] = $father;

                        $aux = array_merge(self::filhos($ArrayNeto, $arrayPais), $aux);
                    }
                }
            }



            return $aux;
        });
    }

    private function getFIlhos($itemRealizado)
    {
        $arrayPais =   array_reduce($itemRealizado, function ($aux, $item) {
            if ($item['id_pai']) {
                $aux[$item['id_pai']][$item['id_topico']] = $item['id_topico'];
                return $aux;
            }
        });





        $aux = $arrayPais;

        foreach ($arrayPais as $key => $pai) {


            $auxfilho = self::filhos($pai, $arrayPais);

            if (!empty($auxfilho)) {
                $aux[$key] = array_merge($arrayPais[$key], $auxfilho);
            }
        }




        return $aux;
    }

    public function _setPlanoDeContaValorRealizado($items, $planoContasRealizado, $versoaoEstrutura, $parcelas)
    {
      
        $response = [];
        
            //modelo DRE
           // $topicos[2] = []; // HOMECARE.            
            //$topicos[77] = [279]; //teleconsulta
           // $topicos[79] = [283]; // topicoServicoLogistico
            //$topicos[80] = [284]; // APH
            //$topicos[82] = []; // PIS
            //$topicos[83] = []; //COFINS
           // $topicos[84] = []; // ISS
            $topicos[85] = [167]; //DEVCancelamento
            // $topicos[86] = []; // GLOSA
           // $topicos[87] = [5, 6, 7, 8, 9]; // RECEITAFianceira
           // $topicos[88] = [132, 133, 204, 134, 205, 274, 135]; // despFinanceira
           // $topicos[89] = [10]; // VENDa IMOBILIZADO
           // $topicos[90] = [11]; // RECUPERACAO de CREDITO
           // $topicos[91] = [12]; //RECEITA ALUGUEL
          //  $topicos[96] = []; //IRPJ
           // $topicos[97] = []; // CSLL
           // $topicos[98] = []; // Reembolso Plano de Saude
           
            foreach ($items as $item) {
               
                if($item['id_plano_de_conta'] == 109){
                    $item['name'] = $item['name']." (PROVISÃO)";
                }
                
                if (array_key_exists($item['id_plano_de_conta'], $planoContasRealizado)) {
                    
                    $item['realizado'] = $planoContasRealizado[$item['id_plano_de_conta']][$item['data']];
                    $item['parcelas'] = array_values($parcelas[$item['id_plano_de_conta']][$item['data']]);
                } elseif (array_key_exists($item['id_topico'], $topicos)) {
                    $realizado = 0;
                    $auxParcela = [];
                    foreach ($topicos[$item['id_topico']] as $t) {
                        $realizado += $planoContasRealizado[$t][$item['data']];

                    }
                    $item['realizado'] = $realizado;
                    $item['parcelas'] = array_values($parcelas[$item['id_plano_de_conta']][$item['data']]);
                }
                array_push($response, $item);
            }

            
        
        return $response;
    }

    public function _ahCalc($items)
    {
        foreach ($items as $key => $item) {
            
            if ($item['value']) {
                $items[$key]['AH'] = round(($item['realizado'] / $item['value']) * 100 - 100, 2);
                $items[$key]['AH_color'] = 'black';
            }
        }

        return array_values($items);
    }

    public function _ahCalcAnterior($items, $mesAnterior)
    {
        foreach ($items as $key => $item) {
            foreach($item['months'] as $monthKey => $month){
                $dataAtual = DateTime::createFromFormat('Y-m-d', $monthKey.'-10');
                //if($dataAtual->format('m') != '01'){
                    $interval = new DateInterval('P1M');
                    $dataMesAnterior = $dataAtual->sub($interval)->format('Y-m');
                    $valorRealizadoAnterior = $items[$key]['months'][$dataMesAnterior]['realizado'];

                    if ($valorRealizadoAnterior) {
                        $items[$key]['months'][$monthKey]['AH'] = round(($month['realizado'] / $valorRealizadoAnterior) * 100 - 100, 2);
                        $items[$key]['months'][$monthKey]['AH_color'] = 'black';
                    }
                    if($dataMesAnterior == $mesAnterior){
                        unset($items[$key]['months'][$dataMesAnterior]);
                    }


                //}               
                
            }
            
        }

        return $items;
    }

    public function _groupItensMonth($items, $versoaoEstrutura)
    {
        $response = [];
        $positivoTopicos = [1, 2, 3, 23, 26];
        $totalRealizadoReceitaLiquida = 0;
        $totalOrcadoReceitaLiquida = 0;

        foreach ($items as $item) {
            $valorRealizado = (float) $item['realizado'];
            if (in_array($item['id_topico'], [22, 25, 30])) {

                $colorRealizado = $valorRealizado < 0 ? 'red' : 'blue';
                $color = (float) $item['value'] < 0 ? 'red' : 'blue';
                $colorTopico = '';
                $colorTopicoRealizado = '';
            } elseif (in_array($item['id_topico'], $positivoTopicos)) {
                $color = 'blue';
                $colorRealizado = $color;
                $colorTopico = $color;
                $colorTopicoRealizado = $color;
            } else {
                $color = in_array($item['id_pai'], $positivoTopicos) ? 'blue' : 'red';
                $colorTopico = $color;
                $colorRealizado = $color;
                $colorTopicoRealizado = $color;
            }
            if (!array_key_exists($item['id_topico'],  $response)) {
                $response[$item['id_topico']]['id_topico'] = $item['id_topico'];
                $response[$item['id_topico']]['ordem'] = $item['ordem'];
                $response[$item['id_topico']]['id_pai'] = $item['id_pai'];
                $response[$item['id_topico']]['name'] = $item['name'];
                $response[$item['id_topico']]['percentage'] = (float) $item['aliquota'];
                $response[$item['id_topico']]['color'] = $colorTopico;
                $response[$item['id_topico']]['color_realized'] = $colorTopicoRealizado;
                $response[$item['id_topico']]['type_calc'] = (float) $item['aliquota'] == 0 ? "VALUE" : "PERCENTAGE";;
                //$response[$item['id_topico']]['id_plano_de_conta'] = $item['id_plano_de_conta'];
                $response[$item['id_topico']]['months'] = [];
                $response[$item['id_topico']]['sub'] = [];
            }
            $mesAno =  \DateTime::createFromFormat('Y-m-d', $item['data'] . '-01');
            // topicos que a cor depende do somatorio de outros itens.

            if ($versoaoEstrutura == 2) {
                $colorRealizado = $valorRealizado < 0 ? 'red' :  'blue';
                $color = $item['value'] < 0 ? 'red' :   'blue';
                if ($item['id_topico'] == 42) {
                    $totalRealizadoReceitaLiquida += $valorRealizado;
                    $totalOrcadoReceitaLiquida += (float) $item['value'];
                }

                if (in_array($item['id_topico'], [75, 70, 53, 49])) {
                    $response[$item['id_topico']]['total']['realizado'] += round($valorRealizado, 2);
                    $response[$item['id_topico']]['total']['value'] += (float) $item['value'];
                    $response[$item['id_topico']]['total']['AV'] = round(((float) $response[$item['id_topico']]['total']['realizado'] / (float) $totalRealizadoReceitaLiquida), 2) * 100;

                    // echo "<pre>";
                    // print_r($response[$item['id_topico']]['total']);
                    // print_r($totalRealizadoReceitaLiquida);
                    // print_r($response[$item['id_topico']]['total']['realizado'] / $totalReceitaLiquida);
                } else {
                    $response[$item['id_topico']]['total']['realizado'] += round($valorRealizado, 2);
                    $response[$item['id_topico']]['total']['value'] += (float) $item['value'];
                    $response[$item['id_topico']]['total']['AV'] = '';
                }
            }

            $colorBackGround = 'green';
            $yellowBackground = [42, 49, 53, 70, 71, 75, 76];

            if ($versoaoEstrutura != 2) {
                if ($item['id_topico'] == 1) {
                    $colorBackGround = 'blue';
                }
            } else {
                if ($item['id_topico'] == 38) {
                    $colorBackGround = 'blue';
                } elseif (in_array($item['id_topico'], $yellowBackground)) {

                    $colorBackGround = 'yellow';
                }
            }



            $response[$item['id_topico']]['color_background'] = $colorBackGround;


            $months = [
                'value' => (float) $item['value'],
                'showCell' => false,
                'id' => $item['id'],
                'topico_id' => $item['id_topico'],
                'table' => $item['tabela'],
                'month' => $mesAno->format('m'),
                'year' => $mesAno->format('Y'),
                'data' => $mesAno->format('Y-m'),
                'realizado' => round($valorRealizado, 2),
                'AH' => (float) $item['AH'],
                'AH_color' => $item['AH_color'],
                'AV' => (float) $item['AV'],
                'AV_color' => $item['AV_color'],
                'color' => $color,
                'color_realized' => $colorRealizado,
                'parcelas' => $item['parcelas']
            ];
            //array_push($response[$item['id_topico']]['months'],$months);
            $response[$item['id_topico']]['months'][$months['data']] = $months;
        }
        // removendo fluxo de investimento do relatório automatico.
        unset($response[99]);
        return $response;
    }
    //TODO somar

    public function _sumItensSon($items)
    {
        $teste = self::_groupItensFather($items);

        foreach ($items as $item) {
            if (array_key_exists($item['id_pai'], $items)) {
                foreach ($item['months'] as $mounth) {
                    $items[$item['id_pai']]['months'][$mounth['data']]['realizado'] += $mounth['realizado'];
                    $items[$item['id_pai']]['months'][$mounth['data']]['AH'] = ($items[$item['id_pai']]['months'][$mounth['data']]['realizado'] / $items[$item['id_pai']]['months'][$mounth['data']]['value']) * 100;
                    $items[$item['id_pai']]['months'][$mounth['data']]['AH'] ? $items[$item['id_pai']]['months'][$mounth['data']]['AH'] -= 100 : 0;
                }
            }
        }

        return $items;
    }

    public function _removeKeyMounth($items)
    {
        //TODO: implementar
        foreach ($items as $key => $item) {
            $items[$key]['months'] = array_values($items[$key]['months']);
        }

        return $items;
    }

    public function _groupItensFather($items)
    {
        $items = self::arrayToObject($items);
        $childs = array();
        /*
        CASO USUÁRIO TENHA RESTRIÇÃO DE NATUREZA NÃO DEVE VISUALIZAR OS FILHOS DESSES TOPICOS
            14 - PRO LABORE
            26 - Captação Empréstimos
            27 - Juros Empréstimo
            28 - Parcela Empréstimo
            financeiro_orcamento_financeiro_relatorio_restricao_natureza
        */
        $notViewChilds =  [];
        if ($_SESSION['adm_user'] != 1 && isset($_SESSION['permissoes']['financeiro']['financeiro_orcamento_financeiro']['financeiro_orcamento_financeiro_relatorio_restricao_natureza'])) {
            $notViewChilds =   [14, 26, 27, 28];
        }


        foreach ($items as $item) {
            if (!in_array($item->id_pai, $notViewChilds))
                $childs[$item->id_pai][] = $item;
        }

        foreach ($items as $item) {
            isset($childs[$item->id_topico]) ? $item->sub = $childs[$item->id_topico] : '';
        }
        $tree = $childs[0];

        return $tree;
    }

    public static function arrayToObject($array)
    {
        $json = json_encode($array);
        $object = json_decode($json);
        return $object;
    }

    public static function formatandoMesesParaEnvio($responseDatas)
    {

        foreach ($responseDatas as $resp) {
            $mesAno =  \DateTime::createFromFormat('Y-m-d', $resp . '-01');
            $arrayMonths[] = [
                'month' => $mesAno->format('m'),
                'year' => $mesAno->format('Y'),
                'date' => $mesAno->format('Y-m')
            ];
        }
        return $arrayMonths;
    }
}
