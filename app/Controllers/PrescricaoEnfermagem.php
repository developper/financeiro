<?php
namespace app\Controllers;

use App\Models\Enfermagem as Enfermagem;

class PrescricaoEnfermagem {
  public static function listarPrescricao($idPaciente, $inicio, $fim)
  {

  }
  public static function prescrever($idPaciente, $inicio, $fim)
  {

  }
  public static function allCuidados(){
    $itens = Enfermagem\PrescricaoEnfermagem::getAllCuidados();
    return $itens;
  }
  public static function cuidadosAtivos(){
    $itens = Enfermagem\PrescricaoEnfermagem::getSQLCuidadosAtivos();
    return $itens;
  }
  public static function allFrequencias()
  {
    $frequencia =Enfermagem\PrescricaoEnfermagem::getAllFrequecias();
    return $frequencia;
  }
  public static function salvar($idPaciente,$inicio,$fim,$dados,$idUser,$carater){
    $response = Enfermagem\PrescricaoEnfermagem::setSalvar($idPaciente,$inicio,$fim,$dados,$idUser,$carater);
    return $response;

  }
  public static function getPrescrioes($idPaciente,$inicio,$fim,$carater )
  {
    return  Enfermagem\PrescricaoEnfermagem::getPrescricoes($idPaciente,$inicio,$fim,$carater );
  }
  public static function getItensPrescricao($idPrescEnf ){
    $itens = Enfermagem\PrescricaoEnfermagem::getItensPrescricao($idPrescEnf);
    return $itens;


  }

  public static function caraterPrescricaoEnfermagem()
{
  $itens = Enfermagem\PrescricaoEnfermagem::getCaraterPrescricaoEnfermagem();
  return $itens;
}
}