<?php
namespace App\Controllers\Faturamento;
use App\Models\Administracao\Filial;
use App\Models\Administracao\Operadora;
use App\Models\Administracao\Paciente;
use App\Models\Faturamento\Fatura;
use \App\Models\Logistica\Saidas as ModelSaidas;
use \App\Models\Sistema\Equipamentos as ModelEquipamento;
use \App\Models\Logistica\Devolucao as ModelDevolucao;
use App\Models\Faturamento\UnidadeMedida;
use App\Models\Faturamento\Relatorios as RelModel;

class FaturarPorPaciente
{
    public static function formBuscarPacientesFaturar()
    {
        $data = [];
        $data['inicio'] = (new \DateTime())->modify('first day of this month')->format('Y-m-d');
        $data['fim'] = (new \DateTime())->modify('last day of this month')->format('Y-m-d');
        $responseFaturasSalvas = Fatura::getFaturasSalvas(0);
        $operadoras = Operadora::getAll();
        $unidades = Filial::getAll();
        $pacientes = Paciente::getAll();

        require($_SERVER['DOCUMENT_ROOT'] . '/faturamento/faturar-por-paciente/templates/form-buscar-pacientes-faturar.phtml');
    }

    public static function buscarPacientesFaturar()
    {
        if (isset($_POST) && !empty($_POST)) {
            $data = [];

            $data['operadora'] = isset($_POST['operadora']) ? $_POST['operadora'] : [];
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $data['unidade'] = isset($_POST['unidade']) ? $_POST['unidade'] : [];

            $response = RelModel::relatorioPacientesFaturar($data);
            $responseFaturasSalvas = Fatura::getFaturasSalvas(0);
            $operadoras = Operadora::getAll();
            $unidades = Filial::getUnidadesAtivas();
            $pacientes = Paciente::getAll();

            require($_SERVER['DOCUMENT_ROOT'] . '/faturamento/faturar-por-paciente/templates/form-buscar-pacientes-faturar.phtml');
        }
    }

    public static function formFatura()
    {
        $inicio = $_GET['inicio'];
        $fim = $_GET['fim'];
        $refaturarmed ='';
        $refaturarserv ='';
        $refaturareqp ="AND E.DATA_INICIO <> '0000-00-00 00:00:00' AND
                    ( ( E.DATA_INICIO BETWEEN '{$inicio}' AND '{$fim}' ) OR
                   ( E.DATA_FIM BETWEEN '{$inicio}' AND '{$fim}') OR
                   (E.DATA_INICIO < '{$inicio}' AND (  E.`DATA_FIM`='0000-00-00 00:00:00' OR E.`DATA_FIM` >'{$fim}' ))) ";
        // $paciente = $_GET['paciente'];
        $paciente = 142;
        $responsePaciente =  Paciente::getById($paciente);
        $responseOperadora = Operadora::getOperadoraByPlanoId($responsePaciente['convenio']);
        $sqlFatura = Fatura::getItensPreFaturaByPaciente($paciente, "C.convenio", $inicio, $fim, $refaturarmed, $refaturarserv, $refaturareqp);
        $devolucoes = ModelDevolucao::getDevolucaoByPaciente($paciente, $inicio, $fim);
        $envios = ModelSaidas::getSaidaEquipamentoByPaciente($paciente, $inicio, $fim);
        $equipamentoAtivos = ModelEquipamento::getEquipamentosAtivosByPaciente($paciente);
        $result = mysql_query($sqlFatura);
        $resultado= mysql_num_rows($result);
        $tipo = 'Paciente';
        $cod = $paciente;
        $inicio = $inicio;
        $fim = $fim;
        $nome = $responsePaciente['nome'];
        $plano = $responsePaciente['convenio'];
        $operadoraId = $responseOperadora['operadora_id'];
        $label_tipo = "FATURA DO PACIENTE {$nome}";

        if($resultado > 0){
            $itensPadronizados =  PadronizarItensPorContrato::padronizarItens($result,$responseOperadora['operadora_id']);
        }
        $responseUnidadesFatura = UnidadeMedida::getAll();

        require ($_SERVER['DOCUMENT_ROOT'] . '/faturamento/faturar-por-paciente/templates/form-faturar.phtml');
    }


}
