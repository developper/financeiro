<?php
namespace App\Controllers\Faturamento;

class PadronizarItensPorContrato
{
    public static function padronizarItens($result, $operadora)
    { 
        
        $resultContratoMedicamento = self::getTabelaMedicamento($operadora);        
        $arrayAux = [];
        
        //Montando array baseado na consulta de fatura já existente, baeadas em  saida e  prescrições;
        
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            
            if (in_array($row['tipo'], [0, 1, 3])) {
                // Agrupando itens pelo catalogoId escolhido e calculando custo médio quando forem
                // dos tipos medicamento, material e dieta.
                $CatalogoId = !empty($row['catalogo_id_escolhido']) ? $row['catalogo_id_escolhido'] : $row['catalogo_id'];

                if (empty($arrayAux[$row['tipo']][$CatalogoId])) {
                    $arrayAux[$row['tipo']][$CatalogoId] = $row;
                    $arrayAux[$row['tipo']][$CatalogoId] ['catalogo_id'] = $CatalogoId ;
                } else {
                    $quantidadeAtual = $arrayAux[$row['tipo']][$CatalogoId]['quantidade'];
                    $custoAtual =  $arrayAux[$row['tipo']][$CatalogoId]['custo'];
                    $quantidadeRow  = $row['quantidade'];
                    $custoRow =  $row['custo'];
                    $custo = (($quantidadeAtual * $custoAtual) + ($quantidadeRow * $custoRow))
                     /
                     ($quantidadeAtual+$quantidadeRow);
                    $quantidade = $quantidadeAtual+$quantidadeRow;
                    $arrayAux[$row['tipo']][$CatalogoId]['custo'] = round($custo, 2);
                    $arrayAux[$row['tipo']][$CatalogoId]['quantidade'] = $quantidade;
                    $arrayAux[$row['tipo']][$CatalogoId]['catalogo_id'] = $CatalogoId ;
                }
            } else {
                $arrayAux[$row['tipo']]['catalogo_id'] = $row;
            }
        }        
        

        // Padronizar item tipo medicamento pela tabela de cobrança.
        if (!empty($resultContratoMedicamento) && !empty($arrayAux[0])) {
            
            $arrayAux[0] = self::getPadraoCobrancaMedicamentos($resultContratoMedicamento, $arrayAux[0]);
        }       
        
        $arrayPadronizado = [];
        foreach($arrayAux as $arr){
          foreach($arr as $a){
            $arrayPadronizado[] = $a;
          }

        }
       
        return $arrayPadronizado;
    }
    // Padronizar itens de acordo com as tabelas de cobranças dos planos.
    public static function getPadraoCobrancaMedicamentos($resultContratoMedicamento, $itensCatalogo)
    {
        $catalogoIdMedicamento = array_keys($itensCatalogo);
        
        $catalogoIds = implode(',', $catalogoIdMedicamento);
        $totalItens = count($catalogoIdMedicamento);
        $medicamentosPadronizados = [];

        while ($row = mysql_fetch_array($resultContratoMedicamento, MYSQL_ASSOC)) {
            $tabela = 'tabelas_cobrancas_medicamentos';
            $comparadorCatalogo = 'catalogo_id';
            $comparadorTabelaCobranca = " AND tabelas_cobrancas_id = {$row['tabela_cobranca_id']}";
            $campos = "catalogo_id, item as principio, valor,codigo_fatura as codigo_proprio, '{$tabela}' as TABELA_ORIGEM" ;
            $arrayAuxItensPadronizados = [];
            if (in_array($row['tabela_cobranca_id'], [1,2,3,4])) {
                $tabela = 'catalogo';
                $comparadorCatalogo = 'ID';
                $comparadorTabelaCobranca = '';
                if (in_array($row['tabela_cobranca_id'], [1,3])) {
                    // valor de fabrica
                    $campoValor = 'VALOR_FABRICA as valor';
                } else {
                    // valor venda
                    $campoValor = 'VALOR_VENDA as valor';
                }
                $campos = "ID as catalogo_id, {$campoValor}";
            }

            $sql = "select 
            {$campos}
            from
            {$tabela}
            where
            $comparadorCatalogo in ($catalogoIds)
            $comparadorTabelaCobranca";
            
            $resultTabelaCobranca = mysql_query($sql);
            $quantidadeItensEncontrados = mysql_num_rows($resultTabelaCobranca);
            if ($quantidadeItensEncontrados > 0) {
                while ($rowTabelaCobranca = mysql_fetch_array($resultTabelaCobranca, MYSQL_ASSOC)) {
                    $fator = $row['fator']/100;
                    $valor = $rowTabelaCobranca['valor'] + ($rowTabelaCobranca['valor'] * $fator);
                    $itensCatalogo[$rowTabelaCobranca['catalogo_id']]['tabela_cobranca_id'] = $row['tabela_cobranca_id'];
                    if ($tabela == 'catalogo') {
                        $itensCatalogo[$rowTabelaCobranca['catalogo_id']]['valor'] = $valor;
                        $itensCatalogo[$rowTabelaCobranca['catalogo_id']]['codigo_proprio'] = 0;
                    } else {
                        $itensCatalogo[$rowTabelaCobranca['catalogo_id']]['valor'] = $valor;
                        $itensCatalogo[$rowTabelaCobranca['catalogo_id']]['codigo_proprio'] = $rowTabelaCobranca['codigo_proprio'];
                        $itensCatalogo[$rowTabelaCobranca['catalogo_id']]['TABELA_ORIGEM'] = $tabela;
                        $itensCatalogo[$rowTabelaCobranca['catalogo_id']]['principio'] = $rowTabelaCobranca['principio'];
                    }
                    $arrayAuxItensPadronizados[$rowTabelaCobranca['catalogo_id']]=$rowTabelaCobranca['catalogo_id'];
                    if (($key = array_search($rowTabelaCobranca['catalogo_id'], $catalogoIdMedicamento)) !== false) {
                        unset($catalogoIdMedicamento[$key]);
                        $catalogoIds = implode(',', $catalogoIdMedicamento);
                    }
                }

                if (count($catalogoIdMedicamento) > 0) {
                    return $itensCatalogo;
                }
            }
        }
        return $itensCatalogo;
    }
    public static function getTabelaMedicamento($operadora)
    {
        $sqlTabelaMedicamento = "SELECT
        
        ctc.*
        FROM
            `contrato_operadora`as co
        inner join
             contratos_tabelas_cobrancas as ctc on (co.id = ctc.contrato_operadora_id)
        where
            co.operadora_id = {$operadora}
        and
            ctc.tipo = 'medicamento'
            order by ctc.ordem ASC";
        $resultTabelaMedicamento = mysql_query($sqlTabelaMedicamento);
        if (mysql_num_rows($resultTabelaMedicamento) >0) {
            return $resultTabelaMedicamento;
        } else {
            return false;
        }
    }
}