<?php
namespace App\Controllers\Faturamento;

use App\Helpers\Paginator;
use App\Models\Faturamento\Operadora;
use App\Models\Faturamento\Relatorios AS RelModel;

class Relatorios
{
    public static function formRelatorioPacientesFaturar()
    {
        $data = [];
        $data['inicio'] = (new \DateTime())->modify('first day of this month')->format('Y-m-d');
        $data['fim'] = (new \DateTime())->modify('last day of this month')->format('Y-m-d');

        $operadoras = Operadora::getAll();

        require ($_SERVER['DOCUMENT_ROOT'] . '/faturamento/relatorios/templates/form-relatorio-pacientes-faturar.phtml');
    }

    public static function relatorioPacientesFaturar()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];

            $data['operadora'] = isset($_POST['operadora']) ? $_POST['operadora'] : [];
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);

            $response = RelModel::relatorioPacientesFaturar($data);
            $operadoras = Operadora::getAll();

            require ($_SERVER['DOCUMENT_ROOT'] . '/faturamento/relatorios/templates/form-relatorio-pacientes-faturar.phtml');
        }
    }
}
