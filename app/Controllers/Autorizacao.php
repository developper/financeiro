<?php

namespace app\Controllers;

use \App\Models\Administracao\Filial;
use \App\Models\Administracao\Operadora;
use \App\Models\Administracao\Paciente;
use \App\Models\Administracao\Planos;
use \App\Models\Autorizacao\ComentarioAutorizacao;
use \App\Models\Medico\Avaliacao;
use \App\Models\Medico\Prorrogacao;
use \App\Models\Enfermagem\PrescricaoEnfermagem;
use \App\Models\Faturamento\Fatura;
use \App\Models\Autorizacao\Autorizacao as AutorizacaoModel;

class Autorizacao
{
	public static function formListarOrcamentosRelatorios()
	{
	    $paciente = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
        $cabecalho = Paciente::getCabecalho($paciente);
        $operadora = Operadora::getOperadoraByPlanoId($cabecalho['plano_id'])['operadora_id'];
		require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/autorizacao/listar-orcamentos-form.phtml');
	}

    public static function pesquisarOrcamentoRelatorio()
    {
        if(isset($_POST) && !empty($_POST)){
            $paciente = filter_input(INPUT_POST, 'paciente_id', FILTER_SANITIZE_NUMBER_INT);
            $operadora = filter_input(INPUT_POST, 'operadora_id', FILTER_SANITIZE_NUMBER_INT);
            $tipo = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_NUMBER_INT);
            $periodoInicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'tipo'          => $tipo,
                'paciente'      => $paciente,
            ];

            if($operadora == 11) {
                /*$response = [
                    Avaliacao::getAvaliacoesNaoAutorizadas($filtros),
                    Prorrogacao::getProrrogacoesNaoAutorizadas($filtros),
                    PrescricaoEnfermagem::getPrescricoesNaoAutorizadas($filtros),
                ];*/
            } else {
               // $response = Fatura::getOrcamentosNaoAutorizados($filtros);
            }

            $comentarios = [];
            $cabecalho = Paciente::getCabecalho($paciente);
            $response = Fatura::getOrcamentosNaoAutorizados($filtros);
            $orcamentos = array_map(function($value) {
                return $value['item_id'];
            }, $response);
            $comentariosOrc = ComentarioAutorizacao::getTotalComentariosByOrcamentoIds($orcamentos);
            foreach ($comentariosOrc as $comentario) {
                $comentarios[$comentario['fatura_orcamento_id']] = $comentario['total_comentarios'];
            }

            $count = count($response);
            require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/autorizacao/listar-orcamentos-form.phtml');
        }
    }

    public static function pesquisarItensOrcamentoAutorizacao()
    {
        $orcamento = filter_input(INPUT_GET, 'orcamento', FILTER_SANITIZE_NUMBER_INT);
        $response = Fatura::getItensOrcamentoAutorizacao($orcamento);
        echo json_encode($response);

    }

    public static function buscarArquivosAutorizacao()
    {
        $autorizacao = filter_input(INPUT_GET, 'autorizacao', FILTER_SANITIZE_NUMBER_INT);
        $response = AutorizacaoModel::getArquivosAutorizacao($autorizacao);

        echo json_encode($response);

    }

    public static function excluirArquivoAutorizacao()
    {
        $arquivo_id = filter_input(INPUT_GET, 'arquivo_id', FILTER_SANITIZE_NUMBER_INT);
        $response = AutorizacaoModel::removerArquivoAutorizacao($arquivo_id);

        echo $response;

    }

    public static function salvarAutorizacao()
    {
        $data = $_POST;
        $files = $_FILES;
        $response = [];
        list($response['result'], $response['id']) = AutorizacaoModel::save($data, $files);

        echo json_encode($response);
    }

    public static function atualizarAutorizacao()
    {
        $data = $_POST;
        $files = $_FILES;
        $response = [];
        list($response['result'], $response['id']) = AutorizacaoModel::update($data, $files);

        echo json_encode($response);
    }

    public static function formListarAutorizacoesAuditoria()
    {
        $pacientes = Paciente::getPacientesByStatus();
        $urs = Filial::getAll();
        require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/autorizacao/listar-autorizacoes-auditoria-form.phtml');
    }

    public static function pesquisarOrcamentoRelatorioAuditoria()
    {
        if(isset($_POST) && !empty($_POST)){
            $pacienteId = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $urId = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_NUMBER_INT);
            $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_NUMBER_INT);
            $tipo = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_NUMBER_INT);
            $periodoInicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $periodoFim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $periodoInicio,
                'fim'           => $periodoFim,
                'tipo'          => $tipo,
                'status'        => $status,
                'paciente'      => $pacienteId,
                'ur'            => $urId,
            ];

            $pacientes = Paciente::getPacientesByStatus();
            $urs = Filial::getAll();
            $response = Fatura::getOrcamentosNaoAutorizados($filtros);
            $count = count($response);

            require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/autorizacao/listar-autorizacoes-auditoria-form.phtml');
        }
    }

    public static function formListarAutorizacoesUr()
    {
        $pacientes = Paciente::getPacientesByStatus();
        $urs = Filial::getAll();
        $planos = Planos::getAll();
        $tiposDocumentos = [
            'A' => 'Avaliação',
            'P' => 'Prorrogação',
        ];

        require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/autorizacao/listar-autorizacoes-ur-form.phtml');
    }

    public static function buscarAutorizacoesUr()
    {
        if(isset($_POST) && !empty($_POST)){
            $pacienteId = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_STRING);
            $planoId = filter_input(INPUT_POST, 'plano', FILTER_SANITIZE_STRING);
            $tipoDocumento = filter_input(INPUT_POST, 'tipo_documento', FILTER_SANITIZE_STRING);
            $inicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);
            $urId = filter_input(INPUT_POST, 'ur', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'plano'         => $planoId,
                'tipo_documento' => $tipoDocumento,
                'paciente'      => $pacienteId,
                'ur'            => $urId,
            ];

            $response = AutorizacaoModel::getAutorizacoesUr($filtros);
            $count = count($response);
            $pacientes = Paciente::getPacientesByStatus();
            $urs = Filial::getAll();
            $planos = Planos::getAll();
            $tiposDocumentos = [
                'A' => 'Avaliação',
                'P' => 'Prorrogação',
            ];

            require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/autorizacao/listar-autorizacoes-ur-form.phtml');
        }
    }

    public static function excelAutorizacoesUr()
    {
        if(isset($_GET) && !empty($_GET)){
            $pacienteId = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_STRING);
            $urId = filter_input(INPUT_GET, 'ur', FILTER_SANITIZE_STRING);
            $planoId = filter_input(INPUT_GET, 'plano', FILTER_SANITIZE_STRING);
            $tipoDocumento = filter_input(INPUT_GET, 'tipo_documento', FILTER_SANITIZE_STRING);
            $inicio = filter_input(INPUT_GET, 'inicio', FILTER_SANITIZE_STRING);
            $fim = filter_input(INPUT_GET, 'fim', FILTER_SANITIZE_STRING);

            $filtros = [
                'inicio'        => $inicio,
                'fim'           => $fim,
                'plano'         => $planoId,
                'tipo_documento' => $tipoDocumento,
                'paciente'      => $pacienteId,
                'ur'            => $urId,
            ];

            $response = AutorizacaoModel::getAutorizacoesUr($filtros);
            $extension = 'xls';
            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }
            $inicio = \DateTime::createFromFormat('Y-m-d', $inicio);
            $fim =  \DateTime::createFromFormat('Y-m-d', $fim);
            $filename = sprintf(
                "autorizacao_servicos_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/autorizacao/listar-autorizacoes-ur-excel.phtml');
        }
    }

    public static function salvarArquivosExtrasAutorizacao()
    {
        $data = $_POST;
        $files = $_FILES;

        $paciente = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
        $tipo = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_NUMBER_INT);
        $periodoInicio = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
        $periodoFim = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);

        $filtros = [
            'inicio'        => $periodoInicio,
            'fim'           => $periodoFim,
            'tipo'          => $tipo,
            'paciente'      => $paciente,
        ];

        AutorizacaoModel::saveExtraFiles($data, $files);

        $msg = "Arquivos enviados com sucesso!";
        $type = 's';

        $comentarios = [];
        $cabecalho = Paciente::getCabecalho($paciente);
        $response = Fatura::getOrcamentosNaoAutorizados($filtros);
        $orcamentos = array_map(function($value) {
            return $value['item_id'];
        }, $response);
        $comentariosOrc = ComentarioAutorizacao::getTotalComentariosByOrcamentoIds($orcamentos);
        foreach ($comentariosOrc as $comentario) {
            $comentarios[$comentario['fatura_orcamento_id']] = $comentario['total_comentarios'];
        }

        $count = count($response);
        require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/autorizacao/listar-orcamentos-form.phtml');
    }

    public static function jsonComentariosAutorizacao()
    {
        $response = [];
        $datas = [];
        $orcamento = filter_input(INPUT_GET, 'fatura_orcamento_id', FILTER_SANITIZE_NUMBER_INT);
        $autorizacao = filter_input(INPUT_GET, 'autorizacao', FILTER_SANITIZE_NUMBER_INT);
        $comentarios = ComentarioAutorizacao::getByFaturaOrcamentoAutorizacao($orcamento, $autorizacao);

        if(count($comentarios) > 0) {
            \Moment\Moment::setLocale('pt_BR');
            foreach ($comentarios as $comentario) {
                $dataHoraComentario = \DateTime::createFromFormat('Y-m-d H:i:s', $comentario['data_hora']);
                $dataHoraComentarioFormated = $dataHoraComentario->format('d/m/Y à\s H:i:s');
                $fromNow = (new \Moment\Moment($dataHoraComentario->format('U')))->fromNow()->getRelative();
                $response[$comentario['id']]['comentario_id'] = $comentario['id'];
                $response[$comentario['id']]['comentario'] = $comentario['comentario'];
                $response[$comentario['id']]['usuario_id'] = $comentario['usuario_id'];
                $response[$comentario['id']]['usuario'] = $comentario['usuario'];
                $response[$comentario['id']]['data_hora'] = $dataHoraComentarioFormated;
                $response[$comentario['id']]['from_now'] = ucfirst($fromNow);
                $datas[] = $comentario['data_hora'];
            }

            array_multisort($datas, SORT_DESC, $response);

            echo json_encode($response);
        } else {
            echo '0';
        }
    }

    public static function salvarComentariosAutorizacao()
    {
        $orcamento = filter_input(INPUT_POST, 'fatura_orcamento_id', FILTER_SANITIZE_NUMBER_INT);
        $autorizacao = filter_input(INPUT_POST, 'autorizacao', FILTER_SANITIZE_NUMBER_INT);
        $comentario = filter_input(INPUT_POST, 'comentario', FILTER_SANITIZE_STRING);
        $response = ComentarioAutorizacao::save($comentario, $orcamento, $autorizacao);

        echo $response;
    }

    public static function removerComentarioAutorizacao()
    {
        $comentario = filter_input(INPUT_GET, 'comentario', FILTER_SANITIZE_STRING);
        $response = ComentarioAutorizacao::delete($comentario);
        echo $response;
    }
}