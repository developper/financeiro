<?php

namespace app\Controllers;

use \App\Models\Administracao\Paciente as Paciente;
use \App\Models\Administracao\StatusPaciente as StatusPaciente;
use	\App\Models\Administracao\Operadora as Operadora;
use	\App\Models\Administracao\Planos as Planos;
use \App\Core\Log;
use	\App\Models\Administracao\Filial as Filial;
use App\Helpers\StringHelper as String;
use \App\Services\CacheAdapter;
use \App\Services\Cache\SimpleCache;

class Administracao
{
	#--------------------------------RELATORIOS--------------------------------
	public static function buscarModalidadePorPaciente()
	{
		$filtroCapMedica	= [];
		$filtroOutros			= [];
		$filtroHistorico	= [];
		$buscandoPor 			= [];

		//PACIENTE - CASO O FILTRO SEJA POR PACIENTE, O RESTO É DESCONSIDERADO
		if(isset($_POST['paciente']) && !empty($_POST['paciente'])){
			$paciente						= filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
			$filtroCapMedica[] 	= " AND paciente = {$paciente} ";
			$filtroOutros[] 		= " AND PACIENTE_ID = {$paciente} ";
			$filtroHistorico[] 	= " AND paciente = {$paciente} ";
			$fetch 							= Paciente::getById($paciente);
			$buscandoPor[] 			= " Paciente {$fetch['nome']} ";

			//PERIODO
			if ((isset($_POST['periodo_inicio']) && !empty($_POST['periodo_inicio'])) &&
				(isset($_POST['periodo_fim']) && !empty($_POST['periodo_fim']))) {
				$periodo_inicio 		= filter_input (INPUT_POST, 'periodo_inicio', FILTER_SANITIZE_NUMBER_INT);
				$periodo_fim 				= filter_input (INPUT_POST, 'periodo_fim', FILTER_SANITIZE_NUMBER_INT);
				$filtroCapMedica[]	= " AND `data` BETWEEN '{$periodo_inicio}' AND '{$periodo_fim}' ";
				$filtroOutros[] 		= " AND `DATA` BETWEEN '{$periodo_inicio}' AND '{$periodo_fim}' ";
				$filtroHistorico[]	= " AND `data_mudanca` BETWEEN '{$periodo_inicio}' AND '{$periodo_fim}' ";;
				$buscandoPor[] 			= " Periodo entre " .
					\DateTime::createFromFormat('Y-m-d', $periodo_inicio)->format('d/m/Y') .
					" e " .
					\DateTime::createFromFormat('Y-m-d', $periodo_fim)->format('d/m/Y') .
					" ";
			}
		}

		$response 	= Paciente::getPacientesByFilter($filtroCapMedica, $filtroOutros, $filtroHistorico);
		$pacientes 	= Paciente::getAll();

		require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/relatorio-modalidade-paciente-form.phtml');
	}

	public static function formBuscarModalidadePorPaciente()
	{
		$pacientes 	= Paciente::getAll();
		require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/relatorio-modalidade-paciente-form.phtml');
	}

	#--------------------------------OPERADORAS--------------------------------

	public static function atualizarOperadora()
	{
		if(isset($_POST) && !empty($_POST)){
			$data['nome']	= filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
			$data['email']	= filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
			$data['id']	= filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
			$data['data_pagamento']	= filter_input(INPUT_POST, 'data_pagamento', FILTER_SANITIZE_STRING);
			$data['dia_maximo_envio_fatura']	= filter_input(INPUT_POST, 'dia_maximo_envio_fatura', FILTER_SANITIZE_NUMBER_INT);
			$data['codigo'] = $_POST['codigo'];


			try {
				$response = Operadora::update ($data);
				$response = Operadora::updateCodigoEmpresaOperadora ($data['id'], $data['codigo']);
				$msg = 'Operadora atualizada com sucesso!';
				$type = 's';
			} catch(\Exception $e){
				$msg = $e->getMessage();
				$backtrace = [
					'text'      => $msg,
					'module'    => __NAMESPACE__,
					'file'      => __FILE__,
					'function'  => __FUNCTION__,
					'class'     => __CLASS__,
					'line'      => __LINE__
				];
				Log::registerErrorLog($backtrace);
				$type = 'f';
			}
			header ("Location: /adm/secmed/?action=operadoras&msg=" . base64_encode ($msg) . "&type=" . $type);
		}
	}

	public static function salvarOperadora()
	{
		if(isset($_POST) && !empty($_POST)){
			$allEmpresas = Filial::getAll();
			foreach ($allEmpresas as $all){
				if($all['ATIVO'] == 'S') {
					$empresas [$all['id']]['id'] = $all['id'];
					$empresas [$all['id']]['nome'] = utf8_decode($all['nome']);
				}
			}
			$data['nome']	= filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
			$data['email']	= filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
			$data['data_pagamento']	= filter_input(INPUT_POST, 'data_pagamento', FILTER_SANITIZE_STRING);
			$data['dia_maximo_envio_fatura']	= filter_input(INPUT_POST, 'dia_maximo_envio_fatura', FILTER_SANITIZE_NUMBER_INT);
			$data['codigo'] = $_POST['codigo'];

			try {
				$idOperadora = Operadora::save ($data);
				$response = Operadora::saveCodigoEmpresaOperadora ($idOperadora, $data['codigo']);
				$msg = 'Operadora criada com sucesso!';
				$type = 's';
			} catch(\Exception $e){
				$msg = $e->getMessage ();
				$backtrace = [
					'text'      => $msg,
					'module'    => __NAMESPACE__,
					'file'      => __FILE__,
					'function'  => __FUNCTION__,
					'class'     => __CLASS__,
					'line'      => __LINE__
				];
				Log::registerErrorLog($backtrace);
				$type = 's';
			}
			require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/operadoras-form.phtml');
		}
	}

	public static function listarOperadoras()
	{
		$response = Operadora::getAll();
		require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/operadoras-listar.phtml');
	}

	public static function buscarOperadoras()
	{
		return Operadora::getAll();
	}

	public static function novaOperadora()
	{
		$allEmpresas = Filial::getUnidadesAtivas();
		foreach ($allEmpresas as $all){
			if($all['ATIVO'] == 'S') {
				$empresas [$all['id']]['id'] = $all['id'];
				$empresas [$all['id']]['nome'] = utf8_decode($all['nome']);
			}
		}

		require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/operadoras-form.phtml');
	}

	public static function buscarPlanosOperadora()
	{
		$response = Operadora::getPlanosByOperadoraId($_GET['id']);
		echo json_encode($response);
	}

	public static function fillAll()
	{//ini_set("display_errors", 1);
		if(isset($_GET) && !empty($_GET)) {
			try{
				$codigoEmpresaOperadora = Operadora::getCodigoEmpresaOperadoraByOperadora($_GET['id']);
				foreach($codigoEmpresaOperadora as $cod){
					$codigo[$cod['empresas_id']]=$cod['codigo'];
				}
				$allEmpresas = Filial::getAll();
				foreach ($allEmpresas as $all){
				    $codigoNaEmpresa = empty($codigo[$all['id']]) ? '' : $codigo[$all['id']];
					if($all['ATIVO'] == 'S') {
						$empresas [$all['id']]['id'] = $all['id'];
						$empresas [$all['id']]['nome'] = utf8_decode($all['nome']);
						$empresas [$all['id']]['codigo'] = $codigoNaEmpresa;
					}
				}


				$response = Operadora::getByCodigo ($_GET['id']);
				require ($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/operadoras-form.phtml');
			} catch(\Exception $e) {
				header ("Location: /adm/secmed/?action=operadoras&msg=" . base64_encode ($e->getMessage ()) . "&type=f");
			}
		}
	}

	public static function excluirOperadora()
	{
		if (isset($_GET) && !empty($_GET)) {
			try {
				$response = Operadora::delete ($_GET['id']);
				$msg = 'Operadora excluída com sucesso!';
				$type = 'w';
				header ("Location: /adm/secmed/?action=operadoras&msg=" . base64_encode ($msg) . "&type=" . $type);
			} catch (\Exception $e) {
				$msg = $e->getMessage ();
				$backtrace = [
					'text' => $msg,
					'module' => __NAMESPACE__,
					'file' => __FILE__,
					'function' => __FUNCTION__,
					'class' => __CLASS__,
					'line' => __LINE__
				];
				Log::registerErrorLog ($backtrace);
				$type = 'f';
			}
			header ("Location: /adm/secmed/?action=operadoras&msg=" . base64_encode ($msg) . "&type=" . $type);
		}
	}

	public static function buscarLogoPeloIdPaciente($id)
	{
		$handler = Filial::getLogoByPacienteId($id);
		return $handler['logo'];
	}

	public static function buscarLogoEmpresa($id)
	{
		$handler = Filial::getLogoByEmpresaPrincipal();
		return $handler['logo'];
	}

	public static function buscarModalidadePaciente($paciente)
	{
		return Paciente::getModalidadePacienteById($paciente);
	}

	public static function buscarDataAdmissaoPaciente($paciente)
	{
		return Paciente::getDataAdmissaoPacienteById($paciente);
	}

	public static function buscarPacientesJSON()
	{
		$array['data'] = array_map(function ($paciente) {
			$paciente['nome'] = String::removeAcentosViaEReg($paciente['nome']);
			return $paciente;
		}, Paciente::getPacientesByUr());

		echo json_encode($array);
	}

	public static function buscarPacientesAdmJSON()
	{
        $itemsPerPage = filter_input(INPUT_GET, 'perPage', FILTER_SANITIZE_NUMBER_INT);
        $pageno = filter_input(INPUT_GET, 'pageno', FILTER_SANITIZE_NUMBER_INT);
        $nomeSearch = filter_input(INPUT_GET, 'nomeSearch', FILTER_SANITIZE_STRING);
        $status = filter_input(INPUT_GET, 'status', FILTER_SANITIZE_NUMBER_INT);
        $empresa = filter_input(INPUT_GET, 'empresa', FILTER_SANITIZE_NUMBER_INT);
        $liminar = filter_input(INPUT_GET, 'liminar', FILTER_SANITIZE_STRING);
        $convenio = filter_input(INPUT_GET, 'convenio', FILTER_SANITIZE_NUMBER_INT);
        $todos = filter_input(INPUT_GET, 'todos', FILTER_SANITIZE_STRING);

        $filters = [
            'itemsPerPage'  => $itemsPerPage,
            'pageno'        => $pageno,
            'nomeSearch'    => $nomeSearch,
            'status'        => $status,
            'empresa'       => $empresa,
            'liminar'       => $liminar,
            'convenio'      => $convenio,
            'todos'         => $todos,
        ];

        list($totalCount, $response) = Paciente::getPacientesPaginate($filters);

        $array['data'] = array_map(function ($paciente) {
            $paciente['nome'] = String::removeAcentosViaEReg($paciente['nome']);
            return $paciente;
        }, $response);
        $array['total_count'] = $totalCount;
        echo json_encode($array);
	}

	public static function buscarStatusPacienteJSON()
	{
		echo json_encode(StatusPaciente::getAll());
	}

	public static function buscarPlanosJSON()
	{
		echo json_encode(Planos::getAll());
	}

	public static function buscarEmpresasJSON()
	{
	    if($_SESSION['empresa_principal'] == 1) {
            echo json_encode(Filial::getAll());
        } else {
            echo json_encode(Filial::getEmpresaUser());
        }
	}

    public static function relatorioEpidemiologico()
    {
        $response                   = [];
        $pacientes                  = [];
        $pacientesCodNome           = [];
        $response['genero']         = [];
        $response['faixaetaria']    = [];
        $response['semNascimento']  = [];
        $response['modalidade']     = [];
        $response['semModalidade']  = [];
        $response['status']         = [];

        $inicio = (new \DateTime())->modify('first day of this month');
        $fim = new \DateTime();
        $inicio = isset($_POST['periodo_inicio']) && $_POST['periodo_inicio'] != '' ? \DateTime::createFromFormat('Y-m-d', $_POST['periodo_inicio']) : $inicio;
        $fim = isset($_POST['periodo_fim']) && $_POST['periodo_fim'] != '' ? \DateTime::createFromFormat('Y-m-d', $_POST['periodo_fim']) : $fim;

        $pacientesAtendidos = Paciente::getPacientesAtendidosByPeriodo($inicio, $fim);

        foreach ($pacientesAtendidos AS $paciente){
            $pacientes[] = $paciente['paciente'];
            $pacientesCodNome[$paciente['paciente']]['codigo'] = $paciente['codigo'];
            $pacientesCodNome[$paciente['paciente']]['nome'] = $paciente['nome'];
            $pacientesCodNome[$paciente['paciente']]['inicio'] = $paciente['inicio'];
        }

        $pacientesAvaliados = Paciente::getPacientesAvaliados($pacientes, $inicio, $fim);

        if(count($pacientesAvaliados) > 0){
            foreach ($pacientesAvaliados AS $paciente){
                if(!isset($pacientes[$paciente['paciente']])) {
                    $pacientes[] = $paciente['paciente'];
                    $pacientesCodNome[$paciente['paciente']]['codigo'] = $paciente['codigo'];
                    $pacientesCodNome[$paciente['paciente']]['nome'] = $paciente['nome'];
                    $pacientesCodNome[$paciente['paciente']]['inicio'] = $paciente['inicio'];
                }
            }
        }

        if(count($pacientes) > 0) {
            $generos = Paciente::getGeneroByPacientesAtendidos($pacientes);
            foreach ($generos as $genero) {
                $genero = $genero['sexo'] == '0' ? 'Masculino' : 'Feminino';
                if (!isset($response['genero'][$genero])) {
                    $response['genero'][$genero] = 1;
                } else {
                    $response['genero'][$genero]++;
                }
            }

            $faixas = Paciente::getFaixaEtariaByPacientesAtendidos($pacientes);

            $response['faixaetaria']['00-10'] = 0;
            $response['faixaetaria']['11-20'] = 0;
            $response['faixaetaria']['21-30'] = 0;
            $response['faixaetaria']['31-40'] = 0;
            $response['faixaetaria']['41-50'] = 0;
            $response['faixaetaria']['51-60'] = 0;
            $response['faixaetaria']['61-70'] = 0;
            $response['faixaetaria']['71-80'] = 0;
            $response['faixaetaria']['81-90'] = 0;
            $response['faixaetaria']['91-100'] = 0;
            $response['faixaetaria']['101+'] = 0;

            foreach ($faixas as $faixa) {
                if ($faixa['nascimento'] != '0000-00-00') {
                    $inicioFatura = \DateTime::createFromFormat('Y-m-d', $pacientesCodNome[$faixa['paciente']]['inicio']);
                    $nascimento = \DateTime::createFromFormat('Y-m-d', $faixa['nascimento']);
                    $diff = $nascimento->diff($inicioFatura)->days;
                    $idade = floor($diff / 365.25);
                    $response['faixaetaria']['00-10'] = $idade >= 0 && $idade <= 10 ? $response['faixaetaria']['00-10'] + 1 : $response['faixaetaria']['00-10'];
                    $response['faixaetaria']['11-20'] = $idade >= 11 && $idade <= 20 ? $response['faixaetaria']['11-20'] + 1 : $response['faixaetaria']['11-20'];
                    $response['faixaetaria']['21-30'] = $idade >= 21 && $idade <= 30 ? $response['faixaetaria']['21-30'] + 1 : $response['faixaetaria']['21-30'];
                    $response['faixaetaria']['31-40'] = $idade >= 31 && $idade <= 40 ? $response['faixaetaria']['31-40'] + 1 : $response['faixaetaria']['31-40'];
                    $response['faixaetaria']['41-50'] = $idade >= 41 && $idade <= 50 ? $response['faixaetaria']['41-50'] + 1 : $response['faixaetaria']['41-50'];
                    $response['faixaetaria']['51-60'] = $idade >= 51 && $idade <= 60 ? $response['faixaetaria']['51-60'] + 1 : $response['faixaetaria']['51-60'];
                    $response['faixaetaria']['61-70'] = $idade >= 61 && $idade <= 70 ? $response['faixaetaria']['61-70'] + 1 : $response['faixaetaria']['61-70'];
                    $response['faixaetaria']['71-80'] = $idade >= 71 && $idade <= 80 ? $response['faixaetaria']['71-80'] + 1 : $response['faixaetaria']['71-80'];
                    $response['faixaetaria']['81-90'] = $idade >= 81 && $idade <= 90 ? $response['faixaetaria']['81-90'] + 1 : $response['faixaetaria']['81-90'];
                    $response['faixaetaria']['91-100'] = $idade >= 91 && $idade <= 100 ? $response['faixaetaria']['91-100'] + 1 : $response['faixaetaria']['91-100'];
                    $response['faixaetaria']['101+'] = $idade > 100 ? $response['faixaetaria']['101+'] + 1 : $response['faixaetaria']['101+'];
                } else {
                    $response['semNascimento'][$faixa['codigo']] = $faixa['nome'];
                }
            }

            $modAux = [];
            $modalidades = Paciente::getModalidadeByPacientesAtendidos($inicio, $fim, $pacientes);
            $cmp = function ($a, $b)
            {
                if ($a == $b) {
                    return 0;
                }
                return ($a < $b) ? -1 : 1;
            };

            foreach ($modalidades as $modalidade) {
                $modAux[] = $modalidade['paciente'];
                $modalidade = utf8_decode($modalidade['modalidade']);
                if (!isset($response['modalidade'][$modalidade])) {
                    $response['modalidade'][$modalidade] = 1;
                } else {
                    $response['modalidade'][$modalidade]++;
                }
            }

            usort($modAux, $cmp);
            $modAux = array_diff($pacientes, $modAux);

            foreach ($modAux as $md) {
                $response['semModalidade'][$pacientesCodNome[$md]['codigo']] = $pacientesCodNome[$md]['nome'];
            }

            $hsAux = [];
            $historicoStatus = Paciente::getHistoricoStatusByPacientesAtendidos($inicio, $fim, $pacientes);
            foreach ($historicoStatus as $hs) {
                $hsAux[$hs['idClientes']] = $hs['status'];
            }

            $stAux = [];
            $status = Paciente::getStatusByPacientesAtendidos($pacientes);
            foreach ($status as $st) {
                $stAux[$st['idClientes']] = $st['status'];
            }

            foreach ($stAux as $paciente => $st) {
                if(!array_key_exists($paciente, $hsAux)) {
                    $st = utf8_decode($st);
                } else {
                    $st = utf8_decode($hsAux[$paciente]);
                }

                if (!isset($response['status'][$st])) {
                    $response['status'][$st] = 1;
                } else {
                    $response['status'][$st]++;
                }
            }
        }

        require($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/relatorio-epidemiologico-form.phtml');
    }

    public static function excelRelatorioEpidemiologico()
    {
        if(isset($_POST) && !empty($_POST)){
            $inicio = \DateTime::createFromFormat('Y-m-d', $_POST['periodo_inicio']);
            $fim = \DateTime::createFromFormat('Y-m-d', $_POST['periodo_fim']);
            $response = $_POST['response'];

            $extension = 'xls';

            if(strstr($_SERVER['HTTP_USER_AGENT'], 'Linux')) {
                $extension = 'xlsx';
            }

            $filename = sprintf(
                "relatorio_epidemiologico_%s_%s.%s",
                $inicio->format('d-m-Y'),
                $fim->format('d-m-Y'),
                $extension
            );

            header("Content-type: application/x-msexce; charset=ISO-8859-1");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Pragma: no-cache");

            require($_SERVER['DOCUMENT_ROOT'] . '/adm/secmed/templates/relatorio-epidemiologico-excel.phtml');
        }
    }

    public function consultarCPFPaciente()
    {
        $cpf = filter_input(INPUT_GET, 'cpf', FILTER_SANITIZE_STRING);
        $cpfFiltrado = strpos($cpf, '.') !== false ?
            str_replace('.', '', str_replace('-', '', $cpf)) :
            $cpf;
        $response = Paciente::getPacienteByCPF($cpf, $cpfFiltrado);
        echo $response['cpf'];
    }

    public static function dadosPaciente()
    {
        if(isset($_GET) && !empty($_GET)){
            $idPaciente = filter_input(INPUT_GET, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $paciente = Paciente::getById($idPaciente);
            echo json_encode($paciente);
        }
    }
}
