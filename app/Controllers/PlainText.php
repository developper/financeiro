<?php
/**
 * Created by PhpStorm.
 * User: ricardotassio
 * Date: 08/05/15
 * Time: 10:02
 */

namespace App\Controllers;


trait PlainText
{
    function utf8_strtr($str) {
        $from = utf8_decode("áàãâéêíóôõúçÁÀÃÂÉÊÍÓÔÕÚÙÇ");
        $to = "aaaaeeiooouucAAAAEEIOOOUUC";
        $str = str_replace(' ', '', $str);
        return strtr($str, $from, $to);
    }
}