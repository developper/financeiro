<?php

namespace App\Controllers;

use App\Models\Administracao\Filial;
use App\Models\Administracao\Paciente;
use App\Models\Administracao\Usuario;
use App\Models\Ocorrencia\OcorrenciaClassificacao;
use App\Models\Ocorrencia\Ocorrencia as OcorrenciaModel;
use App\Models\Ocorrencia\OcorrenciaResposta;
use App\Models\Ocorrencia\OcorrenciaSubclassificacao;
use App\Models\Sistema\Setores;

class Ocorrencia
{
    private static function getLists()
    {
        return [
            'simNao' => [
                '0' => 'Não',
                '1' => 'Sim'
            ],
            'demandatario' => [
                '1' => 'Familia/Cuidador',
                '2' => 'Profissional Interno',
                '3' => 'Profissional Prestador',
                '4' => 'Operadora/Contratante',
            ],
            'tipo_contato' => [
                '1' => 'Ocorrência',
                '2' => 'Chamada pró-ativa',
                '3' => 'Chamada recebida',
            ],
            'encaminhamento' => [
                '1' => 'Interno',
                '2' => 'Externo',
            ],

        ];
    }

    public static function form()
    {
        $classificacoes = OcorrenciaClassificacao::getAll();
        $pacientes = Paciente::getPacientesByUr();
        $urs = Filial::getUnidadesAtivas();
        $setores = Setores::getAll();
        $setoresUsuario = Usuario::getSetoresUsuario($_SESSION['id_user']);
        $ursUsuario = Filial::getUnidadesUsuario($_SESSION["empresa_user"]);

        require ($_SERVER['DOCUMENT_ROOT'] . '/ocorrencia/templates/form.phtml');
    }

    public static function salvarOcorrencia()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['demandatario'] = filter_input(INPUT_POST, 'demandatario', FILTER_SANITIZE_NUMBER_INT);
            $data['contato'] = filter_input(INPUT_POST, 'contato', FILTER_SANITIZE_STRING);
            $data['telefone_contato'] = filter_input(INPUT_POST, 'telefone_contato', FILTER_SANITIZE_STRING);
            $data['prioridade'] = filter_input(INPUT_POST, 'prioridade', FILTER_SANITIZE_STRING);
            $data['ocorrencia_paciente'] = filter_input(INPUT_POST, 'ocorrencia_paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['paciente'] = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['data_ocorrencia'] = filter_input(INPUT_POST, 'data_ocorrencia', FILTER_SANITIZE_STRING);
            $data['hora_ocorrencia'] = filter_input(INPUT_POST, 'hora_ocorrencia', FILTER_SANITIZE_STRING);
            $data['tipo_contato'] = filter_input(INPUT_POST, 'tipo_contato', FILTER_SANITIZE_NUMBER_INT);
            $data['descricao'] = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);
            $data['classificacao'] = filter_input(INPUT_POST, 'classificacao', FILTER_SANITIZE_NUMBER_INT);
            $data['subclassificacao'] = filter_input(INPUT_POST, 'subclassificacao', FILTER_SANITIZE_NUMBER_INT);
            $data['ur'] = isset($_POST['ur']) ? $_POST['ur'] : [];
            $data['encaminhamento'] = filter_input(INPUT_POST, 'encaminhamento', FILTER_SANITIZE_NUMBER_INT);
            $data['nome_encaminhamento'] = filter_input(INPUT_POST, 'nome_encaminhamento', FILTER_SANITIZE_STRING);
            $data['email_encaminhamento'] = isset($_POST['email_encaminhamento']) ? $_POST['email_encaminhamento'] : [];
            $data['setor'] = isset($_POST['setor']) ? $_POST['setor'] : [];
            $data['setor_origem'] = $_POST['setor_origem'];
            $data['ur_origem'] = $_POST['ur_origem'];
            $data['arquivos'] = $_FILES['arquivos'];

            list($response, $idOcorrencia, $falhaEmails) = OcorrenciaModel::save($data);

            $classificacoes = OcorrenciaClassificacao::getAll();
            $pacientes = Paciente::getPacientesByUr();
            $urs = Filial::getUnidadesAtivas();
            $setores = Setores::getAll();
            $setoresUsuario = Usuario::getSetoresUsuario($_SESSION['id_user']);
            $ursUsuario = Filial::getUnidadesUsuario($_SESSION["empresa_user"]);

            $msg = 'Houve um erro ao registrar a ocorrência! ';
            $type = 'f';

            if($response == true) {
                $msg = "Ocorrência #{$idOcorrencia} registrada com sucesso! ";
                $type = 's';
                if(!empty($falhaEmails)) {
                    $msg .= "<br> Falha ao enviar uma notificação para os emails: " . implode(', ', $falhaEmails);
                }
            }

            require ($_SERVER['DOCUMENT_ROOT'] . '/ocorrencia/templates/form.phtml');
        }
    }

    public static function listarForm()
    {
        $ur[] = $_SESSION['empresa_principal'];

        $data = [
            'status' => 'Em Aberto',
            'inicio' => '',
            'fim' => '',
            'ur' => $ur
        ];
        $ocorrencias = OcorrenciaModel::getByFilter($data);
        $lists = self::getLists();
        $classificacoes = OcorrenciaClassificacao::getAll();
        $pacientes = Paciente::getPacientesByUr();
        $usuarios = Usuario::getAll();
        $urs = Filial::getUnidadesAtivas();
        $setores = Setores::getAll();

        require ($_SERVER['DOCUMENT_ROOT'] . '/ocorrencia/templates/listar-form.phtml');
    }

    public static function listarOcorrencias()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['paciente'] = filter_input(INPUT_POST, 'paciente', FILTER_SANITIZE_NUMBER_INT);
            $data['solicitado_por'] = filter_input(INPUT_POST, 'solicitado_por', FILTER_SANITIZE_NUMBER_INT);
            $data['status'] = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);
            $data['ur'] = isset($_POST['ur']) ? $_POST['ur'] : [];
            $data['setor'] = isset($_POST['setor']) ? $_POST['setor'] : [];
            $data['classificacao'] = filter_input(INPUT_POST, 'classificacao', FILTER_SANITIZE_STRING);
            $data['subclassificacao'] = filter_input(INPUT_POST, 'subclassificacao', FILTER_SANITIZE_STRING);
            $data['tipo_contato'] = filter_input(INPUT_POST, 'tipo_contato', FILTER_SANITIZE_NUMBER_INT);
            $data['prioridade'] = filter_input(INPUT_POST, 'prioridade', FILTER_SANITIZE_STRING);
            $data['inicio'] = filter_input(INPUT_POST, 'inicio', FILTER_SANITIZE_STRING);
            $data['fim'] = filter_input(INPUT_POST, 'fim', FILTER_SANITIZE_STRING);


            $ocorrencias = OcorrenciaModel::getByFilter($data);
            $lists = self::getLists();
            $classificacoes = OcorrenciaClassificacao::getAll();
            $pacientes = Paciente::getPacientesByUr();
            $usuarios = Usuario::getAll();
            $urs = Filial::getUnidadesAtivas();
            $setores = Setores::getAll();

            require ($_SERVER['DOCUMENT_ROOT'] . '/ocorrencia/templates/listar-form.phtml');
        }
    }

    public static function enviarRespostaOcorrencia()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['ocorrencia'] = filter_input(INPUT_POST, 'ocorrencia', FILTER_SANITIZE_NUMBER_INT);
            $data['resposta'] = filter_input(INPUT_POST, 'resposta', FILTER_SANITIZE_STRING);
            $data['sugerir_resolvido'] = filter_input(INPUT_POST, 'sugerir_resolvido', FILTER_SANITIZE_STRING);
            $data['arquivos'] = array_values($_FILES);

            $respostaId = OcorrenciaResposta::save($data);

            $arquivos = OcorrenciaResposta::getArquivosOcorrenciaResposta($respostaId);
            $arquivosList = '<ul>';
            $arquivosReduce = array_reduce($arquivos, function ($buffer, $value) {
                $nome = explode('/', $value['full_path']);
                $nome = end($nome);
                $fullPath = explode('storage', $value['full_path']);
                $fullPath = '/storage' . $fullPath[1];
                return $buffer .= "<li><a href='{$fullPath}' target='_blank'>{$nome}</a> </li>";
            });
            $arquivosList .= $arquivosReduce . '</ul>';

            if(is_int($respostaId)) {
                $response = [
                    'id' => $respostaId,
                    'usuario' => $_SESSION['nome_user'],
                    'arquivos' => $arquivosList
                ];
                echo json_encode($response);
            } else {
                echo 0;
            }
        }
    }

    public static function resolverOcorrencia()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['ocorrencia_id'] = filter_input(INPUT_POST, 'ocorrencia', FILTER_SANITIZE_NUMBER_INT);
            $data['retorno_hc'] = filter_input(INPUT_POST, 'retorno_hc', FILTER_SANITIZE_STRING);

            $data['descricao_qualidade_retorno'] = filter_input(INPUT_POST, 'descricao_qualidade_retorno', FILTER_SANITIZE_STRING);

            $response = OcorrenciaModel::resolve($data);

            echo $response;
        }
    }

    public static function desativarOcorrencia()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['ocorrencia_id'] = filter_input(INPUT_POST, 'ocorrencia', FILTER_SANITIZE_NUMBER_INT);

            $response = OcorrenciaModel::deactivate($data);

            echo $response;
        }
    }

    public static function reabrirOcorrencia()
    {
        if(isset($_POST) && !empty($_POST)) {
            $data = [];
            $data['ocorrencia_id'] = filter_input(INPUT_POST, 'ocorrencia', FILTER_SANITIZE_NUMBER_INT);

            $response = OcorrenciaModel::reabrir($data);

            echo $response;
        }
    }

    public static function jsonOcorrenciaSubclassificacoes()
    {
        if(isset($_GET) && !empty($_GET)) {
            $classificacao_id = filter_input(INPUT_GET, 'classificacao_id', FILTER_SANITIZE_NUMBER_INT);

            $response = OcorrenciaSubclassificacao::getByClassificacaoId($classificacao_id);

            echo json_encode($response);
        }
    }
}