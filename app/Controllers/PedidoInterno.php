<?php
/**
 * Created by PhpStorm.
 * User: jeferson
 * Date: 02/10/2017
 * Time: 11:28
 */

namespace app\Controllers;


use App\Models\Administracao\Filial;
use App\Models\Logistica\PedidosInternos as ModelPedidoInterno;
use app\Models\Logistica\PedidosInternos;

class PedidoInterno
{
    public static function buscarPedidosEnviadosENaoConfirmados()
    {


		$pedidos = '';
		if(isset($_POST) && !empty($_POST)) {
			$empresaId = filter_input(INPUT_POST, 'empresa', FILTER_SANITIZE_STRING);
			$pedidos =  ModelPedidoInterno::getPedidosEnviadosENaoConfirmadosPorUr($empresaId);
		}
		$empresas = Filial::getUnidadesAtivas();



		require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PedidosInternos/templates/buscarPedidosEnviadosENaoConfirmados.phtml');

    }

	public static function formCancelarItensNaoConfirmados()
	{
		if(isset($_GET) && !empty($_GET)) {

			$pedidoId = filter_input(INPUT_GET, 'pedido_id', FILTER_SANITIZE_STRING);
			$sqlMedicamento = ModelPedidoInterno::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($pedidoId, 0);
			$sqlMaterial = ModelPedidoInterno::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($pedidoId, 1);
			$sqlDieta = ModelPedidoInterno::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($pedidoId, 3);
			$sqlEquipamento = ModelPedidoInterno::getEquipamentoEnviadoNaoConfimandoPorPedido($pedidoId);

			$itensPedidos[] =  ModelPedidoInterno::getBySql($sqlMedicamento);
			$itensPedidos[] =  ModelPedidoInterno::getBySql($sqlMaterial);
			$itensPedidos[] =  ModelPedidoInterno::getBySql($sqlDieta);
			$itensPedidos[] = 	ModelPedidoInterno::getBySql($sqlEquipamento);
			$cont = 0;
			$auxiliar = [];
			foreach($itensPedidos as $itens){
				$auxiliar = array_merge($auxiliar,$itens);
			}

			foreach($auxiliar as  $aux){

				if($aux['TIPO'] != 5){
					$itensSaida[$aux['ID']] = ModelPedidoInterno::getBySql(PedidosInternos::getItensMatMedDiePedidosInternosSaida($aux['ID']));
				}else{
					$itensSaida[$aux['ID']] = ModelPedidoInterno::getBySql(PedidosInternos::getItensEquipamentosPedidosInternosSaida($aux['ID']));
				}

			}

			$itensPedidos = $auxiliar;
			require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PedidosInternos/templates/formCancelarItemEnviado.phtml');

		}
	}

	public static function cancelarItensNaoConfirmados()
	{
session_start();

if( $_SERVER['REQUEST_METHOD']=='POST' )
{
	$request = md5( implode( $_POST ) );

	if( isset( $_SESSION['last_request'] ) && $_SESSION['last_request']== $request )
	{
		$pedidoId = $_POST['pedido-id'];
		$sqlMedicamento = ModelPedidoInterno::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($pedidoId, 0);
		$sqlMaterial = ModelPedidoInterno::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($pedidoId, 1);
		$sqlDieta = ModelPedidoInterno::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($pedidoId, 3);
		$sqlEquipamento = ModelPedidoInterno::getEquipamentoEnviadoNaoConfimandoPorPedido($pedidoId);

		$itensPedidos[] = ModelPedidoInterno::getBySql($sqlMedicamento);
		$itensPedidos[] = ModelPedidoInterno::getBySql($sqlMaterial);
		$itensPedidos[] = ModelPedidoInterno::getBySql($sqlDieta);
		$itensPedidos[] = ModelPedidoInterno::getBySql($sqlEquipamento);
		$cont = 0;
		$auxiliar = [];
		foreach ($itensPedidos as $itens) {
			$auxiliar = array_merge($auxiliar, $itens);
		}

		foreach ($auxiliar as $aux) {

			if ($aux['TIPO'] != 5) {
				$itensSaida[$aux['ID']] = ModelPedidoInterno::getBySql(PedidosInternos::getItensMatMedDiePedidosInternosSaida($aux['ID']));
			} else {
				$itensSaida[$aux['ID']] = ModelPedidoInterno::getBySql(PedidosInternos::getItensEquipamentosPedidosInternosSaida($aux['ID']));
			}

		}
		$itensPedidos = $auxiliar;


		require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PedidosInternos/templates/formCancelarItemEnviado.phtml');

		return;
	}
	else
	{
		$_SESSION['last_request']  = $request;
		$var = 'post';
	}
}

		$arrayItens = '';

			if (isset($_POST['item']) && !empty($_POST['item'])) {

				foreach ($_POST['item'] as $post) {
					$pedidoId = $post['pedido-id'];
					if ($post['cancelar'] > 0) {
						$arrayItens[] = $post;
					}
				}

						$retorno = ModelPedidoInterno::cancelarItemEnviadoNaoConfirmado($arrayItens);
						$msg = $retorno;
						$classInfoMsg = 'danger';
						if ($retorno == 1) {
							$msg = "Itens cancelados com sucesso.";
							$classInfoMsg = 'success';

					$sqlMedicamento = ModelPedidoInterno::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($pedidoId, 0);
					$sqlMaterial = ModelPedidoInterno::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($pedidoId, 1);
					$sqlDieta = ModelPedidoInterno::getMatMedDieEnviadoNaoConfimandoPorPedidoETipo($pedidoId, 3);
					$sqlEquipamento = ModelPedidoInterno::getEquipamentoEnviadoNaoConfimandoPorPedido($pedidoId);

					$itensPedidos[] = ModelPedidoInterno::getBySql($sqlMedicamento);
					$itensPedidos[] = ModelPedidoInterno::getBySql($sqlMaterial);
					$itensPedidos[] = ModelPedidoInterno::getBySql($sqlDieta);
					$itensPedidos[] = ModelPedidoInterno::getBySql($sqlEquipamento);
					$cont = 0;
					$auxiliar = [];
					foreach ($itensPedidos as $itens) {
						$auxiliar = array_merge($auxiliar, $itens);
					}

					foreach ($auxiliar as $aux) {

						if ($aux['TIPO'] != 5) {
							$itensSaida[$aux['ID']] = ModelPedidoInterno::getBySql(PedidosInternos::getItensMatMedDiePedidosInternosSaida($aux['ID']));
						} else {
							$itensSaida[$aux['ID']] = ModelPedidoInterno::getBySql(PedidosInternos::getItensEquipamentosPedidosInternosSaida($aux['ID']));
						}

					}
					$itensPedidos = $auxiliar;


					require($_SERVER['DOCUMENT_ROOT'] . '/logistica/PedidosInternos/templates/formCancelarItemEnviado.phtml');


				}
			}

	}



}