<?php

namespace App\Integration\Obsequium;

use \GuzzleHttp\Client as HttpClient;

class PacientesAPI
{

  const URL_PACIENTES_GET   = '/api/users/%s/pacientes';
  const URL_PACIENTES_POST  = '/api/users/%s/pacientes';
  const URL_PACIENTES_PATCH = '/api/users/%s/pacientes';

  protected $client;
  protected $auth;

  public function __construct($config)
  {
    $this->client = new HttpClient(['base_url' => $config->host]);
    $this->auth = [$config->user, $config->pass];
  }

  public function getAll($user)
  {
    $route = sprintf(self::URL_PACIENTES_GET, $user);
    $res = $this->client->get($route, ['auth' =>  $this->auth]);
    return $res->json();
  }

  public function create($user, array $pacientes)
  {
    $route = sprintf(self::URL_PACIENTES_POST, $user);
    $res = $this->client->post($route, ['json' => $pacientes, 'auth' =>  $this->auth]);
    return $res->getStatusCode() == 201;
  }

  public function update($user, array $pacientes)
  {
    $route = sprintf(self::URL_PACIENTES_PATCH, $user);
    $res = $this->client->patch($route, ['json' => $pacientes, 'auth' =>  $this->auth]);
    return $res->getStatusCode() == 204;
  }

}