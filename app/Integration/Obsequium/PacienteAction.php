<?php

namespace App\Integration\Obsequium;

use App\Core\Config;
use App\Services\Queue;

class PacienteAction
{

  const QUEUE_NAME = 'obsequium.pacientes';
  const ACTION_CREATE = 'CREATE';
  const ACTION_UPDATE = 'UPDATE';

  protected $action, $data, $config;

  public function __construct()
  {
    $this->config = (object) Config::get('obsequium_api');
    $this->api = new PacientesAPI($this->config);
  }

  public function created($id, $nome, $sexo)
  {
    $data = $this->getData(self::ACTION_CREATE, $id, $nome, $sexo);

    $queue = new Queue;
    $queue->push(json_encode($data), self::QUEUE_NAME);
  }

  public function updated($id, $nome, $sexo)
  {
    $data = $this->getData(self::ACTION_UPDATE, $id, $nome, $sexo);

    $queue = new Queue;
    $queue->push(json_encode($data), self::QUEUE_NAME);
  }

  protected function getData($action, $id, $nome, $sexo)
  {
    $paciente = ['nome' => $nome, 'sexo' => $this->sexoFromInt($sexo), 'external_ref' => $id];
    return ['action' => $action, 'user' => $this->config->userId, 'paciente' => $paciente];
  }

  protected function sexoFromInt($int)
  {
    $sexos = ['M', 'F'];
    return $sexos[$int];
  }



}