<?php

namespace App\Integration\GoogleCloud;

class Storage 
{

  protected $privatekey;
  protected $client_email;
  protected $scopes;
  protected $gclient;
  protected $gstorage;

  public function __construct($privatekey, $client_email, $scopes)
  {
    $this->privatekey = $privatekey;
    $this->client_email = $client_email;
    $this->scopes = $scopes;

    $this->login();
  }

  protected function login()
  {
    $credentials = new \Google_Auth_AssertionCredentials(
      $this->client_email,
      $this->scopes,
      $this->privatekey
    );

    $this->gclient = new \Google_Client();
    $this->gclient->setAssertionCredentials($credentials);
    if ($this->gclient->getAuth()->isAccessTokenExpired()) {
      $this->gclient->getAuth()->refreshTokenWithAssertion();
    }

    $this->gstorage = new \Google_Service_Storage($this->gclient);
  }

  public function getObject($bucket, $filename)
  {
    return $this->gstorage->objects->get($bucket, $filename);
  }

  public function getAccessToken()
  {
    $accessToken = json_decode($this->gclient->getAccessToken());
    return $accessToken->access_token;
  }

  public function getClient()
  {
    return $this->gclient;
  }

  public function getPublicUrl($bucketName, $filename)
  {
    $object = $this->getObject($bucketName, $filename);
    $token  = $this->getAccessToken();

    return $object->mediaLink . '&' . http_build_query(['access_token' => $token]);
  }

  public function sendFile($bucket, $sourceDir, $targetDir, $params = array())
  {
    $obj = new \Google_Service_Storage_StorageObject();
    $obj->setName($targetDir);
    $paramsDefault = ['name' => $targetDir, 'data' => file_get_contents($sourceDir), 'uploadType' => 'media'];
    return $this->gstorage->objects->insert($bucket, $obj, array_merge($paramsDefault, $params));
  }

}