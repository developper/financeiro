<?php

namespace App\Integration\GoogleCloud;

use App\Core\Config;
use App\Integration\GoogleCloud\Storage as GStorage;

class StorageFactory 
{

  private function __construct() {}

  public static function getDefault()
  {
    $private_key  = file_get_contents(Config::get('GAE_PRIVATE_KEY'));
    $bucketName   = Config::get('GS_BUCKET_NAME');
    $client_email = Config::get('GS_CLIENT_EMAIL');
    $scopes       = Config::get('GS_SCOPES');

    return new GStorage($private_key, $client_email, $scopes);
  }

}