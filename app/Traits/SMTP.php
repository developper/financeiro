<?php

namespace app\Traits;

include_once("../../utils/class.phpmailer.php");

use App\Core\Config;

trait SMTP
{
	public function getInfos($paciente, $origem, $empresa)
	{
		$sql = "SELECT clientes.nome FROM clientes WHERE clientes.idClientes = {$paciente}";
		$nome_Paciente = mysql_result(mysql_query($sql), 0);

		$configEmail = include $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';

		$options = array();

		$lista = $configEmail['lista'];
		$tipo = $origem < 6 ? 'enfermagem' : 'medico';

		if (isset($lista['emails'][$tipo]) && isset($lista['emails'][$tipo][$empresa])) {
			$options['to'][] = $lista['emails'][$tipo][$empresa];
		}


		if (isset($lista['diretoria'][$tipo])) {
			$options['to'][] = $lista['diretoria'][$tipo];
		}


		$profissional = utf8_decode($_SESSION["nome_user"]);
		$msgHTML      = $configEmail['msgHTML'];
		$origem       = utf8_decode($lista['origem'][$origem]);
		$nomePaciente = utf8_decode($nome_Paciente);

		$options['msgHTML']    = sprintf($msgHTML, $origem, $nomePaciente, $profissional);

		if (isset($configEmail['addAddress'])) {
			foreach($configEmail['addAddress'] as $address) {
				$options['to'][] = $address;
			}
		}

		$options['from']       = $configEmail['from'];
		$options['subject']    = 'Avisos do Sistema';
		$options['altBody']    = $configEmail['altBody'];

		$servicoEmail = $this->sendMail($options);
		$servicoHipChat = $this->sendNotificationHipChat($options['msgHTML']);

		if($servicoEmail || $servicoHipChat){
			return true;
		}else{
			echo 'Alguns serviços estão indisponíveis. Seu relatório foi salvo com sucesso, mas nenhum email ou mensagem foi enviado. ',
			'Avise ao SECMED sobre o relatório e também comunique o TI sobre a indisponibilidade! Agradecemos pela compreensão!';
		}
	}

	private function sendMail($options)
	{
		# Verifica o status do serviço no banco de dados
		$status = checkServiceStatus('email')['status'];

		$configEmail = include $_SERVER['DOCUMENT_ROOT'] .'/app/configs/email.php';

		$mail = new \PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->SMTPAuth = true;                  // enable SMTP authentication
		$mail->Host = $configEmail['smtp']['host'];      // sets GMAIL as the SMTP server
		$mail->Port = $configEmail['smtp']['port'];                   // set the SMTP port for the GMAIL server
		$mail->Username = $configEmail['smtp']['username'];  // GMAIL username
		$mail->Password = $configEmail['smtp']['password'];            // GMAIL password
		if (isset($options['to'])) {
			foreach($options['to'] as $to) {
				$mail->AddAddress($to['email'], utf8_decode($to['nome']));
			}
		}

		$mail->SetFrom($options['from']['email'], $options['from']['nome']);
		$mail->Subject = $options['subject'];
		$mail->AltBody = $options['altBody'];
		$mail->MsgHTML($options['msgHTML']);

		try {
			$send = $mail->Send();
			if($send){
				if($status === 'off'){
					$sql = "UPDATE services SET `status` = 'on' WHERE name = 'email'";
					$rs  = mysql_query($sql);
				}
			}
			return $send;

		} catch (Exception $ex) {
			$mail->SMTPAuth = true;                  // enable SMTP authentication
			$mail->Host = $configEmail['smtp-gmail']['host'];      // sets GMAIL as the SMTP server
			$mail->Port = $configEmail['smtp-gmail']['port'];                   // set the SMTP port for the GMAIL server
			$mail->Username = $configEmail['smtp-gmail']['username'];  // GMAIL username
			$mail->Password = $configEmail['smtp-gmail']['password'];            // GMAIL password

			try {
				$send = $mail->Send ();
				if ($send) {
					if ($status === 'off') {
						$sql = "UPDATE services SET `status` = 'on' WHERE name = 'email'";
						$rs = mysql_query ($sql);
					}
				}
				return $send;

			} catch (Exception $e) {
				if ($status === 'on' &&
					($ex->getCode () == 0 && $e->getCode () == 0)
				) {
					if (APP_ENV === 'prd') {
						sendNotificationHipChat (sprintf ('Erro Under: %s | Erro Gmail: %s', $ex->getMessage (), $e->getMessage ()), 'warning');
					} else {
						echo sprintf ('Erro Under: %s | Erro Gmail: %s', $ex->getMessage (), $e->getMessage ());
					}

				}
				$sql = "UPDATE services SET `status` = 'off' WHERE name = 'email'";
				$rs = mysql_query ($sql);
				return false;
			}
		}
	}

	private function sendNotificationHipChat($options)
	{
		$token = Config::get('TOKEN_HIPCHAT');
		$env = Config::get('APP_ENV');
		$hip = new \App\Controllers\HipChatSismederi($token, $env);
		$mensagem = $msg;
		$roomId = $env == 'prd' ? '1179954' : '469564';
		return $hip->sendNotificationRoom($roomId, $mensagem, array('color'=>'yellow','notify'=>'true'));
	}

	private function checkServiceStatus($service)
	{
		$sql = <<<SQL
SELECT
	`status`
FROM
	services
WHERE
	name = '{$service}'
SQL;
		$rs = mysql_query($sql) or die(mysql_error());
		return mysql_fetch_array($rs, MYSQL_ASSOC);

	}
}