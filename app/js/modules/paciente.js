(function(){
    'use strict';

    angular
        .module('paciente', ['ui.bootstrap', 'LocalStorageModule', 'statusPaciente', 'planos', 'urs','angularUtils.directives.dirPagination']);
})();