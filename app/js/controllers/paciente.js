(function(){
    'use strict';

    angular
        .module('paciente')
        .controller('PacienteController', PacienteController)
        .filter('Capitalize', Capitalize);

    PacienteController.$inject = [
        '$scope',
        '$window',
        'Paciente',
        'StatusPaciente',
        'Planos',
        'Urs',
        'localStorageService',
        '$timeout'
    ];

    function Capitalize() {
        return function(input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    }

    function PacienteController (
        $scope,
        $window,
        Paciente,
        StatusPaciente,
        Planos,
        Urs,
        localStorageService,
        $timeout
    ) {
        var vm = this;
        vm.message          = 'Carregando lista de pacientes...';
        vm.pacientes        = [];
        vm.statusPaciente   = {};
        vm.searchValue      = {};
        vm.planos           = {};
        vm.urs              = {};
        vm.prefix           = '';
        vm.nomeSearch       = '';
        vm.status           = '';
        vm.todosSearch      = '';

        vm.pageno           = 1;
        vm.total_count      = 0;
        vm.itemsPerPage     = 50;

        vm.init                         = init;
        vm.getPacientesList             = getPacientesList;
        vm.getPacientesListByNameCod    = getPacientesListByNameCod;
        vm.getStatusPaciente            = getStatusPaciente;
        vm.getPlanos                    = getPlanos;
        vm.getUrs                       = getUrs;
        vm.reloadPacientes              = reloadPacientes;
        vm.todos                        = todos;
        vm.checkHistoricoBusca          = checkHistoricoBusca;
        vm.cleanHistoricoBusca          = cleanHistoricoBusca;
        vm.objectSize                   = objectSize;

        function init () {
            vm.getPacientesList(vm.pageno);
            vm.getStatusPaciente();
            //if($window.location.pathname == '/adm/') {
            vm.getPlanos();
            vm.getUrs();
            //}

            vm.checkHistoricoBusca();

            $scope.$watch(function(){
                if(typeof $scope.nomeSearch != 'undefined') {
                    localStorageService.set(vm.prefix + '.nomeSearch', $scope.nomeSearch);
                }
                if(typeof $scope.selectStatusPaciente != 'undefined') {
                    localStorageService.set(vm.prefix + '.status', $scope.selectStatusPaciente);
                }
                if(typeof $scope.todos != 'undefined') {
                    localStorageService.set(vm.prefix + '.todos', $scope.todos);
                }
                //if($window.location.pathname == '/adm/') {

                    if (typeof vm.searchValue.empresa != 'undefined') {
                        localStorageService.set(vm.prefix + '.empresa', vm.searchValue.empresa);
                    }
                    if (typeof vm.searchValue.liminar != 'undefined') {
                        localStorageService.set(vm.prefix + '.liminar', vm.searchValue.liminar);
                    }
                    if (typeof vm.searchValue.convenio != 'undefined') {
                        localStorageService.set(vm.prefix + '.convenio', vm.searchValue.convenio);
                    }
                //}
            });
        }

        function checkHistoricoBusca () {
            vm.prefix = 'adm';
            if($window.location.pathname == '/medico/') {
                vm.prefix = 'medico';
            } else if ($window.location.pathname == '/enfermagem/') {
                vm.prefix = 'enfermagem';
            } else if ($window.location.pathname == '/nutricao/') {
                vm.prefix = 'nutricao';
            }
            vm.nomeSearch   = localStorageService.get(vm.prefix + '.nomeSearch');
            vm.status       = localStorageService.get(vm.prefix + '.status');
            vm.todosSearch  = localStorageService.get(vm.prefix + '.todos');

            if(!localStorageService.get(vm.prefix + '.todos')) {
                $scope.selectStatusPaciente = '4';
            }
            if(vm.nomeSearch) {
                $scope.nomeSearch = vm.nomeSearch;
            }
            if(vm.status) {
                $scope.selectStatusPaciente = vm.status;
            }
            if(vm.todosSearch) {
                $scope.todos = vm.todosSearch;
            }
            //if($window.location.pathname == '/adm/') {
            var empresa     = localStorageService.get(vm.prefix + '.empresa');
            var liminar     = localStorageService.get(vm.prefix + '.liminar');
            var convenio    = localStorageService.get(vm.prefix + '.convenio');
            if(empresa) {
                vm.searchValue.empresa = empresa;
            }
            if(liminar) {
                vm.searchValue.liminar = liminar;
            }
            if(convenio) {
                vm.searchValue.convenio = convenio;
            }
            //}
        }

        function cleanHistoricoBusca () {
            vm.prefix = 'adm';
            if($window.location.pathname == '/medico/') {
                vm.prefix = 'medico';
            } else if ($window.location.pathname == '/enfermagem/') {
                vm.prefix = 'enfermagem';
            } else if ($window.location.pathname == '/nutricao/') {
                vm.prefix = 'nutricao';
            }
            $scope.nomeSearch = '';
            $scope.selectStatusPaciente = '';
            $scope.todos = '';

            localStorageService.remove(vm.prefix + '.nomeSearch');
            localStorageService.remove(vm.prefix + '.status');
            localStorageService.remove(vm.prefix + '.todos');
            //if($window.location.pathname == '/adm/') {
            vm.searchValue.empresa    = '';
            vm.searchValue.liminar    = '';
            vm.searchValue.convenio   = '';
            localStorageService.remove(vm.prefix + '.empresa');
            localStorageService.remove(vm.prefix + '.liminar');
            localStorageService.remove(vm.prefix + '.convenio');
            //}
        }

        function getPacientesList (pageno) {
            vm.pacientes = [];
            vm.message = 'Carregando lista de pacientes...';

            $timeout(function () {
                var data = {
                    perPage: vm.itemsPerPage,
                    pageno: pageno,
                    nomeSearch: vm.search || '',
                    status: $scope.selectStatusPaciente,
                    empresa: vm.searchValue.empresa || '',
                    liminar: vm.searchValue.liminar || '',
                    convenio: vm.searchValue.convenio || '',
                    todos: $scope.todos || ''
                };

                Paciente.get(data, function (response) {
                    if(typeof response.data != 'undefined') {
                        if (objectSize(response.data) == 0) {
                            vm.message = 'Nenhum resultado encontrado!';
                        }
                        vm.pacientes = response.data;
                        vm.total_count = response.total_count;
                        vm.total_pacientes = vm.objectSize(response.data);
                    } else {
                        vm.message = 'Nenhum resultado encontrado!';
                    }
                });
            }, 0);
        }

        function getPacientesListByNameCod() {
            $timeout(function () {
                if (vm.search.length >= 3) {
                    vm.getPacientesList(1);
                    return true;
                }

                if (vm.search.length == 0) {
                    vm.getPacientesList(1);
                    return true;
                }
            }, 0);
        }

        function getStatusPaciente() {
            StatusPaciente.get(function (data) {
                vm.statusPaciente = data;
                vm.statusPaciente.unshift({status: ''});
            });
        }

        function getPlanos() {
            Planos.get(function (data) {
                vm.planos = data;
                vm.planos.unshift({id: '', nome: ''});
            });
        }

        function getUrs() {
            Urs.get(function (data) {
                vm.urs = data;
                vm.urs.unshift({id: '', nome: ''});
            });
        }

        function reloadPacientes (section) {
            localStorageService.remove(section == 'adm' ? 'list-adm' : 'list');
            $window.location.reload();
        }

        function todos() {
            if($scope.todos) {
                $scope.selectStatusPaciente = '';
                vm.searchValue.empresa = '';
                vm.searchValue.liminar = '';
                vm.searchValue.convenio = '';
            } else {
                $scope.selectStatusPaciente = '';
            }
            vm.getPacientesList(1);
        }

        function objectSize (obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) {
                    size++;
                }
            }
            return size;
        }

        vm.init();
    }
})();