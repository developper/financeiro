(function() {
    'use strict';

    angular
        .module('paciente')
        .filter('Busca', Busca);

    Busca.$inject = [];

    function Busca(){
        return function (pacientes, filtros){
            if( (typeof filtros.liminar != 'undefined' && filtros.liminar != '')  ||
                (typeof filtros.convenio != 'undefined' && filtros.convenio != '') ||
                (typeof filtros.empresa != 'undefined' && filtros.empresa != '')) {
                pacientes = pacientes.filter(function (paciente){
                    if(
                        (typeof filtros.liminar == 'undefined' || filtros.liminar == paciente.liminar || filtros.liminar == '') &&
                        (typeof filtros.convenio == 'undefined' || filtros.convenio == paciente.convenio || filtros.convenio == '') &&
                        (typeof filtros.empresa == 'undefined' || filtros.empresa == paciente.empresa || filtros.empresa == '')
                    ) {
                        return paciente;
                    }
                });
            }
            return pacientes;
        }
    };
})();
