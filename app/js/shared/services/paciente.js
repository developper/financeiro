(function(){
    'use strict';

    angular
        .module('paciente')
        .factory('Paciente', Paciente);

    Paciente.$inject = ['$http'];

    function Paciente($http) {
        return{
            get: function(filters, callback){
                //if(window.location.search == '?adm=paciente&act=listar') {
                    var url = '/adm/secmed/?action=get-pacientes-adm-json';
                    angular.forEach(filters, function (value, index) {
                        url += '&' + index + '=' + value;
                    });
                    $http({
                        method: 'GET',
                        url: url
                    }).then(
                        function success(response) {
                            callback(response.data);
                        },
                        function failure(response) {
                            console.log(response);
                        }
                    );
                /*} else {
                    $http({
                        method: 'GET',
                        url: '/adm/secmed/?action=get-pacientes-json'
                    }).then(
                        function success(response) {
                            callback(response.data);
                        },
                        function failure(response) {
                            console.log(response);
                        }
                    );
                }*/
            }
        };
    }
})();