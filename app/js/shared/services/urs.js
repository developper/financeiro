(function(){
    'use strict';

    angular
        .module('urs')
        .factory('Urs', Urs);

    Urs.$inject = ['$http'];

    function Urs($http) {
        return{
            get: function(callback){
                $http({
                    method: 'GET',
                    url: '/adm/secmed/?action=get-empresas-json'
                }).then(
                    function success(response){
                        callback(response.data);
                    },
                    function failure(response){
                        console.log(response);
                    }
                );
            }
        };
    }
})();