(function(){
    'use strict';

    angular
        .module('planos')
        .factory('Planos', Planos);

    Planos.$inject = ['$http'];

    function Planos($http) {
        return{
            get: function(callback){
                $http({
                    method: 'GET',
                    url: '/adm/secmed/?action=get-planos-json'
                }).then(
                    function success(response){
                        callback(response.data);
                    },
                    function failure(response){
                        console.log(response);
                    }
                );
            }
        };
    }
})();