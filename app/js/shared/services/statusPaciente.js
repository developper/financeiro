(function(){
    'use strict';

    angular
        .module('statusPaciente')
        .factory('StatusPaciente', StatusPaciente);

    StatusPaciente.$inject = ['$http'];

    function StatusPaciente($http) {
        return{
            get: function(callback){
                $http({
                    method: 'GET',
                    url: '/adm/secmed/?action=get-status-paciente-json'
                }).then(
                    function success(response){
                        callback(response.data);
                    },
                    function failure(response){
                        console.log(response);
                    }
                );
            }
        };
    }
})();