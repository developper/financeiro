<?php
namespace App\Core;

class Config
{

    private $config;

    public static function get($key)
    {
        $config = require __DIR__ . '/../../../config.php';
        return isset($config[$key]) ? $config[$key] : false;
    }
}