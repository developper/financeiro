<?php
/**
 * Created by PhpStorm.
 * User: jweber
 * Date: 10/7/14
 * Time: 4:02 PM
 */

namespace App\Core;


class DateTime
{

  public static function getPeriodos(\DateTime $inicio, \DateTime $fim, $tipo = 'M')
  {
    $periodos = [];
    if($tipo === 'M'){
      $oneMonth = new \DateInterval('P1M');

      $ultimoDiaFim = clone $fim;
      $ultimoDiaFim = $ultimoDiaFim->modify('last day of this month');

      for ($atual = clone $inicio; $atual <= $ultimoDiaFim; $atual->add($oneMonth)) {
        $periodo = new \StdClass;
        $periodo->inicio = clone $atual;
        if ($atual > $inicio)
          $periodo->inicio->modify('first day of this month');

        $periodo->fim = clone $atual;
        if ($atual->format('m-Y') != $fim->format('m-Y'))
          $periodo->fim->modify('last day of this month');
        else
          $periodo->fim = clone $fim;

        $periodos[] = $periodo;
      }
    }else {
      $oneDay = new \DateInterval('P1D');
      for ($atual = clone $inicio; $atual <= $fim; $atual->add($oneDay)) {
        $periodo = new \StdClass;
        $periodo->dia = clone $atual;
        
        $periodos[] = $periodo;
      }      
    }
    return $periodos;
  }

} 