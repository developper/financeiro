<?php

namespace App\Core;

class Mensagem
{

  public static function getHTML($mensagemTemp)
  {
    $html = false;
    $errors = isset($mensagemTemp['errors']) ? $mensagemTemp['errors'] : null;
    if (isset($errors) && !empty($errors)) {
      $errorsHTML = '';
      foreach ($errors as $error)
        $errorsHTML = $errorsHTML . sprintf('<li>%s</li>', $error);
      $html .= sprintf('<div class="errors-box"><ul>%s</ul></div>', $errorsHTML);
    }

    $successs = isset($mensagemTemp['success']) ? $mensagemTemp['success'] : null;
    if (isset($successs) && !empty($successs)) {
      $successsHTML = '';
      foreach ($successs as $success)
        $successsHTML = $successsHTML . sprintf('<li>%s</li>', $success);
      $html .= sprintf('<div class="errors-box"><ul>%s</ul></div>', $successsHTML);
    }

    return $html;
  }

  public static function forDatabase($errorMensage)
  {
    $base = "Desculpa! Ocorreu algum erro ao tentar salvar. Por favor, informe essa mensagem à equipe de suporte técnico: %s";
    return sprintf($base, $errorMensage);
  }

} 