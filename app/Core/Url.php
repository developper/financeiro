<?php

namespace App\Core;

class Url
{

  public static function get($pattern, $params = array(), $first = false)
  {
    $routes = self::getRoutes();
    if (! isset($routes[$pattern]))
      throw new \InvalidArgumentException('URL "'. $pattern .'" não existe!');

    $first = $first ? '?' : '&';
    $params = isset($params) && !empty($params) ? $first . http_build_query($params) : null;
    return $routes[$pattern] . $params;
  }

  private static function getRoutes()
  {
    $root = self::getRoot();
    return array(
			'faturamento-index'          => $root . '/faturamento/index.php',
			'faturamento-gerenciar-lote' => $root . '/faturamento/?view=gerenciarlote',
			'tiss-processar-xml'         => $root . '/faturamento/tissxml/index.php?action=processar-xml',
			'tiss-baixar-xml'            => $root . '/faturamento/tissxml/index.php?action=baixar-xml',
			'ipcf'                       => $root . '/financeiros/plano-contas/?action=ipcf',
			'ipcc'                       => $root . '/financeiros/plano-contas/?action=ipcc'
    );
  }

  private static function getRoot()
  { return '';
  }

} 