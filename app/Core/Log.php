<?php
namespace app\Core;

class Log
{
  public static function registerErrorLog($backtrace)
  {
    $backtrace = array_map(function($element){
      return addslashes($element);
    }, $backtrace);
    $sql = "INSERT INTO error_backtrace
            VALUES ('', NOW(), '{$backtrace['text']}', '{$backtrace['module']}', '{$backtrace['file']}',
                    {$backtrace['line']}, '{$backtrace['function']}', '{$backtrace['class']}')";
    return mysql_query($sql);
  }

  public static function registerUserLog($query)
  {
    @session_start();
    $query = addslashes($query);
    $sql = "INSERT INTO loguser VALUES ('', '{$query}', 261, NOW())";
    return mysql_query($sql);
  }
}