<?php

namespace App\Core;

class Kernel
{
    private $env;

    public function __construct($env) {
        $this->env = $env;
    }
}