<?php

namespace App;

class Application
{
	protected $routes = [];
	public function setRoutes($routes)
	{
		$this->routes = $routes;
	}
	public function dispatch($method, $uri, $args = null)
	{
		$uri = explode('&', $uri);
		if (! isset($this->routes[$method][$uri[0]]))
			throw new NotFoundException('Página não encontrada!');
		if (! is_callable($this->routes[$method][$uri[0]]))
			throw new BaseException("Função callback inválida!");

		return call_user_func($this->routes[$method][$uri[0]]);
	}
}