<?php

namespace app\Services;


use app\Services\Interfaces\CacheInterface;

class CacheAdapter
{
	public $adapter;

	public function __construct(CacheInterface $adapter)
	{
		$this->adapter = $adapter;
	}
}