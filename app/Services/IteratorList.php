<?php
/**
 * Created by PhpStorm.
 * User: ricardotassio
 * Date: 18/09/15
 * Time: 14:45
 */

namespace App\Services;


class IteratorList implements \Iterator
{

    private $index = 0;
    private $list  = [];

    public function __construct(Array $list)
    {
        $this->list = $list;
    }

    public function current()
    {
       return $this->list[$this->index];
    }

    public function key()
    {
        return $this->index;
    }

    public function next()
    {
        return $this->index++;
    }

    public function valid()
    {

        return isset($this->list[$this->index]);
    }


    public function rewind()
    {
        return $this->index = 0;
    }
}