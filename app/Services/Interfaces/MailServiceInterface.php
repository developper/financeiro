<?php

namespace app\Services\Interfaces;

interface MailServiceInterface
{
	public function fillMailHeader();

	public function fillMailBody();

	public function sendMail();
}
