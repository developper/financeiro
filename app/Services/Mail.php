<?php

namespace App\Services;

include_once(__DIR__ . "/../../utils/class.phpmailer.php");

class Mail 
{

  protected $engine;
  protected $config;

  public function __construct($emailConfig = array())
  {
    $this->config = $emailConfig;

    $this->engine = new \PHPMailer(true);
    $this->engine->SMTPAuth = true;
    $this->engine->SMTPSecure = "SSL";
    $this->engine->Host = "smtp.mederi.com.br";
    $this->engine->Port = 456;
    $this->engine->Username = "avisos@mederi.com.br";
    $this->engine->Password = "2017avisosistema";
  }

  public function send(Array $emails, $subject, $msg, $from = 'ti@mederi.com.br')
  {
    foreach ($emails as $email)
      $this->engine->AddAddress($email);

    $this->engine->SetFrom($from);
    $this->engine->Subject = $subject;
    $this->engine->MsgHTML(utf8_decode($msg));
    return $this->engine->Send();
  }

}