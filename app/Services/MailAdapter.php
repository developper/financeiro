<?php
namespace app\Services;

use app\Services\Interfaces\MailServiceInterface;

class MailAdapter
{
	public $adapter;

	public function __construct(MailServiceInterface $adapter)
	{
		$this->adapter = $adapter;
	}

	public function sendNotificationHipChat($hipchat, $msg, $debug = false)
	{
		$roomId = $debug === false ? '1179954' : '469564';
		return $hipchat->sendNotificationRoom($roomId, $msg, array('color'=>'yellow','notify'=>'true'));
        // return;
	}
}
