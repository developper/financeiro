<?php

namespace App\Services;

use App\Core\Config;
use Pheanstalk\Pheanstalk;

class Queue
{

  public function __construct()
  {
    $this->loadServer((object) Config::get('queue'));
  }

  protected function loadServer($config)
  {
    $this->server = new Pheanstalk($config->host);

    if (! $this->server->getConnection()->isServiceListening())
      throw new \Exception('Queue server inaccessible!');
  }

  public function push($data = null, $tube = 'default')
  {
    $this->server->useTube($tube)->put($data);
  }

  public function listen($tube)
  {
    return $this->server->watch($tube)->reserve();
  }

  public function delete($job)
  {
    $this->server->delete($job);
  }

}