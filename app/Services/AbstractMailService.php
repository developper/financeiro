<?php

namespace app\Services;

abstract class AbstractMailService
{
	protected $email_config = [];

	public function getEmailConfig()
	{
		$temp = require $_SERVER['DOCUMENT_ROOT'] . '/app/configs/email.php';
		$this->email_config = $temp;
	}
}
