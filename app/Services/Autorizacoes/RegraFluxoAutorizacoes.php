<?php
namespace App\Services\Autorizacoes;

class RegraFluxoAutorizacoes
{
    public function verificarItensRestritos($item_list)
    {

        $itens_autorizados[0] = $this->medicamentos_autorizados($item_list);
        $itens_autorizados[1] = $this->materiais_autorizados($item_list);
        $itens_autorizados[2] = $this->servicos_autorizados($item_list);
        $itens_autorizados[3] = $this->dietas_autorizadas($item_list);
        $itens_autorizados[5] = $this->equipamentos_autorizados($item_list);
        
        if(!empty($itens_autorizados[0])) {
            $medicamentos_regra_fluxo = $this->regrasMedicamentoByPlano($item_list[0]['plano_id']);
            if(!empty($medicamentos_regra_fluxo)){
                $medicamentos_regra_fluxo_agrupado  = $this->separar_grupo_item($medicamentos_regra_fluxo);
                foreach($itens_autorizados[0] as $key => $medicamento) {
                    
                    if(!empty($medicamentos_regra_fluxo_agrupado['item']) && in_array($medicamento['item_id'], $this->array_column($medicamentos_regra_fluxo_agrupado['item'],'item_id'))){
                        if($medicamentos_regra_fluxo_agrupado['item'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                            $itens_restritos[] = $medicamento;
                            
                        }
                        unset($itens_autorizados[0][$key]);
                    }elseif (!empty($medicamentos_regra_fluxo_agrupado['grupo']) && in_array($medicamento['grupo'], $this->array_column($medicamentos_regra_fluxo_agrupado['grupo'],'item_id'))){
                        if($medicamentos_regra_fluxo_agrupado['grupo'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                            $itens_restritos[] = $medicamento;
                        }
                        unset($itens_autorizados[0][$key]);
                    }elseif(!empty($medicamentos_regra_fluxo_agrupado['grupo']) && $medicamentos_regra_fluxo_agrupado['grupo'][0]['item_id'] == 0 ||
                        $medicamentos_regra_fluxo_agrupado['grupo'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                        $itens_restritos[] = $medicamento;
                    }
                }
            }
        }
        
        if(!empty($itens_autorizados[1])){
            
            $materiais_regra_fluxo = $this->regrasMaterialByPlano($item_list[0]['plano_id']);
            
            if(!empty($materiais_regra_fluxo)){
                
                $materiais_regra_fluxo_agrupado = $this->separar_grupo_item($materiais_regra_fluxo);
                
                foreach($itens_autorizados[1] as $key => $material) {
                    if(!empty($materiais_regra_fluxo_agrupado['item']) && in_array($material['item_id'], $this->array_column($materiais_regra_fluxo_agrupado['item'],'item_id'))){
                        if($materiais_regra_fluxo_agrupado['item'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                            $itens_restritos[] = $material;
                        }
                        unset($itens_autorizados[1][$key]);
                        
                    }elseif(!empty($materiais_regra_fluxo_agrupado['grupo']) && $materiais_regra_fluxo_agrupado['grupo'][0]['item_id'] == 0 ||
                        $materiais_regra_fluxo_agrupado['grupo'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                        
                        $itens_restritos[] = $material;
                        
                    }elseif(!empty($materiais_regra_fluxo_agrupado['grupo']) && in_array($material['grupo'], $this->array_column($materiais_regra_fluxo_agrupado['grupo'],'item_id'))){
                        if($materiais_regra_fluxo_agrupado['grupo'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                            $itens_restritos[] = $material;
                        }
                        unset($itens_autorizados[1][$key]);
                    }
                }
                
            }
            
        }
        
        if(!empty($itens_autorizados[2])){
            
            $servicos_regra_fluxo = $this->regrasServicoByPlano($item_list[0]['plano_id']);
            
            if(!empty($servicos_regra_fluxo)){
                $servicos_regra_fluxo_agrupado = $this->separar_grupo_item($servicos_regra_fluxo);
                foreach($itens_autorizados[2] as $key => $servico) {
                    if(!empty($servicos_regra_fluxo_agrupado['item']) && in_array($servico['item_id'], $this->array_column($servicos_regra_fluxo_agrupado['item'],'item_id'))){
                        if($servicos_regra_fluxo_agrupado['item'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                            $itens_restritos[] = $servico;
                        }
                        unset($itens_autorizados[2][$key]);
                        
                    }elseif(!empty($servicos_regra_fluxo_agrupado['grupo']) && $servicos_regra_fluxo_agrupado['grupo'][0]['item_id'] == 0 ||
                        $servicos_regra_fluxo_agrupado['grupo'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                        
                        $itens_restritos[] = $servico;
                        
                    }elseif(!empty($servicos_regra_fluxo_agrupado['grupo']) && in_array($servico['grupo'], $this->array_column($servicos_regra_fluxo_agrupado['grupo'],'item_id'))){
                        if($servicos_regra_fluxo_agrupado['grupo'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                            $itens_restritos[] = $servico;
                        }
                        unset($itens_autorizados[2][$key]);
                    }
                }
            }
            
        }
        
        if(!empty($itens_autorizados[3])){
            
            $dietas_regra_fluxo = $this->regrasDietaByPlano($item_list[0]['plano_id']);
            
            
            if(!empty($dietas_regra_fluxo)){
                $dietas_regra_fluxo_agrupado = $this->separar_grupo_item($dietas_regra_fluxo);
                
                foreach($itens_autorizados[3] as $key => $dieta) {
                    if(!empty($dietas_regra_fluxo_agrupado['itens']) && in_array($dieta['item_id'], $this->array_column($dietas_regra_fluxo_agrupado['item'],'item_id'))){
                        if($dietas_regra_fluxo_agrupado['item'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                            $itens_restritos[] = $dieta;
                        }
                        unset($itens_autorizados[3][$key]);
                    }elseif(!empty($dietas_regra_fluxo_agrupado['grupo']) && $dietas_regra_fluxo_agrupado['grupo'][0]['item_id'] == 0 ||
                        $dietas_regra_fluxo_agrupado['grupo'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                        
                        $itens_restritos[] = $dieta;
                        
                    }elseif(!empty($dietas_regra_fluxo_agrupado['grupo']) && in_array($dieta['grupo'], $this->array_column($dietas_regra_fluxo_agrupado['grupo'],'item_id'))){
                        if($dietas_regra_fluxo_agrupado['grupo'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                            $itens_restritos[] = $dieta;
                        }
                        unset($itens_autorizados[3][$key]);
                    }
                }
            }
            
        }
        
        if(!empty($itens_autorizados[5])){
            
            
            
            $equipamentos_regra_fluxo = $this->regrasEquipamentoByPlano($item_list[0]['plano_id']);
            
            if(!empty($equipamentos_regra_fluxo)){
                $equipamentos_regra_fluxo_agrupado  = $this->separar_grupo_item($equipamentos_regra_fluxo);
                foreach($itens_autorizados[5] as $key => $equipamento) {
                    if(empty($equipamentos_regra_fluxo_agrupado['grupo']) && in_array($equipamento['item_id'], $this->array_column($equipamentos_regra_fluxo_agrupado['item'],'item_id'))){
                        if($equipamentos_regra_fluxo_agrupado['item'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                            $itens_restritos[] = $equipamento;
                        }
                        unset($itens_autorizados[5][$key]);
                    }elseif(!empty($equipamentos_regra_fluxo_agrupado['grupo']) && $dietas_regra_fluxo_agrupado['grupo'][0]['item_id'] == 0 ||
                        $dietas_regra_fluxo_agrupado['grupo'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                        
                        $itens_restritos[] = $equipamento;
                        
                    }
                    elseif(!empty($equipamentos_regra_fluxo_agrupado['grupo']) && in_array($equipamento['grupo'], $this->array_column($equipamentos_regra_fluxo_agrupado['grupo'],'item_id'))){
                        if($equipamentos_regra_fluxo_agrupado['grupo'][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                            $itens_restritos[] = $equipamento;
                        }
                        unset($itens_autorizados[5][$key]);
                    }
                }
            }
        }


        
        if(!empty($itens_restritos)){
            foreach($itens_restritos as $itens) {
                unset($itens['grupo']);
                $itens_restritos_aux[] = "(NULL, '". implode("','",$itens)."')";
            }

            $itens_restritos_final = implode(",",$itens_restritos_aux);
            $sql = "INSERT INTO regra_fluxo_autorizado_cron (id,autorizacao_item_id, periodo_autorizacao_inicio, periodo_autorizacao_fim, plano_id, paciente_id, qtd_autorizado, item_id, tipo_item, tipo_documento) VALUES ".$itens_restritos_final;

            $result  = mysql_query($sql);
            if(!$result){
                echo "Erro ao inseriri itens na cron";

            }

        };

        
    }
    
    public function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( !array_key_exists($columnKey, $value)) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( !array_key_exists($indexKey, $value)) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
    public function medicamentos_autorizados($item_list)
    {
        return array_filter($item_list, function($item){
            if($item['tipo'] == 0)
                return $item;
        });
    }
    public function materiais_autorizados($item_list)
    {
        return array_filter($item_list, function($item){
            if($item['tipo']== 1)
                return $item;
        });
    }
    public function servicos_autorizados($item_list)
    {
        return array_filter($item_list, function($item){
            if($item['tipo']== 2)
                return $item;
        });
    }
    public function dietas_autorizadas($item_list)
    {
        return array_filter($item_list, function($item){
            if($item['tipo']== 3)
                return $item;
        });
    }
    public function equipamentos_autorizados($item_list)
    {
        return array_filter($item_list, function($item){
            if($item['tipo']== 5)
                return $item;
        });
    }
    public function separar_grupo_item($lista_itens)
    {
        if(isset($lista_itens) && !empty($lista_itens)){
            $itens['grupo'] = array_filter($lista_itens, function($item){
                if($item['opcao_regra'] == 'grupo')
                    return $item;
            });
            $itens['item'] = array_filter($lista_itens,function($item){
                if($item['opcao_regra'] == 'item')
                    return $item;
            });
            return $itens;
            
        }
        return [];
        
    }
    
    public function separarGrupoItemRegraFluxoSolicitacao($lista_itens)
    {
        if(isset($lista_itens) && !empty($lista_itens)) {
            foreach ($lista_itens as $item) {
                if($item['opcao_regra'] == 'grupo') {
                    $itens['grupo'][$item['item_id']][] = $item;
                } else {
                    $itens['item'][$item['item_id']][] = $item;
                }
            }
            return $itens;
        }
        return [];
    }
    
    public function regrasMedicamentoByPlano($planoId, $tipoSolicitacao)
    {
        $sql = "SELECT
                   regra_primaria,
                   opcao_regra,
                   item_id,
                   regra_fluxo_medicamentos.id,
                   regra_fluxo_medicamentos.tipo_validacao,
                   regra_fluxo_condicionais.campo_comparacao,
                   regra_fluxo_condicionais.operador,
                   regra_fluxo_condicionais.valor_referencia
                FROM
                  regra_fluxo_medicamentos
                RIGHT JOIN regra_fluxo ON ( regra_fluxo_medicamentos.regra_fluxo_id = regra_fluxo.id )
                Left JOIN regra_fluxo_condicionais ON ( regra_fluxo_medicamentos.id = regra_fluxo_condicionais.tabela_origem_id AND regra_fluxo_condicionais.tabela_origem = 'medicamento')
                WHERE
                  plano_id = '{$planoId}'
                  AND regra_fluxo_medicamentos.tipo_solicitacao in('".$tipoSolicitacao."', 'todos')
                ORDER BY
                    opcao_regra";
        
        $result  = mysql_query($sql);

        if($result){
            while ($row = mysql_fetch_array($result)){
                $medicamentos_regra_fluxo[] = $row;
            }
            return $medicamentos_regra_fluxo;
        }
        return ;
        
    }
    
    public function regrasMaterialByPlano($planoId, $tipoSolicitacao){
        $sql  =  "SELECT
                        regra_primaria,
                        opcao_regra,
                        item_id ,
                        regra_fluxo_materiais.id,
                        regra_fluxo_materiais.tipo_validacao,
                        regra_fluxo_condicionais.campo_comparacao,
                        regra_fluxo_condicionais.operador,
                        regra_fluxo_condicionais.valor_referencia
                    FROM
                        regra_fluxo_materiais
                        RIGHT JOIN regra_fluxo ON ( regra_fluxo_materiais.regra_fluxo_id = regra_fluxo.id )
                        Left JOIN regra_fluxo_condicionais ON ( regra_fluxo_materiais.id = regra_fluxo_condicionais.tabela_origem_id AND regra_fluxo_condicionais.tabela_origem = 'material')
                    WHERE
                        plano_id = {$planoId}
                        AND regra_fluxo_materiais.tipo_solicitacao in('".$tipoSolicitacao."', 'todos')
                    ORDER BY
                        opcao_regra";
        $result  = mysql_query($sql);
        if($result){
            while ($row = mysql_fetch_array($result)){
                $materiais_regra_fluxo[] = $row;
            }
            return $materiais_regra_fluxo;
        }
        return;
        
    }
    
    public function regrasServicoByPlano($planoId, $tipoSolicitacao){
        $sql     =  "SELECT
                            regra_primaria,
                            opcao_regra,
                            item_id,
                            regra_fluxo_servicos.id ,
                            regra_fluxo_servicos.tipo_validacao,
                            regra_fluxo_condicionais.campo_comparacao,
                            regra_fluxo_condicionais.operador,
                            regra_fluxo_condicionais.valor_referencia
                        FROM
                          regra_fluxo_servicos
                        RIGHT JOIN regra_fluxo ON (regra_fluxo_servicos.regra_fluxo_id = regra_fluxo.id)
                        LEft JOIN regra_fluxo_condicionais ON ( regra_fluxo_servicos.id = regra_fluxo_condicionais.tabela_origem_id AND regra_fluxo_condicionais.tabela_origem = 'servico')
                        WHERE
                            plano_id = {$planoId}
                            AND regra_fluxo_servicos.tipo_solicitacao in('".$tipoSolicitacao."', 'todos')
                        ORDER BY
                          opcao_regra";
        
        $result  = mysql_query($sql);
        if($result){
            while ($row = mysql_fetch_array($result)){
                $servicos_regra_fluxo[] = $row;
            }
            return $servicos_regra_fluxo;
        }
        return;
    }
    
    public function regrasDietaByPlano($planoId, $tipoSolicitacao){
        $sql      =  "SELECT
                          regra_primaria,
                          opcao_regra,
                          item_id,
                          regra_fluxo_dietas.id ,
                          regra_fluxo_dietas.tipo_validacao,
                          regra_fluxo_condicionais.campo_comparacao,
                          regra_fluxo_condicionais.operador,
                          regra_fluxo_condicionais.valor_referencia
                        FROM
                          regra_fluxo_dietas
                        RIGHT JOIN regra_fluxo ON (regra_fluxo_dietas.regra_fluxo_id = regra_fluxo.id)
                        Left JOIN regra_fluxo_condicionais ON ( regra_fluxo_dietas.id = regra_fluxo_condicionais.tabela_origem_id AND regra_fluxo_condicionais.tabela_origem ='dieta')
                        WHERE
                          plano_id = {$planoId}
                          AND regra_fluxo_dietas.tipo_solicitacao in('".$tipoSolicitacao."', 'todos')
                        ORDER BY
                          opcao_regra";
        $result  = mysql_query($sql);
        if($result){
            while ($row = mysql_fetch_array($result)){
                $dietas_regra_fluxo[] = $row;
            }
            return $dietas_regra_fluxo;
        }
        return ;
        
    }
    
    public function regrasEquipamentoByPlano($planoId, $tipoSolicitacao){
        $sql =  "SELECT
                    regra_primaria,
                    opcao_regra,
                    item_id,
                    regra_fluxo_equipamentos.id ,
                    regra_fluxo_equipamentos.tipo_validacao,
                    regra_fluxo_condicionais.campo_comparacao,
                    regra_fluxo_condicionais.operador,
                    regra_fluxo_condicionais.valor_referencia
                FROM
                  regra_fluxo_equipamentos
                RIGHT JOIN regra_fluxo ON (regra_fluxo_equipamentos.regra_fluxo_id = regra_fluxo.id)
                LEFT JOIN regra_fluxo_condicionais ON ( regra_fluxo_equipamentos.id = regra_fluxo_condicionais.tabela_origem_id and regra_fluxo_condicionais.tabela_origem ='equipamento')
                WHERE
                    plano_id = {$planoId}
                    AND regra_fluxo_equipamentos.tipo_solicitacao in('".$tipoSolicitacao."', 'todos')
                ORDER BY
                  opcao_regra";

        $result  = mysql_query($sql);
        if($result){
            while ($row = mysql_fetch_array($result)){
                $equipamentos_regra_fluxo[] = $row;
            }
            return $equipamentos_regra_fluxo;
        }
        return ;
    }

    public function condicionalRegraBloqueio($item, $regrasTipo, $entregaImediata){

        $liberar = ['coluna' => ",autorizado,data_auditado,AUDITOR_ID",
                    'valores' => ",'{$item['quantidade']}', now(),{$item['usuario']}",
                    'status' => 1];
        $negar = ['coluna' => "",
            'valores' => "",
            'status' => 0];


          // verifica se existe id do item nas geras por item
            if(!empty($regrasTipo['item']) && array_key_exists($item['id'],$regrasTipo['item'])){

                if($regrasTipo['item'][$item['id']][0]['regra_primaria'] == 'liberar_tudo_exceto'){

                    if($regrasTipo['item'][$item['id']][0]['tipo_validacao'] == 'binaria' ){
                        return $negar;

                    }else{
                        if($regrasTipo['item'][$item['id']][0]['campo_comparacao'] == 'quantidade'){
                            if(eval("return {$item['quantidade']} {$regrasTipo['item'][$item['id']][0]['operador']} {$regrasTipo['item'][$item['id']][0]['valor_referencia']} ")){
                                return $negar;
                            }else{
                                return $liberar;
                            }
                        } else if($regrasTipo['item'][$item['id']][0]['campo_comparacao'] == 'valor'){
                                    if(eval("return {$item['valor']} {$regrasTipo['item'][$item['id']][0]['operador']} {$regrasTipo['item'][$item['id']][0]['valor_referencia']} ")){
                                        return $negar;
                                    }else{
                                        return $liberar;
                                    }
                                }
                    }
                }else{
                    if($regrasTipo['item'][$item['id']][0]['tipo_validacao'] == 'binaria' ){
                        return $liberar;

                    }else{
                        if($regrasTipo['item'][$item['id']][0]['campo_comparacao'] == 'quantidade'){

                            if(eval("return {$item['quantidade']} {$regrasTipo['item'][$item['id']][0]['operador']} {$regrasTipo['item'][$item['id']][0]['valor_referencia']} ")){
                                return $liberar;
                            }else{
                                return $negar;
                            }

                        }else if($regrasTipo['item'][$item['id']][0]['campo_comparacao'] == 'valor'){

                            if(eval("return {$item['valor']} {$regrasTipo['item'][$item['id']][0]['operador']} {$regrasTipo['item'][$item['id']][0]['valor_referencia']} ")){
                                return $liberar;
                            }else{
                                return $negar;
                            }

                        }

                    }


                }
         // verifica se existe id do grupo do item nas geras por grupo
            }elseif (!empty($regrasTipo['grupo']) && array_key_exists($item['grupo'],$regrasTipo['grupo'])){
                if($regrasTipo['grupo'][$item['grupo']][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                    if($regrasTipo['grupo'][$item['grupo']][0]['tipo_validacao'] == 'binaria' ){
                        return $negar;
                    }else{
                        if($regrasTipo['grupo'][$item['grupo']][0]['campo_comparacao'] == 'quantidade'){
                            if(eval("return {$item['quantidade']} {$regrasTipo['grupo'][$item['grupo']][0]['operador']} {$regrasTipo['grupo'][$item['grupo']][0]['valor_referencia']} ")){
                                return $negar;
                            }else{
                                return $liberar;
                            }

                        } else if($regrasTipo['grupo'][$item['grupo']][0]['campo_comparacao'] == 'valor'){
                            if(eval("return {$item['valor']} {$regrasTipo['grupo'][$item['grupo']][0]['operador']} {$regrasTipo['grupo'][$item['grupo']][0]['valor_referencia']} ")){
                                return $negar;
                            }else{
                                return $liberar;
                            }

                        }
                    }

                }else{

                    if($regrasTipo['grupo'][$item['grupo']][0]['tipo_validacao'] == 'binaria' ){
                        return $liberar;

                    }else{
                        if($regrasTipo['grupo'][$item['grupo']][0]['campo_comparacao'] == 'quantidade'){

                            if(eval("return {$item['quantidade']} {$regrasTipo['grupo'][$item['id']][0]['operador']} {$regrasTipo['grupo'][$item['grupo']][0]['valor_referencia']} ")){

                                return $liberar;
                            }else{
                                return $negar;
                            }

                        }else if($regrasTipo['grupo'][$item['id']][0]['campo_comparacao'] == 'valor'){

                            if(eval("return {$item['valor']} {$regrasTipo['grupo'][$item['grupo']][0]['operador']} {$regrasTipo['grupo'][$item['grupo']][0]['valor_referencia']} ")){
                                return $liberar;
                            }else{
                                return $negar;
                            }

                        }

                    }


                }

            }else{
                // caso não exista grupo nem item verifica a regra por grupo
                    $keyGrupo = array_keys($regrasTipo['grupo']);
                    $keyItem = array_keys($regrasTipo['item']);




                        if(!empty($regrasTipo['grupo'])){
                            //grupo todos.
                            if(!empty($regrasTipo['grupo'][0][0]['regra_primaria']) ){
                                if($regrasTipo['grupo'][$keyGrupo[0]][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                                    return $negar;
                                }else{
                                    return $liberar;
                                }

                            }else {
                                if ($regrasTipo['grupo'][$keyGrupo[0]][0]['regra_primaria'] == 'liberar_tudo_exceto') {
                                    return $liberar;
                                } else {
                                    return $negar;
                                }
                            }

                        }

                        if(!empty($regrasTipo['item'])){
                            if($regrasTipo['item'][$keyItem[0]][0]['regra_primaria'] == 'liberar_tudo_exceto'){
                                return $liberar;
                            }else{
                                return $negar;
                            }
                        }






            }






    }
    
    
    
}