<?php


namespace App\Services\Autorizacoes;

class CronLiberarItem
{
    public function verificar_itens_restritos()
    {
        $responseMaterial = [];
        $responseMedicamento = [];
        $responseDieta = [];
        $responseEquipamento = [];
        $arrayAcoes = [];

        $itensTipoAditivoTabelaCron = $this->getItensAutorizadosCronByTipoDocumento('aditivo');

        if(!empty($itensTipoAditivoTabelaCron)){
            $peridoAditivo = $this->getPeriodoCronByTipoDocumento('aditivo');

            $inicioAditivo = $peridoAditivo['inicio'];
            $fimAditivo = $peridoAditivo['fim'];

            if(!empty($inicioAditivo) && !empty($fimAditivo)){
                // CATEGORIZAR OS ITENS DA TABELA DE CRON
                $itensAutorizadosAditivo[0] = $this->arrayPrepare($this->medicamentos_autorizados($itensTipoAditivoTabelaCron));
                $itensAutorizadosAditivo[1] = $this->arrayPrepare($this->materiais_autorizados($itensTipoAditivoTabelaCron));
//                $itensAutorizadosAditivo[2] = $this->arrayPrepare($this->servicos_autorizados($itensTipoAditivoTabelaCron));
                $itensAutorizadosAditivo[3] = $this->arrayPrepare($this->dietas_autorizadas($itensTipoAditivoTabelaCron));
                $itensAutorizadosAditivo[5] = $this->arrayPrepare($this->equipamentos_autorizados($itensTipoAditivoTabelaCron));
                $itensSolicitacoesAditivas = $this->getItensSolicitadosPendentesTipoAditivo($inicioAditivo, $fimAditivo);
                if(!empty($itensSolicitacoesAditivas)){
                    $itensSolicitadosAditiva[0] = $this->arrayPrepare($this->medicamentos_autorizados($itensSolicitacoesAditivas));
                    $itensSolicitadosAditiva[1] = $this->arrayPrepare($this->materiais_autorizados($itensSolicitacoesAditivas));
                    $itensSolicitadosAditiva[3] = $this->arrayPrepare($this->dietas_autorizadas($itensSolicitacoesAditivas));
                    $itensSolicitadosAditiva[5] = $this->arrayPrepare($this->equipamentos_autorizados($itensSolicitacoesAditivas));
                    if(!empty($itensSolicitadosAditiva[0]) && !empty($itensAutorizadosAditivo[0])){
                        $responseMedicamento = $this->sicronizarItensParaSeremLiberados($itensAutorizadosAditivo[0], $itensSolicitadosAditiva[0]);
                        if(!empty($responseMedicamento)){
                            if(!empty($responseMedicamento['update-solicitacao'])){
                                $arrayAcoes['update-solicitacao'][] = $responseMedicamento['update-solicitacao'];
                            }
                            if(!empty($responseMedicamento['itens-autorizados'])){
                                $arrayAcoes['itens-autorizados'][] = $responseMedicamento['itens-autorizados'];
                            }
                            if(!empty($responseMedicamento['itens-solicitados'])){
                                $arrayAcoes['itens-solicitados'][] = $responseMedicamento['itens-solicitados'];
                            }
                            if(!empty($responseMedicamento['remover-cron'])){
                                $arrayAcoes['remover-tabela-cron'][] = $responseMedicamento['remover-cron'];
                            }

                            if(!empty($responseMedicamento['itens-insert-cron-log'])){
                                $arrayAcoes['insert-cron-log'][] = $responseMedicamento['itens-insert-cron-log'];
                            }
                        }
                    }

                    if(!empty($itensSolicitadosAditiva[1]) && !empty($itensAutorizadosAditivo[1])){
                        $responseMaterial = $this->sicronizarItensParaSeremLiberados($itensAutorizadosAditivo[1], $itensSolicitadosAditiva[1]);
                        if(!empty($responseMaterial)){
                            if(!empty($responseMaterial['update-solicitacao'])){
                                $arrayAcoes['update-solicitacao'][] = $responseMaterial['update-solicitacao'];
                            }
                            if(!empty($responseMaterial['itens-autorizados'])){
                                $arrayAcoes['itens-autorizados'][] = $responseMaterial['itens-autorizados'];
                            }
                            if(!empty($responseMaterial['itens-solicitados'])){
                                $arrayAcoes['itens-solicitados'][] = $responseMaterial['itens-solicitados'];
                            }
                            if(!empty($responseMaterial['remover-cron'])){
                                $arrayAcoes['remover-tabela-cron'][] = $responseMaterial['remover-cron'];
                            }
                            if(!empty($responseMaterial['itens-insert-cron-log'])){
                                $arrayAcoes['insert-cron-log'][] = $responseMaterial['itens-insert-cron-log'];
                            }
                        }
                    }

                    if(!empty($itensSolicitadosAditiva[3]) && !empty($itensAutorizadosAditivo[3])){
                        $responseDieta = $this->sicronizarItensParaSeremLiberados($itensAutorizadosAditivo[3], $itensSolicitadosAditiva[3]);
                        if(!empty($responseDieta)){
                            if(!empty($responseDieta['update-solicitacao'])){
                                $arrayAcoes['update-solicitacao'][] = $responseDieta['update-solicitacao'];
                            }
                            if(!empty($responseDieta['itens-autorizados'])){
                                $arrayAcoes['itens-autorizados'][] = $responseDieta['itens-autorizados'];
                            }
                            if(!empty($responseDieta['itens-solicitados'])){
                                $arrayAcoes['itens-solicitados'][] = $responseDieta['itens-solicitados'];
                            }
                            if(!empty($responseDieta['remover-cron'])){
                                $arrayAcoes['remover-tabela-cron'][] = $responseDieta['remover-cron'];
                            }
                            if(!empty($responseDieta['itens-insert-cron-log'])){
                                $arrayAcoes['insert-cron-log'][] = $responseDieta['itens-insert-cron-log'];
                            }
                        }
                    }
                    if(!empty($itensSolicitadosAditiva[5]) && !empty($itensAutorizadosAditivo[5])){
                        $responseEquipamento = $this->sicronizarItensParaSeremLiberados($itensAutorizadosAditivo[5], $itensSolicitadosAditiva[5]);
                        if(!empty($responseEquipamento)){
                            if(!empty($responseEquipamento['update-solicitacao'])){
                                $arrayAcoes['update-solicitacao'][] = $responseEquipamento['update-solicitacao'];
                            }
                            if(!empty($responseEquipamento['itens-autorizados'])){
                                $arrayAcoes['itens-autorizados'][] = $responseEquipamento['itens-autorizados'];
                            }
                            if(!empty($responseEquipamento['itens-solicitados'])){
                                $arrayAcoes['itens-solicitados'][] = $responseEquipamento['itens-solicitados'];
                            }
                            if(!empty($responseEquipamento['remover-cron'])){
                                $arrayAcoes['remover-tabela-cron'][] = $responseEquipamento['remover-cron'];
                            }
                            if(!empty($responseEquipamento['itens-insert-cron-log'])){
                                $arrayAcoes['insert-cron-log'][] = $responseEquipamento['itens-insert-cron-log'];
                            }
                        }
                    }
                }
            }
        }
        // Fim solicitações e autorizações aditivas.

        // Solicitacoes e autorizaçoes Periodicas 
        $itensTipoPeriodicoTabelaCron = $this->getItensAutorizadosCronByTipoDocumento('periodico');
        if(!empty($itensTipoPeriodicoTabelaCron)) {
            $peridoPeriodico = $this->getPeriodoCronByTipoDocumento('periodico');
            $inicioPeriodico = $peridoPeriodico['inicio'];
            $fimPeriodico = $peridoPeriodico['fim'];


            if (!empty($inicioPeriodico) && !empty($fimPeriodico)) {


                // CATEGORIZAR OS ITENS DA TABELA DE CRON
                $itensAutorizadosPeriodico[0] = $this->arrayPrepare($this->medicamentos_autorizados($itensTipoPeriodicoTabelaCron));
                $itensAutorizadosPeriodico[1] = $this->arrayPrepare($this->materiais_autorizados($itensTipoPeriodicoTabelaCron));
//                $itensAutorizadosPeriodico[2] = $this->arrayPrepare($this->servicos_autorizados($itensTipoPeriodicoTabelaCron));
                $itensAutorizadosPeriodico[3] = $this->arrayPrepare($this->dietas_autorizadas($itensTipoPeriodicoTabelaCron));
                $itensAutorizadosPeriodico[5] = $this->arrayPrepare($this->equipamentos_autorizados($itensTipoPeriodicoTabelaCron));


                $itensSolicitacoesPeriodicos = $this->getItensSolicitadosPendentesTipoPeriodico($inicioPeriodico, $fimPeriodico);
                if (!empty($itensSolicitacoesPeriodicos)) {
                    $itensSolicitadosPeriodico[0] = $this->arrayPrepare($this->medicamentos_autorizados($itensSolicitacoesPeriodicos));
                    $itensSolicitadosPeriodico[1] = $this->arrayPrepare($this->materiais_autorizados($itensSolicitacoesPeriodicos));
                    $itensSolicitadosPeriodico[3] = $this->arrayPrepare($this->dietas_autorizadas($itensSolicitacoesPeriodicos));
                    $itensSolicitadosPeriodico[5] = $this->arrayPrepare($this->equipamentos_autorizados($itensSolicitacoesPeriodicos));

                    if (!empty($itensSolicitadosPeriodico[0]) && !empty($itensAutorizadosPeriodico[0])) {
                        $responseMedicamento = $this->sicronizarItensParaSeremLiberados($itensAutorizadosPeriodico[0], $itensSolicitadosPeriodico[0]);
                        if (!empty($responseMedicamento)) {
                            if (!empty($responseMedicamento['update-solicitacao'])) {
                                $arrayAcoes['update-solicitacao'][] = $responseMedicamento['update-solicitacao'];
                            }
                            if (!empty($responseMedicamento['itens-autorizados'])) {
                                $arrayAcoes['itens-autorizados'][] = $responseMedicamento['itens-autorizados'];
                            }
                            if (!empty($responseMedicamento['itens-solicitados'])) {
                                $arrayAcoes['itens-solicitados'][] = $responseMedicamento['itens-solicitados'];
                            }
                            if (!empty($responseMedicamento['remover-cron'])) {
                                $arrayAcoes['remover-tabela-cron'][] = $responseMedicamento['remover-cron'];
                            }
                            if(!empty($responseMedicamento['itens-insert-cron-log'])){
                                $arrayAcoes['insert-cron-log'][] = $responseMedicamento['itens-insert-cron-log'];
                            }
                        }
                    }


                    if (!empty($itensSolicitadosPeriodico[1]) && !empty($itensAutorizadosPeriodico[1])) {
                        $responseMaterial = $this->sicronizarItensParaSeremLiberados($itensAutorizadosPeriodico[1], $itensSolicitadosPeriodico[1]);
                        if (!empty($responseMaterial)) {
                            if (!empty($responseMaterial['update-solicitacao'])) {
                                $arrayAcoes['update-solicitacao'][] = $responseMaterial['update-solicitacao'];
                            }
                            if (!empty($responseMaterial['itens-autorizados'])) {
                                $arrayAcoes['itens-autorizados'][] = $responseMaterial['itens-autorizados'];
                            }
                            if (!empty($responseMaterial['itens-solicitados'])) {
                                $arrayAcoes['itens-solicitados'][] = $responseMaterial['itens-solicitados'];
                            }
                            if (!empty($responseMaterial['remover-cron'])) {
                                $arrayAcoes['remover-tabela-cron'][] = $responseMaterial['remover-cron'];
                            }
                            if(!empty($responseMaterial['itens-insert-cron-log'])){
                                $arrayAcoes['insert-cron-log'][] = $responseMaterial['itens-insert-cron-log'];
                            }
                        }
                    }

                    if (!empty($itensSolicitadosPeriodico[3]) && !empty($itensAutorizadosPeriodico[3])) {
                        $responseDieta = $this->sicronizarItensParaSeremLiberados($itensAutorizadosPeriodico[3], $itensSolicitadosPeriodico[3]);
                        if (!empty($responseDieta)) {
                            if (!empty($responseDieta['update-solicitacao'])) {
                                $arrayAcoes['update-solicitacao'][] = $responseDieta['update-solicitacao'];
                            }
                            if (!empty($responseDieta['itens-autorizados'])) {
                                $arrayAcoes['itens-autorizados'][] = $responseDieta['itens-autorizados'];
                            }
                            if (!empty($responseDieta['itens-solicitados'])) {
                                $arrayAcoes['itens-solicitados'][] = $responseDieta['itens-solicitados'];
                            }
                            if (!empty($responseDieta['remover-cron'])) {
                                $arrayAcoes['remover-tabela-cron'][] = $responseDieta['remover-cron'];
                            }
                            if(!empty($responseDieta['itens-insert-cron-log'])){
                                $arrayAcoes['insert-cron-log'][] = $responseDieta['itens-insert-cron-log'];
                            }

                        }
                    }
                    if (!empty($itensSolicitadosPeriodico[5]) && !empty($itensAutorizadosPeriodico[5])) {
                        $responseEquipamento = $this->sicronizarItensParaSeremLiberados($itensAutorizadosPeriodico[5], $itensSolicitadosPeriodico[5]);
                        if (!empty($responseEquipamento)) {
                            if (!empty($responseEquipamento['update-solicitacao'])) {
                                $arrayAcoes['update-solicitacao'][] = $responseEquipamento['update-solicitacao'];
                            }
                            if (!empty($responseEquipamento['itens-autorizados'])) {
                                $arrayAcoes['itens-autorizados'][] = $responseEquipamento['itens-autorizados'];
                            }
                            if (!empty($responseEquipamento['itens-solicitados'])) {
                                $arrayAcoes['itens-solicitados'][] = $responseEquipamento['itens-solicitados'];
                            }
                            if (!empty($responseEquipamento['remover-cron'])) {
                                $arrayAcoes['remover-tabela-cron'][] = $responseEquipamento['remover-cron'];
                            }
                            if(!empty($responseEquipamento['itens-insert-cron-log'])){
                                $arrayAcoes['insert-cron-log'][] = $responseEquipamento['itens-insert-cron-log'];
                            }
                        }
                    }
                }
            }
        }

        mysql_query('begin');

        if(!empty($arrayAcoes['update-solicitacao'])){
            foreach ($arrayAcoes['update-solicitacao'] as $itens){
                foreach ($itens as $solicitacao){
                    $sql = "
                        UPDATE
                        solicitacoes
                        set
                        solicitacoes.autorizado = solicitacoes.autorizado + {$solicitacao['autorizado']},                        
                        solicitacoes.data_auditado = now(),
                        solicitacoes.status = 1                       
                        
                        where
                        solicitacoes.idSolicitacoes = {$solicitacao['solicitacao_id']}";

                    $result  = mysql_query($sql);
                    if(!$result){
                        echo "Erro ao atualizar uma solicitacão";
                        mysql_query('ROLLBACK');
                        return ;
                    }
                }
            }

        }
// Inicio tabela historico tabela cron
        if(!empty($arrayAcoes['insert-cron-log'])){
            foreach ($arrayAcoes['insert-cron-log'] as $itensInsert){
                foreach ($itensInsert as $item){

                    $valuesInsertLog[] = "(
                       NULL,
'{$item['autorizacao_item_id']}',
now(),
'{$item['inicio']}',
'{$item['fim']}',
'{$item['plano_id']}',
'{$item['paciente_id']}',
'{$item['qtd']}',
'{$item['item_id']}',
'{$item['tipo_item']}',
'{$item['tipo_documento']}',
'{$item['solicitacao_id']}'
)";
                }

            }
            $insert = "INSERT INTO 
regra_fluxo_autorizado_cron_log ( 
id,
autorizacao_item_id,
data,
periodo_autorizacao_inicio,
periodo_autorizacao_fim, 
plano_id, 
paciente_id,
qtd_autorizado,
item_id, 
tipo_item,
tipo_documento,
 solicitacoes_id)
VALUES";
//            echo "<pre>";
//            print_r($valuesInsertLog);
            $values = implode(',',$valuesInsertLog);
            // print_r($values);
            $sql = $insert.' '.$values.";";
            // echo $sql;
            $result  = mysql_query($sql);
            if(!$result){
                echo "Erro ao criar log das  autorização cron.";
                mysql_query('ROLLBACK');
                return ;
            }
        }

        if(!empty($arrayAcoes['remover-tabela-cron'])){
            $idRemover = [];

            foreach ($arrayAcoes['remover-tabela-cron'] as $itens){
                foreach ($itens as $remover){
                    $idRemover[] = $remover['id'];
                }
            }

            if(!empty($idRemover)){
                $listId = implode(',',$idRemover);

                $sql = "
                        delete
                         from 
                         regra_fluxo_autorizado_cron
                         where
                         id  in ({$listId}) ";


                $result  = mysql_query($sql);
                if(!$result){
                    echo "Erro ao excluir um item na tabela de regra de autorização cron.";
                    mysql_query('ROLLBACK');
                    return ;
                }

            }

        }
// fim revomover tabela cron

        if(!empty($arrayAcoes['itens-autorizados'])){
            foreach ($arrayAcoes['itens-autorizados'] as $pacientes){
                if(!empty($pacientes)){
                    foreach ($pacientes as $itens){
                        foreach ($itens as $item){
                            $sql = "
                       update
regra_fluxo_autorizado_cron
set
regra_fluxo_autorizado_cron.qtd_autorizado = {$item['qtd']}
where
id ={$item['id']}";

                            $result  = mysql_query($sql);
                            if(!$result){
                                echo "Erro ao atualizar tabela de itens autorizados cron ";
                                mysql_query('ROLLBACK');
                                return ;
                            }
                        }
                    }
                }
            }
        }

        mysql_query('commit');
        return true;
        //echo "ok";
    }

    public function getItensAutorizadosCronByTipoDocumento($tipo){
        $sql = "
        SELECT
        id,
        autorizacao_item_id,
	periodo_autorizacao_inicio AS inicio,
	periodo_autorizacao_fim AS fim,
	plano_id,
	qtd_autorizado AS qtd,
	item_id,
	tipo_item,
	tipo_documento,
	paciente_id
FROM
	regra_fluxo_autorizado_cron 
WHERE
	tipo_documento = '{$tipo}' 
ORDER BY
inicio ASC
";

        $result  = mysql_query($sql);

        if($result){
            while ($row = mysql_fetch_array($result)){
                $autorizados[] = $row;
            }
            return $autorizados;
        }
        return ;
    }

    public function getPeriodoCronByTipoDocumento($tipo)
    {
        $sql = <<<SQL
        SELECT
	MIN( periodo_autorizacao_inicio ) AS inicio,
	MAX( periodo_autorizacao_fim ) AS fim 
FROM
	regra_fluxo_autorizado_cron 
WHERE
	tipo_documento = '{$tipo}'  
SQL;
        $result  = mysql_query($sql);

        if($result){
            while ($row = mysql_fetch_array($result)){
                $periodo['inicio'] = $row['inicio'];
                $periodo['fim'] = $row['fim'];
            }
            return $periodo;
        }
        return ;
    }

    public function getItensSolicitadosPendentesTipoAditivo($inicio, $fim)
    {
        $sql = <<<SQL
SELECT
                                solicitacoes.idSolicitacoes,
                                solicitacoes.paciente as paciente_id,
                                solicitacoes.qtd,
                                clientes.convenio as plano_id,
                                solicitacoes.CATALOGO_ID as item_id,
                                solicitacoes.autorizado,
                                solicitacoes.tipo as tipo_item, 
                                relatorio_aditivo_enf.INICIO as inicio,
                                relatorio_aditivo_enf.FIM as fim
                            FROM
                                solicitacoes 
                            INNER JOIN
                                relatorio_aditivo_enf  on (solicitacoes.`relatorio_aditivo_enf_id`=relatorio_aditivo_enf.ID)
                            INNER JOIN
                                  clientes ON (solicitacoes.paciente = clientes.idClientes)
                            WHERE
                                relatorio_aditivo_enf.INICIO between '{$inicio}' and '{$fim}'
                            AND
                                relatorio_aditivo_enf.FIM between '{$inicio}' and '{$fim}'
                            AND
                                solicitacoes.status not in (-1,-2)
                            AND
                                solicitacoes.autorizado < solicitacoes.qtd
                            UNION
                            SELECT
                             solicitacoes.idSolicitacoes,
                                solicitacoes.paciente as paciente_id,
                                solicitacoes.qtd,
                                clientes.convenio as plano_id,
                                solicitacoes.CATALOGO_ID as item_id,
                                solicitacoes.autorizado,
                                solicitacoes.tipo,
                                prescricoes.inicio as inicio,
                                prescricoes.inicio as fim
                            FROM
                                solicitacoes
                            INNER JOIN
                                prescricoes  on (solicitacoes.`idPrescricao`= prescricoes.id)
                            INNER JOIN
                                  clientes ON (solicitacoes.paciente = clientes.idClientes)
                            WHERE
                                prescricoes.inicio between '{$inicio}' and '{$fim}'
                            AND
                                prescricoes.fim between '{$inicio}' and '{$fim}'
                            AND
                                solicitacoes.status not in (-1,-2)
                            AND   
                                prescricoes.Carater in (3,6)
                            AND
                                solicitacoes.autorizado < solicitacoes.qtd
                            order by inicio ASC
SQL;

        $result  = mysql_query($sql);

        if($result){
            while ($row = mysql_fetch_array($result)){
                $itens[] = $row;

            }
            return $itens;
        }
        return ;
    }

    public function getItensSolicitadosPendentesTipoPeriodico($inicio, $fim)
    {
        $sql = <<<SQL

                            SELECT
                             solicitacoes.idSolicitacoes,
                                solicitacoes.paciente as paciente_id,
                                solicitacoes.qtd,
                                clientes.convenio as plano_id,
                                solicitacoes.CATALOGO_ID as item_id,
                                solicitacoes.autorizado,
                                solicitacoes.tipo,
                                prescricoes.inicio as inicio,
                                prescricoes.inicio as fim
                            FROM
                                solicitacoes
                            INNER JOIN
                                prescricoes  on (solicitacoes.`idPrescricao`= prescricoes.id)
                            INNER JOIN
                                  clientes ON (solicitacoes.paciente = clientes.idClientes)
                            WHERE
                                prescricoes.inicio between '{$inicio}' and '{$fim}'
                            AND
                                prescricoes.fim between '{$inicio}' and '{$fim}'
                            AND
                            prescricoes.Carater in (2,5)
                            AND
                                solicitacoes.status not in (-1,-2)
                            AND
                                solicitacoes.autorizado < solicitacoes.qtd
                            order by inicio ASC
SQL;

        $result  = mysql_query($sql);

        if($result){
            while ($row = mysql_fetch_array($result)){
                $itens[] = $row;

            }
            return $itens;
        }

        return ;


    }

    public function sicronizarItensParaSeremLiberados($itensAutorizados, $itensSolicitados){
        $updateSolicitacoes =[];
        foreach($itensSolicitados as $paciente => $itensSolicitadosPacientes) {
            //Array dos itens dos paciente.

            if (array_key_exists($paciente, $itensAutorizados)) {
                foreach ($itensSolicitadosPacientes as $key => $item) {

                    foreach ($itensAutorizados[$paciente] as $chave => $itemAutorizado) {

                        if ($item['plano_id'] == $itemAutorizado['plano_id']) {
                            $mesAnoItemAutorizadoInicio = \DateTime::createFromFormat('Y-m-d', $itemAutorizado['inicio'])->format('Y-m');
                            $mesAnoItemSolicitacaoInicio = \DateTime::createFromFormat('Y-m-d', $item['inicio'])->format('Y-m');

                            if ($mesAnoItemAutorizadoInicio == $mesAnoItemSolicitacaoInicio) {
                                if ($item['item_id'] == $itemAutorizado['item_id'] && $item['tipo_item'] == $itemAutorizado['tipo_item']) {

                                    $diff = ($item['qtd'] - $item['autorizado']);
                                    $qtd = ($diff - $itemAutorizado['qtd']);

                                    if ($qtd < 0) {
                                        $arrayAuxInsertCronLog = [];
                                        $arrayAuxInsertCronLog = $itemAutorizado;
                                        $arrayAuxInsertCronLog['solicitacao_id'] = $item['idSolicitacoes'];

                                        $arrayAuxInsertCronLog['qtd'] =  $itemAutorizado['qtd'] + $qtd;
                                        $itensInsertCronLog[] = $arrayAuxInsertCronLog;

                                        $itensAutorizados[$paciente][$chave]['qtd'] = $qtd * -1;
                                        $autorizar = $itemAutorizado['qtd'] + $qtd;
                                        unset($itensSolicitados[$paciente][$key]);
                                        $updateSolicitacoes[] = [
                                            'solicitacao_id' => $item['idSolicitacoes'],
                                            'autorizado' => $autorizar
                                        ];
                                        break;

                                        // acumular para fazer o update
                                    } elseif ($qtd > 0) {
                                        $arrayAuxInsertCronLog = $itemAutorizado;
                                        $arrayAuxInsertCronLog['solicitacao_id'] = $item['idSolicitacoes'];

                                        $itensInsertCronLog[] = $arrayAuxInsertCronLog;
                                        $autorizar = $itemAutorizado['qtd'];
                                        $itensSolicitados[$paciente][$key]['qtd'] += $qtd;
                                        $removerTabelaCron[] = $itensAutorizados[$paciente][$chave];
                                        unset($itensAutorizados[$paciente][$chave]);
                                        $updateSolicitacoes[] = [
                                            'solicitacao_id' => $item['idSolicitacoes'],
                                            'autorizado' => $autorizar
                                        ];

                                    } else {

                                        $arrayAuxInsertCronLog = $itemAutorizado;
                                        $arrayAuxInsertCronLog['solicitacao_id'] = $item['idSolicitacoes'];
                                        $itensInsertCronLog[] = $arrayAuxInsertCronLog;
                                        $autorizar = $itemAutorizado['qtd'];
                                        $removerTabelaCron[] = $itensAutorizados[$paciente][$chave];
                                        unset($itensSolicitados[$paciente][$key]);
                                        unset($itensAutorizados[$paciente][$chave]);
                                        $updateSolicitacoes[] = [
                                            'solicitacao_id' => $item['idSolicitacoes'],
                                            'autorizado' => $autorizar
                                        ];
                                        break;
                                    }
                                    //$itens_autorizados[0][$key]['plano_id']['CATALOGO_ID'] = $itens_autorizados[0][$key]['plano_id']['CATALOGO_ID'] - $item['plano_id']]['CATALOGO_ID'];
                                }
                            }

                        }
                    }
                }
            }
        }

        return $itens = ['update-solicitacao' => $updateSolicitacoes,
            'itens-autorizados' => $itensAutorizados,
            'itens-solicitados' =>$itensSolicitados,
            'remover-cron' => $removerTabelaCron,
            'itens-insert-cron-log' => $itensInsertCronLog ] ;

    }


    public function medicamentos_autorizados($item_list)
    {
        return array_filter($item_list, function($item){

            if($item['tipo_item']== 0){

                return $item;
            }

        });

    }
    public function materiais_autorizados($item_list)
    {
        return array_filter( $item_list,function($item){
            if($item['tipo_item']== 1)
                return $item;
        });
    }
    public function servicos_autorizados($item_list)
    {
        return array_filter( $item_list,function($item){
            if($item['tipo_item']== 2)
                return $item;
        });
    }
    public function dietas_autorizadas($item_list)
    {
        return array_filter( $item_list,function($item){
            if($item['tipo_item']== 3)
                return $item;
        });
    }
    public function equipamentos_autorizados($item_list)
    {
        return array_filter( $item_list,function($item){
            if($item['tipo_item']== 5)
                return $item;
        });
    }

    public function arrayPrepare($arr)
    {
        $arr_transform = [];

        foreach ($arr as $value) {
            $arr_transform[$value['paciente_id']][] = $value;
        }
        return $arr_transform;
    }

}
