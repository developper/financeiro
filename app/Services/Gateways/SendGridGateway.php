<?php

namespace app\Services\Gateways;
use App\Core\Config;
use App\Services\AbstractMailService;
use App\Services\Interfaces\MailServiceInterface;
use App\Helpers\StringHelper as String;
use App\Models\Administracao\Paciente;
use SendGrid\Email as SendGridMail;
use App\Models\Administracao\Filial;
class SendGridGateway extends AbstractMailService implements MailServiceInterface
{
    public function __construct(\SendGrid $sendgrid, SendGridMail $mail)
    {
        $this->sendgrid = $sendgrid;
        $this->mail 		= $mail;
    }

    public function setSendData($paciente, $tipo, $origem, $empresa, $msgHTML = '')
    {
        $this->send_data = [
            'paciente' 	=> $paciente,
            'tipo' 	    => $tipo,
            'origem' 	=> $origem,
            'empresa'	=> $empresa,
            'msgHTML'   => $msgHTML
        ];
        parent::getEmailConfig();
    }

    public function fillMailHeader()
    {
        $tipo           = $this->send_data['tipo'];
        $empresa        = $this->send_data['empresa'];
        $lista          = $this->email_config['lista'];
        $origem         = $this->send_data['origem'];
        $paciente_name  = $this->send_data['paciente'];
        $responseEmpresa = Filial::getById($empresa);
        $nomeEmpresa    = $responseEmpresa['nome'];

        if(Config::get('APP_ENV') === 'prd') {
            $subject = 'Avisos do Sistema da Mederi';
            if($origem == 'exame' ){
                if(isset($lista['origem'][$origem]) && isset($lista['emails'][$origem][$tipo])){
                    $subject = "Avisos do Sistema da Mederi." . htmlentities($lista['emails'][$origem][$tipo]);
                }
                if (isset($lista['emails']['enfermeira_administrativa']) && isset($lista['emails']['enfermeira_administrativa'][$empresa])) {
                    $this->addTo ($lista['emails']['enfermeira_administrativa'][$empresa]['email'], $lista['emails']['enfermeira_administrativa'][$empresa]['nome']);
                }
                if (isset($lista['emails']['enfermagem']) && isset($lista['emails']['enfermagem'][$empresa])) {
                    $this->addTo ($lista['emails']['enfermagem'][$empresa]['email'], $lista['emails']['enfermagem'][$empresa]['nome']);
                }
                if (isset($lista['emails']['secretaria']) && isset($lista['emails']['secretaria'][$empresa])) {
                    $this->addTo($lista['emails']['secretaria'][$empresa]['email'], $lista['emails']['secretaria'][$empresa]['nome']);
                }

                if (isset($lista['emails']['medico']) && isset($lista['emails']['medico'][$empresa])) {
                    $this->addTo ($lista['emails']['medico'][$empresa]['email'], $lista['emails']['medico'][$empresa]['nome']);
                }


            }else{
                if (isset($lista['emails'][$tipo]) && isset($lista['emails'][$tipo][$empresa])) {
                    $this->addTo ($lista['emails'][$tipo][$empresa]['email'], $lista['emails'][$tipo][$empresa]['nome']);
                }

                if (isset($lista['diretoria'][$tipo])) {
                    $this->addTo ($lista['diretoria'][$tipo]['email'], $lista['diretoria'][$tipo]['nome']);
                }

                //if($this->send_data['origem'] == '8' || $this->send_data['origem'] == '3') {
                if($this->send_data['origem'] == '3') {
                    $this->addTo($this->email_config['addAddress']['email'], $this->email_config['addAddress']['nome']);
                }

                if($this->send_data['origem'] == '6') {
                    $this->addTo('auditoria.adm@mederi.com.br','Auditora Administrativa');
                }

                if($this->send_data['origem'] == '1' || $this->send_data['origem'] == '6') {
                    $this->addTo($this->email_config['grupo-auditoria']['email'], $this->email_config['addAddress']['nome']);
                }

                // PENDÊNCIAS E ITENS NEGADOS PELA AUDITORIA
                if($origem == '15' || $origem == '14'){
                    if(isset($lista['origem'][$tipo]) && isset($lista['origem'][$tipo][$origem])){
                        $subject = "({$paciente_name}) " . htmlentities($lista['origem'][$tipo][$origem]);
                    }
                }

                if(($this->send_data['empresa'] == '11' || $this->send_data['empresa'] == '1') && $tipo == 'medico') {
                    $this->addTo($this->email_config['secretaria-fsa']['email'], $this->email_config['secretaria-fsa']['nome']);
                }
                if($this->send_data['empresa'] == '5') {
                    $this->addTo('karlaenf.alagoinhas@mederi.com.br', 'Enfermeira Karla - Alagoinhas');
                }
            }
            $this->mail
                ->addTo ($this->mail_list, $this->name_list)
                ->setFrom ($this->email_config['from']['email'])
                ->setSubject ($subject);
        } else {
            $subject = 'Avisos do Sistema (DEV)';
            $this->mail
                ->addTo ('ti@mederi.com.br', 'TI Mederi')
                ->setFrom ('ti@mederi.com.br')
                ->setSubject ($subject);

        }
    }

    public function fillMailBody()
    {
        session_start();
        $profissional                           = String::UTF8DecodeString($_SESSION["nome_user"]);
        $origem				                    = $this->send_data['origem'];
        $tipo				                    = $this->send_data['tipo'];
        $origem                                 = String::UTF8DecodeString($this->email_config['lista']['origem'][$tipo][$origem]);
        $this->email_config['paciente_name']    = Paciente::getById($this->send_data['paciente'])['nome'];
        $nomePaciente                           = String::UTF8DecodeString($this->email_config['paciente_name']);
        $responseEmpresa = Filial::getEmpresaById($this->send_data['empresa']);
        $nomeEmpresa    = $responseEmpresa['nome'];

        $this->mail->setHtml(
            sprintf(
                $this->send_data['msgHTML'] != '' ? $this->send_data['msgHTML'] : $this->email_config['msgHTML'],
                String::UTF8EncodeString($origem),
                $nomePaciente, $profissional,$nomeEmpresa
            )
        );
    }

    public function sendMail()
    {
        $this->fillMailHeader();
        $this->fillMailBody();

        try {
            $this->sendgrid->send($this->mail);
        } catch(\SendGrid\Exception $e) {
            echo $e->getMessage();
            foreach($e->getErrors() as $er) {
                echo $er;
            }
        }
    }

    private function addTo($email, $name)
    {
        $this->mail_list[] = $email;
        $this->name_list[] = $name;
    }

    public function sendMailToCoodenadoria($type, $ur, $msg)
    {
        if(Config::get('APP_ENV') === 'prd') {
            parent::getEmailConfig();
            $lista = $this->email_config['lista'];
            $this->addTo($lista['emails'][$type][$ur]['email'], $lista['emails'][$type][$ur]['nome']);
            $this->mail->setFrom ($this->email_config['from']['email']);
        }else {
            $this->addTo('ti@mederi.com.br', 'TI Mederi');
            $this->mail->setFrom ('ti@mederi.com.br');
        }
        $this->mail
            ->addTo ($this->mail_list, $this->name_list)
            ->setSubject ('Avisos do Sistema')
            ->setHtml($msg);
        try {
            $this->sendgrid->send($this->mail);
        } catch(\SendGrid\Exception $e) {
            echo $e->getMessage();
            foreach($e->getErrors() as $er) {
                echo $er;
            }
        }
    }

    public function sendSimpleMail($subject, $msg, $to, $filePath = '', $fileName = '')
    {
        if(Config::get('APP_ENV') ==='prd') {
            foreach($to as $t){
                $this->addTo($t['email'], $t['nome']);
            }
            $this->mail->setFrom ('avisos@mederi.com.br');
        }else {
            $to = ['email' => 'ti@mederi.com.br', 'nome' => 'TI'];
            $this->addTo('ti@mederi.com.br', 'TI Mederi');
            $this->mail->setFrom ('ti@mederi.com.br');
        }
        $this->mail
            ->addTo ($this->mail_list, $this->name_list)
            ->setSubject ($subject)
            ->setHtml($msg);
        if(!empty($filePath)){
            $this->mail->addAttachment(
                $filePath,
                $fileName
            );
            //$this->mail->addAttachment($filePath, $fileName);
        }
        try {
            $response = $this->sendgrid->send($this->mail);
            $options = '';
            $i = 0;


            if($response->code != 200){
                foreach($to as $t){
                    $options['to'][$i]['email'] = $t['email'];
                    $options['to'][$i]['nome'] =  $t['nome'];
                    $i++;
                }

                $options['from']['email'] = $this->mail->from;
                $options['from']['nome'] = $this->mail->fromName;
                $options['subject'] = $this->mail->subject;
                $options['altBody'] = $this->mail->fromName;
                $options['msgHTML'] = $this->mail->html;
                return $options;
            }
            return $response->code;
        } catch(\SendGrid\Exception $e) {
            echo $e->getMessage();
            foreach($e->getErrors() as $er) {
                echo $er;
            }
        }
    }

    public function addAttachment($filePath)
    {
        $this->mail->addAttachment(
            $filePath,
            $fileName
        );
    }
}
